<?php
/**
 * Dynamic Styles
 * 
 * @package Preschool And Kindergarten Pro
*/

function preschool_and_kindergarten_pro_dynamic_css(){
    
    $body_font      = get_theme_mod( 'body_font', array( 'font-family'=>'Lato', 'variant'=>'regular' ) );    
    $body_fonts     = preschool_and_kindergarten_pro_get_fonts( $body_font['font-family'], $body_font['variant'] );
    $body_font_size = get_theme_mod( 'body_font_size', 18 );
    $body_line_ht   = get_theme_mod( 'body_line_height', 28 );
    $body_color     = get_theme_mod( 'body_color', '#616161' );
    
    $site_title_font      = get_theme_mod( 'site_title_font', array( 'font-family'=>'Pacifico', 'variant'=>'regular' ) );    
    $site_title_fonts     = preschool_and_kindergarten_pro_get_fonts( $site_title_font['font-family'], $site_title_font['variant'] );

    $h1_font      = get_theme_mod( 'h1_font', array( 'font-family'=>'Lato', 'variant'=>'regular') );
    $h1_fonts     = preschool_and_kindergarten_pro_get_fonts( $h1_font['font-family'], $h1_font['variant'] );
    $h1_font_size = get_theme_mod( 'h1_font_size', 60 );
    $h1_line_ht   = get_theme_mod( 'h1_line_height', 72 );
    $h1_color     = get_theme_mod( 'h1_color', '#313131' );
    
    $h2_font      = get_theme_mod( 'h2_font', array('font-family'=>'Lato', 'variant'=>'regular') );
    $h2_fonts     = preschool_and_kindergarten_pro_get_fonts( $h2_font['font-family'], $h2_font['variant'] );
    $h2_font_size = get_theme_mod( 'h2_font_size', 42 );
    $h2_line_ht   = get_theme_mod( 'h2_line_height', 50 );
    $h2_color     = get_theme_mod( 'h2_color', '#313131' );
    
    $h3_font      = get_theme_mod( 'h3_font', array('font-family'=>'Lato', 'variant'=>'regular') );
    $h3_fonts     = preschool_and_kindergarten_pro_get_fonts( $h3_font['font-family'], $h3_font['variant'] );
    $h3_font_size = get_theme_mod( 'h3_font_size', 36 );
    $h3_line_ht   = get_theme_mod( 'h3_line_height', 43 );
    $h3_color     = get_theme_mod( 'h3_color', '#313131' );
    
    $h4_font      = get_theme_mod( 'h4_font', array('font-family'=>'Lato', 'variant'=>'regular') );
    $h4_fonts     = preschool_and_kindergarten_pro_get_fonts( $h4_font['font-family'], $h4_font['variant'] );
    $h4_font_size = get_theme_mod( 'h4_font_size', 24 );
    $h4_line_ht   = get_theme_mod( 'h4_line_height', 29 );
    $h4_color     = get_theme_mod( 'h4_color', '#313131' );
    
    $h5_font      = get_theme_mod( 'h5_font', array('font-family'=>'Lato', 'variant'=>'regular') );
    $h5_fonts     = preschool_and_kindergarten_pro_get_fonts( $h5_font['font-family'], $h5_font['variant'] );
    $h5_font_size = get_theme_mod( 'h5_font_size', 20 );
    $h5_line_ht   = get_theme_mod( 'h5_line_height', 24 );
    $h5_color     = get_theme_mod( 'h5_color', '#313131' );
    
    $h6_font      = get_theme_mod( 'h6_font', array('font-family'=>'Lato', 'variant'=>'regular') );
    $h6_fonts     = preschool_and_kindergarten_pro_get_fonts( $h6_font['font-family'], $h6_font['variant'] );
    $h6_font_size = get_theme_mod( 'h6_font_size', 16 );
    $h6_line_ht   = get_theme_mod( 'h6_line_height', 19 );
    $h6_color     = get_theme_mod( 'h6_color', '#313131' );
    
    $primary_color     = get_theme_mod( 'primary_color_scheme', '#41aad4' );
    $secondary_color   = get_theme_mod( 'secondary_color_scheme', '#f380b2' );
    $tertiary_color    = get_theme_mod( 'tertiary_color_scheme', '#4fbba9' );
    $bg_color          = get_theme_mod( 'bg_color', '#ffffff' );
    $body_bg           = get_theme_mod( 'body_bg', 'image' );
    $bg_image          = get_theme_mod( 'bg_image' );
    $bg_pattern        = get_theme_mod( 'bg_pattern', 'nobg' );
    $ed_auth_comment   = get_theme_mod( 'ed_auth_comments' );
    $slider_caption_bg = get_theme_mod( 'ed_caption_bg', '1' );
    
    $rgb = preschool_and_kindergarten_pro_hex2rgb( preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ) ); 
    
    $image = '';
    if( $body_bg == 'image' && $bg_image ){
        $image = $bg_image;    
    }elseif( $body_bg == 'pattern' && $bg_pattern != 'nobg' ){
        $image = get_template_directory_uri() . '/images/patterns/' . $bg_pattern . '.png';
    }
    
    echo "<style type='text/css' media='all'>"; ?>
    
    .site-branding .site-title{
        font-family: <?php echo $site_title_fonts['font']; ?>;
    }

    /*for body*/
    body{
        font-size: <?php echo absint( $body_font_size ); ?>px;
        line-height: <?php echo absint( $body_line_ht ); ?>px;
        color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $body_color ); ?>;
        font-family: <?php echo $body_fonts['font']; ?>;
        font-weight: <?php echo esc_attr( $body_fonts['weight'] ); ?>;
        font-style: <?php echo esc_attr( $body_fonts['style'] ); ?>;
        background: url(<?php echo esc_url( $image ); ?>) <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $bg_color ); ?>;
    }

    #primary .post .entry-content h1,
    #primary .page .entry-content h1{
        font-family: <?php echo $h1_fonts['font']; ?>;
        font-size: <?php echo absint( $h1_font_size ); ?>px;
        line-height: <?php echo absint( $h1_line_ht ); ?>px;
        font-weight: <?php echo esc_attr( $h1_fonts['weight'] ); ?>;
        font-style: <?php echo esc_attr( $h1_fonts['style'] ); ?>;
        color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $h1_color ); ?>;
    }

    #primary .post .entry-content h2,
    #primary .page .entry-content h2{
        font-family: <?php echo $h2_fonts['font']; ?>;
        font-size: <?php echo absint( $h2_font_size ); ?>px;
        line-height: <?php echo absint( $h2_line_ht ); ?>px;
        font-weight: <?php echo esc_attr( $h2_fonts['weight'] ); ?>;
        font-style: <?php echo esc_attr( $h2_fonts['style'] ); ?>;
        color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $h2_color ); ?>;
    }

    #primary .post .entry-content h3,
    #primary .page .entry-content h3{
        font-family: <?php echo $h3_fonts['font']; ?>;
        font-size: <?php echo absint( $h3_font_size ); ?>px;
        line-height: <?php echo absint( $h3_line_ht ); ?>px;
        font-weight: <?php echo esc_attr( $h3_fonts['weight'] ); ?>;
        font-style: <?php echo esc_attr( $h3_fonts['style'] ); ?>;
        color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $h3_color ); ?>;
    }

    #primary .post .entry-content h4,
    #primary .page .entry-content h4{
        font-family: <?php echo $h4_fonts['font']; ?>;
        font-size: <?php echo absint( $h4_font_size ); ?>px;
        line-height: <?php echo absint( $h4_line_ht ); ?>px;
        font-weight: <?php echo esc_attr( $h4_fonts['weight'] ); ?>;
        font-style: <?php echo esc_attr( $h4_fonts['style'] ); ?>;
        color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $h4_color ); ?>;
    }

    #primary .post .entry-content h5,
    #primary .page .entry-content h5{
        font-family: <?php echo $h5_fonts['font']; ?>;
        font-size: <?php echo absint( $h5_font_size ); ?>px;
        line-height: <?php echo absint( $h5_line_ht ); ?>px;
        font-weight: <?php echo esc_attr( $h5_fonts['weight'] ); ?>;
        font-style: <?php echo esc_attr( $h5_fonts['style'] ); ?>;
        color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $h5_color ); ?>;
    }

    #primary .post .entry-content h6,
    #primary .page .entry-content h6{
        font-family: <?php echo $h6_fonts['font']; ?>;
        font-size: <?php echo absint( $h6_font_size ); ?>px;
        line-height: <?php echo absint( $h6_line_ht ); ?>px;
        font-weight: <?php echo esc_attr( $h6_fonts['weight'] ); ?>;
        font-style: <?php echo esc_attr( $h6_fonts['style'] ); ?>;
        color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $h6_color ); ?>;
    }
    
    /* primary color */
    a{
        color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
    }
    
    a:hover,
    a:focus{
        color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
    }

    .header-t .contact-info li a:hover,
    .header-t .contact-info li a:focus,
    .header-t .widget_rttk_social_links ul li a:hover,
    .header-t .widget_rttk_social_links ul li a:focus,
    .section-2 .col .text-holder .title a:hover,
    .section-2 .col .text-holder .title a:focus,
    .latest-events .col .text-holder .event-title a:hover,
    .latest-events .col .text-holder .event-title a:focus,
    .featured .btn-holder .btn-view:hover,
    .featured .btn-holder .btn-view:focus,
    .our-staff .btn-holder .btn-view:hover,
    .our-staff .btn-holder .btn-view:focus,
    .news .post .entry-title a:hover,
    .news .post .entry-title a:focus,
    .news .btn-holder .btn-view:hover,
    .news .btn-holder .btn-view:focus,
    .promotional-block .text-holder .btn-contact:hover,
    .promotional-block .text-holder .btn-contact:focus,
    .contact-section .holder .text .text-holder .tel-link a:hover,
    .contact-section .holder .text .text-holder .email-link a:hover,
    .contact-section .holder .text .text-holder .tel-link a:focus,
    .contact-section .holder .text .text-holder .email-link a:focus,
    .contact-page .right .contact-info a:hover,
    .contact-page .right .contact-info a:focus,
    #primary .post .entry-header .entry-title a:hover,
    #primary .post .entry-header .entry-title a:focus,
    .search #primary .page .entry-header .entry-title a:hover,
    .search #primary .page .entry-header .entry-title a:focus,
    .widget ul li a:hover,
    .widget ul li a:focus,
    .widget.widget_recent_comments ul li a,
    .widget.widget_rss ul li a,
    .widget.widget_kindergarten_latest_posts ul li .entry-header .entry-title a:hover,
    .widget.widget_kindergarten_latest_posts ul li .entry-header .entry-title a:focus,
    .widget.widget_rttk_pro_category_post ul li .entry-header .entry-title a:hover,
    .widget.widget_rttk_pro_category_post ul li .entry-header .entry-title a:focus,
    .widget.widget_rttk_pro_author_post ul li .entry-header .entry-title a:hover,
    .widget.widget_rttk_pro_author_post ul li .entry-header .entry-title a:focus,
    .widget.widget_rttk_pro_popular_post ul li .entry-header .entry-title a:hover,
    .widget.widget_rttk_pro_popular_post ul li .entry-header .entry-title a:focus,
    .widget.widget_rttk_pro_recent_post ul li .entry-header .entry-title a:hover,
    .widget.widget_rttk_pro_recent_post ul li .entry-header .entry-title a:focus,
    .photo-gallery .holder .btn-view:hover,
    .photo-gallery .holder .btn-view:focus,
    .comments-area .comment-body .comment-metadata a:hover,
    .comments-area .comment-body .comment-metadata a:focus,
    .site-header.header-three .main-navigation ul li a:hover,
    .site-header.header-three .main-navigation ul li a:focus,
    .site-header.header-three .main-navigation ul li:hover > a,
    .site-header.header-three .main-navigation ul li:focus > a,
    .site-header.header-three .main-navigation ul .current-menu-item > a,
    .site-header.header-three .main-navigation ul .current-menu-ancestor > a,
    .site-header.header-three .main-navigation ul .current_page_item > a,
    .site-header.header-three .main-navigation ul .current_page_ancestor > a,
    .site-header.header-three .header-b .info-list li a:hover,
    .site-header.header-three .header-b .info-list li a:focus,
    .site-header.header-four .header-b .right .email a:hover,
    .site-header.header-four .header-b .right .email a:focus,
    .site-header.header-four .header-b .right .phone a:hover,
    .site-header.header-four .header-b .right .phone a:focus,
    .site-header.header-five .right .top .tel-link:hover,
    .site-header.header-five .right .top .tel-link:focus,
    .site-header.header-five .right .top .widget_rttk_social_links .social-networks li a:hover,
    .site-header.header-five .right .top .widget_rttk_social_links .social-networks li a:focus,
    .site-header.header-six .header-t .right .contact-info a:hover,
    .site-header.header-six .header-t .right .contact-info a:focus,
    .site-header.header-six .main-navigation ul ul li a:hover,
    .site-header.header-six .main-navigation ul ul li a:focus,
    .site-header.header-six .main-navigation ul ul li:hover > a,
    .site-header.header-six .main-navigation ul ul li:focus > a,
    .site-header.header-six .main-navigation ul ul .current-menu-item > a,
    .site-header.header-six .main-navigation ul ul .current-menu-ancestor > a,
    .site-header.header-six .main-navigation ul ul .current_page_item > a,
    .site-header.header-six .main-navigation ul ul .current_page_ancestor > a,
    .site-header.header-eight .main-navigation ul li a:hover,
    .site-header.header-eight .main-navigation ul li a:focus,
    .site-header.header-eight .main-navigation ul li:hover > a,
    .site-header.header-eight .main-navigation ul li:focus > a,
    .site-header.header-eight .main-navigation ul .current-menu-item > a,
    .site-header.header-eight .main-navigation ul .current-menu-ancestor > a,
    .site-header.header-eight .main-navigation ul .current_page_item > a,
    .site-header.header-eight .main-navigation ul .current_page_ancestor > a,
    .services-page .facilities .widget_preschool_and_kindergarten_pro_icon_text_widget .col .text-holder .widget-title a:hover,
    .services-page .facilities .widget_preschool_and_kindergarten_pro_icon_text_widget .col .text-holder .widget-title a:focus,
    .services-page .widget_preschool_and_kindergarten_pro_icon_text_widget .text-holder .widget-title a:hover,
    .services-page .widget_preschool_and_kindergarten_pro_icon_text_widget .text-holder .widget-title a:focus,
    .site-header.header-three .header-t .widget_rttk_social_links ul li a,
    #primary .course-list .courses-holder .element-item .text-holder .title a:hover,
    #primary .course-list .courses-holder .element-item .text-holder .title a:focus,
    .event-list-page .event-holder .text-holder .entry-title a:hover,
    .event-list-page .event-holder .text-holder .entry-title a:focus,
    .site-footer .widget_rttk_pro_cta_widget .widget-content .btn-donate:hover,
    .site-footer .widget_rttk_pro_cta_widget .widget-content .btn-donate:focus,
    .widget_rttk_pro_stat_counter_widget .col .icon-holder{
        color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
    }

    .course-list .courses-holder .button-group .button,
    .photo-gallery .caption{
        background: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
    }

    .course-list .courses-holder .button-group .button:hover,
    .course-list .courses-holder .button-group .button:focus,
    .course-list .courses-holder .button-group .is-checked{
        background: none;
        color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
    }

    .site-footer{background-color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;}

    #secondary .widget_rttk_pro_cta_widget .widget-content .btn-donate{
        border-color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
    }

    #secondary .widget_rttk_pro_cta_widget .widget-content .btn-donate:hover,
    #secondary .widget_rttk_pro_cta_widget .widget-content .btn-donate:focus{
        background: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
    }

    .main-navigation ul ul,
    .welcome .text-holder .btn-more:hover,
    .welcome .text-holder .btn-more:focus,
    .section-3 .text-holder .btn-detail:hover,
    .section-3 .text-holder .btn-detail:focus,
    .latest-events .btn-holder .btn-view:hover,
    .latest-events .btn-holder .btn-view:focus,
    .featured .col .text-holder .btn-detail:hover,
    .featured .col .text-holder .btn-detail:focus,
    button:hover,
    input[type="button"]:hover,
    input[type="reset"]:hover,
    input[type="submit"]:hover,
    button:focus,
    input[type="button"]:focus,
    input[type="reset"]:focus,
    input[type="submit"]:focus,
    #primary .post .btn-readmore:hover,
    #primary .post .btn-readmore:focus,
    .search #primary .page .btn-readmore:hover,
    .search #primary .page .btn-readmore:focus,
    #primary .post .entry-content .rara_accordian .rara_accordian_title,
    #primary .page .entry-content .rara_accordian .rara_accordian_title,
    #primary .entry-content .rara_call_to_action_button:hover,
    #primary .entry-content .rara_call_to_action_button:focus,
    .rara_toggle .rara_toggle_title,
    #primary .entry-content .rara_tab_wrap .rara_tab_group .tab-title.active,
    #primary .entry-content .rara_tab_wrap .rara_tab_group .tab-title:hover,
    #primary .entry-content .rara_tab_wrap .rara_tab_group .tab-title:focus,
    .wp-caption-text,
    .site-header.header-three .nav-holder,
    .site-header.header-four .header-t .main-navigation ul li a:hover,
    .site-header.header-four .header-t .main-navigation ul li a:focus,
    .site-header.header-four .header-t .main-navigation ul li:hover > a,
    .site-header.header-four .header-t .main-navigation ul li:focus > a,
    .site-header.header-four .header-t .main-navigation ul .current-menu-item > a,
    .site-header.header-four .header-t .main-navigation ul .current-menu-ancestor > a,
    .site-header.header-four .header-t .main-navigation ul .current_page_item > a,
    .site-header.header-four .header-t .main-navigation ul .current_page_ancestor > a,
    .site-header.header-six .main-navigation ul li a:hover,
    .site-header.header-six .main-navigation ul li a:focus,
    .site-header.header-six .main-navigation ul li:hover > a,
    .site-header.header-six .main-navigation ul li:focus > a,
    .site-header.header-six .main-navigation ul .current-menu-item > a,
    .site-header.header-six .main-navigation ul .current-menu-ancestor > a,
    .site-header.header-six .main-navigation ul .current_page_item > a,
    .site-header.header-six .main-navigation ul .current_page_ancestor > a,
    .testimonial-section .table{
        background: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
    }

    .testimonial-section .btn-holder .btn-view{
        border-color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
        color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
    }

    .testimonial-section .btn-holder .btn-view:hover,
    .testimonial-section .btn-holder .btn-view:focus{
        background: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
    }

    .main-navigation ul ul:before{
        border-bottom-color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
    }

    .welcome .text-holder .btn-more,
    .section-3 .text-holder .btn-detail,
    .latest-events .btn-holder .btn-view,
    .featured .col .text-holder .btn-detail,
    button,
    input[type="button"],
    input[type="reset"],
    input[type="submit"],
    #primary .post .btn-readmore,
    .search #primary .page .btn-readmore,
    #primary .post .entry-content .rara_accordian,
    #primary .page .entry-content .rara_accordian,
    #primary .entry-content .rara_call_to_action_button,
    .rara_toggle,
    #primary .entry-content .rara_tab_wrap .rara_tab_group .tab-title,
    .site-header.header-five .main-navigation,
    .event-list-page .event-holder .text-holder .btn-more{
        border-color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
    }

    .event-list-page .event-holder .text-holder .btn-more:hover,
    .event-list-page .event-holder .text-holder .btn-more:focus{
        background: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
    }

    .featured .btn-holder .btn-view,
    .our-staff .btn-holder .btn-view,
    .news .btn-holder .btn-view,
    .promotional-block .text-holder .btn-mission,
    .widget.widget_tag_cloud .tagcloud a:hover,
    .widget.widget_tag_cloud .tagcloud a:focus,
    .banner .banner-text .btn-enroll{
        border-color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
        background: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
    }

    .widget.widget_rttk_pro_author_bio .readmore,
    .widget_rttk_pro_featured_post .text-holder .readmore{
        border-color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
        color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
    }

    .widget.widget_rttk_pro_author_bio .readmore:hover,
    .widget.widget_rttk_pro_author_bio .readmore:focus,
    .widget_rttk_pro_featured_post .text-holder .readmore:hover,
    .widget_rttk_pro_featured_post .text-holder .readmore:focus{
        background: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
    }

    .site-header.header-five .main-navigation ul li:before{
        border-top-color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
    }

    /*secondary color*/
    .welcome .text-holder .title,
    .section-2 .header .title,
    .section-3 .header .title,
    .latest-events .header .title,
    .featured .header .title,
    .our-staff .header .title,
    .testimonial .header .title,
    .news .header .title,
    .contact-section .header .title,
    .page-header .page-title,
    .testimonial-section .header .title,
    .event-detail-page .detail-info .title,
    .event-detail-page .venue .text-holder .title{
        color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $secondary_color ); ?>;
    }

    /* tertiary color */
    .main-navigation .current_page_item > a,
    .main-navigation .current-menu-item > a,
    .main-navigation .current_page_ancestor > a,
    .main-navigation .current-menu-ancestor > a,
    .main-navigation ul li a:hover,
    .main-navigation ul li a:focus,
    .news .post .posted-on,
    .site-header.header-two .header-t .social-networks li a,
    .site-header.header-four .header-b .right .email:before,
    .site-header.header-four .header-b .right .phone:before,
    .site-header.header-seven .main-navigation ul ul li a:hover,
    .site-header.header-seven .main-navigation ul ul li a:focus,
    .site-header.header-seven .main-navigation ul ul li:hover > a,
    .site-header.header-seven .main-navigation ul ul li:focus > a,
    .site-header.header-seven .main-navigation ul ul .current-menu-item > a,
    .site-header.header-seven .main-navigation ul ul .current-menu-ancestor > a,
    .site-header.header-seven .main-navigation ul ul .current_page_item > a,
    .site-header.header-seven .main-navigation ul ul .current_page_ancestor > a,
    .site-header.header-eight .header-t .social-networks li a:hover,
    .site-header.header-eight .header-t .social-networks li a:focus,
    .event-list-page .event-holder .text-holder .entry-meta .date:before,
    .event-list-page .event-holder .text-holder .entry-meta address:before{
        color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $tertiary_color ); ?>;
    }

    .section-3 ul li:before,
    .latest-events .col .img-holder .date-holder,
    .latest-events .col .event-address,
    .photo-gallery,
    .about-page .services .widget_preschool_and_kindergarten_pro_icon_text_widget .style2 .img-holder .fa,
    .about-page .services .widget_preschool_and_kindergarten_pro_icon_text_widget .style2 .img-holder img,
    .services-page .widget_preschool_and_kindergarten_pro_icon_text_widget .style1 .img-holder .fa,
    .services-page .widget_preschool_and_kindergarten_pro_icon_text_widget .style3 .img-holder,
    .services-page .widget_preschool_and_kindergarten_pro_icon_text_widget .style2 .img-holder,
    .contact-page .right .contact-info .address .icon-holder,
    .contact-page .right .contact-info .phone .icon-holder,
    .contact-page .right .contact-info .email .icon-holder,
    #primary .post .entry-content .highlight,
    #primary .page .entry-content .highlight,
    #secondary .widget.widget_preschool_and_kindergarten_pro_icon_text_widget .style1 .holder .img-holder,
    #secondary .widget.widget_preschool_and_kindergarten_pro_icon_text_widget .style2 .holder .img-holder,
    .widget.widget_calendar caption,
    .site-footer .widget.widget_preschool_and_kindergarten_pro_icon_text_widget .style1 .holder .img-holder,
    .site-footer .widget.widget_preschool_and_kindergarten_pro_icon_text_widget .style2 .holder .img-holder,
    .site-header.header-two .header-t,
    .site-header.header-seven .main-navigation ul li a:hover,
    .site-header.header-seven .main-navigation ul li a:focus,
    .site-header.header-seven .main-navigation ul li:hover > a,
    .site-header.header-seven .main-navigation ul li:focus > a,
    .site-header.header-seven .main-navigation ul .current-menu-item > a,
    .site-header.header-seven .main-navigation ul .current-menu-ancestor > a,
    .site-header.header-seven .main-navigation ul .current_page_item > a,
    .site-header.header-seven .main-navigation ul .current_page_ancestor > a,
    .site-header.header-seven .main-navigation ul ul,
    .site-header.header-eight .header-t,
    .event-detail-page .event-info{
        background: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $tertiary_color ); ?>;
    }

    .news .post .posted-on,
    #primary .post .entry-content blockquote, #primary .page .entry-content blockquote{
        border-color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $tertiary_color ); ?>;
    }

    <?php if( $ed_auth_comment ){ ?>
        /* Author Comment Style */
        .comments-area .bypostauthor > .comment-body{
            background: #e5e5e5;
            padding: 10px;
        }
    
    <?php } ?>

    <?php if( $slider_caption_bg ){ ?>
        /* slider text background */
        .banner .banner-text .text-holder{
            background: rgba(0, 0, 0, 0.5);
            padding: 15px;
        }

    <?php } ?>

    <?php if( preschool_and_kindergarten_pro_is_woocommerce_activated() ) { ?>
        .woocommerce span.onsale,
        .woocommerce #respond input#submit,
        .woocommerce a.button,
        .woocommerce button.button,
        .woocommerce input.button,
        .woocommerce a.added_to_cart,
        .woocommerce #respond input#submit.alt,
        .woocommerce a.button.alt,
        .woocommerce button.button.alt,
        .woocommerce input.button.alt{
            background: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
        }

        .woocommerce a.button:hover,
        .woocommerce a.button:focus,
        .woocommerce #respond input#submit:hover,
        .woocommerce #respond input#submit:focus,
        .woocommerce button.button:hover,
        .woocommerce button.button:focus,
        .woocommerce input.button:hover,
        .woocommerce input.button:focus,
        .woocommerce a.added_to_cart:hover,
        .woocommerce a.added_to_cart:focus,
        .woocommerce #respond input#submit.alt:hover,
        .woocommerce a.button.alt:hover,
        .woocommerce button.button.alt:hover,
        .woocommerce input.button.alt:hover,
        .woocommerce #respond input#submit.alt:focus,
        .woocommerce a.button.alt:focus,
        .woocommerce button.button.alt:focus,
        .woocommerce input.button.alt:focus{
            background: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
            opacity: 0.8;
        }

        .woocommerce nav.woocommerce-pagination ul li a{
            border-color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
        }

        .woocommerce nav.woocommerce-pagination ul li a:focus,
        .woocommerce nav.woocommerce-pagination ul li a:hover,
        .woocommerce nav.woocommerce-pagination ul li span.current{
            background: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
            border-color: <?php echo preschool_and_kindergarten_pro_sanitize_hex_color( $primary_color ); ?>;
        }
            
    <?php } ?>
    
    <?php echo "</style>";
       
}
add_action( 'wp_head', 'preschool_and_kindergarten_pro_dynamic_css', 99 );

/**
 * Function for sanitizing Hex color 
 */
function preschool_and_kindergarten_pro_sanitize_hex_color( $color ){
	if ( '' === $color )
		return '';

    // 3 or 6 hex digits, or the empty string.
	if ( preg_match('|^#([A-Fa-f0-9]{3}){1,2}$|', $color ) )
		return $color;
}

/**
 * convert hex to rgb
 * @link http://bavotasan.com/2011/convert-hex-color-to-rgb-using-php/
*/
function preschool_and_kindergarten_pro_hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   //return implode(",", $rgb); // returns the rgb values separated by commas
   return $rgb; // returns an array with the rgb values
}