jQuery(document).ready(function($) {

    $('.services-page .widget_preschool_and_kindergarten_pro_icon_text_widget').matchHeight();
    $('.about-page .services .widget_preschool_and_kindergarten_pro_icon_text_widget').matchHeight();
    
    /** Variables from Customizer for Slider settings */
    if (preschool_and_kindergarten_pro_data.auto == '1') {
         var slider_auto = true;
    }else {
         slider_auto = false;
    }

    if (preschool_and_kindergarten_pro_data.loop == '1') {
        var slider_loop = true;
    } else {
        var slider_loop = false;
    }

    if( preschool_and_kindergarten_pro_data.animation == 'slide' ){
        var animation = '';
    }else{
        var animation = preschool_and_kindergarten_pro_data.animation;
    }

    if( preschool_and_kindergarten_pro_data.rtl == '1' ){
        var rtl = true;
    }else{
        var rtl = false;
    }

    if (preschool_and_kindergarten_pro_data.t_auto == '1') {
         var testi_slider_auto = true;
    }else {
         testi_slider_auto = false;
    }

    jQuery("#banner-slider").owlCarousel({

        items           : 1,
        animateOut      : animation,
        loop            : slider_loop,
        autoplay        : slider_auto,
        nav             : true,
        lazyLoad        : true,
        rtl             : rtl,
        autoplayTimeout : preschool_and_kindergarten_pro_data.autoplayTimeout,
    });

    $("#testimonial-slider").owlCarousel({
        loop        :true,
        margin      :0,
        nav         :true,
        autoplay    :testi_slider_auto,
        autoHeight  : true,
        dots        : false,
        rtl         : rtl,
        responsive  :{
            0:{
                items:1
            }
        }
    });

    $("#gallery-slider").owlCarousel({
        loop        :true,
        margin      :0,
        nav         :true,
        autoHeight  : false,
        dots        : false,
        rtl         : rtl,
        lazyLoad    : true,
        responsive  :{
            0:{
                items: 1
            },
            768:{
                items: 2
            },
            992:{
                items: 5
            },
        }
    });

    jQuery('#responsive-menu-button').sidr({
        name: 'sidr-main',
        source: '#site-navigation',
        side: 'right'

    });


    /** Lightbox */
    if (preschool_and_kindergarten_pro_data.lightbox == '1') {

        $('.entry-content').find('.gallery-columns-1').find('.gallery-icon > a').attr('rel', 'group1');
        $('.entry-content').find('.gallery-columns-2').find('.gallery-icon > a').attr('rel', 'group2');
        $('.entry-content').find('.gallery-columns-3').find('.gallery-icon > a').attr('rel', 'group3');
        $('.entry-content').find('.gallery-columns-4').find('.gallery-icon > a').attr('rel', 'group4');
        $('.entry-content').find('.gallery-columns-5').find('.gallery-icon > a').attr('rel', 'group5');
        $('.entry-content').find('.gallery-columns-6').find('.gallery-icon > a').attr('rel', 'group6');
        $('.entry-content').find('.gallery-columns-7').find('.gallery-icon > a').attr('rel', 'group7');
        $('.entry-content').find('.gallery-columns-8').find('.gallery-icon > a').attr('rel', 'group8');
        $('.entry-content').find('.gallery-columns-9').find('.gallery-icon > a').attr('rel', 'group9');

        $("a[href$='.jpg'],a[href$='.jpeg'],a[href$='.png'],a[href$='.gif']").fancybox();

    }

    // Script for back to top
    $(window).scroll(function(){
        if($(this).scrollTop() > 300){
          $('#rara-top').fadeIn();
        }else{
          $('#rara-top').fadeOut();
        }
    });
        
    $("#rara-top").click(function(){
        $('html,body').animate({scrollTop:0},600);
    });


        // init Isotope
        var $grid = $('.grid').isotope({
            itemSelector: '.element-item',
            layoutMode: 'fitRows'
        });
        // filter functions
        var filterFns = {
            // show if number is greater than 50
            numberGreaterThan50: function() {
                var number = $(this).find('.number').text();
                return parseInt(number, 10) > 50;
            },
            // show if name ends with -ium
            ium: function() {
                var name = $(this).find('.name').text();
                return name.match(/ium$/);
            }
        };
        // bind filter button click
        $('.filters-button-group').on('click', 'button', function() {
            var filterValue = $(this).attr('data-filter');
            // use filterFn if matches value
            filterValue = filterFns[filterValue] || filterValue;
            $grid.isotope({ filter: filterValue });
        });
        // change is-checked class on buttons
        $('.button-group').each(function(i, buttonGroup) {
            var $buttonGroup = $(buttonGroup);
            $buttonGroup.on('click', 'button', function() {
                $buttonGroup.find('.is-checked').removeClass('is-checked');
                $(this).addClass('is-checked');
            });
        });


    $('html').click(function() {
        $('.form-holder').slideUp();
    });

    $('.btn-search').click(function(event) {
        event.stopPropagation();
    });
    $(".btn-search > a").click(function() {
        $(".form-holder").slideToggle();
        return false;
    });

    /** Sticky Header */
    var header_layout = preschool_and_kindergarten_pro_data.hlayout;

    var windowWidth = $(window).width();
    if (preschool_and_kindergarten_pro_data.sticky == '1' && windowWidth >= 992) {

        var mns = "sticky-menu";

        //mn = $(".header-b");
        if (header_layout == 'three' || header_layout == 'two' || header_layout == 'eight') {

            hdr = $('.header-t').outerHeight();
            mn = $('.header-b');
            stickyHolderHeight = $('.header-b').outerHeight();

        } else if (header_layout == 'one') {

            hdr = $('.header-holder').outerHeight();
            mn = $('.nav-holder');
            stickyHolderHeight = $('.nav-holder').outerHeight();

        } else if (header_layout == 'four') {

            hdr = 0.5;
            mn = $('.header-t');
            stickyHolderHeight = $('.header-t').outerHeight();

        } else if (header_layout == 'five' || header_layout == 'seven') {

            hdr = 0.5;
            mn = $('.site-header');
            stickyHolderHeight = $('.site-header').outerHeight();

        } else if (header_layout == 'six') {

            hdr = $('.header-t').outerHeight();
            mn = $('.nav-holder');
            stickyHolderHeight = $('.nav-holder').outerHeight();
            
        }

        $(window).scroll(function() {
            if ($(this).scrollTop() > hdr) {
                mn.addClass(mns);
                $('.sticky-holder').height(stickyHolderHeight);
            } else {
                mn.removeClass(mns);
                $('.sticky-holder').height(0);
            }
        });
    }

    /** One page Scroll */
    $('#site-navigation').onePageNav({
        currentClass: 'current-menu-item',
        changeHash: false,
        scrollSpeed: 1500,
        scrollThreshold: 0.5,
        filter: '',
        easing: 'swing',        
    });

});
