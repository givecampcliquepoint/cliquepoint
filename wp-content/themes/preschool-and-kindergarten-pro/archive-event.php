<?php
   /**
    * The template for displaying archive page for events
    *
    * * @package Preschool and kindergarten Pro 
    */
    get_header();  
    ?>
        <div id="primary" class="content-area"> 
            <main id="main" class="site-main">
                <div class="event-list-page">
                    <?php
                    if( have_posts() ) : ?>
                        <div class="entry-content">
                            <?php the_archive_description( '<div class="taxonomy-description">', '</div>' );?>
                        </div>
                    <?php
                        /* Start the Loop */
                        while ( have_posts() ) : the_post();
            
                            /*
                             * Include the Post-Format-specific template for the content.
                             * If you want to override this in a child theme, then include a file
                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                             */
                            get_template_part( 'template-parts/content', 'event' );
            
                        endwhile;
                        
                        preschool_and_kindergarten_pro_pagination(); //Pagination 

                    else: 
                    
                        get_template_part( 'template-parts/content', 'none' );
    
                    endif;
                   ?>

                </div>
            </main>
        </div>
        
    <?php get_sidebar(); ?>
    <?php get_footer();
