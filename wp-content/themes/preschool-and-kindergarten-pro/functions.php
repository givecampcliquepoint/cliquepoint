<?php
/**
 * Preschool and Kindergarten functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Preschool_and_Kindergarten_pro
 */

//define theme version
if ( ! defined( 'PRESCHOOL_AND_KINDERGARTEN_PRO_THEME_VERSION' ) ) {
	$theme_data = wp_get_theme();	
	define ( 'PRESCHOOL_AND_KINDERGARTEN_PRO_THEME_VERSION', $theme_data->get( 'Version' ) );
}

/**
 * * Custom template function for this theme.
 */
require get_template_directory() . '/inc/custom-functions.php';

/**
 * Implement the WordPress Hooks.
 */
require get_template_directory() . '/inc/wp-hooks.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';
/**
 * Custom Controls 
*/
require get_template_directory() . '/inc/custom-controls/custom-control.php';

/**
 ** Custom template functions for this theme.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Implement the template hooks.
 */
require get_template_directory() . '/inc/template-hooks.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/widgets/widgets.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Demo content
*/
require get_template_directory() . '/inc/demo-content.php';

/**
 * Plugin Recommendation
*/
require get_template_directory() . '/inc/tgmpa/recommended-plugins.php';

/**
 * CPT
*/
require get_template_directory() . '/inc/cpt/cpt.php';

/**
 * Metabox
*/
require get_template_directory() . '/inc/cpt/metabox.php';

/**
 * WooCommerce Related funcitons
*/
if( preschool_and_kindergarten_pro_is_woocommerce_activated() )
require get_template_directory() . '/inc/woocommerce-functions.php';

/**
 * Typography Functions
*/
require get_template_directory() . '/inc/typography-functions.php';

/**
 * Dynamic Styles
*/
require get_template_directory() . '/css/style.php';

/**
 * Theme Updater
*/
require get_template_directory() . '/updater/theme-updater.php';

/**
 * Demo Import
*/
require get_template_directory() . '/inc/import-hooks.php';

/**
 * Performance
*/
require get_template_directory() . '/inc/performance.php';


