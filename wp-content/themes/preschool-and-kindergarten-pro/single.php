<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Preschool_and_Kindergarten_pro
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', 'single' );

			/**
             * After post content
             * 
             * @hooked preschool_and_kindergarten_post_author  - 20 
            */
            do_action( 'preschool_and_kindergarten_pro_after_post_content' );

			preschool_and_kindergarten_pro_pagination();

			/**
	            * Preschool and kindergarten pro Comment
	            * 
	            * @hooked preschool_and_kindergarten_pro_get_comment_section 
	        */
	           do_action( 'preschool_and_kindergarten_pro_comment' );

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
