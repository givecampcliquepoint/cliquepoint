<?php
  /** 
	*
    * The template for displaying all single testimonial and testimonial listing
    * 
    * @package Preschool_and_Kindergarten_pro
    * 
    */
    get_header(); ?>    
    <div id="primary" class="content-area" style="width: 100%;">
        <main id="main" class="site-main">
            <article class="page testimonial-page">
            <?php 
                if ( have_posts() ) : ?>
                    <div class="entry-content">
                        <?php the_archive_description( '<div class="taxonomy-description">', '</div>' );?>
                    </div>

                    <div class="testimonial-holder">
                    <?php
                        /* Start the Loop */
                        while ( have_posts() ) : the_post();
            
                            /*
                             * Include the Post-Format-specific template for the content.
                             * If you want to override this in a child theme, then include a file
                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                             */
                            get_template_part( 'template-parts/content', 'testimonial' );
            
                        endwhile;
                        
                        preschool_and_kindergarten_pro_pagination(); //Pagination ?>

                    </div>  

                <?php else: 
                    
                    get_template_part( 'template-parts/content', 'none' );
    
                endif;
            ?>
                    
            </article>
        </main>
    </div>
    <?php
    get_footer();