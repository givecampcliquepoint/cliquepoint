<?php
/**
 * Template part for displaying testimonials.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package preschool_and_kindergarten_pro
 */

$rttk_setting         = get_post_meta( get_the_ID(), '_rttk_setting', true );
$testimonial_img_size = apply_filters('testimonial_img_size','thumbnail'); 
$post_thumbnail_id    = get_post_thumbnail_id( $post->ID );
$src                  = wp_get_attachment_image_src( $post_thumbnail_id, $testimonial_img_size );
$company              = isset($rttk_setting['testimonial']['company']) ? $rttk_setting['testimonial']['company']: '';
$position             = isset($rttk_setting['testimonial']['position']) ? $rttk_setting['testimonial']['position']: '';
?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="img-holder">
            <?php if( $src ) : ?><img src="<?php echo esc_url( $src[0]); ?>" alt=""> <?php endif; ?>
            <?php if( $company ) { ?><strong class="name"><?php echo esc_attr( $company ); ?></strong> <?php } ?>
            <span class="designation">
                <?php if($position) echo esc_attr( $position ); ?>
            </span>
        </div>
        <div class="text-holder">
            <div class="entry-content">
                <h2><?php the_title(); ?></h2>
                <?php the_content(); ?>
            </div>
        </div>
     
    	<footer class="entry-footer">
            <?php preschool_and_kindergarten_pro_entry_footer(); ?>
    	</footer><!-- .entry-footer -->
	
    </article><!-- #post-## -->