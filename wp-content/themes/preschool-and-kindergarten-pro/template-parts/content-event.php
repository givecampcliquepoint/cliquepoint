<?php
/**
 * Template part for displaying Events.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package preschool_and_kindergarten_pro
 */
    $event_img_size    = apply_filters( 'event_img_size','preschool-and-kindergarten-pro-courses-archive' ); 
    $post_thumbnail_id = get_post_thumbnail_id( $post->ID );
    $src               = wp_get_attachment_image_src( $post_thumbnail_id, $event_img_size );
    $rttk_setting      = get_post_meta( $post->ID, '_rttk_setting', true );
    $start_date        = get_post_meta( $post->ID, '_preschool_and_kindergarten_pro_event_start_date',true );
    $end_date          = get_post_meta( $post->ID, '_preschool_and_kindergarten_pro_event_end_date', true );
    $address           = isset( $rttk_setting['event']['address']) ? esc_attr($rttk_setting['event']['address'] ): '';
?>
    <article <?php post_class(); ?>>

        <div class="img-holder">
            <a href="<?php the_permalink(); ?>"><img src="<?php echo esc_url( $src[0] );?>" alt=""></a>
        </div>

        <div class="text-holder">
            
            <header class="entry-header">
                <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h2>
                <?php 
                if( $start_date || $end_date || $address ){ ?>
                    <div class="entry-meta">
                    <?php 
                        if( $start_date ){ ?>
                            <span class="date"><?php echo $start_date; ?> - <?php echo $end_date; ?>
                            </span>
                    <?php }
                        if( $address ){ ?>
                            <address><?php echo $address; ?></address>
                    <?php } ?>
                    </div>
                <?php } ?>
            </header>
            
            <div class="entry-content">
                <?php 
                do_action( 'preschool_and_kindergarten_pro_excerpt_section' ); ?>
            </div>
            
            <div class="footer">
                <a href="<?php the_permalink();?>" class="btn-more"><?php _e('Find out more','preschool-and-kindergarten-pro');?></a>
            </div>

        </div> 

    </article>