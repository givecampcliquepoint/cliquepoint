<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Preschool_and_Kindergarten_pro
 */

$read_more = get_theme_mod( 'readmore_text', __( 'Read More', 'preschool-and-kindergarten-pro' ) ); //From Customizer
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    
    <?php 
    /**
     * Before Page entry content
     * 
     * @hooked preschool_and_kindergarten_pro_post_content_image - 10
     * @hooked preschool_and_kindergarten_pro_post_entry_header  - 20 
    */
    do_action( 'preschool_and_kindergarten_pro_before_post_image' ); ?>

    <div class="text-holder"> 

        <?php do_action( 'preschool_and_kindergarten_pro_before_post_entry_content' ); ?>

    	<div class="entry-content" itemprop="text">
    		<?php
    			if( false === get_post_format() ){
                   do_action( 'preschool_and_kindergarten_pro_excerpt_section' );
                }else{
                    the_content( sprintf(
        				/* translators: %s: Name of current post. */
        				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'preschool-and-kindergarten-pro' ), array( 'span' => array( 'class' => array() ) ) ),
        				the_title( '<span class="screen-reader-text">"', '"</span>', false )
        			) );
    	        }

    			wp_link_pages( array(
    				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'preschool-and-kindergarten-pro' ),
    				'after'  => '</div>',
    			) );
    		?>
    	</div><!-- .entry-content -->
        
        <footer class="entry-footer">		
            <a href="<?php the_permalink(); ?>" class="btn-readmore"><?php  echo esc_html( $read_more );?></a>
            <?php preschool_and_kindergarten_pro_entry_footer(); ?>
    	</footer><!-- .entry-footer -->
        
    </div>

</article><!-- #post-## -->
