<?php
/**
 * Template part for displaying teams.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package preschool_and_kindergarten_pro
 */

    $team_img_size = apply_filters('team_img_size','preschool-and-kindergarten-pro-staff-thumb'); 
    $post_thumbnail_id = get_post_thumbnail_id( $post->ID );
    $src = wp_get_attachment_image_src( $post_thumbnail_id, $team_img_size );
    $rttk_setting = get_post_meta( $post->ID, '_rttk_setting', true );
?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
        <div class="img-holder">
        <?php
            $team_img_size = apply_filters('archive_team_img_size','preschool-and-kindergarten-pro-staff-thumb'); 
            $post_thumbnail_id = get_post_thumbnail_id( $post->ID );
            $src = wp_get_attachment_image_src( $post_thumbnail_id, $team_img_size );
            $rttk_setting = get_post_meta( $post->ID, '_rttk_setting', true );
        ?>
            <img src="<?php echo esc_url($src[0]); ?>" alt="">
        </div>
        <div class="text-holder">
            <header class="header">
                <h2 class="name"><?php the_title();?></h2>
                <span class="designation"><?php echo isset($rttk_setting['team']['position']) ? esc_attr($rttk_setting['team']['position']): ''; ?></span>
            </header>
            <div class="entry-content">
            <?php 
                $content = $post->post_content;
                echo wpautop( $content );
            ?>
            </div>
            <ul class="social-networks">
            <?php
                $obj = new Rara_Theme_Toolkit_Pro_Functions;
                if(isset($rttk_setting['team']['social'])){

	                $icons  = $rttk_setting['team']['social'];
	                $arr_keys  = array_keys( $icons );
	                foreach ($arr_keys as $key => $value){ 

		                if ( array_key_exists( $value,$rttk_setting['team']['social'] ) )
		                { 
		                    $icon = $obj->rttk_get_team_social_icon_name( $rttk_setting['team']['social'][$value] );
		                    ?>
                            <li>
                                <a href="<?php echo esc_url($rttk_setting['team']['social'][$value]);?>" target="_blank">
	                                <span class="rttk-social-icons-field-handle fontawesome fa-<?php echo esc_attr( $icon )?>" style="font-family: 'FontAwesome'">
	                                </span>
                                </a>
                            </li>
                       <?php
                        }
                    }
                } ?>
            </ul>
        </div>

        <footer class="entry-footer">
            <?php preschool_and_kindergarten_pro_entry_footer(); ?>
    	</footer><!-- .entry-footer -->
    
    </article>