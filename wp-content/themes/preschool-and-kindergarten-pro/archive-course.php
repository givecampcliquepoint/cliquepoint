	<?php
	/**
    * The template for displaying all single course and course listing
    *
    * @package Rara_Theme_Toolkit_Pro
    * @subpackage Rara_Theme_Toolkit_Pro/includes/rara-toolkit-pro-templates
    * @since 1.0.0
    */

    get_header(); 
    $course_order = get_theme_mod( 'cpt_course_page_order_type', 'date' );
	?>    
	<div id="primary" class="content-area" style="width: 100%;">
        <main id="main" class="site-main">
            <article class="page course-list">
                <div class="entry-content">
                    <?php the_archive_description( '<div class="taxonomy-description">', '</div>' );?>
                </div>
				<div class="courses-holder">
					<div id="filters" class="button-group filters-button-group">
						<button class="button is-checked" data-filter="*"><?php echo esc_html__( 'All', 'preschool-and-kindergarten-pro'); ?></button>
						<?php
						$args = array('orderby'=>'asc','hide_empty'=>true);
						$rttk_custom_terms = get_terms('course_categories', $args);
						if( $rttk_custom_terms ){
							foreach($rttk_custom_terms as $term){
								echo '<button class="button" data-filter=".'.$term->slug.'">'.$term->name.'</button>';
							}
						}
						?>
					</div>
					<?php
						$course_args = array( 
							'post_type'      => 'course', 
							'post_status'    => 'publish', 
							'posts_per_page' => -1 
						);

						if( $course_order == 'menu_order' ){
			                $course_args['orderby'] = 'menu_order title';            
			                $course_args['order']   = 'ASC';
			            }

						$course_qry = new WP_Query( $course_args );
                        if( $course_qry->have_posts() ){ ?>
                            <div class="grid">
                                <?php 
                                while( $course_qry->have_posts() ){
                                    $course_qry->the_post();
                                    $terms = get_the_terms( get_the_ID(), 'course_categories' );
                                    $s = '';
                                    $i = 0;
                                    if( $terms ){
                                        foreach( $terms as $t ){
                                            $i++;
                                            $s .= $t->slug;
                                            if( count( $terms ) > $i ){
                                                $s .= ' ';
                                            }
                                        }
                                    }  ?>
								    <div class="element-item <?php echo esc_attr( $s ); ?>"  data-category="<?php echo esc_attr( $s ); ?>" >
									<?php
										if( has_post_thumbnail() ){ ?>
											<div class="img-holder">
												<a href="<?php the_permalink(); ?>">
													<?php the_post_thumbnail( 'preschool-and-kindergarten-pro-courses-archive', array( 'itemprop' => 'image' ) ); ?>
												</a>
											</div>
										<?php } ?>
										<div class="text-holder">
											<h3 class="title"><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h3>
											<?php 
											$out = array();
											if( $terms ){ ?>
											    <span class="category">
	                                                <?php 
	                                                foreach( $terms as $term ){ 
												        $out[] = sprintf( '<a href="%1$s">%2$s</a>', esc_url( get_term_link( $term->term_id,'course_categories' ) ), esc_html( $term->name ) );
												    }
												    echo implode( ", ", $out );
												    ?>
											    </span>
											<?php } ?>
											<div class="entry-content">
                					        <?php 
                					            do_action( 'preschool_and_kindergarten_pro_excerpt_section' ); ?> 
                						    </div>
										</div>
								    </div>
						        <?php 
							    } ?>
							</div>
						<?php 
						} ?>
				</div>
			</article>
		</main>
	</div>
<?php get_footer(); ?>