<?php
/**
 * Header Five
 * 
 * @package Preschool And Kindergarten Pro
*/
?>
        <div class="sticky-holder"></div>
        <header class="site-header header-five" itemscope itemtype="http://schema.org/WPHeader">
        <div class="container">
               <?php  preschool_and_kindergarten_pro_site_branding(); ?> 
            <div class="right">
                <div class="top">
                    <?php 
                    /**
                     * Social Links
                     * 
                     * @hooked 
                    */
                    preschool_and_kindergarten_pro_header_social();
                    
                    preschool_and_kindergarten_pro_header_phone( 'five' );
                    ?>
                </div>
                <?php preschool_and_kindergarten_pro_primary_nav(); ?>
            </div>
        </div>
    </header>