<?php
/**
 * Header One
 * 
 * @package Preschool And Kindergarten Pro
*/ 
?>
    <header id="masthead" class="site-header header-one" role="banner" itemscope itemtype="http://schema.org/WPHeader">
        
         <div class="header-t">
            <div class="container">
               <ul class="contact-info">
                <?php 
                    preschool_and_kindergarten_pro_header_phone( 'one' ); 
                    preschool_and_kindergarten_pro_header_email( 'one' ); ?>
                </ul>
                <?php 
                $ed_search_form = get_theme_mod( 'ed_search', '1' );
        
                if( $ed_search_form ){  ?>
                    <div class="btn-search"> 
                        <div  class="form-holder">  
                            <?php get_search_form(); ?>
                        </div> 
                        <a href="#" class="fa fa-search"> </a>
                    </div>
                <?php }
                /**
                 * Social Links
                 * 
                 * @hooked 
                */
                preschool_and_kindergarten_pro_header_social();
             ?>
            </div>
        </div> 
        <div class="sticky-holder"></div>
        <div class="header-b">
            <div class="container"> 
                
                <?php 
                preschool_and_kindergarten_pro_site_branding(); 
                   
                preschool_and_kindergarten_pro_primary_nav();

                ?>
            
            </div>
        </div>
        
    </header><!-- #masthead -->
