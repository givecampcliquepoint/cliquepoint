<?php
/**
 * Header Two
 * 
 * @package Preschool And Kindergarten Pro
*/
?>
<header class="site-header header-two" itemscope itemtype="http://schema.org/WPHeader">
        <div class="header-t">
            <div class="container">
                <ul class="contact-info">
                <?php 
                    preschool_and_kindergarten_pro_header_phone( 'two' ); 
                    preschool_and_kindergarten_pro_header_email( 'two' ); ?>
                </ul>
                <?php 
                /**
                 * Social Links
                 * 
                 * @hooked 
                */
                preschool_and_kindergarten_pro_header_social();
                ?>
            </div>
        </div>
        <div class="sticky-holder"></div>
        <div class="header-b">
            <div class="container">
                
                <?php 
                    preschool_and_kindergarten_pro_site_branding(); 
                       
                    preschool_and_kindergarten_pro_primary_nav();

                ?>
            </div>
        </div>
    </header>