<?php
/**
 * Header Four
 * 
 * @package Preschool And Kindergarten Pro
*/

?>
    <header class="site-header header-four" itemscope itemtype="http://schema.org/WPHeader">
        <div class="sticky-holder"></div>
        <div class="header-t">
            <div class="container">
              
                <?php preschool_and_kindergarten_pro_primary_nav(); ?>

                <div class="btn-search"><a href="#" class="fa fa-search"></a>
                <?php preschool_and_kindergarten_pro_search_form(); ?>
                </div>
            </div>
        </div>
        <div class="header-b">
            <div class="container">
            <?php 
                preschool_and_kindergarten_pro_site_branding(); ?>
                <div class="right">
                    <ul class="info-list">
                    <?php 
                        preschool_and_kindergarten_pro_header_phone( 'four' );
                        preschool_and_kindergarten_pro_header_email( 'four' );
                    ?>
                    </ul>
                </div>
            </div>
        </div>
    </header>