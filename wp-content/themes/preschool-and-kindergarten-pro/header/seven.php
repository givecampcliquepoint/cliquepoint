<?php
/**
 * Header Seven
 * 
 * @package Preschool And Kindergarten Pro
*/

?>
	<div class="sticky-holder"></div>
    <header class="site-header header-seven" itemscope itemtype="http://schema.org/WPHeader">
		<div class="container">
		<?php 
            preschool_and_kindergarten_pro_site_branding(); 
			preschool_and_kindergarten_pro_primary_nav(); 
		?>
		</div>
	</header>