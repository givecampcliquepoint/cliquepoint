<?php
/**
 * Header Three
 * 
 * @package Preschool And Kindergarten Pro
*/

?>
<header class="site-header header-three" itemscope itemtype="http://schema.org/WPHeader">
        <div class="header-holder">
            <div class="header-t">
                <div class="container">
                    <?php  
                    /**
                     * Social Links
                     * 
                     * @hooked 
                    */
                    preschool_and_kindergarten_pro_header_social();

                    $ed_search_form = get_theme_mod( 'ed_search', '1' );
                    
                    if( $ed_search_form ){
                        get_search_form();
                    }                    
                    ?>
                </div>
            </div>
            <div class="header-b">
                <div class="container">
                    <?php 
                    preschool_and_kindergarten_pro_site_branding(); ?>
                    <div class="right">
                        <ul class="info-list"> 
                        <?php 
                            preschool_and_kindergarten_pro_header_phone( 'three' );
                            preschool_and_kindergarten_pro_header_email( 'three' ); 
                        ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="sticky-holder"></div>
        <div class="nav-holder">
            <div class="container">
                <?php                    
                preschool_and_kindergarten_pro_primary_nav();
                ?>
            </div>
        </div>
    </header>