<?php
/**
 * Header Six
 * 
 * @package Preschool And Kindergarten Pro
*/
?>
	<header class="site-header header-six" itemscope itemtype="http://schema.org/WPHeader">
		<div class="header-t">
			<div class="container">
                <?php preschool_and_kindergarten_pro_site_branding(); ?> 
				<div class="right">
					<?php 
					/**
	                 * Social Links
	                 * 
	                 * @hooked 
	                */
	                preschool_and_kindergarten_pro_header_social(); ?>
	                <ul class="contact-info">
	                <?php 
	                    preschool_and_kindergarten_pro_header_phone( 'six' ); 
	                    preschool_and_kindergarten_pro_header_email( 'six' ); ?>
	                </ul>
				</div>
			</div>
		</div>
		<div class="sticky-holder"></div>
		<div class="nav-holder">
			<div class="container">
				<?php preschool_and_kindergarten_pro_primary_nav(); ?>
			</div>
		</div>
	</header>