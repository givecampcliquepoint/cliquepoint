<?php
/**
 * Front Page
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Preschool and Kindergarten Pro
 */

get_header(); 

$home_sections = get_theme_mod( 'sort_home_section', array( 'about', 'activities', 'subscription', 'features', 'events', 'promotional', 'course', 'team', 'testimonial', 'gallery', 'blog', 'CTA', 'contact' ) );

    if ( 'posts' == get_option( 'show_on_front' ) ) { //Show Static Blog Page
        include( get_home_template() );
        get_sidebar();
    }elseif( $home_sections ){ 
        
        //If all section are enabled then show custom home page.
        foreach( $home_sections as $section ){
            get_template_part( 'sections/' . esc_attr( $section ) );  
        }
        
    }else {
        //If all section are disabled then this respective page template. 
        include( get_page_template() );
        get_sidebar();
    }

get_footer();