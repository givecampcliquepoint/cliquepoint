<?php
/**
 * Template Name: Contact Page
 *
 * @package Preschool and kindergarten Pro 
 */

get_header();

$contact_form = get_theme_mod( 'contact_form' );
$google_map   = get_theme_mod( 'ed_google_map' );

while ( have_posts() ) : the_post();

	if( $google_map ){ ?>
		<div id="map" class="map-holder"></div>
	<?php } ?>

    <div class="container">
		<div id="content" class="site-content">
            <div class="row">
                <div id="primary" class="content-area" style="width: 100%;">
                    <main id="main" class="site-main">
                        <article class="page contact-page">
                            <div class="row">
								
								<div class="left">
									<?php 
									if( preschool_and_kindergarten_pro_is_cf7_activated() && $contact_form ):
									    
									    $qry = new WP_Query( array( 
								                'post_type' => 'wpcf7_contact_form',
								                'p'         => $contact_form,
								            ) );

					                    if( $contact_form && $qry->have_posts() ){ 
			                                while( $qry->have_posts() ){ $qry->the_post();
			                                    $form_ID = get_the_ID(); ?>
											    <div class="contact-form">
													<?php echo do_shortcode( '[contact-form-7 id="<?php echo $form_ID; ?>" title="<?php the_title(); ?>"]' ); ?>        
												</div>
											<?php 
											} 
								        }
								    endif; ?>
							    </div>
                                <?php do_action( 'preschool_and_kindergarten_pro_contact_details' ); ?>
							</div>
						</article>
                    </main>
                </div>
            </div>
        </div>
    </div>    
				
<?php 
endwhile;
get_footer(); ?>