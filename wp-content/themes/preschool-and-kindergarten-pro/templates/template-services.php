<?php
/**
 * Template Name: Services Page
 *
 * @package Preschool and kindergarten Pro 
 */

get_header();

$fclty_title       = get_theme_mod( 'facilities_section_title' ); 
$fclty_descrptn    = get_theme_mod( 'facilities_section_description' ); 
$fclty_bg_img      = get_theme_mod( 'facilities_bg_image' ); 
$fclty_feature_img = get_theme_mod( 'facilities_featured_image' );

$title            = get_theme_mod( 'service_promotional_section_title' );
$description      = get_theme_mod( 'service_promotional_section_description' );
$bg_image         = get_theme_mod( 'service_promotional_bg_image' );
$button_label_one = get_theme_mod( 'service_promotional_section_button_label' );
$button_label_two = get_theme_mod( 'service_promotional_section_button_label_two' );
$button_link_one  = get_theme_mod( 'service_promotional_section_button_link' ); 
$button_link_two  = get_theme_mod( 'service_promotional_section_button_link_two' );

while ( have_posts() ) : the_post(); 

?>       
    <div id="content" class="site-content">
        <div id="primary" class="content-area" style="width: 100%; padding: 0;">
            <main id="main" class="site-main" style="padding: 0;">
                <article class="page services-page">
                    <div class="container">
                        <div class="entry-content">
                            <?php the_content(); ?>
                        </div>
                        <?php if( is_active_sidebar( 'service-icon-text' ) ){ ?>
                        <section class="services-featured">
                            <div class="row">
                                
                              <?php dynamic_sidebar( 'service-icon-text' ); ?>

                            </div>
                        </section>
                        <?php } ?>
                    </div>

                    <?php if( $fclty_title || $fclty_descrptn || $fclty_bg_img || $fclty_feature_img || is_active_sidebar( 'facilities-icon-text' ) ){ ?>

                    <div class="facilities" style="background: url(<?php echo esc_html( $fclty_bg_img ); ?>) no-repeat; background-size: cover; background-position: center; background-attachment: fixed;">
                        <div class="container">
                            <div class="holder">
                            <?php if( $fclty_title || $fclty_descrptn ){ ?>
                                <header class="header">
                                    <?php 
                                    if( $fclty_title ) echo '<h2 class="title">'. $fclty_title . '</h2>';
                                    if( $fclty_descrptn ) echo '<p>' . $fclty_descrptn . '</p>';
                                    ?>
                                </header>
                                <?php  }
                                if( is_active_sidebar( 'facilities-icon-text' ) ): ?>
                                    <div class="row">
                                        <?php dynamic_sidebar( 'facilities-icon-text' ); ?>
                                    </div>
                                <?php 
                                endif; ?>
                            </div>
                        </div>
                    </div>

                    <?php } if( is_active_sidebar( 'featured-icon-text' ) ){ ?>
                        <div class="services">
                            <div class="container">
                                <div class="row">
                                   
                                    <?php dynamic_sidebar( 'featured-icon-text' ); ?>

                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </article>
            </main>
        </div>
    </div>
    <?php 
    if( $title || $description || $button_label_one || $button_label_two ){ ?>
        <div class="promotional-block" style="background-image: url(<?php echo esc_html( $bg_image ); ?>);  background-size: cover; background-position: center; background-attachment: fixed;">
            <div class="container">
                <div class="text-holder">
                    <?php 
                        preschool_and_kindergarten_pro_get_section_header( $title, $description );

                        if( $button_link_one ){ ?>
                        <a href="<?php echo esc_url( $button_link_one ); ?>" class="btn-contact"><?php echo esc_html( $button_label_one ); ?></a>
                        <?php } if( $button_link_two ){ ?>
                        <a href="<?php echo esc_url( $button_link_two ); ?>" class="btn-mission"><?php echo esc_html( $button_label_two ); ?></a>
                        <?php } ?>
                </div>
            </div>
        </div>
    <?php 
    } 

endwhile;
get_footer(); ?>
