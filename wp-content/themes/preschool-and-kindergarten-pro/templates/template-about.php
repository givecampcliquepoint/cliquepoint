<?php
/**
 * Template Name: About Page
 *
 * @package Preschool and kindergarten Pro 
 */

get_header();

$video  = get_theme_mod( 'about_video' );

while ( have_posts() ) : the_post();

?>
    <div id="content" class="site-content">
        <div id="primary" class="content-area" style="width: 100%; padding: 0;">
            <main id="main" class="site-main" style="padding: 0;">
                <article class="page about-page">
                    <div class="container">
                        <div class="intro">
                            <?php if( has_post_thumbnail() ){ ?>
                            <div class="img-holder">
                                <?php the_post_thumbnail( 'preschool-and-kindergarten-pro-about-page-thumb', array( 'itemprop' => 'image' ) ); ?>
                            </div>
                            <?php } ?>
                            <div class="text-holder">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                    <?php 
                    if( $video ){ ?>
                        <div class="video-container">
                            <?php echo $embed_code = wp_oembed_get( $video ); ?>   
                        </div>
                    <?php 
                    }
                    if( is_active_sidebar( 'about-icon-text' ) ){ ?>
                    <div class="services">
                        <div class="container">
                            <div class="row">
                               <?php dynamic_sidebar( 'about-icon-text' ); ?>                              
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </article>
            </main>
        </div>
    </div> 

    <?php
	/**
	 * subscription Section
	 * 
	 * @package Preschool_and_Kindergarten_pro
	*/
    $ed_form = get_theme_mod( 'ed_subscription_on_about' );
    
	if( $ed_form && is_jetpack_subscription_module_active() && is_active_sidebar( 'newsletter-form' ) ){ 
	?>
	<div class="cta-section">
			<div class="container">
				<div class="row">
					<div class="col">
					<?php dynamic_sidebar( 'newsletter-form' ); ?>
					</div>
				</div>
			</div>
		</div>
	<?php } 
endwhile;
get_footer(); ?>
