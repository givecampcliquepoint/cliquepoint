<?php
/**
 * Contact Section
 * 
 * @package preschool_and_kindergarten_pro
*/   
    $title       = get_theme_mod( 'contact_section_title', __( 'Contact Us', 'preschool-and-kindergarten-pro' ) );
    $description = get_theme_mod( 'contact_section_description', __( 'Lorem ipsum dolor sit amet consectetur lorem', 'preschool-and-kindergarten-pro' ) );
    $contact_title = get_theme_mod( 'contact_title', __( 'Get in touch', 'preschool-and-kindergarten-pro' ) );
    $contact_des = get_theme_mod( 'contact_section_contact_description', __( 'Communication is the act of conveying intended meanings from one entity or group to another through the use of mutually understood signs and semiotic rules.', 'preschool-and-kindergarten-pro' ) );
    $address     = get_theme_mod( 'home_contact_section_address', __( 'Address Here', 'preschool-and-kindergarten-pro' ));

    $as_header   = get_theme_mod( 'contact_phone_email_as_header', '1' );
    $map_onhome  = get_theme_mod( 'ed_google_map_on_home' );
    $demo_content= get_theme_mod( 'ed_demo', '1' );

    $email       = $as_header ? get_theme_mod( 'preschool_and_kindergarten_email_address', __( 'contact@kinderschool.com', 'preschool-and-kindergarten-pro' ) ) : get_theme_mod( 'home_contact_section_email' ); 
    $phone       = $as_header ? get_theme_mod( 'preschool_and_kindergarten_phone', __( '123 456 1234', 'preschool-and-kindergarten-pro' ) ) : get_theme_mod( 'home_contact_section_phone' );
    $phones 	 = explode( ',', $phone);
    $emails 	 = explode( ',', $email);
?>
<section id="contact_section" class="contact-section">
	<div class="container">		
		<?php preschool_and_kindergarten_pro_get_section_header( $title, $description ); ?>
	</div>
	<div class="holder">
		<div class="text">
			<div class="text-holder">
			    <h2><?php echo esc_html( $contact_title ); ?></h2>
				<p><?php echo esc_html( $contact_des ); ?></p>
				<?php if( $address ){ ?><address><?php echo esc_html( $address ); ?> </address> <?php } ?>
				<?php if( $phones ){ ?>
					<span class="tel-link"><strong><?php echo esc_html_e( 'Phone:', 'preschool-and-kindergarten-pro' ) ?></strong>
					<?php
						$numItems = count($phones);
							$i = 0;
						 foreach ($phones as $tel) {
						 	$comma = (++$i === $numItems ) ? '' : ',';
						  ?>
				            <a href="tel:<?php echo preg_replace('/\D/', '', $tel ); ?>"><?php echo esc_html( $tel ).$comma; ?></a>
						<?php 
						}
						 echo '</span>'; 
						}
					 	if( $email ){ ?>
				        <span class="email-link"><strong><?php esc_html_e( 'Email:', 'preschool-and-kindergarten-pro'); ?></strong>
				        	<?php 
					        	if( $emails ){
					        		$numItems = count($emails);
									$j = 0;
					        		foreach( $emails as $ema ){
					        		$comma1 = (++$j === $numItems ) ? '' : ','; ?>
								        <a href="mailto:<?php echo esc_html( $ema ); ?>"><?php echo esc_html( $ema ).$comma1; ?></a>
					        		<?php	
					        		}	
					        	}
				        	 ?>
				        </span>
				<?php } ?>
		    </div>
		</div>
		<?php 
		if( $map_onhome ){ ?>
			<div id="map" class="map-holder"></div>
		<?php 
		}elseif( $demo_content ){
            preschool_and_kindergarten_pro_demo_content( 'contact' );
		} ?>
	</div>

</section>