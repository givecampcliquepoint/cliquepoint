<?php 
/**
 * CTA1 Section
 * 
 * @package Preschool_and_Kindergarten_Pro
*/ 
    $title        = get_theme_mod( 'preschool_and_kindergarten_promotional_section_title', __( 'Summer Camp Special', 'preschool-and-kindergarten-pro' ) );
    $description  = get_theme_mod( 'preschool_and_kindergarten_promotional_section_description', __( 'The traditional view of a summer camp as a woody place with hiking, canoeing, and campfires is evolving, with greater acceptance of newer summer camps that offer a wide variety of specialized activities.', 'preschool-and-kindergarten-pro' ) );
    $bg_image     = get_theme_mod( 'promotional_bg_image', get_template_directory_uri() . '/images/demo/img47.jpg' );
    $button_label = get_theme_mod( 'preschool_and_kindergarten_button_label', __( 'View Details', 'preschool-and-kindergarten-pro' ) );
    $button_link  = get_theme_mod( 'preschool_and_kindergarten_button_link', __( '#', 'preschool-and-kindergarten-pro' ) ); 
?>
<div id="promotional_section" class="promotional-block" style="background-image: url(<?php echo esc_html( $bg_image ); ?>);  background-size: cover; background-position: center; background-attachment: fixed;">

    <div class="container">
        
        <?php 
        preschool_and_kindergarten_pro_get_section_header( $title, $description );
        
        if( $button_link ){ ?>
            <a href="<?php echo esc_url( $button_link ); ?>" class="btn-detail"><?php echo esc_html( $button_label ); ?></a>
        <?php 
        } ?>
    
    </div>
</div>