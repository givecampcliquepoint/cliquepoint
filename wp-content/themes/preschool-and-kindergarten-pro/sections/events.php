<?php
/**
 * Events Section
 * 
 * @package Preschool_and_Kindergarten_Pro
*/ 
    $title       = get_theme_mod( 'events_section_title', __( 'Latest Events', 'preschool-and-kindergarten-pro' ));
    $description = get_theme_mod( 'events_section_description', __( 'A ceremony is an event of ritual significance, performed on a special occasion.', 'preschool-and-kindergarten-pro' ));
    
    $event_one   = get_theme_mod('events_post_one');
    $event_two   = get_theme_mod('events_post_two');
    $event_three = get_theme_mod('events_post_three');

    $view_all = get_theme_mod( 'events_viewall_label', __( 'View All', 'preschool-and-kindergarten-pro' ) );

    $demo_content = get_theme_mod( 'ed_demo', '1' );    
    $home_events  = array( $event_one, $event_two, $event_three );
    $home_events  = array_diff( array_unique( $home_events ), array('') );
?>
<section id="events_section" class="latest-events">
    <div class="container">
        <?php 
		preschool_and_kindergarten_pro_get_section_header($title, $description ); 
        
        $today = current_time( 'Y-m-d' );
		$events_qry = new WP_Query(array(
			    'post_type'           => 'event',
			    'post__in'            => $home_events,
	            'orderby'             => 'post__in',
	            'posts_per_page'      => -1,
	            'ignore_sticky_posts' => true,
	            'meta_query'          => array(
                        'relation' => 'OR',
                        array(
                            'key'     => '_preschool_and_kindergarten_pro_event_start_date',
                            'value'   =>  $today,
                            'compare' => '>=',
                            'type'    => 'DATE'
                            ),
                        array(
                            'key'     => '_preschool_and_kindergarten_pro_event_end_date',
                            'value'   => $today,
                            'compare' => '>=',
                            'type'    => 'DATE'
                            ),
                    )
		));
	    if( is_rara_toolkit_activated() && $home_events && $events_qry->have_posts() ){ ?>
			<div class="row">		
			<?php 
			    while($events_qry->have_posts()){ $events_qry->the_post();

			    	$rttk_setting = get_post_meta( $post->ID, '_rttk_setting', true );
                    $start_date   = get_post_meta( $post->ID, '_preschool_and_kindergarten_pro_event_start_date',true );
                    $address      = isset( $rttk_setting['event']['address'] ) ? esc_attr( $rttk_setting['event']['address'] ): '';
			        $day          = date('d', strtotime( $start_date ) );
                    $month        = date('M', strtotime( $start_date ) ); ?>

					<div class="col">
					<?php
					    if(has_post_thumbnail()){ ?>
							<div class="img-holder">
							    <a href="<?php the_permalink(); ?>" class="post-thumbnail">
								    <?php the_post_thumbnail('preschool-and-kindergarten-pro-courses-archive', array( 'itemprop' => 'image' ) ); ?>
								</a>
								<div class="date-holder">
								    <span><?php echo esc_html( $day ); ?></span>&nbsp;
								    <?php echo esc_html( $month ); ?>
							    </div>
							</div>
					<?php 
						} 
						if( $address ){ ?>
							<div class="event-address">
								<i class="fa fa-map-marker"></i>
								<address><?php echo esc_html( $address ); ?></address>
							</div>
						<?php } ?>
						<div class="text-holder">
							<h2 class="event-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						<?php 
                            do_action( 'preschool_and_kindergarten_pro_excerpt_section' );
                        ?>
						</div>
					</div>
				<?php 
				} ?>
			</div>
			<?php 
        	if( $view_all && preschool_and_kindergarten_pro_cpt_has_archive( 'event' ) ) echo '<div class="btn-holder"><a href="' . esc_url( get_post_type_archive_link( 'event' ) ) . '" class="btn-view">' . esc_html( $view_all ) . '</a></div>';

			wp_reset_query(); 
	    }elseif( $demo_content ){
            preschool_and_kindergarten_pro_demo_content( 'event' );
		} ?>
	</div>
</section>