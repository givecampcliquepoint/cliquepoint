<?php
/**
 * Gallery Section
 * 
 * @package Preschool_and_Kindergarten_pro
*/  
     $title        = get_theme_mod( 'gallery_section_title', __( 'Photo Gallery', 'preschool-and-kindergarten-pro') );
     $description  = get_theme_mod( 'gallery_section_description',__( 'Gallery is a building or space for the exhibition of art, usually visual art.', 'preschool-and-kindergarten-pro') ); 
     $gallery_img  = get_theme_mod( 'choose_gallery_images' ); 

     $info         = get_theme_mod( 'gallery_section_slogan', __( 'See Our Kindergarten Photo Gallery', 'preschool-and-kindergarten-pro') );
     $button_label = get_theme_mod( 'gallery_viewall_label', __( 'View Gallery', 'preschool-and-kindergarten-pro' ) );
     $button_link  = get_theme_mod( 'gallery_viewall_link', '#' );
     $demo_content = get_theme_mod( 'ed_demo', '1' );

?>  
<section id="gallery_section" class="photo-gallery">
	<?php if( $title || $description ){ ?> 
		<div class="container">
			<?php preschool_and_kindergarten_pro_get_section_header( $title, $description ); ?>
		</div>
	<?php } 

		if( $gallery_img ){
			echo '<div id="gallery-slider" class="owl-theme owl-carousel">';
				foreach( $gallery_img as $gallery ){
				    $image = wp_get_attachment_image_url( $gallery, 'preschool-and-kindergarten-pro-gallery' );
		?>
					<div>
						<img class="owl-lazy" data-src="<?php echo esc_url( $image ); ?>" alt="<?php echo get_the_title( $gallery ); ?>">
						<div class="caption">
							<?php echo get_the_title( $gallery ); ?>
						</div>
					</div>

			<?php }
			echo '</div>';
		}elseif( $demo_content ){
            preschool_and_kindergarten_pro_demo_content( 'gallery' );
		} 
	
		if($info || ( $button_label && $button_link) ) {
	?>
			<div class="container">
				<div class="holder">
					<?php 
				    if( $info){ ?><span><?php echo esc_html( $info ); ?></span> <?php }

					if( $button_label && $button_link ){ ?>
					<a href="<?php echo esc_url( $button_link ); ?>" class="btn-view">
					    <?php echo esc_html( $button_label ); ?>
					</a>
					<?php } ?>
				</div>
			</div>
		<?php } ?>
</section>