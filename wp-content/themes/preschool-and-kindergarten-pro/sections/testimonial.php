<?php
/**
 * Testimonials Section
 * 
 * @package Preschool_and_Kindergarten_Pro
*/ 
    $title              = get_theme_mod( 'preschool_and_kindergarten_testimonials_section_title', __( 'What Parent&rsquo;s Says', 'preschool-and-kindergarten-pro') );
    $description        = get_theme_mod( 'preschool_and_kindergarten_testimonials_section_description', __( 'Testimonial or show consists of a person&rsquo;s written or spoken statement extolling the virtue of a product.', 'preschool-and-kindergarten-pro') );
    $testimonial_number = get_theme_mod( 'number_of_testimonial', '3' );
    $testimonial_order  = get_theme_mod( 'testimonial_order_type', 'date' );
    $view_all           = get_theme_mod( 'testimonial_viewall_label', __( 'View All Testimonials', 'preschool-and-kindergarten-pro' ) );
	$demo_content       = get_theme_mod( 'ed_demo', '1' );

	$testimonial_args = array(
	    'post_type'           => 'testimonial',
    	'posts_per_page'      => $testimonial_number,
		'ignore_sticky_posts' => true,
    );

    if( $testimonial_order == 'menu_order' ){
    	$testimonial_args['orderby'] = 'menu_order title';            
        $testimonial_args['order']   = 'ASC';
    }

	$testimonials_qry = new WP_Query( $testimonial_args ); 
	?>
    <section id="testimonial_section" class="testimonial-section">
        <div class="container">
		<?php  
		    preschool_and_kindergarten_pro_get_section_header( $title, $description );
		    if( is_rara_toolkit_activated() && $testimonials_qry->have_posts() ){ ?> 
					<div id="testimonial-slider" class="owl-carousel owl-theme">
						<?php 
						while( $testimonials_qry->have_posts() ){ $testimonials_qry->the_post();
						    $rttk_setting = get_post_meta( $post->ID, '_rttk_setting', true );
						    $company  = isset($rttk_setting['testimonial']['company']) ? $rttk_setting['testimonial']['company'] : '';
						    $position = isset($rttk_setting['testimonial']['position']) ? $rttk_setting['testimonial']['position'] : ''; ?>
							<div>
								<div class="table">
									<div class="table-row">
										<div class="text-holder">
								            <div class="header">
											    <h3 class="title"><?php the_title(); ?> </h3>
											</div>
											<?php the_content(); ?>
								            <span class="name">
				                                <?php 
				                                if( $company || $position )  echo esc_attr( '- '.$company ); 
				                                if( $position && $company ) echo esc_attr( ', ' );
				                                echo esc_attr( $position ); 
				                                ?>
				                            </span>
										</div>
										<?php 
										if( has_post_thumbnail() ){ ?>
								            <div class="img-holder">
									           <?php the_post_thumbnail( 'preschool-and-kindergarten-pro-testimonials-thumb', array( 'itemprop' => 'image' ) ); ?>
								            </div>
							            <?php 
							            } ?>
									</div>
								</div>
							</div>
						<?php 
						} ?>
					</div>
					<?php if( $view_all && preschool_and_kindergarten_pro_cpt_has_archive( 'testimonial' ) ){ ?>
                        <div class="btn-holder"><a href="<?php echo esc_url( get_post_type_archive_link( 'testimonial' ) ); ?>" class="btn-view"><?php echo esc_html( $view_all ); ?></a></div>
                    <?php 
                    } 
	            wp_reset_postdata();
	        }elseif( $demo_content ){
            preschool_and_kindergarten_pro_demo_content( 'testimonial' );
		    } ?>
		</div>
	</section>

