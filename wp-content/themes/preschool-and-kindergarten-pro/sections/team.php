<?php
/**
 * Staff Section
 * 
 * @package Preschool_and_Kindergarten_Pro
*/ 
    $title       = get_theme_mod('preschool_and_kindergarten_staff_section_title', __( 'Our Staffs', 'preschool-and-kindergarten-pro' ));
    $description = get_theme_mod('preschool_and_kindergarten_staff_section_description', __( 'Teams are especially appropriate for conducting tasks that are high in complexity and have many interdependent subtasks.', 'preschool-and-kindergarten-pro' ));
    $staff_one   = get_theme_mod( 'team_one' );
    $staff_two   = get_theme_mod( 'team_two' );
    $staff_three = get_theme_mod( 'team_three' );
    $view_all    = get_theme_mod( 'team_viewall_label', __( 'View All', 'preschool-and-kindergarten-pro' ) );
    $demo_content = get_theme_mod( 'ed_demo', '1' );

    $staffs_posts = array( $staff_one, $staff_two, $staff_three );
    $staffs_posts = array_diff( array_unique( $staffs_posts ), array('') );
?>
<section id="team_section" class="our-staff">
    <div class="container">
		<?php 
		preschool_and_kindergarten_pro_get_section_header( $title, $description ); 
        
			$staff_qry = new WP_Query( array(
				        'post_type'           => 'team',
			    	    'post__in'            => $staffs_posts,
	                    'orderby'             => 'post__in',
	                    'posts_per_page'      => -1,
	                    'ignore_sticky_posts' => true
			));

		    if( is_rara_toolkit_activated() && $staffs_posts && $staff_qry->have_posts() ){ ?>
				<div class="row">
				    <?php 
				    while( $staff_qry->have_posts() ){ $staff_qry->the_post();

				    	$rttk_setting = get_post_meta( $post->ID, '_rttk_setting', true );
				    	$position  = isset($rttk_setting['team']['position']) ? $rttk_setting['team']['position'] : ''; ?>
						
						<div class="col">
							<?php 
							if( has_post_thumbnail() ){ ?>
								<div class="img-holder">
									<?php the_post_thumbnail('preschool-and-kindergarten-pro-staff-thumb', array( 'itemprop' => 'image' ) ); ?>
								</div>
						    <?php 
						    } ?>
							<div class="text-holder">
								<strong class="name"><?php the_title(); ?></strong>
								<?php if( $position ){ ?><span class="designation"><?php echo esc_attr( $position ); ?></span><?php } ?>
							</div>
						</div>
			        <?php 
			        } ?>
				</div>
		    <?php 
		    wp_reset_postdata(); 
		    }elseif( $demo_content ){
                preschool_and_kindergarten_pro_demo_content( 'team' );
		    }
		    
		    if( $view_all && preschool_and_kindergarten_pro_cpt_has_archive( 'team' ) ){ ?>
                <div class="btn-holder"><a href="<?php echo esc_url( get_post_type_archive_link( 'team' ) ); ?>" class="btn-view"><?php echo esc_html( $view_all ); ?></a></div>
            <?php 
            } 
		?>
	</div>
</section>