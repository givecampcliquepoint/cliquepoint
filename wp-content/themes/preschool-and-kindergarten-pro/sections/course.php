<?php
/**
 * Program Section
 * 
 * @package Preschool_and_Kindergarten_Pro
*/ 
      $title       = get_theme_mod('preschool_and_kindergarten_program_section_title', __( 'Featured Programs', 'preschool-and-kindergarten-pro' ));
      $description = get_theme_mod('preschool_and_kindergarten_program_section_description', __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit integer risus sem..', 'preschool-and-kindergarten-pro' ));
            
      $program_post_one   = get_theme_mod('popular_course_course_one');
      $program_post_two   = get_theme_mod('popular_course_course_two');
      $program_post_three = get_theme_mod('popular_course_course_three');

      $view_all = get_theme_mod( 'courses_viewall_label', __( 'View All', 'preschool-and-kindergarten-pro' ) );

      $demo_content = get_theme_mod( 'ed_demo', '1' );    
      $program_post = array( $program_post_one, $program_post_two, $program_post_three );
      $program_post = array_diff( array_unique( $program_post ), array('') );
?>
    <section id="course_section" class="featured">
		<div class="container">
		    <?php 
		    preschool_and_kindergarten_pro_get_section_header($title, $description ); 

		        $program_qry = new WP_Query(array(
			    	 'post_type'           => 'course',
			    	 'post__in'            => $program_post,
	                 'orderby'             => 'post__in',
	                 'posts_per_page'      => -1,
	                 'ignore_sticky_posts' => true

		    	));

			    if( is_rara_toolkit_activated() && $program_post && $program_qry->have_posts() ){ ?>
				
					<div class="row">
					
					    <?php 
					    while($program_qry->have_posts()){ $program_qry->the_post() ?>
						
							<div class="col">
								<div class="holder">
							
								    <?php
								    if(has_post_thumbnail()){ ?>
										<div class="img-holder">
											<?php the_post_thumbnail('preschool-and-kindergarten-pro-program-thumb', array( 'itemprop' => 'image' ) ); ?>
										</div>
								    <?php 
								    } ?>

									<div class="text-holder">
										<h3 class="title"><?php the_title(); ?></h3>
										<?php 
                                        do_action( 'preschool_and_kindergarten_pro_excerpt_section' );
                                        ?>
										<a href="<?php the_permalink(); ?>" class="btn-detail"><?php esc_html_e('View Details','preschool-and-kindergarten-pro');?></a>
									</div>
								
								</div>
							</div>

					    <?php 
					    } ?>
					
				    </div>
				    <?php if( $view_all && preschool_and_kindergarten_pro_cpt_has_archive( 'course' ) ){ ?>
                        <div class="btn-holder"><a href="<?php echo esc_url( get_post_type_archive_link( 'course' ) ); ?>" class="btn-view"><?php echo esc_html( $view_all ); ?></a></div>
                    <?php 
                    } 
                    wp_reset_postdata(); 
				}elseif( $demo_content ){
                       preschool_and_kindergarten_pro_demo_content( 'course' );
					} ?>
				
		</div>
	</section>