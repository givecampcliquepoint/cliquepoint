<?php 
/**
 * CTA Section
 * 
 * @package Preschool_and_Kindergarten_pro
*/ 
    $title        = get_theme_mod( 'CTA_section_title', __( 'How to Enroll Your Child to a Class?', 'preschool-and-kindergarten-pro' ) );
    $description  = get_theme_mod( 'CTA_section_description', __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit integer risus sem.', 'preschool-and-kindergarten-pro' ) );
    $bg_image     = get_theme_mod( 'CTA_bg_image', get_template_directory_uri() . '/images/demo/img50.jpg' );
    $button_label_one = get_theme_mod( 'CTA_section_button_label', __( 'Contact Us', 'preschool-and-kindergarten-pro' ) );
    $button_label_two = get_theme_mod( 'CTA_section_button_label_two', __( 'Our Mission', 'preschool-and-kindergarten-pro' ) );
    $button_link_one  = get_theme_mod( 'CTA_section_button_link', '#' ); 
    $button_link_two  = get_theme_mod( 'CTA_section_button_link_two', '#' ); 
?>
<div id="CTA_section" class="promotional-block" style="background-image: url(<?php echo esc_html( $bg_image ); ?>);  background-size: cover; background-position: center; background-attachment: fixed;">
    <div class="container">
        <div class="text-holder">
            <?php 
                preschool_and_kindergarten_pro_get_section_header( $title, $description );

                if( $button_link_one ){ ?>
                <a href="<?php echo esc_url( $button_link_one ); ?>" class="btn-contact"><?php echo esc_html( $button_label_one ); ?></a>
                <?php } if( $button_link_two ){ ?>
                <a href="<?php echo esc_url( $button_link_two ); ?>" class="btn-mission"><?php echo esc_html( $button_label_two ); ?></a>
                <?php } ?>
	    </div>
	</div>
</div>