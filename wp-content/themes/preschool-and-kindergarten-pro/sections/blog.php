<?php
/**
 * News Section
 * 
 * @package Preschool_and_Kindergarten_Pro
*/    
    $title          = get_theme_mod('preschool_and_kindergarten_news_section_title', __( 'News and Events', 'preschool-and-kindergarten-pro' ) );
    $description    = get_theme_mod('preschool_and_kindergarten_news_section_description' , __( 'News is information about current events.', 'preschool-and-kindergarten-pro' ) );
    $view_all       = get_theme_mod( 'blog_viewall_label', __( 'View All Blog', 'preschool-and-kindergarten-pro' ) );
    $page_for_post  = get_option( 'page_for_posts' );
    $demo_content   = get_theme_mod( 'ed_demo', '1' );
?>
<section id="blog_section" class="news">
   <div class="container">
	    <?php 
	        preschool_and_kindergarten_pro_get_section_header($title, $description ); 
		    
		    $news_qry = new WP_Query(array(
		    	    'posts_per_page' => 3,
					'post_type' => 'post',
					'ignore_sticky_posts' => true,
		    	));

		    if($news_qry->have_posts()){ ?>
				<div class="row">
				    <?php
				    while( $news_qry->have_posts() ){ $news_qry->the_post() ?>
						<article class="post">
							
							<div class="posted-on">
								<strong><?php echo esc_html( get_the_date( 'd' ) ); ?></strong>
								<span><?php echo esc_html( get_the_date( 'M' ) ); ?></span>
							</div>
							
							<div class="text-holder">
								
								<header class="entry-header">
									<h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
								</header>
								
								<div class="entry-content">
							        <?php  do_action( 'preschool_and_kindergarten_pro_excerpt_section' ); ?>
								</div>
								
								
								<footer class="entry-footer">
									<a href="<?php the_permalink(); ?>" class="readmore"><?php esc_html_e('View Details','preschool-and-kindergarten-pro');?></a>
								</footer>
							
							</div>
						
						</article>
				    <?php 
				    } ?>	
				</div>

				<?php wp_reset_postdata(); 
				if( $view_all && $page_for_post ){ ?>
                    <div class="btn-holder"><a href="<?php echo esc_url( get_permalink( $page_for_post ) ); ?>" class="btn-view"><?php echo esc_html( $view_all ); ?></a></div>
                <?php 
                } 
		    }
		    elseif( $demo_content ){
            preschool_and_kindergarten_pro_demo_content( 'blog' );
		    } ?>
	</div>
</section>