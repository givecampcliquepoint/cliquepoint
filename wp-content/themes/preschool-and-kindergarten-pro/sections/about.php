<?php   
/**
 * About Section
 * 
 * @package Preschool_and_Kindergarten_pro
*/ 
    $about_page   = get_theme_mod('preschool_and_kindergarten_about_page');
    $read_more    = get_theme_mod( 'welcome_read_more', __( 'More About Us', 'preschool-and-kindergarten-pro' ) );
    $demo_content = get_theme_mod( 'ed_demo', '1' );

    
    if($about_page){

	    $about_query = new WP_Query( "post_type=page&p=$about_page" );        
        if($about_query->have_posts()){ $about_query->the_post();?>
			<section id="about_section" class="welcome">
				<div class="container">
					<div class="row">
				    <?php 
						if(has_post_thumbnail()){ ?>
							<div class="img-holder">
								<?php the_post_thumbnail('preschool-and-kindergarten-pro-about-thumb', array( 'itemprop' => 'image' ) ); ?>
							</div>
					<?php 
					    } ?>
						<div class="text-holder">
							<h2 class="title"><?php the_title(); ?></h2>
								<?php   if( has_excerpt() ){
                                            the_excerpt();        
			                            }else{
			                                echo wpautop( wp_kses_post( force_balance_tags( preschool_and_kindergarten_pro_excerpt( get_the_content(), 450, '...', false, false ) ) ) );        
			                            } ?>
							<a href="<?php the_permalink(); ?>" class="btn-more"><?php echo esc_html( $read_more ); ?>
							</a>
						</div>

					</div>
				</div>
			</section>	
	    <?php 

	    }
	    wp_reset_postdata(); 
	}elseif( $demo_content ){
            //Demo Content
            preschool_and_kindergarten_pro_demo_content( 'about' ); 
    }