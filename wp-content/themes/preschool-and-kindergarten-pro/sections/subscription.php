<?php
/**
 * Subscription Section
 * 
 * @package Preschool_and_Kindergarten_pro
*/
if( is_jetpack_subscription_module_active() && is_active_sidebar( 'newsletter-form' ) ){ 
?>
<div id="subscription_section" class="cta-section">
		<div class="container">
			<div class="row">
				<div class="col">
				<?php dynamic_sidebar( 'newsletter-form' ); ?>
				</div>
			</div>
		</div>
	</div>
<?php } ?>