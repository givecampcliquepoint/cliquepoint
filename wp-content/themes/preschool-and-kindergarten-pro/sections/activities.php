 <?php
/**
 * lessons Section
 * 
 * @package Preschool_and_Kindergarten_pro
*/  
     $title        = get_theme_mod( 'preschool_and_kindergarten_lessons_section_title', __( 'Lessons thats sparks Creativity
', 'preschool-and-kindergarten-pro' ) );
     $description  = get_theme_mod( 'preschool_and_kindergarten_lessons_section_description', __( 'Creativity is a phenomenon whereby something new and different is formed.', 'preschool-and-kindergarten-pro' ) ); 
     $lesson_one   = get_theme_mod( 'preschool_and_kindergarten_lesson_post_one' );
     $lesson_two   = get_theme_mod( 'preschool_and_kindergarten_lesson_post_two' );
     $lesson_three = get_theme_mod( 'preschool_and_kindergarten_lesson_post_three' );
     $demo_content = get_theme_mod( 'ed_demo', '1' );


     $lessons_posts = array( $lesson_one, $lesson_two, $lesson_three );
     $lessons_posts = array_diff( array_unique( $lessons_posts ), array('') );
?>  
<section id="activities_section" class="section-2">
    <div class="container">
		<?php 
		    preschool_and_kindergarten_pro_get_section_header( $title, $description ); 
                        	
			    $lesson_qry = new WP_Query( array(
			    	'post_type'           => array( 'post', 'page' ),
                    'post__in'            => $lessons_posts,
                    'orderby'             => 'post__in',
                    'posts_per_page'      => -1,
                    'ignore_sticky_posts' => true
                    ));

				if( $lessons_posts && $lesson_qry->have_posts() ){ ?>
				    <div class="row">
				       
				       <?php 
				        while( $lesson_qry->have_posts() ){ $lesson_qry->the_post(); ?>
						
							<div class="col">
								
								<?php 
								    if( has_post_thumbnail() ){ ?>
										<div class="img-holder">
											<?php the_post_thumbnail( 'preschool-and-kindergarten-pro-lesson-thumb', array( 'itemprop' => 'image' ) ); ?>
										</div>
								<?php } ?>
								
								<div class="text-holder">
									<h3 class="title"><a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a></h3>
									<?php 
									    do_action( 'preschool_and_kindergarten_pro_excerpt_section' ); ?>
								</div>
							
							</div>		
							
			            <?php } ?>
				
				    </div>
				<?php 
			wp_reset_postdata();
		}elseif( $demo_content ){
			preschool_and_kindergarten_pro_demo_content( 'activities' );
		}
	?>
	</div>
</section>
