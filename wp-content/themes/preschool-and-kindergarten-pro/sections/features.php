<?php
/**
 * feature Section
 * 
 * @package Preschool_and_Kindergarten_pro
*/  
     $title         = get_theme_mod( 'preschool_and_kindergarten_features_section_title', __( 'Relax at your home', 'preschool-and-kindergarten-pro' ) );
     $description   = get_theme_mod( 'preschool_and_kindergarten_features_section_description', __( 'It requires little effort and can be done.', 'preschool-and-kindergarten-pro' ) );
     $services_post = get_theme_mod( 'preschool_and_kindergarten_features_post' );
     $view_all      = get_theme_mod( 'features_viewall_label', __( 'View Details', 'preschool-and-kindergarten-pro' ) );
     $link          = get_theme_mod( 'features_viewall_link', '#' );
     $demo_content  = get_theme_mod( 'ed_demo', '1' );          
     ?>
<section id="features_section" class="section-3">
    <div class="container">
	    <?php
	    preschool_and_kindergarten_pro_get_section_header( $title, $description );  
            
            $services_query = new WP_Query( array( 
            	'post_type'    => array( 'post', 'page' ),
			    'p'            => $services_post,
	            'ignore_sticky_posts' => true
            	
            	)
            );

            if( $services_post && $services_query->have_posts() ){ $services_query->the_post(); ?>
				<div class="row">
					
					<div class="text-holder">
						<?php  the_content();
						if( $view_all && $link ){ ?>
	                        <a href="<?php echo esc_url( $link ); ?>" class="btn-detail"><?php echo esc_html( $view_all ); ?></a>
	                    <?php 
	                    }  ?>
					</div>
					<?php 
					if( has_post_thumbnail() ){ ?>
						<div class="img-holder">
							<?php the_post_thumbnail(); ?>
						</div>
					<?php 
					} ?>
				</div>
	        <?php   
	        wp_reset_postdata(); 
	        }elseif( $demo_content ){
                preschool_and_kindergarten_pro_demo_content( 'feature' );
	    	}?>
	</div>
</section>