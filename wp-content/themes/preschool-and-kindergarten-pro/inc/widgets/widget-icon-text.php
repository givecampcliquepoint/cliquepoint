<?php
/**
 * Icon Text Widget
 *
 * @package preschool_and_kindergarten_pro
 */

// register preschool_and_kindergarten_pro_Icon_Text_Widget widget
function preschool_and_kindergarten_pro_register_icon_text_widget(){
    register_widget( 'preschool_and_kindergarten_pro_Icon_Text_Widget' );
}
add_action('widgets_init', 'preschool_and_kindergarten_pro_register_icon_text_widget');
 
 /**
 * Adds preschool_and_kindergarten_pro_Icon_Text_Widget widget.
 */
class preschool_and_kindergarten_pro_Icon_Text_Widget extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    public function __construct() {
        parent::__construct(
			'preschool_and_kindergarten_pro_icon_text_widget', // Base ID
			__( 'RARA: Icon Text Widget', 'preschool-and-kindergarten-pro' ), // Name
			array( 'description' => __( 'An Icon Text Widget.', 'preschool-and-kindergarten-pro' ), ) // Args
		);
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
        
        $title   = ! empty( $instance['title'] ) ? $instance['title'] : '' ;		
        $content = ! empty( $instance['content'] ) ? $instance['content'] : '';
        $icon    = ! empty( $instance['icon'] ) ? $instance['icon'] : '';
        $image   = ! empty( $instance['image'] ) ? ( $instance['image'] ) : '';
        $style   = ! empty( $instance['style'] ) ? $instance['style'] : 'style1';
        $link    = ! empty( $instance['link'] ) ? $instance['link'] : '';
        if( $image ){
                $attachment_id = $image;
                $fimg_url   = wp_get_attachment_image_url( $attachment_id, 'thumbnail' );
        }
        echo $args['before_widget']; ?>
                
                <div class="col <?php echo $style; ?> ">
                    <div class="holder">
				    <?php 
                        if( $image ){ ?>
                            <div class="img-holder">
                                <img src="<?php echo esc_url( $fimg_url ); ?>" alt="<?php echo esc_attr( $title ); ?>" />
                            </div>
                        <?php 
                        }elseif( $icon ){ ?>
                            <div class="img-holder">
                                <span class="fa <?php echo esc_attr( $icon ); ?>"></span>
                            </div>
                        <?php 
                        }else{ ?>
                            <div class="img-holder"><i class="fa fa-check"></i></div>
                        <?php } ?>
                        <div class="text-holder">
                        <?php 
                            if( $title ){ 
                                echo $args['before_title'];
                                if( $link ){ echo '<a href="'.esc_url($link).'" target="_blank">'; }
                                echo apply_filters( 'widget_title', $title, $instance, $this->id_base );
                                if( $link ){ echo '</a>'; }
                                echo $args['after_title'];
                            }   
                                
                            if( $content ) echo wpautop( wp_kses_post( $content ) );
                        ?>								
						</div>
				    </div>
                </div>
        <?php    
        echo $args['after_widget'];
    }

    /**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
        
        $title   = ! empty( $instance['title'] ) ? $instance['title'] : '' ;		
        $content = ! empty( $instance['content'] ) ? $instance['content'] : '';
        $icon    = ! empty( $instance['icon'] ) ? $instance['icon'] : '';
        $image   = ! empty( $instance['image'] ) ? ( $instance['image'] ) : '';
        $style   = ! empty( $instance['style'] ) ? $instance['style'] : 'style1';
        $link    = ! empty( $instance['link'] ) ? $instance['link'] : '';

        ?>
		
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title', 'preschool-and-kindergarten-pro' ); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />            
		</p>
        
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'content' ) ); ?>"><?php esc_html_e( 'Content', 'preschool-and-kindergarten-pro' ); ?></label>
            <textarea name="<?php echo esc_attr( $this->get_field_name( 'content' ) ); ?>" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'content' ) ); ?>"><?php print $content; ?></textarea>
        </p>

        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'link' ) ); ?>"><?php esc_html_e( 'Featured Link', 'preschool-and-kindergarten-pro' ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'link' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'link' ) ); ?>" type="text" value="<?php echo esc_url( $link ); ?>" />            
        </p>
        
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'style' ) ); ?>"><?php esc_html_e( 'Select Style', 'preschool-and-kindergarten-pro' ); ?></label>
			<select id="<?php echo esc_attr( $this->get_field_id( 'style' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'style' ) ); ?>" class="widefat">
				<option value="style1" <?php selected( 'style1', $style ) ?>><?php esc_html_e( 'Style 1', 'preschool-and-kindergarten-pro' ); ?></option>
				<option value="style2" <?php selected( 'style2', $style ) ?>><?php esc_html_e( 'Style 2', 'preschool-and-kindergarten-pro' ); ?></option>
                <option value="style3" <?php selected( 'style3', $style ) ?>><?php esc_html_e( 'Style 3', 'preschool-and-kindergarten-pro' ); ?></option>
				
			</select>
		</p>
        
        <?php preschool_and_kindergarten_pro_get_image_field( $this->get_field_id( 'image' ), $this->get_field_name( 'image' ), $image, __( 'Upload Image', 'preschool-and-kindergarten-pro' ) ); ?>
        
        <p><strong><?php esc_html_e( 'or', 'preschool-and-kindergarten-pro' ); ?></strong></p>
        
        <p>
            <label for="<?php echo $this->get_field_id( 'icon' ); ?>"><?php esc_html_e( 'Icons', 'preschool-and-kindergarten-pro' ); ?></label><br />
            <span class="icon-receiver"><i class="<?php echo esc_attr( $icon ); ?>"></i></span>
            <input class="hidden-icon-input" name="<?php echo $this->get_field_name( 'icon' ); ?>" type="hidden" id="<?php echo $this->get_field_id( 'icon' ); ?>" value="<?php echo esc_html( $icon ); ?>" />            
        </p>
        
        <?php preschool_and_kindergarten_pro_get_icon_list(); ?>
                        
        <?php
	}
    
    /**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		
        $instance['title']   = ! empty( $new_instance['title'] ) ? strip_tags( $new_instance['title'] ) : '' ;
        $instance['content'] = ! empty( $new_instance['content'] ) ? $new_instance['content'] : '';
        $instance['style']   = ! empty( $new_instance['style'] ) ? $new_instance['style'] : 'style1';        
        $instance['image']   = ! empty( $new_instance['image'] ) ? absint( $new_instance['image'] ) : '';
        $instance['icon']    = ! empty( $new_instance['icon'] ) ? $new_instance['icon'] : '';
        $instance['link']    = ! empty( $new_instance['link'] ) ? esc_attr( $new_instance['link'] ) : '';

        
        return $instance;
	}
    
}  // class preschool_and_kindergarten_pro_Icon_Text_Widget 