<?php /**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function preschool_and_kindergarten_pro_widgets_init() {
	
    $sidebars = array(
        'sidebar'   => array(
            'name'        => __( 'Sidebar', 'preschool-and-kindergarten-pro' ),
            'id'          => 'sidebar', 
            'description' => __( 'Default Sidebar', 'preschool-and-kindergarten-pro' ),
        ),

        'header-social'=> array(
            'name'        => __( 'Header Social Icon Widget', 'preschool-and-kindergarten-pro' ),
            'id'          => 'header-social', 
            'description' => __( 'Add RARA: Social Links Widget for header social icon widgets.', 'preschool-and-kindergarten-pro' ),
        ), 

        'about-icon-text'=> array(
            'name'        => __( 'Why Choose us Section (About)', 'preschool-and-kindergarten-pro' ),
            'id'          => 'about-icon-text', 
            'description' => __( 'Add Icon Text Widgets.', 'preschool-and-kindergarten-pro' ),
        ),

        'service-icon-text'=> array(
            'name'        => __( 'Services  Section (Service)', 'preschool-and-kindergarten-pro' ),
            'id'          => 'service-icon-text', 
            'description' => __( 'Add Icon Text Widgets for service section.', 'preschool-and-kindergarten-pro' ),
        ),

        'facilities-icon-text'=> array(
            'name'         => __( 'Facilities Section Widgets (Service)', 'preschool-and-kindergarten-pro' ),
            'id'           => 'facilities-icon-text', 
            'description'  => __( 'Add Icon Text Widgets for facilities section.', 'preschool-and-kindergarten-pro' ),
        ),
        
        'featured-icon-text'=> array(
            'name'        => __( 'Featured  Section (Service)', 'preschool-and-kindergarten-pro' ),
            'id'          => 'featured-icon-text', 
            'description' => __( 'Add Icon Text Widgets for featured section.', 'preschool-and-kindergarten-pro' ),
        ),
        
        'footer-one'=> array(
            'name'        => __( 'Footer One', 'preschool-and-kindergarten-pro' ),
            'id'          => 'footer-one', 
            'description' => __( 'Add footer one widgets.', 'preschool-and-kindergarten-pro' ),
        ),

        'footer-two'=> array(
            'name'        => __( 'Footer Two', 'preschool-and-kindergarten-pro' ),
            'id'          => 'footer-two', 
            'description' => __( 'Add footer two widgets.', 'preschool-and-kindergarten-pro' ),
        ),

        'footer-three'=> array(
            'name'        => __( 'Footer Three', 'preschool-and-kindergarten-pro' ),
            'id'          => 'footer-three', 
            'description' => __( 'Add footer three widgets.', 'preschool-and-kindergarten-pro' ),
        ),
    );
    
    foreach( $sidebars as $sidebar ){
        register_sidebar( array(
    		'name'          => $sidebar['name'],
    		'id'            => $sidebar['id'],
    		'description'   => $sidebar['description'],
    		'before_widget' => '<section id="%1$s" class="widget %2$s">',
    		'after_widget'  => '</section>',
    		'before_title'  => '<h2 class="widget-title">',
    		'after_title'   => '</h2>',
    	) );
    }
    if( is_jetpack_subscription_module_active() ){ 

        register_sidebar(  
            array(
                'name'          => __( 'Subscription Section Widget', 'preschool-and-kindergarten-pro' ),
                'id'            => 'newsletter-form', 
                'description'   => __( 'Click on Add a Widget and select Blog Subscriptions(Jetpack) widget. Fill in the details.', 'preschool-and-kindergarten-pro' ),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
                )
            );
    }

    /** Dynamic sidebars */
    $dynamic_sidebars = preschool_and_kindergarten_pro_get_dynamnic_sidebar(); 
    
        foreach( $dynamic_sidebars as $k => $v ){
            register_sidebar( array(
        		'name'          => esc_html( $v ),
        		'id'            => esc_attr( $k ),
        		'description'   => '',
        		'before_widget' => '<section id="%1$s" class="widget %2$s">',
        		'after_widget'  => '</section>',
        		'before_title'  => '<h2 class="widget-title">',
        		'after_title'   => '</h2>',
        	) );
        }
    
}

add_action( 'widgets_init', 'preschool_and_kindergarten_pro_widgets_init' );

function preschool_and_kindergarten_pro_remove_widget(){
    remove_action( 'widgets_init', 'rttk_pro_register_icon_text_widget' );  
}
add_action( 'after_setup_theme', 'preschool_and_kindergarten_pro_remove_widget' );

require get_template_directory() . '/inc/widgets/widget-icon-text.php';