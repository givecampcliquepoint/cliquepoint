<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Preschool_and_Kindergarten_pro
 */

if ( ! function_exists( 'preschool_and_kindergarten_pro_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function preschool_and_kindergarten_pro_setup() {
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on Preschool and Kindergarten, use a find and replace
     * to change 'preschool-and-kindergarten-pro' to the name of your theme in all the template files.
     */
    load_theme_textdomain( 'preschool-and-kindergarten-pro', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

        /** Custom Logo */
    add_theme_support( 'custom-logo', array(        
        'header-text' => array( 'site-title', 'site-description' ),
    ) );

     //Add Excerpt support on page
    add_post_type_support( 'page', 'excerpt' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support( 'post-thumbnails' );

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
        'primary' => esc_html__( 'Primary', 'preschool-and-kindergarten-pro' ),
    ) );

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );

    /*
     * Enable support for Post Formats.
     * See https://developer.wordpress.org/themes/functionality/post-formats/
     */
    add_theme_support( 'post-formats', array(
        'aside',
        'image',
        'video',
        'quote',
        'link',
    ) );


    add_image_size( 'preschool-and-kindergarten-pro-with-sidebar', 832, 475, true);
    add_image_size( 'preschool-and-kindergarten-pro-without-sidebar', 1140, 475, true);
    add_image_size( 'preschool-and-kindergarten-pro-layout-post', 246, 246, true);
    add_image_size( 'preschool-and-kindergarten-pro-banner-thumb', 1920, 636, true);
    add_image_size( 'preschool-and-kindergarten-pro-about-thumb', 555, 335, true);
    add_image_size( 'preschool-and-kindergarten-pro-about-page-thumb', 520, 520, true);
    add_image_size( 'preschool-and-kindergarten-pro-lesson-thumb', 185, 185, true);
    add_image_size( 'preschool-and-kindergarten-pro-program-thumb', 220, 220, true);
    add_image_size( 'preschool-and-kindergarten-pro-testimonials-thumb', 570, 474, true);
    add_image_size( 'preschool-and-kindergarten-pro-staff-thumb', 360, 385, true);
    add_image_size( 'preschool-and-kindergarten-pro-popular-post-thumb', 60, 60, true);
    add_image_size( 'preschool-and-kindergarten-pro-courses-archive', 360, 268, true);
    add_image_size( 'preschool-and-kindergarten-pro-gallery', 384, 384, true);
    add_image_size( 'preschool-and-kindergarten-pro-schema', 600, 60, true);
    
    /**
     * Images for courses
     */
    apply_filters('course_img_size','preschool-and-kindergarten-pro-courses');
}
endif;

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function preschool_and_kindergarten_pro_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'preschool_and_kindergarten_pro_content_width', 832 );
}
/**
* Adjust content_width value according to template.
*
* @return void
*/
function preschool_and_kindergarten_pro_template_redirect_content_width() {

    // Full Width in the absence of sidebar.
    $sidebar = preschool_and_kindergarten_pro_sidebar( true );
    if( $sidebar ){
       $GLOBALS['content_width'] = 832;
    }elseif ( ! ( is_active_sidebar( 'right-sidebar' ) ) ) {
       $GLOBALS['content_width'] = 1140;
    }

}
/**
 * Enqueue scripts and styles.
 */
function preschool_and_kindergarten_pro_scripts() {

    // Use minified libraries if SCRIPT_DEBUG is false
    $build  = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '/build' : '';
    $suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
    $map_api = get_theme_mod( 'map_api' );
    
    $key = $map_api ? '?key='.esc_attr( $map_api ) : '' ;

    wp_enqueue_style( 'font-awesome', get_template_directory_uri(). '/css' . $build . '/font-awesome' . $suffix . '.css' );
    wp_enqueue_style( 'jquery-sidr-light', get_template_directory_uri(). '/css' . $build . '/jquery.sidr.light' . $suffix . '.css' );
    wp_enqueue_style( 'animate', get_template_directory_uri(). '/css' . $build . '/animate' . $suffix . '.css' );
    wp_enqueue_style( 'owl-carousel-min', get_template_directory_uri(). '/css' . $build . '/owl.carousel' . $suffix . '.css' );
    wp_enqueue_style( 'owl-theme-default-min', get_template_directory_uri(). '/css' . $build . '/owl.theme.default' . $suffix . '.css' );

    
    wp_enqueue_style( 'preschool-and-kindergarten-style', get_stylesheet_uri() , array(), PRESCHOOL_AND_KINDERGARTEN_PRO_THEME_VERSION );
    
    if( preschool_and_kindergarten_pro_is_woocommerce_activated() )
    wp_enqueue_style( 'preschool-and-kindergarten-woocommerce-style', get_template_directory_uri(). '/css' . $build . '/woocommerce' . $suffix . '.css', PRESCHOOL_AND_KINDERGARTEN_PRO_THEME_VERSION );
    
    //Fancy Box
    if( get_theme_mod( 'ed_lightbox') ){
        wp_enqueue_style( 'jquery-fancybox', get_template_directory_uri() . '/js/fancybox/jquery.fancybox.css', '', '2.1.5' );
        wp_enqueue_script( 'jquery-fancybox-pack', get_template_directory_uri() . '/js/fancybox/jquery.fancybox.pack.js', array('jquery'), '2.1.5', true );
    }
    if( is_page_template( 'templates/template-contact.php' ) || is_front_page() )
    wp_enqueue_script( 'preschool-and-kindergarten-pro-googlemap', '//maps.googleapis.com/maps/api/js'.$key, array('jquery'), '3.0', false );
    
    if( get_theme_mod( 'ed_lazy_load', false ) || get_theme_mod( 'ed_lazy_load_cimage', false ) ) {
        wp_enqueue_script( 'layzr', get_template_directory_uri() . '/js' . $build . '/layzr' . $suffix . '.js', array('jquery'), '2.0.4', true );
    }
    
    wp_enqueue_script( 'isotope-pkgd', get_template_directory_uri() . '/js' . $build . '/isotope.pkgd' . $suffix . '.js', array('jquery'), '3.0.1', true );
    wp_enqueue_script( 'jquery-sidr', get_template_directory_uri() . '/js' . $build . '/jquery.sidr' . $suffix . '.js', array('jquery'), '2.2.1', true );
    wp_enqueue_script( 'owl-carousel-min', get_template_directory_uri() . '/js' . $build . '/owl.carousel' . $suffix . '.js', array('jquery'), '2.2.1', true);
    wp_enqueue_script( 'jquery-matchHeight', get_template_directory_uri() . '/js' . $build . '/jquery.matchHeight' . $suffix . '.js', array('jquery'), '0.7.2', true);
    wp_enqueue_script( 'scroll-nav', get_template_directory_uri() . '/js' . $build . '/scroll-nav' . $suffix . '.js', array('jquery'), '3.0.0', true ); 
    wp_enqueue_script( 'preschool-and-kindergarten-custom', get_template_directory_uri() . '/js' . $build . '/custom' . $suffix . '.js', array('jquery'), PRESCHOOL_AND_KINDERGARTEN_PRO_THEME_VERSION, true);
    wp_register_script( 'preschool-and-kindergarten-pro-ajax', get_template_directory_uri() . '/js' . $build . '/ajax' . $suffix . '.js', array('jquery'), PRESCHOOL_AND_KINDERGARTEN_PRO_THEME_VERSION, true );
    
    $slider_auto       = get_theme_mod( 'ed_auto_transition', '1' );
    $slider_loop       = get_theme_mod( 'ed_slider_loop', '1' );
    $slider_animation  = get_theme_mod( 'slider_animation', 'slide' );
    $slider_speed      = get_theme_mod( 'ed_slider_speed_control', '3000' );
    $sticky_header     = get_theme_mod( 'ed_sticky_header' );
    $header_layout     = get_theme_mod( 'header_layout', 'one');
    $testi_slider_auto = get_theme_mod( 'preschool_and_kindergarten_testimonials_autoplay_slider', '1' );
      
    $slider_array = array(
        'auto'            => esc_attr( $slider_auto ),
        'loop'            => esc_attr( $slider_loop ),
        'animation'       => esc_attr( $slider_animation ),
        'lightbox'        => esc_attr( get_theme_mod( 'ed_lightbox') ), 
        'sticky'          => $sticky_header,
        'hlayout'         => $header_layout,
        'autoplayTimeout' => absint( $slider_speed ),
        'rtl'             => is_rtl(), 
        't_auto'          => esc_attr( $testi_slider_auto ),
    );
    
    wp_localize_script( 'preschool-and-kindergarten-custom', 'preschool_and_kindergarten_pro_data', $slider_array );

    $pagination = get_theme_mod( 'pagination_type', 'default' );
    $loadmore   = get_theme_mod( 'load_more_label', __( 'Load More Posts', 'preschool-and-kindergarten-pro' ) );
    $loading    = get_theme_mod( 'loading_label', __( 'Loading...', 'preschool-and-kindergarten-pro' ) );
    $nomore    = get_theme_mod( 'nomore_post_label', __( 'No More Posts', 'preschool-and-kindergarten-pro' ) );
   
    
    if( get_theme_mod( 'ed_ajax_search' ) || $pagination == 'load_more' || $pagination == 'infinite_scroll' ){
        
        // Add parameters for the JS
        global $wp_query;
        $max = $wp_query->max_num_pages;
        $paged = ( get_query_var( 'paged' ) > 1 ) ? get_query_var( 'paged' ) : 1;
        
        wp_enqueue_script( 'preschool-and-kindergarten-pro-ajax' );
        
        wp_localize_script( 
            'preschool-and-kindergarten-pro-ajax', 
            'preschool_and_kindergarten_pro_ajax',
            array(
                'url'           => admin_url( 'admin-ajax.php' ),
                'startPage'     => $paged,
                'maxPages'      => $max,
                'nextLink'      => next_posts( $max, false ),
                'autoLoad'      => $pagination,
                'loadmore'      => $loadmore,
                'loading'       => $loading,
                'nomore'        => $nomore,
                'plugin_url'    => plugins_url()
             )
        );
    }  
    
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}

if ( is_admin() ) : // Load only if we are viewing an admin page

    function preschool_and_kindergarten_pro_admin_scripts( $hook_suffix ) {
        
        if( function_exists( 'wp_enqueue_media' ) ){
        wp_enqueue_media();
        }
        wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/css/font-awesome.css', array(), '4.6.1' );
        wp_enqueue_style( 'preschool-and-kindergarten-pro-admin-style', get_template_directory_uri() . '/inc/css/admin.css', array(), PRESCHOOL_AND_KINDERGARTEN_PRO_THEME_VERSION );
        
        wp_register_script( 'preschool-and-kindergarten-pro-admin-js', get_template_directory_uri() . '/inc/js/admin.js', array('jquery'), PRESCHOOL_AND_KINDERGARTEN_PRO_THEME_VERSION );

        wp_register_script( 'preschool-and-kindergarten-pro-media-uploader', get_template_directory_uri() . '/inc/js/media-uploader.js', array('jquery'), PRESCHOOL_AND_KINDERGARTEN_PRO_THEME_VERSION );
        
        wp_localize_script( 'preschool-and-kindergarten-pro-media-uploader', 'preschool_and_kindergarten_pro_uploader', array(
            'upload' => __( 'Upload', 'preschool-and-kindergarten-pro' ),
            'change' => __( 'Change', 'preschool-and-kindergarten-pro' ),
            'msg'    => __( 'Please upload valid image file.', 'preschool-and-kindergarten-pro' )
        ));
         
        wp_enqueue_script( 'preschool-and-kindergarten-pro-media-uploader' );
        wp_enqueue_script( 'preschool-and-kindergarten-pro-admin-js' );

        if( in_array( $hook_suffix, array('post.php', 'post-new.php' ) ) ){

            $screen = get_current_screen();
            if( is_object( $screen ) &&  'event' == $screen->post_type ){
            //for date-picker
                wp_register_script( 'preschool-and-kindergarten-pro-date-picker-js', get_template_directory_uri() . '/inc/js/date-picker.js', array('jquery'), PRESCHOOL_AND_KINDERGARTEN_PRO_THEME_VERSION );
                wp_enqueue_script( 'preschool-and-kindergarten-pro-date-picker-js' );
            }
        }
        
    }

endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_customizer_js' ) ) :
/** 
 * Registering and enqueuing scripts/stylesheets for Customizer controls.
 */ 
function preschool_and_kindergarten_pro_customizer_js() {
    wp_enqueue_style( 'preschool-and-kindergarten-pro-customizer-style', get_template_directory_uri() . '/inc/css/customizer.css', array(), PRESCHOOL_AND_KINDERGARTEN_PRO_THEME_VERSION );
    wp_enqueue_script( 'preschool-and-kindergarten-pro-customizer-js', get_template_directory_uri() . '/inc/js/customizer.js', array('jquery'), PRESCHOOL_AND_KINDERGARTEN_PRO_THEME_VERSION, true  );

    $ed_form = array( 'subscribe' => is_jetpack_subscription_module_active() );
    
    wp_localize_script( 'preschool-and-kindergarten-pro-customizer-js', 'preschool_and_kindergarten_pro_form_data', $ed_form );

}
endif;

/**
 * Enqueue google fonts
*/
function preschool_and_kindergarten_pro_scripts_styles() {
    wp_enqueue_style( 'preschool-and-kindergarten-pro-google-fonts', preschool_and_kindergarten_pro_fonts_url(), array(), null );
}

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function preschool_and_kindergarten_pro_body_classes( $classes ) {
    
    global $post;
    $blog_layout   = get_theme_mod( 'blog_layout', 'default' ); //From Customizer
    $bg_color      = get_theme_mod( 'bg_color', '#ffffff' );
    $bg_image      = get_theme_mod( 'bg_image' );
    $bg_pattern    = get_theme_mod( 'bg_pattern', 'nobg' );
    $bg            = get_theme_mod( 'body_bg', 'image' );
    $enable_slider = get_theme_mod( 'ed_slider' );

    
    // Adds a class of group-blog to blogs with more than 1 published author.
    if ( is_multi_author() ) {
        $classes[] = 'group-blog';
    }

    // Adds a class of hfeed to non-singular pages.
    if ( ! is_singular() ) {
        $classes[] = 'hfeed';
    }

    // Adds a class for custom background Color
    if( $bg_color !== '#ffffff' ){
        $classes[] = 'custom-background custom-background-color';
    }
    
    // Adds a class for custom background Image
    if( $bg == 'image' && $bg_image ){
        $classes[] = 'custom-background custom-background-image';
    }
    
    // Adds a class for custom background Pattern
    if( $bg == 'pattern' && $bg_pattern != 'nobg' ){
        $classes[] = 'custom-background custom-background-pattern';
    }
    
    if( is_404() ){
        $classes[] = 'full-width';
    }

    if( !is_single() &&  $blog_layout === 'round' ){
        $classes[] = 'blog-round';
    }
    
    if( !is_single() && $blog_layout === 'square' ){
        $classes[] = 'blog-medium';
    }
    if ( ! $enable_slider || ! is_front_page()  ) {
        $classes[] = 'no-slider';
    }
    
    if( preschool_and_kindergarten_pro_is_woocommerce_activated() && ( is_shop() || is_product_category() || is_product_tag() || 'product' === get_post_type() ) && ! is_active_sidebar( 'shop-sidebar' ) ){
        $classes[] = 'full-width';
    }
    
    $classes[] = preschool_and_kindergarten_pro_sidebar( false, true );

    return $classes;
}

if( ! function_exists( 'preschool_and_kindergarten_pro_post_classes' ) ) :
/**
 * Adds custom classes to the array of post classes.
 *
 * @param array $classes Classes for the post element.
 * @return array
 */
function preschool_and_kindergarten_pro_post_classes( $classes ) {
    
    $classes[] = 'latest_post';

    if( is_singular( array( 'team', 'testimonial' ) ) ){

        $classes[] = 'post';
    }
    
    if( is_post_type_archive( 'testimonial' ) ){

        $classes[] = 'testimonial-item';

    }elseif( is_post_type_archive( 'event' ) ){

        $classes[] = 'event-holder';
        
    }
    
    return $classes;
}
endif;
/**
 * Flush out the transients used in preschool_and_kindergarten_pro_categorized_blog.
 */
function preschool_and_kindergarten_pro_category_transient_flusher() {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }
    // Like, beat it. Dig?
    delete_transient( 'preschool_and_kindergarten_pro_categories' );
}

/**
 * Callback Function for Google Map 
 * @link https://developers.google.com/maps/documentation/javascript/examples/marker-simple#try-it-yourself
*/
function preschool_and_kindergarten_pro_google_map_cb(){
    if( is_page_template( 'templates/template-contact.php' ) || is_front_page() ){
        
        $ed_google_map   = get_theme_mod( 'ed_google_map' );
        $ed_map_onhome   = get_theme_mod( 'ed_google_map_on_home', '1' );
        $lattitude       = get_theme_mod( 'latitude', '27.7304135' );
        $longitude       = get_theme_mod( 'longitude', '85.3304937' );
        $map_height      = get_theme_mod( 'map_height', '430' );
        $map_zoom        = get_theme_mod( 'map_zoom', '17' );
        $map_type        = get_theme_mod( 'map_type', 'ROADMAP' );
        $map_scroll      = ( get_theme_mod( 'ed_map_scroll', '1' ) == 1 ) ? 'true' : 'false';
        $map_control     = ( get_theme_mod( 'ed_map_controls', '1' ) != 1 ) ? 'true' : 'false';
        $map_control_inv = ( get_theme_mod( 'ed_map_controls', '1' ) == 1 ) ? 'true' : 'false';
        $ed_map_marker   = get_theme_mod( 'ed_map_marker' );
        $marker_title    = get_theme_mod( 'marker_title' );
            
        if( $ed_google_map || $ed_map_onhome ){ ?>
        <style>
          #map {
          /*  width: 100%;*/
            height: <?php echo $map_height; ?>px;
          }
        </style>
        <script type="text/javascript">
        
            var map;
            var myLatLng = {lat: <?php echo $lattitude;?>, lng: <?php echo $longitude;?>};
            
            function initialize() {
                var mapOptions = {
                    zoom: <?php echo $map_zoom; ?>,
                    center: new google.maps.LatLng(<?php echo $lattitude;?>, <?php echo $longitude;?> ),
                    mapTypeId: google.maps.MapTypeId.<?php echo $map_type; ?>,
                    scrollwheel: <?php echo $map_scroll ?>,
                    zoomControl: <?php echo $map_control_inv ?>,
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.SMALL
                    },
                    mapTypeControl: <?php echo $map_control_inv ?>,
                    mapTypeControlOptions: {
                        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                    },
                    disableDefaultUI: <?php echo $map_control ?>,
                };
                map = new google.maps.Map(document.getElementById('map'), mapOptions);
                
                <?php if( $ed_map_marker ){ ?>
                var marker = new google.maps.Marker({
                  position: myLatLng,
                  map: map,
                  <?php if( $marker_title ) echo 'title: "' . esc_html( $marker_title ) . '"' ?>
                });
                <?php } ?>
            }
            google.maps.event.addDomListener(window, 'load', initialize); 
          
        </script>
    <?php }
    }    
}


if( ! function_exists( 'preschool_and_kindergarten_pro_custom_js' ) ) :
/**
 * Custom Script
*/
function preschool_and_kindergarten_pro_custom_js(){
    $custom_script = get_theme_mod( 'custom_script' );
    if( $custom_script ){
        echo '<script type="text/javascript">';
        echo wp_strip_all_tags( $custom_script );
        echo '</script>';
    }
}
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_ajax_search' ) ) :
/**
 * AJAX Search results
 */ 
function preschool_and_kindergarten_pro_ajax_search() {
    $query = $_REQUEST['q']; // It goes through esc_sql() in WP_Query
    $search_query = new WP_Query( array( 's' => $query, 'posts_per_page' => 3, 'post_status' => 'publish' )); 
    $search_count = new WP_Query( array( 's' => $query, 'posts_per_page' => -1, 'post_status' => 'publish' ));
    $search_count = $search_count->post_count;
    if ( !empty( $query ) && $search_query->have_posts() ) : 
        
        echo '<ul class="ajax-search-results">';
        while ( $search_query->have_posts() ) : $search_query->the_post(); ?>
        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>   
        <?php
        endwhile;
        echo '</ul>';
        
        echo '<div class="ajax-search-meta"><span class="results-count">'.$search_count.' '.__( 'Results', 'preschool-and-kindergarten-pro' ).'</span><a href="'. esc_url( get_search_link( $query ) ) .'" class="results-link">'.__( 'Show all results.', 'preschool-and-kindergarten-pro' ).'</a></div>';
    else:
        echo '<div class="no-results">'.__( 'No results found.', 'preschool-and-kindergarten-pro' ).'</div>';
    endif;
    
    wp_die(); // this is required to terminate immediately and return a proper response
}
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_exclude_cat' ) ) :
/**
 * Exclude post with Category from blog and archive page. 
*/
function preschool_and_kindergarten_pro_exclude_cat( $query ){
    $cat = get_theme_mod( 'exclude_categories' );
    
    if( $cat && ! is_admin() && $query->is_main_query() ){
        $cat = array_diff( array_unique( $cat ), array('') );
        if( $query->is_home() || $query->is_archive() ) {
      $query->set( 'category__not_in', $cat );
    }
    }    
}
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_custom_category_widget' ) ) :
/** 
 * Exclude Categories from Category Widget 
*/ 
function preschool_and_kindergarten_pro_custom_category_widget( $arg ) {
    $cat = get_theme_mod( 'exclude_categories' );
    
    if( $cat ){
        $cat = array_diff( array_unique( $cat ), array('') );
        $arg["exclude"] = $cat;
    }
    return $arg;
}
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_exclude_posts_from_recentPostWidget_by_cat' ) ) :
/**
 * Exclude post from recent post widget of excluded catergory
 * 
 * @link http://blog.grokdd.com/exclude-recent-posts-by-category/
*/
function preschool_and_kindergarten_pro_exclude_posts_from_recentPostWidget_by_cat( $arg ){
    
    $cat = get_theme_mod( 'exclude_categories' );
   
    if( $cat ){
        $cat = array_diff( array_unique( $cat ), array('') );
        $arg["category__not_in"] = $cat;
    }
    
    return $arg;   
}
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_setup_pages' ) ) :
/**
 * Setup Contact and About Us Page Programatically
*/
function preschool_and_kindergarten_pro_setup_pages(){
    
    $pages = array(
        'contact-us' => array( 
            'page_name'     => 'Contact Us',
            'page_template' => 'templates/template-contact.php'
        ),
        'about-us' => array( 
            'page_name'     => 'About Us',
            'page_template' => 'templates/template-about.php'
        )
    );
    
    foreach( $pages as $page => $val ){
        preschool_and_kindergarten_pro_create_post( $val['page_name'], $page, $val['page_template'] );
    }
    
}
endif;
add_filter( 'after_switch_theme', 'preschool_and_kindergarten_pro_setup_pages' );

if( ! function_exists( 'preschool_and_kindergarten_pro_get_the_archive_title' ) ) :
/**
 * Filter Archive Title
*/
function preschool_and_kindergarten_pro_get_the_archive_title( $title ){
    
    $event_header       = get_theme_mod( 'event_archive_title', __( 'Events', 'preschool-and-kindergarten-pro' ) );
    $team_header        = get_theme_mod( 'team_archive_title', __( 'Teams', 'preschool-and-kindergarten-pro' ) );
    $testimonail_header = get_theme_mod( 'testimonial_archive_title', __( 'Testimonials', 'preschool-and-kindergarten-pro' ) );
    $course_header      = get_theme_mod( 'courses_archive_title', __( 'Courses', 'preschool-and-kindergarten-pro' ) );
    
    if( is_category() ){
        $title = single_cat_title( '', false );
    }elseif ( is_tag() ){
        $title = single_tag_title( '', false );
    }elseif( is_author() ){
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    }elseif ( is_year() ) {
        $title = get_the_date( __( 'Y', 'preschool-and-kindergarten-pro' ) );
    }elseif ( is_month() ) {
        $title = get_the_date( __( 'F Y', 'preschool-and-kindergarten-pro' ) );
    }elseif ( is_day() ) {
        $title = get_the_date( __( 'F j, Y', 'preschool-and-kindergarten-pro' ) );
    }elseif ( is_post_type_archive() ) {
        if( is_post_type_archive( 'event' ) ){
            $title = $event_header ? $event_header : post_type_archive_title( '', false );  
        }elseif( is_post_type_archive( 'team' ) ){
            $title = $team_header ? $team_header : post_type_archive_title( '', false );
        }elseif( is_post_type_archive( 'testimonial' ) ){
            $title = $testimonail_header ? $testimonail_header : post_type_archive_title( '', false );
        }elseif( is_post_type_archive( 'course' ) ){
            $title = $course_header ? $course_header : post_type_archive_title( '', false );
        }else{
            $title = post_type_archive_title( '', false );
        }        
    } 
    
    return $title;
}
endif;

if( ! function_exists( 'set_args_for_cpt' ) ):

// Custom query for events
    function set_args_for_cpt( $query ) {
        if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'event' ) ) {
            
            $event_order_by = get_theme_mod( 'cpt_event_page_order_type', 'date' ); 
            
            if( $event_order_by == 'date' ) {
                // set date
                $today = current_time( 'Y-m-d' );
                $meta_query = array(
                    'relation' => 'AND',
                    array(
                        'key'     => '_preschool_and_kindergarten_pro_event_start_date',
                        'value'   =>  $today,
                        'compare' => '>=',
                        'type'    => 'DATE'
                        ),
                    array(
                        'key'     => '_preschool_and_kindergarten_pro_event_end_date',
                        'value'   => $today,
                        'compare' => '>=',
                        'type'    => 'DATE'
                        ),
                );

                $query->set( 'meta_query', $meta_query );
                $query->set( 'posts_per_page', '5' );
                $query->set( 'meta_key', '_preschool_and_kindergarten_pro_event_start_date' );
                $query->set( 'orderby', 'meta_value' );
                $query->set( 'order', 'ASC' );

            } elseif ( $event_order_by == 'menu_order' ) {

                $query->set( 'orderby', 'menu_order title' );
                $query->set( 'order', 'ASC' );

            } else {
                return;
            }
        }
        if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'team' ) ) {

            $team_order_by = get_theme_mod( 'cpt_team_page_order_type', 'date' ); 

            if( $team_order_by == 'menu_order' ){
                $query->set( 'orderby', 'menu_order title' );
                $query->set( 'order', 'ASC' );
            }

            $query->set( 'posts_per_page', '-1' );
            
        }
        if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'testimonial' ) ) {
            
            $testimonial_number   = get_theme_mod( 'number_of_testimonial', '3' );
            $testimonial_order_by = get_theme_mod( 'cpt_testimonial_page_order_type', 'date' ); 

            $query->set( 'posts_per_page', $testimonial_number );

            if( $testimonial_order_by == 'menu_order' ){
                $query->set( 'orderby', 'menu_order title' );
                $query->set( 'order', 'ASC' );
            }
        }
    }
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_og_header' ) ) :
/**
 * Add og meta tags in header for social sharing
*/
function preschool_and_kindergarten_pro_og_header() {
    if( is_single() ){
        global $post;
        
        $post_thumbnail_id = get_post_thumbnail_id( $post->ID );
        if( $post_thumbnail_id ){
            $featured_image = wp_get_attachment_image_url( $post_thumbnail_id, 'preschool-and-kindergarten-pro-about-page-thumb' );
        }
        
        if( has_excerpt( $post->ID ) ){
            $description = $post->post_excerpt;
        }
        else {
            $description = preschool_and_kindergarten_pro_truncate( $post->post_content, 250 );
        }
    
        if( ( $post_thumbnail_id && $featured_image ) && $description ){ ?>
            <meta property="og:url"         content="<?php echo esc_url( get_permalink( $post->ID ) ); ?>" />
            <meta property="og:type"        content="article" />
            <meta property="og:title"       content="<?php echo esc_html( $post->post_title )?>" />
            <meta property="og:description" content="<?php echo esc_html( $description ); ?>" />
            <meta property="og:image"       content="<?php echo esc_url( $featured_image ); ?>" />
        <?php
        }
    }
}
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_page_attribute_post' ) ):
    /**
     * Add Page attribute in post for post ordering
    */
    function preschool_and_kindergarten_pro_page_attribute_post(){
        add_post_type_support( 'course', 'page-attributes' );       
        add_post_type_support( 'event', 'page-attributes' ); 
        add_post_type_support( 'team', 'page-attributes' );
        add_post_type_support( 'testimonial', 'page-attributes' );
    }
endif;
add_action( 'init', 'preschool_and_kindergarten_pro_page_attribute_post' );

if( ! function_exists( 'preschool_and_kindergarten_pro_columns_head' ) ) :
    /**
     * Adds a Order column header in the item list admin page.
     *
     * @param array $defaults
     * @return array
     */
    function preschool_and_kindergarten_pro_columns_head( $defaults ){
           
        if( get_post_type() === 'team' ){
            $defaults['team_order'] = __( 'Order', 'preschool-and-kindergarten-pro' );
        }

        if( get_post_type() === 'event' ){
            $defaults['event_order'] = __( 'Order', 'preschool-and-kindergarten-pro' );
        }

        if( get_post_type() === 'course' ){
            $defaults['course_order'] = __( 'Order', 'preschool-and-kindergarten-pro' );
        }

         if( get_post_type() === 'testimonial' ){
            $defaults['testimonial_order'] = __( 'Order', 'preschool-and-kindergarten-pro' );
        }
        
        return $defaults;
    }
endif;
add_filter( 'manage_posts_columns', 'preschool_and_kindergarten_pro_columns_head' );

if( ! function_exists( 'preschool_and_kindergarten_pro_columns_content' ) ) :
    /**
     * @param string $column_name The name of the column to display.
     * @param int $post_ID The ID of the current post.
     */
    function preschool_and_kindergarten_pro_columns_content( $column_name, $post_ID ){
        global $post;
        
        if( $column_name == 'course_order' ){
            echo $post->menu_order;
        }
        if( $column_name == 'team_order' ){
            echo $post->menu_order;
        } 
        if( $column_name == 'event_order' ){
            echo $post->menu_order;
        }
        if( $column_name == 'testimonial_order' ){
            echo $post->menu_order;
        }
    }
endif;
add_action( 'manage_posts_custom_column', 'preschool_and_kindergarten_pro_columns_content', 10, 2 );

if( ! function_exists( 'preschool_and_kindergarten_pro_team_order_column_sortable' ) ) :
    /**
    * make column sortable
    */
    function preschool_and_kindergarten_pro_team_order_column_sortable( $columns ){
        $columns['team_order'] = 'menu_order';
        return $columns;
    }
endif;
add_filter( 'manage_edit-team_sortable_columns', 'preschool_and_kindergarten_pro_team_order_column_sortable' );

if( ! function_exists( 'preschool_and_kindergarten_pro_event_order_column_sortable' ) ) :
    /**
    * make column sortable
    */
    function preschool_and_kindergarten_pro_event_order_column_sortable( $columns ){
        $columns['event_order'] = 'menu_order';
        return $columns;
    }
endif;
add_filter( 'manage_edit-event_sortable_columns', 'preschool_and_kindergarten_pro_event_order_column_sortable' );

if( ! function_exists( 'preschool_and_kindergarten_pro_course_order_column_sortable' ) ) :
    /**
    * make column sortable
    */
    function preschool_and_kindergarten_pro_course_order_column_sortable( $columns ){
        $columns['course_order'] = 'menu_order';
        return $columns;
    }
endif;
add_filter( 'manage_edit-course_sortable_columns', 'preschool_and_kindergarten_pro_course_order_column_sortable' );

if( ! function_exists( 'preschool_and_kindergarten_pro_testimonial_order_column_sortable' ) ) :
    /**
    * make column sortable
    */
    function preschool_and_kindergarten_pro_testimonial_order_column_sortable( $columns ){
        $columns['testimonial_order'] = 'menu_order';
        return $columns;
    }
endif;
add_filter( 'manage_edit-testimonial_sortable_columns', 'preschool_and_kindergarten_pro_testimonial_order_column_sortable' );

if( ! function_exists( 'rename_custom_post_type_slug' ) ) :
    /**
    * rename custom post type slug
    */
function rename_custom_post_type_slug( $args, $post_type ) {
    $testimonial_slug = get_theme_mod( 'rename_testimonial_slug' );
    $team_slug        = get_theme_mod( 'rename_team_slug' );
    $event_slug       = get_theme_mod( 'rename_event_slug' );
    $course_slug      = get_theme_mod( 'rename_course_slug' );

    if ( 'testimonial' === $post_type ) {

        if( $testimonial_slug ){
            $args['rewrite']['slug'] = $testimonial_slug;
        }
    }
    if ( 'team' === $post_type ) {

        if( $team_slug ){
            $args['rewrite']['slug'] = $team_slug;
        }
    }
    if ( 'event' === $post_type ) {

        if( $event_slug ){
            $args['rewrite']['slug'] = $event_slug;
        }
    }
    if ( 'course' === $post_type ) {

        if( $course_slug ){
            $args['rewrite']['slug'] = $course_slug;
        }
    }

    return $args;
}

add_filter( 'register_post_type_args', 'rename_custom_post_type_slug', 10, 2 );

endif;

if ( ! function_exists( 'preschool_and_kindergarten_pro_events_listing_query' ) ) :

    /**
     * Modify the events date according the end date comparing with the current date
    */

function preschool_and_kindergarten_pro_events_listing_query($query){

    if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'event' ) ) {

        $posts_per_page = get_option( 'posts_per_page' );
        $today = current_time( 'Y-m-d' );
        $meta_query = array(
            array(
                'key'     => '_preschool_and_kindergarten_pro_event_end_date',
                'value'   => $today,
                'compare' => '>=',
                'type'    => 'DATE'
                ),
        );
        
        $query->set( 'meta_query', $meta_query );
        $query->set( 'meta_key', '_preschool_and_kindergarten_pro_event_end_date' );
        $query->set( 'orderby', 'meta_value' );
        $query->set( 'posts_per_page', $posts_per_page );
    }
}
add_filter( 'pre_get_posts','preschool_and_kindergarten_pro_events_listing_query' );

endif;


if( ! function_exists( 'preschool_and_kindergarten_pro_change_comment_form_default_fields' ) ) :
/**
* Change Comment form default fields i.e. author, email & url.
*/
function preschool_and_kindergarten_pro_change_comment_form_default_fields( $fields ){    
   // get the current commenter if available
   $commenter = wp_get_current_commenter();

   // core functionality
   $req = get_option( 'require_name_email' );
   $aria_req = ( $req ? " aria-required='true'" : '' );    

   // Change just the author field
   $fields['author'] = '<p class="comment-form-author"><input id="author" name="author" placeholder="' . esc_attr__( 'Name*', 'preschool-and-kindergarten-pro' ) . '" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>';
   
   $fields['email'] = '<p class="comment-form-email"><input id="email" name="email" placeholder="' . esc_attr__( 'Email*', 'preschool-and-kindergarten-pro' ) . '" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p>';
   
   $fields['url'] = '<p class="comment-form-url"><input id="url" name="url" placeholder="' . esc_attr__( 'Website', 'preschool-and-kindergarten-pro' ) . '" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></p>';
   
   return $fields;    
}
endif;
add_filter( 'comment_form_default_fields', 'preschool_and_kindergarten_pro_change_comment_form_default_fields' );

if( ! function_exists( 'preschool_and_kindergarten_pro_change_comment_form_defaults' ) ) :
/**
* Change Comment Form defaults
*/
function preschool_and_kindergarten_pro_change_comment_form_defaults( $fields ){
   $comment_field = $fields['comment'];  
   $fields['comment'] = '<p class="comment-form-comment"><textarea id="comment" name="comment" cols="40" rows="8" required="required" placeholder="' . esc_attr__( 'Comment','preschool-and-kindergarten-pro' ) . '"></textarea></p>';;
   
   return $fields;    
}
endif;
add_filter( 'comment_form_fields', 'preschool_and_kindergarten_pro_change_comment_form_defaults' );

if( ! function_exists( 'preschool_and_kindergarten_pro_single_post_schema' ) ) :
/**
 * Single Post Schema
 *
 * @return string
 */
function preschool_and_kindergarten_pro_single_post_schema() {
    if ( is_singular( 'post' ) ) {
        global $post;
        $custom_logo_id = get_theme_mod( 'custom_logo' );

        $site_logo   = wp_get_attachment_image_src( $custom_logo_id , 'preschool-and-kindergarten-pro-schema' );
        $images      = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
        $excerpt     = preschool_and_kindergarten_pro_escape_text_tags( $post->post_excerpt );
        $content     = $excerpt === "" ? mb_substr( preschool_and_kindergarten_pro_escape_text_tags( $post->post_content ), 0, 110 ) : $excerpt;
        $schema_type = ! empty( $custom_logo_id ) && has_post_thumbnail( $post->ID ) ? "BlogPosting" : "Blog";

        $args = array(
            "@context"  => "http://schema.org",
            "@type"     => $schema_type,
            "mainEntityOfPage" => array(
                "@type" => "WebPage",
                "@id"   => get_permalink( $post->ID )
            ),
            "headline"  => get_the_title( $post->ID ),
            "image"     => array(
                "@type"  => "ImageObject",
                "url"    => $images[0],
                "width"  => $images[1],
                "height" => $images[2]
            ),
            "datePublished" => get_the_time( DATE_ISO8601, $post->ID ),
            "dateModified"  => get_post_modified_time(  DATE_ISO8601, __return_false(), $post->ID ),
            "author"        => array(
                "@type"     => "Person",
                "name"      => preschool_and_kindergarten_pro_escape_text_tags( get_the_author_meta( 'display_name', $post->post_author ) )
            ),
            "publisher" => array(
                "@type"       => "Organization",
                "name"        => get_bloginfo( 'name' ),
                "description" => get_bloginfo( 'description' ),
                "logo"        => array(
                    "@type"   => "ImageObject",
                    "url"     => $site_logo[0],
                    "width"   => $site_logo[1],
                    "height"  => $site_logo[2]
                )
            ),
            "description" => ( class_exists('WPSEO_Meta') ? WPSEO_Meta::get_value( 'metadesc' ) : $content )
        );

        if ( has_post_thumbnail( $post->ID ) ) :
            $args['image'] = array(
                "@type"  => "ImageObject",
                "url"    => $images[0],
                "width"  => $images[1],
                "height" => $images[2]
            );
        endif;

        if ( ! empty( $custom_logo_id ) ) :
            $args['publisher'] = array(
                "@type"       => "Organization",
                "name"        => get_bloginfo( 'name' ),
                "description" => get_bloginfo( 'description' ),
                "logo"        => array(
                    "@type"   => "ImageObject",
                    "url"     => $site_logo[0],
                    "width"   => $site_logo[1],
                    "height"  => $site_logo[2]
                )
            );
        endif;

        echo '<script type="application/ld+json">' , PHP_EOL;
        if ( version_compare( PHP_VERSION, '5.4.0' , '>=' ) ) {
            echo wp_json_encode( $args, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT ) , PHP_EOL;
        } else {
            echo wp_json_encode( $args ) , PHP_EOL;
        }
        echo '</script>' , PHP_EOL;
    }
}
endif;
add_action( 'wp_head', 'preschool_and_kindergarten_pro_single_post_schema' );