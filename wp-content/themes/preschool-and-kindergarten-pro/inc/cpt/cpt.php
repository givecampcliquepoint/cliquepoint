<?php
/**
 * Rara Theme custom post and taxonomy definitions.
 *
 * @package Rara_Theme
 */
 
if( is_rara_toolkit_activated() ) :
//only execute if Rara Theme Toolkit plugin is installed and activated
    
/**
 * Filter the CPT generated by RTTK
*/    
function preschool_and_kindergarten_pro_cpt(){
    
    $posts = array(
		
        'course' => array( 
			'singular_name'	      => __( 'Course', 'preschool-and-kindergarten-pro' ),
			'general_name'	      => __( 'Courses', 'preschool-and-kindergarten-pro' ),
			'dashicon'		      => 'dashicons-book-alt',
			'taxonomy'		      => 'course_categories',
            'taxonomy_slug'	      => 'courses',    
            'has_archive'         => true,		
			'exclude_from_search' => false,
			'show_in_nav_menus'	  => true,
			'supports' 			  => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
			'path'				  => dirname( __FILE__ ).DIRECTORY_SEPARATOR ,
		),

		'event' => array( 
			'singular_name'	      => __( 'Event', 'preschool-and-kindergarten-pro' ),
			'general_name'	      => __( 'Events', 'preschool-and-kindergarten-pro' ),
			'dashicon'		      => 'dashicons-calendar-alt',
			'taxonomy'		      => 'event_categories',
			'taxonomy_slug'	      => 'events',
            'has_archive'         => true,		
			'exclude_from_search' => false,
			'show_in_nav_menus'	  => true,
			'supports' 			  => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
			'path'				  => dirname( __FILE__ ).DIRECTORY_SEPARATOR,
		),

		'team' => array( 
			'singular_name'	      => __( 'Team Member', 'preschool-and-kindergarten-pro' ),
			'general_name'	      => __( 'Team Members', 'preschool-and-kindergarten-pro' ),
			'dashicon'	          => 'dashicons-groups',
            'has_archive'         => true,		
			'exclude_from_search' => false,
			'show_in_nav_menus'	  => true,
			'supports' 			  => array( 'title', 'editor', 'thumbnail' ),
			'path'				  => dirname( __FILE__ ).DIRECTORY_SEPARATOR,
		),
		
		'testimonial' => array( 
			'singular_name'	      => __( 'Testimonial', 'preschool-and-kindergarten-pro' ),
			'general_name'	      => __( 'Testimonials', 'preschool-and-kindergarten-pro' ),
			'dashicon'	          => 'dashicons-testimonial',
            'has_archive'         => true,		
			'exclude_from_search' => false,
			'show_in_nav_menus'	  => true,
			'supports' 			  => array( 'title', 'editor', 'thumbnail' ),
			'path'				  => dirname( __FILE__ ).DIRECTORY_SEPARATOR,
		),
    );
    
    return $posts;
}   
add_filter( 'rttk_get_posttype_array', 'preschool_and_kindergarten_pro_cpt' ); 
    
endif;