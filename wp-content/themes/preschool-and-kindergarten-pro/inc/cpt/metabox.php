<?php
/**
 * Preschool and Kindergarten Pro Meta Box
 * 
 * @package preschool_and_kindergarten_pro
 */

add_action('add_meta_boxes', 'preschool_and_kindergarten_pro_add_sidebar_layout_box');

function preschool_and_kindergarten_pro_add_sidebar_layout_box(){    
    
    $screens = array( 'post', 'page' );
    foreach ( $screens as $screen ){
        add_meta_box(
                 'preschool_and_kindergarten_pro_sidebar_layout', // $id
                 __( 'Sidebar Layout', 'preschool-and-kindergarten-pro' ), // $title
                 'preschool_and_kindergarten_pro_sidebar_layout_callback', // $callback
                 $screen, // $page
                 'normal', // $context
                 'high' // $priority
        );
    }
    // Meta box to add addition detials for "Event"
        add_meta_box(
                     'preschool_and_kindergarten_pro_event_date', // $id
                     __( 'Events Date', 'preschool-and-kindergarten-pro' ), // $title
                     'preschool_and_kindergarten_pro_event_details_callback', // $callback
                     'event', // $post_type
                     'normal', // $context
                     'high'// $priority
        ); 

}

$preschool_and_kindergarten_pro_sidebar_layout = array(         
    'default-sidebar' => array(
                            'value' => 'default-sidebar',
                            'thumbnail' => get_template_directory_uri() . '/images/default-sidebar.png'
                        ),
    'left-sidebar' => array(
                            'value' => 'left-sidebar',
                            'thumbnail' => get_template_directory_uri() . '/images/left-sidebar.png'
                        ),
    'right-sidebar' => array(
                            'value' => 'right-sidebar',
                            'thumbnail' => get_template_directory_uri() . '/images/right-sidebar.png'
                        )
    );

function preschool_and_kindergarten_pro_sidebar_layout_callback(){
    
    global $post , $preschool_and_kindergarten_pro_sidebar_layout;
    wp_nonce_field( basename( __FILE__ ), 'preschool_and_kindergarten_pro_sidebar_layout_nonce' ); 
    
    $sidebars = preschool_and_kindergarten_pro_get_dynamnic_sidebar( true, true, true );
    $sidebar  = get_post_meta( $post->ID, '_preschool_and_kindergarten_pro_sidebar', true );
?>
<table class="form-table page-meta-box">
    <tr>
        <td colspan="4"><em class="f13"><?php _e( 'Choose Sidebar Template', 'preschool-and-kindergarten-pro' ); ?></em></td>
    </tr>

    <tr>
        <td>
        <?php  
            foreach( $preschool_and_kindergarten_pro_sidebar_layout as $field ){  
                $layout = get_post_meta( $post->ID, '_preschool_and_kindergarten_pro_sidebar_layout', true ); ?>

            <div class="hide-radio radio-image-wrapper" style="float:left; margin-right:30px;">
                <input id="<?php echo esc_attr( $field['value'] ); ?>" type="radio" name="preschool_and_kindergarten_pro_sidebar_layout" value="<?php echo esc_attr( $field['value'] ); ?>" <?php checked( $field['value'], $layout ); if( empty( $layout ) ){ checked( $field['value'], 'default-sidebar' ); }?>/>
                <label class="description" for="<?php echo esc_attr( $field['value'] ); ?>">
                    <img src="<?php echo esc_url( $field['thumbnail'] ); ?>" alt="<?php echo esc_attr( $field['value'] ); ?>" />
                </label>
            </div>
            <?php } // end foreach 
            ?>
            <div class="clear"></div>
        </td>
    </tr>
    
    <tr>
        <td colspan="3"><em class="f13"><?php esc_html_e('Choose Sidebar', 'preschool-and-kindergarten-pro'); ?></em></td>
    </tr>
    
    <tr>
        <td>
            <select name="preschool_and_kindergarten_pro_sidebar">
            <?php 
                foreach( $sidebars as $k => $v ){ ?>
                    <option value="<?php echo esc_attr( $k ); ?>" <?php selected( $sidebar, $k ); if( empty( $sidebar ) && $k == 'default-sidebar' ){ echo "selected='selected'";}?> ><?php echo esc_html( $v ); ?></option>
                <?php }
            ?>
            </select>
        </td>    
    </tr>
    
    <tr>
        <td><em class="f13"><?php printf( esc_html__( 'You can set up the sidebar content from %s', 'preschool-and-kindergarten-pro' ), '<a href="'. esc_url( admin_url( 'widgets.php' ) ) .'">here</a>' ); ?></em></td>
    </tr>
    
</table>
<?php        
}

/**
 * save the custom metabox data
 * @hooked to save_post hook
 */
function preschool_and_kindergarten_pro_save_sidebar_layout( $post_id ) { 
    
    global $preschool_and_kindergarten_pro_sidebar_layout, $post; 

    // Verify the nonce before proceeding.
    if ( !isset( $_POST[ 'preschool_and_kindergarten_pro_sidebar_layout_nonce' ] ) || !wp_verify_nonce( $_POST[ 'preschool_and_kindergarten_pro_sidebar_layout_nonce' ], basename( __FILE__ ) ) )
        return;

    // Stop WP from clearing custom fields on autosave
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )  
        return;
        
    if ('page' == $_POST['post_type']) {  
        if ( !current_user_can( 'edit_page', $post_id ) )  
            return $post_id;  
    } elseif ( !current_user_can( 'edit_post', $post_id ) ) {  
            return $post_id;  
    }  
    
    // Make sure that it is set.
	if ( ! isset( $_POST['preschool_and_kindergarten_pro_sidebar'] ) ) {
		return;
	}
    
    foreach( $preschool_and_kindergarten_pro_sidebar_layout as $field ){  
        //Execute this saving function
        $old = get_post_meta( $post_id, '_preschool_and_kindergarten_pro_sidebar_layout', true ); 
        $new = sanitize_text_field( $_POST['preschool_and_kindergarten_pro_sidebar_layout'] );
        if ( $new && $new != $old ){  
            update_post_meta( $post_id, '_preschool_and_kindergarten_pro_sidebar_layout', $new );  
        } elseif( '' == $new && $old ){  
            delete_post_meta( $post_id, '_preschool_and_kindergarten_pro_sidebar_layout', $old );  
        } 
    } // end foreach
    
    // Sanitize user input.
	$sidebar = sanitize_text_field( $_POST['preschool_and_kindergarten_pro_sidebar'] );

	// Update the meta field in the database.
	update_post_meta( $post_id, '_preschool_and_kindergarten_pro_sidebar', $sidebar );   
}
add_action( 'save_post', 'preschool_and_kindergarten_pro_save_sidebar_layout' ); 

/**
 * Callback for Additional Info for events
*/
function preschool_and_kindergarten_pro_event_details_callback(){

    global $post;
    wp_nonce_field( basename( __FILE__ ), 'preschool_and_kindergarten_pro_event_details_nonce' );
    
    $start_date = get_post_meta( $post->ID, '_preschool_and_kindergarten_pro_event_start_date', true );
    $end_date   = get_post_meta( $post->ID, '_preschool_and_kindergarten_pro_event_end_date', true );
   
   
   ?>
    <div class="clearfix">
        <label class="bold-label float-left input_label" for="preschool_and_kindergarten_pro_event_start_date"><?php _e( 'Start Date', 'preschool-and-kindergarten-pro' ); ?></label>
        <div class="below_row_input float-left"><input type="text" id="preschool_and_kindergarten_pro_event_start_date" 
         name="preschool_and_kindergarten_pro_event_start_date" value="<?php echo esc_attr( $start_date ); ?>" /></div>
    </div>

    <div class="clearfix">
        <label class="bold-label float-left input_label" for="preschool_and_kindergarten_pro_event_end_date"><?php _e( 'End Date', 'preschool-and-kindergarten-pro' ); ?></label>
        <div class="below_row_input float-left"><input type="text" id="preschool_and_kindergarten_pro_event_end_date" name="preschool_and_kindergarten_pro_event_end_date" value="<?php echo esc_attr( $end_date ); ?>" /></div>
    </div>

    
    <?php
}

function preschool_and_kindergarten_pro_save_event_details( $post_id ){
    // Check if our nonce is set.
    if ( ! isset( $_POST['preschool_and_kindergarten_pro_event_details_nonce'] ) || ! wp_verify_nonce( $_POST['preschool_and_kindergarten_pro_event_details_nonce'], basename( __FILE__ ) ) ) {
        return;
    }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
        if ( ! current_user_can( 'edit_page', $post_id ) ) return;      
    } else {
        if ( ! current_user_can( 'edit_post', $post_id ) ) return;
    }
    
    if ( ! isset( $_POST['preschool_and_kindergarten_pro_event_start_date'] ) 
        && ! isset( $_POST['preschool_and_kindergarten_pro_event_end_date'] ) ) {
        return;
    }
    
    // Sanitize user input.
    $start_date = esc_html( $_POST['preschool_and_kindergarten_pro_event_start_date'] ); 
    $end_date   = esc_html( $_POST['preschool_and_kindergarten_pro_event_end_date'] );       
    
 
    // Update the meta field in the database.
    update_post_meta( $post_id, '_preschool_and_kindergarten_pro_event_start_date', $start_date );
    update_post_meta( $post_id, '_preschool_and_kindergarten_pro_event_end_date', $end_date );
    
}
add_action( 'save_post', 'preschool_and_kindergarten_pro_save_event_details' );