<?php
if( is_rara_toolkit_activated() ) :
	global $post;
	$rttk_setting = get_post_meta( $post->ID, '_rttk_setting', true );

  /**
    * Get testimonial post type fields
    *
    * @return array of default fields
    */
    function rttk_get_testimonial_fields_array() {

        $fields = array(
            'position' => 
                    array( 
                        'name'          =>'Position',
                        'key'           =>'position',
                        'class'         =>'',    
                        'id'            =>'position',
                        'type'          =>'text'
                        ),
            'company' => 
                    array( 
                        'name'          =>'Company',
                        'key'           =>'company',
                        'class'         =>'',    
                        'id'            =>'company',
                        'type'          =>'text'
                        ),
            );
        $fields = apply_filters( 'rttk_get_testimonial_fields_array', $fields );
        return $fields;
    }
    endif;
$testimonial_fields = rttk_get_testimonial_fields_array();
foreach ($testimonial_fields as $key => $value) { ?>
    <div class="testimonial-info">
        <label for="<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $value['name'].':' ); ?></label>
        <input type="<?php echo esc_attr( $value['type'] );?>" class="<?php echo esc_attr( $value['class'] );?>" name="rttk_setting[testimonial][<?php echo esc_attr( $key );?>]" id="<?php echo esc_attr( $value['id'] );?>" value="<?php echo isset($rttk_setting['testimonial'][$key]) ? esc_attr( $rttk_setting['testimonial'][$key] ): ''; ?>">
    </div>
<?php
}
