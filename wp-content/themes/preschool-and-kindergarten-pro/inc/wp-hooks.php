<?php 
/**
 * WordPress Hooks
 *
 * @package preschool_and_kindergarten_pro
 */

if( get_theme_mod( 'ed_adminbar' ) ) add_filter( 'show_admin_bar', '__return_false' );

/**
 * @see preschool_and_kindergarten_pro_setup
*/
add_action( 'after_setup_theme', 'preschool_and_kindergarten_pro_setup' );

/**
 * @see preschool_and_kindergarten_pro_content_width
*/

add_action( 'after_setup_theme', 'preschool_and_kindergarten_pro_content_width', 0 );

add_action( 'template_redirect', 'preschool_and_kindergarten_pro_template_redirect_content_width' );

/**
 * @see preschool_and_kindergarten_pro_scripts
*/

add_action( 'wp_enqueue_scripts', 'preschool_and_kindergarten_pro_scripts' );
add_action( 'admin_enqueue_scripts', 'preschool_and_kindergarten_pro_admin_scripts' );

/**
 * Enqueue Customizer Control Script for displaying widget in respective panels
 * @see preschool_and_kindergarten_pro_customizer_js
*/
add_action( 'customize_controls_enqueue_scripts', 'preschool_and_kindergarten_pro_customizer_js' );

/**
 * @see preschool_and_kindergarten_pro_scripts_styles
*/
add_action( 'wp_enqueue_scripts', 'preschool_and_kindergarten_pro_scripts_styles' );


/**
 * @see preschool_and_kindergarten_pro_body_classes
*/
add_filter( 'body_class', 'preschool_and_kindergarten_pro_body_classes' );

/**
 * Add Post Class
 * @see preschool_and_kindergarten_pro_post_classes
*/
add_filter( 'post_class', 'preschool_and_kindergarten_pro_post_classes' );


/**
 * @see preschool_and_kindergarten_pro_category_transient_flusher
*/
add_action( 'edit_category', 'preschool_and_kindergarten_pro_category_transient_flusher' );
add_action( 'save_post',     'preschool_and_kindergarten_pro_category_transient_flusher' );

/**
 * Custom JS
 * @see preschool_and_kindergarten_pro_custom_js
*/
add_action( 'wp_footer', 'preschool_and_kindergarten_pro_custom_js', 500 );
/**
 * Gooble Map for contact page templates
 * @see preschool_and_kindergarten_pro_google_map_cb
*/
add_action( 'wp_head', 'preschool_and_kindergarten_pro_google_map_cb' );

/**
 * Ajax Search
 * @see preschool_and_kindergarten_pro_ajax_search
*/
if( get_theme_mod( 'ed_ajax_search' ) ) {
    add_action( 'wp_ajax_preschool_and_kindergarten_pro_search', 'preschool_and_kindergarten_pro_ajax_search' );
    add_action( 'wp_ajax_nopriv_preschool_and_kindergarten_pro_search', 'preschool_and_kindergarten_pro_ajax_search' );
}

/**
 * Excluding post from blog of specific categories
 * 
 * @see preschool_and_kindergarten_pro_exclude_cat
 * @see preschool_and_kindergarten_pro_custom_category_widget
 * @see preschool_and_kindergarten_pro_exclude_posts_from_recentPostWidget_by_cat
 */
add_filter( 'pre_get_posts', 'preschool_and_kindergarten_pro_exclude_cat' );
add_filter( "widget_categories_args", "preschool_and_kindergarten_pro_custom_category_widget" );
add_filter( "widget_categories_dropdown_args", "preschool_and_kindergarten_pro_custom_category_widget" );

/**
* Archive Title
* @see preschool_and_kindergarten_pro_get_the_archive_title
*/
add_filter( 'get_the_archive_title', 'preschool_and_kindergarten_pro_get_the_archive_title' );

/**
* CPT Custom Query
* @see set_args_for_cpt
*/
add_action( 'pre_get_posts', 'set_args_for_cpt' );

/**
 * Add OG Tags in header
*/
add_action( 'wp_head', 'preschool_and_kindergarten_pro_og_header' );