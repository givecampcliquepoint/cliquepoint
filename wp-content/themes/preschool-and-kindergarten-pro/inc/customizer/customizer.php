<?php
/**
 * Preschool and Kindergarten Pro Theme Customizer.
 *
 * @package preschool_and_kindergarten_pro
 */


function preschool_and_kindergarten_pro_modify_sections( $wp_customize ){
	if ( version_compare( get_bloginfo('version'),'4.9', '>=') ) {
		$wp_customize->get_section( 'static_front_page' )->title = __( 'Static Front Page', 'preschool-and-kindergarten-pro' );
	}
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_modify_sections' );

/* Option list of all contact form */	
$preschool_and_kindergarten_pro_options_contact_form = array();
$preschool_and_kindergarten_pro_options_contact_form_obj = get_posts( 'posts_per_page=-1&post_type=wpcf7_contact_form' );
$preschool_and_kindergarten_pro_options_contact_form[''] = __( 'Choose Contact Form', 'preschool-and-kindergarten-pro' );
foreach ( $preschool_and_kindergarten_pro_options_contact_form_obj as $preschool_and_kindergarten_pro_contact_form ) {
	$preschool_and_kindergarten_pro_options_contact_form[$preschool_and_kindergarten_pro_contact_form->ID] = $preschool_and_kindergarten_pro_contact_form->post_title;
}

/* Option list of all categories */
$preschool_and_kindergarten_pro_args = array(
   'type'                     => 'post',
   'orderby'                  => 'name',
   'order'                    => 'ASC',
   'hide_empty'               => 1,
   'hierarchical'             => 1,
   'taxonomy'                 => 'category'
); 
$preschool_and_kindergarten_pro_option_categories = array();
$preschool_and_kindergarten_pro_category_lists = get_categories( $preschool_and_kindergarten_pro_args );
$preschool_and_kindergarten_pro_option_categories[''] = __( 'Choose Category', 'preschool-and-kindergarten-pro' );
foreach( $preschool_and_kindergarten_pro_category_lists as $preschool_and_kindergarten_pro_category ){
    $preschool_and_kindergarten_pro_option_categories[$preschool_and_kindergarten_pro_category->term_id] = $preschool_and_kindergarten_pro_category->name;
}
$preschool_and_kindergarten_pro_panels       = array( 'header', 'slider', 'general', 'home', 'about', 'services', 'contact', 'typography', 'custom', 'cpt' );
$preschool_and_kindergarten_pro_sections     = array( 'info', 'styling', 'sidebar', 'footer' );

$preschool_and_kindergarten_pro_sub_sections = array(
	'header'     => array( 'layout', 'misc', 'archive' ),
	'slider'     => array( 'options', 'content' ),
	'general'    => array( 'breadcrumb', 'blog', 'post-page', 'post-meta', 'share', 'misc', 'performance' ),
	'home'       => array( 'about', 'activities', 'subscription', 'features', 'events', 'promotional', 'course', 'team', 'testimonial', 'gallery', 'blog', 'CTA', 'contact', 'sort' ),
	'about'      => array( 'video', 'subscription' ),
	'services'   => array( 'facilities', 'promotional' ),
	'contact'    => array( 'google', 'contact-details' ),
	'typography' => array( 'body', 'site-title', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6' ),
	'cpt'        => array( 'team', 'course', 'event', 'testimonial' ),
);

foreach( $preschool_and_kindergarten_pro_sections as $s ){
    require get_template_directory() . '/inc/customizer/sections/' . $s . '.php';
}

foreach( $preschool_and_kindergarten_pro_panels as $p ){
   require get_template_directory() . '/inc/customizer/panels/' . $p . '.php';
}

foreach( $preschool_and_kindergarten_pro_sub_sections as $k => $v ){
    foreach( $v as $w ){        
        require get_template_directory() . '/inc/customizer/panels/' . $k . '/' . $w . '.php';
    }
}

/**
 * Sanitization Functions
*/
require get_template_directory() . '/inc/customizer/sanitization-functions.php';

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function preschool_and_kindergarten_pro_customize_preview_js() {
	// Use minified libraries if SCRIPT_DEBUG is false
    $build  = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '/build' : '';
    $suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
	wp_enqueue_script( 'preschool_and_kindergarten_pro_customizer', get_template_directory_uri() . '/js' . $build . '/customizer' . $suffix . '.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'preschool_and_kindergarten_pro_customize_preview_js' );

/**
 * Filter for rara repeater conrol labels.
*/
add_filter( 'rara/rara-repeater/l10n', 'preschool_and_kindergarten_pro_rara_repeater_string' );
function preschool_and_kindergarten_pro_rara_repeater_string(){
    return $translation_strings = array(
			'no-image-selected' => __( 'No image selected', 'preschool-and-kindergarten-pro' ),
			'remove'            => __( 'Remove', 'preschool-and-kindergarten-pro' ),            
            'select-image'      => __( 'Select Image', 'preschool-and-kindergarten-pro' ),			
            'select-category'   => __( 'Select Category', 'preschool-and-kindergarten-pro' ),            
			'latest-posts'      => __( 'Latest Posts', 'preschool-and-kindergarten-pro' ),
			'delete'            => __( 'Delete', 'preschool-and-kindergarten-pro' ),
			'close'             => __( 'Close', 'preschool-and-kindergarten-pro' ),			
		);
}

/**
 * Filter for Repeater control labels.
*/
add_filter( 'rara/repeater/l10n', 'preschool_and_kindergarten_pro_repeater_strings' );
function preschool_and_kindergarten_pro_repeater_strings(){
    return $translation_strings = array(
			'row'               => esc_attr__( 'row', 'preschool-and-kindergarten-pro' ),
			'add-new'           => esc_attr__( 'Add new', 'preschool-and-kindergarten-pro' ),
			'select-page'       => esc_attr__( 'Select a Page', 'preschool-and-kindergarten-pro' ),
			'limit-rows'        => esc_attr__( 'Limit: %s rows', 'preschool-and-kindergarten-pro' ),
			'hex-value'         => esc_attr__( 'Hex Value', 'preschool-and-kindergarten-pro' ),
			'no-image-selected' => esc_attr__( 'No Image Selected', 'preschool-and-kindergarten-pro' ),
			'remove'            => esc_attr__( 'Remove', 'preschool-and-kindergarten-pro' ),
			'add-image'         => esc_attr__( 'Add Image', 'preschool-and-kindergarten-pro' ),
			'change-image'      => esc_attr__( 'Change Image', 'preschool-and-kindergarten-pro' ),
			'no-file-selected'  => esc_attr__( 'No File Selected', 'preschool-and-kindergarten-pro' ),
			'add-file'          => esc_attr__( 'Add File', 'preschool-and-kindergarten-pro' ),
			'change-file'       => esc_attr__( 'Change File', 'preschool-and-kindergarten-pro' ),
    );
}

/**
 * Filter for google font variants labels
*/
add_filter( 'rara-fonts/fonts/font_variants', 'preschool_and_kindergarten_pro_fonts_variants' );
function preschool_and_kindergarten_pro_fonts_variants(){
    return $font_variant = array(
			'100'       => esc_attr__( 'Ultra-Light 100', 'preschool-and-kindergarten-pro' ),
			'100italic' => esc_attr__( 'Ultra-Light 100 Italic', 'preschool-and-kindergarten-pro' ),
			'200'       => esc_attr__( 'Light 200', 'preschool-and-kindergarten-pro' ),
			'200italic' => esc_attr__( 'Light 200 Italic', 'preschool-and-kindergarten-pro' ),
			'300'       => esc_attr__( 'Book 300', 'preschool-and-kindergarten-pro' ),
			'300italic' => esc_attr__( 'Book 300 Italic', 'preschool-and-kindergarten-pro' ),
			'regular'   => esc_attr__( 'Normal 400', 'preschool-and-kindergarten-pro' ),
			'italic'    => esc_attr__( 'Normal 400 Italic', 'preschool-and-kindergarten-pro' ),
			'500'       => esc_attr__( 'Medium 500', 'preschool-and-kindergarten-pro' ),
			'500italic' => esc_attr__( 'Medium 500 Italic', 'preschool-and-kindergarten-pro' ),
			'600'       => esc_attr__( 'Semi-Bold 600', 'preschool-and-kindergarten-pro' ),
			'600italic' => esc_attr__( 'Semi-Bold 600 Italic', 'preschool-and-kindergarten-pro' ),
			'700'       => esc_attr__( 'Bold 700', 'preschool-and-kindergarten-pro' ),
			'700italic' => esc_attr__( 'Bold 700 Italic', 'preschool-and-kindergarten-pro' ),
			'800'       => esc_attr__( 'Extra-Bold 800', 'preschool-and-kindergarten-pro' ),
			'800italic' => esc_attr__( 'Extra-Bold 800 Italic', 'preschool-and-kindergarten-pro' ),
			'900'       => esc_attr__( 'Ultra-Bold 900', 'preschool-and-kindergarten-pro' ),
			'900italic' => esc_attr__( 'Ultra-Bold 900 Italic', 'preschool-and-kindergarten-pro' ),
		);
}

/**
 * Filter for standard font
*/
add_filter( 'rara-fonts/fonts/standard_fonts', 'preschool_and_kindergarten_pro_stantard_fonts' );
function preschool_and_kindergarten_pro_stantard_fonts(){
    return $standard_font = array(
		'serif'      => array(
			'label' => _x( 'Serif', 'font style', 'preschool-and-kindergarten-pro' ),
			'stack' => 'serif',
		),
		'sans-serif' => array(
			'label' => _x( 'Sans Serif', 'font style', 'preschool-and-kindergarten-pro' ),
			'stack' => 'sans-serif',
		),
		'monospace'  => array(
			'label' => _x( 'Monospace', 'font style', 'preschool-and-kindergarten-pro' ),
			'stack' => 'monospace',
		),
    );
}

/**
 * Filter for typography strings
*/
add_filter( 'rara-typography-control/il8n/strings', 'preschool_and_kindergarten_pro_typography_strings' );
function preschool_and_kindergarten_pro_typography_strings(){
    return $typo_string = array(
			'on'                 => esc_attr__( 'ON', 'preschool-and-kindergarten-pro' ),
			'off'                => esc_attr__( 'OFF', 'preschool-and-kindergarten-pro' ),
			'all'                => esc_attr__( 'All', 'preschool-and-kindergarten-pro' ),
			'cyrillic'           => esc_attr__( 'Cyrillic', 'preschool-and-kindergarten-pro' ),
			'cyrillic-ext'       => esc_attr__( 'Cyrillic Extended', 'preschool-and-kindergarten-pro' ),
			'devanagari'         => esc_attr__( 'Devanagari', 'preschool-and-kindergarten-pro' ),
			'greek'              => esc_attr__( 'Greek', 'preschool-and-kindergarten-pro' ),
			'greek-ext'          => esc_attr__( 'Greek Extended', 'preschool-and-kindergarten-pro' ),
			'khmer'              => esc_attr__( 'Khmer', 'preschool-and-kindergarten-pro' ),
			'latin'              => esc_attr__( 'Latin', 'preschool-and-kindergarten-pro' ),
			'latin-ext'          => esc_attr__( 'Latin Extended', 'preschool-and-kindergarten-pro' ),
			'vietnamese'         => esc_attr__( 'Vietnamese', 'preschool-and-kindergarten-pro' ),
			'hebrew'             => esc_attr__( 'Hebrew', 'preschool-and-kindergarten-pro' ),
			'arabic'             => esc_attr__( 'Arabic', 'preschool-and-kindergarten-pro' ),
			'bengali'            => esc_attr__( 'Bengali', 'preschool-and-kindergarten-pro' ),
			'gujarati'           => esc_attr__( 'Gujarati', 'preschool-and-kindergarten-pro' ),
			'tamil'              => esc_attr__( 'Tamil', 'preschool-and-kindergarten-pro' ),
			'telugu'             => esc_attr__( 'Telugu', 'preschool-and-kindergarten-pro' ),
			'thai'               => esc_attr__( 'Thai', 'preschool-and-kindergarten-pro' ),
			'serif'              => _x( 'Serif', 'font style', 'preschool-and-kindergarten-pro' ),
			'sans-serif'         => _x( 'Sans Serif', 'font style', 'preschool-and-kindergarten-pro' ),
			'monospace'          => _x( 'Monospace', 'font style', 'preschool-and-kindergarten-pro' ),
			'font-family'        => esc_attr__( 'Font Family', 'preschool-and-kindergarten-pro' ),
			'font-size'          => esc_attr__( 'Font Size', 'preschool-and-kindergarten-pro' ),
			'font-weight'        => esc_attr__( 'Font Weight', 'preschool-and-kindergarten-pro' ),
			'line-height'        => esc_attr__( 'Line Height', 'preschool-and-kindergarten-pro' ),
			'font-style'         => esc_attr__( 'Font Style', 'preschool-and-kindergarten-pro' ),
			'letter-spacing'     => esc_attr__( 'Letter Spacing', 'preschool-and-kindergarten-pro' ),
			'text-align'         => esc_attr__( 'Text Align', 'preschool-and-kindergarten-pro' ),
			'text-transform'     => esc_attr__( 'Text Transform', 'preschool-and-kindergarten-pro' ),
			'none'               => esc_attr__( 'None', 'preschool-and-kindergarten-pro' ),
			'uppercase'          => esc_attr__( 'Uppercase', 'preschool-and-kindergarten-pro' ),
			'lowercase'          => esc_attr__( 'Lowercase', 'preschool-and-kindergarten-pro' ),
			'top'                => esc_attr__( 'Top', 'preschool-and-kindergarten-pro' ),
			'bottom'             => esc_attr__( 'Bottom', 'preschool-and-kindergarten-pro' ),
			'left'               => esc_attr__( 'Left', 'preschool-and-kindergarten-pro' ),
			'right'              => esc_attr__( 'Right', 'preschool-and-kindergarten-pro' ),
			'center'             => esc_attr__( 'Center', 'preschool-and-kindergarten-pro' ),
			'justify'            => esc_attr__( 'Justify', 'preschool-and-kindergarten-pro' ),
			'color'              => esc_attr__( 'Color', 'preschool-and-kindergarten-pro' ),
			'select-font-family' => esc_attr__( 'Select a font-family', 'preschool-and-kindergarten-pro' ),
			'variant'            => esc_attr__( 'Variant', 'preschool-and-kindergarten-pro' ),
			'style'              => esc_attr__( 'Style', 'preschool-and-kindergarten-pro' ),
			'size'               => esc_attr__( 'Size', 'preschool-and-kindergarten-pro' ),
			'height'             => esc_attr__( 'Height', 'preschool-and-kindergarten-pro' ),
			'spacing'            => esc_attr__( 'Spacing', 'preschool-and-kindergarten-pro' ),
			'ultra-light'        => esc_attr__( 'Ultra-Light 100', 'preschool-and-kindergarten-pro' ),
			'ultra-light-italic' => esc_attr__( 'Ultra-Light 100 Italic', 'preschool-and-kindergarten-pro' ),
			'light'              => esc_attr__( 'Light 200', 'preschool-and-kindergarten-pro' ),
			'light-italic'       => esc_attr__( 'Light 200 Italic', 'preschool-and-kindergarten-pro' ),
			'book'               => esc_attr__( 'Book 300', 'preschool-and-kindergarten-pro' ),
			'book-italic'        => esc_attr__( 'Book 300 Italic', 'preschool-and-kindergarten-pro' ),
			'regular'            => esc_attr__( 'Normal 400', 'preschool-and-kindergarten-pro' ),
			'italic'             => esc_attr__( 'Normal 400 Italic', 'preschool-and-kindergarten-pro' ),
			'medium'             => esc_attr__( 'Medium 500', 'preschool-and-kindergarten-pro' ),
			'medium-italic'      => esc_attr__( 'Medium 500 Italic', 'preschool-and-kindergarten-pro' ),
			'semi-bold'          => esc_attr__( 'Semi-Bold 600', 'preschool-and-kindergarten-pro' ),
			'semi-bold-italic'   => esc_attr__( 'Semi-Bold 600 Italic', 'preschool-and-kindergarten-pro' ),
			'bold'               => esc_attr__( 'Bold 700', 'preschool-and-kindergarten-pro' ),
			'bold-italic'        => esc_attr__( 'Bold 700 Italic', 'preschool-and-kindergarten-pro' ),
			'extra-bold'         => esc_attr__( 'Extra-Bold 800', 'preschool-and-kindergarten-pro' ),
			'extra-bold-italic'  => esc_attr__( 'Extra-Bold 800 Italic', 'preschool-and-kindergarten-pro' ),
			'ultra-bold'         => esc_attr__( 'Ultra-Bold 900', 'preschool-and-kindergarten-pro' ),
			'ultra-bold-italic'  => esc_attr__( 'Ultra-Bold 900 Italic', 'preschool-and-kindergarten-pro' ),
			'invalid-value'      => esc_attr__( 'Invalid Value', 'preschool-and-kindergarten-pro' ),
		);
}