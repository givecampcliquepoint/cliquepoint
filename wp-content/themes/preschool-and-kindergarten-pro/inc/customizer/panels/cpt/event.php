<?php
/**
 * Event Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_event_page( $wp_customize ) {
    
    $wp_customize->add_section( 'cpt_event_page_setting', array(
        'title'      => __( 'Event Page Settings', 'preschool-and-kindergarten-pro' ),
        'panel'      => 'custom_post_type_page_settings',
        'priority'   => 20,
    ) );

    if( is_rara_toolkit_activated() ){
        /** Event Order  */
        $wp_customize->add_setting(
            'cpt_event_page_order_type',
            array(
                'default'           => 'date',
                'sanitize_callback' => 'esc_attr',
            )
        );
       
        /** Event Order */
        $wp_customize->add_control( 
            new Rara_Controls_Radio_Buttonset_Control( 
                $wp_customize,
                'cpt_event_page_order_type', 
                array(
                    'type'       => 'radio',
                    'label'      => __( 'Event Order', 'preschool-and-kindergarten-pro' ),
                    'help'       => __( 'Choose event order for event page.', 'preschool-and-kindergarten-pro' ),
                    'section'    => 'cpt_event_page_setting',
                    'default'    => 'date',
                    'choices'    => array(
                        'date'       => __( 'Event Date', 'preschool-and-kindergarten-pro' ),
                        'menu_order' => __( 'Menu Order', 'preschool-and-kindergarten-pro' ),
                    )
                )  
            )
        );

         /** Event Archive Slug */
        $wp_customize->add_setting(
            'rename_event_slug',
            array(
                'default'           => '',
                'sanitize_callback' => 'sanitize_text_field',
            )
        );
            
        $wp_customize->add_control(
            'rename_event_slug',
            array(
                'label'   => __( 'Rename Event Slug', 'preschool-and-kindergarten-pro' ),
                'description' => sprintf( __( 'You can rename event slug from here, after changing slug you have to save changes on permalinks. Go to %1$sPermalinks%2$s.', 'preschool-and-kindergarten-pro' ), '<a href="' . admin_url( 'options-permalink.php' ) . '" target="_blank">','</a>' ),
                'section' => 'cpt_event_page_setting',
                'type'    => 'text',
            )
        );
    }else{
        $wp_customize->add_setting(
            'cpt_event_page_order_note',
            array(
                'sanitize_callback' => 'wp_kses_post'
            )
        );
    
        $wp_customize->add_control(
            new Rara_Controls_Info_Text( 
                $wp_customize,
                'cpt_event_page_order_note',
                array(
                    'section'      => 'cpt_event_page_setting', 
                    'description' => sprintf( __( 'Please install/activate the %1$sRara Theme Tool Kit Pro%2$s which you have received along with this theme for setting related with Testimonial post type.', 'preschool-and-kindergarten-pro' ), '<a href="' . admin_url( 'themes.php?page=tgmpa-install-plugins' ) . '" target="_blank">', '</a>' ),
                )
            )
        );
    }
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_event_page' );

