<?php
/**
 * Custom Options
 *
 * @package preschool_and_kindergarten_pro
 */
 
function preschool_and_kindergarten_pro_customize_register_custom( $wp_customize ) {
    
    /** Custom Code */
    $wp_customize->add_panel( 'custom_codes', array(
        'title'      => __( 'Custom Codes', 'preschool-and-kindergarten-pro' ),
        'priority'   => 125,
        'capability' => 'edit_theme_options',
    ) );
    
    $wp_customize->add_section( 'custom_section', array(
        'title'    => __( 'Custom Scripts', 'preschool-and-kindergarten-pro' ),
        'priority' => 10,
        'panel'    => 'custom_codes',
    ) );
    
    /** Custom Script */
    $wp_customize->add_setting(
        'custom_script',
        array(
            'default'           => '',
            'sanitize_callback' => 'wp_strip_all_tags',
        )
    );
    
    $wp_customize->add_control(
	   'custom_script',
		array(
			'section'	  => 'custom_section',
			'label'		  => __( 'Custom Script', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Put the script like anlytics or any other here.', 'preschool-and-kindergarten-pro' ),
			'type'        => 'textarea',
		)		
	);
    
    if ( version_compare( $GLOBALS['wp_version'], '4.7', '>=' ) ){
        $wp_customize->get_section( 'custom_css' )->panel = 'custom_codes';
    }
    
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_custom' );