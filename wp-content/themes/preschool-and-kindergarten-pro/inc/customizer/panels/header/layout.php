<?php
/**
 * Header Layouts
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_header_layout( $wp_customize ) {
    
    $wp_customize->add_section( 'header_layout_setting', array(
        'title'    => __( 'Layout Settings', 'preschool-and-kindergarten-pro' ),
        'priority' => 21,
        'panel'    => 'header_setting',
    ) );
    
    /** Header Layout */
    $wp_customize->add_setting( 'header_layout', array(
        'default'           => 'one',
        'sanitize_callback' => 'esc_attr'
    ) );
    
    $wp_customize->add_control(
		new Rara_Controls_Radio_Image_Control(
			$wp_customize,
			'header_layout',
			array(
				'section'		=> 'header_layout_setting',
				'label'			=> __( 'Header Layout', 'preschool-and-kindergarten-pro' ),
				'description'	=> __( 'Choose the layout of header for your site.', 'preschool-and-kindergarten-pro' ),
				'choices'		=> array(
					'one'    => get_template_directory_uri() . '/images/layouts/header-1.png',
                    'two'    => get_template_directory_uri() . '/images/layouts/header-2.png',
                    'three'  => get_template_directory_uri() . '/images/layouts/header-3.png',
                    'four'   => get_template_directory_uri() . '/images/layouts/header-4.png',
                    'five'   => get_template_directory_uri() . '/images/layouts/header-5.png',
                    'six'    => get_template_directory_uri() . '/images/layouts/header-6.png',
                    'seven'  => get_template_directory_uri() . '/images/layouts/header-7.png',
                    'eight'  => get_template_directory_uri() . '/images/layouts/header-8.png',
				)
			)
		)
	);
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_header_layout' );