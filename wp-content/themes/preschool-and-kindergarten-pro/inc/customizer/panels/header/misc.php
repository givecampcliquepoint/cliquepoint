<?php
/**
 * Header Miscellaneous Options
 *
 * @package Preschool and kindergarten pro
 */

function preschool_and_kindergarten_pro_customize_register_header_misc( $wp_customize ) {
    
    $wp_customize->add_section( 'header_misc_setting', array(
        'title'    => __( 'Misc Settings', 'preschool-and-kindergarten-pro' ),
        'priority' => 30,
        'panel'    => 'header_setting',
    ) );

    /** Section Menu */
    $wp_customize->add_setting(
        'ed_section_menu',
        array(
            'default'           => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'ed_section_menu',
            array(
                'section'     => 'header_misc_setting',
                'label'       => __( 'Section Menu', 'preschool-and-kindergarten-pro' ),
                'description' => __( 'Enable to make one page scrolling home page.', 'preschool-and-kindergarten-pro' ),
            )
        )
    );

    /** Enable/Disable Home Link */
    $wp_customize->add_setting(
        'ed_home_link',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'ed_home_link',
            array(
                'section'     => 'header_misc_setting',
                'label'       => __( 'Home Link', 'preschool-and-kindergarten-pro' ),
                'description' => __( 'Enable to display "Home" link in section menu.', 'preschool-and-kindergarten-pro' ),
                'active_callback' => 'preschool_and_kindergarten_pro_header_ac'
            )
        )
    );
    
    /** Enable/Disable Sticky Header */
    $wp_customize->add_setting(
        'ed_sticky_header',
        array(
            'default'           => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
		new Rara_Controls_Toggle_Control( 
			$wp_customize,
			'ed_sticky_header',
			array(
				'section'	  => 'header_misc_setting',
				'label'		  => __( 'Sticky Header', 'preschool-and-kindergarten-pro' ),
				'description' => __( 'Enable to make header sticky.', 'preschool-and-kindergarten-pro' ),
			)
		)
	);
    
    /** Enable/Disable Search Form */
    $wp_customize->add_setting(
        'ed_search',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
		new Rara_Controls_Toggle_Control( 
			$wp_customize,
			'ed_search',
			array(
				'section'	  => 'header_misc_setting',
				'label'		  => __( 'Search Form', 'preschool-and-kindergarten-pro' ),
				'description' => __( 'Enable to show search form in header.', 'preschool-and-kindergarten-pro' ),
			)
		)
	);
    
    /** Phone Label  */
    $wp_customize->add_setting(
        'phone_label',
        array(
            'default'           => __( 'Call Us Anytime', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'phone_label',
        array(
            'label'       => __( 'Phone Label', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Add phone label in header. This label will be visibile in header four and five.', 'preschool-and-kindergarten-pro' ),
            'section'    => 'header_misc_setting',
            'type'       => 'text',
        )
    );

    /** Phone Number  */
    $wp_customize->add_setting(
        'preschool_and_kindergarten_phone',
        array(
            'default'           => __( '123 456 1234', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'preschool_and_kindergarten_phone',
        array(
            'label'   => __( 'Phone Number', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Add phone no. in header.. You can add multiple phone number seperating with comma', 'preschool-and-kindergarten-pro' ),
            'section' => 'header_misc_setting',
            'type'    => 'text',
        )
    );

    /** Email Label  */
    $wp_customize->add_setting(
        'email_label',
        array(
            'default'           => __( 'Mail Us', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'email_label',
        array(
            'label'       => __( 'Email Label', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Add Email label in header. This label will be visibile in header four and five.', 'preschool-and-kindergarten-pro' ),
            'section'    => 'header_misc_setting',
            'type'       => 'text',
        )
    );

    /** Email Address */
    $wp_customize->add_setting(
        'preschool_and_kindergarten_email_address',
        array(
            'default'           => __( 'contact@kinderschool.com,info@test.com,support@test.com', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    
    $wp_customize->add_control(
        'preschool_and_kindergarten_email_address',
        array(
            'label'   => __( 'Email Address', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Add email address in header. You can add multiple emails by seperating it with comma', 'preschool-and-kindergarten-pro' ),
            'section' => 'header_misc_setting',
            'type'    => 'textarea',
        )
    );

    /** Opening time label  */
    $wp_customize->add_setting(
        'opening_time_label',
        array(
            'default'           => __( 'Opening Time', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'opening_time_label',
        array(
            'label'   => __( 'Opening Time Label', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Add Opening Time label in header. This label will be visibile in header four.', 'preschool-and-kindergarten-pro' ),
            'section' => 'header_misc_setting',
            'type'    => 'text',
        )
    );

    /** Opening time  */
    $wp_customize->add_setting(
        'opening_time',
        array(
            'default'           => '',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'opening_time',
        array(
            'label'   => __( 'Opening Time', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Add Opening Time in header. This will be visibile in header four.', 'preschool-and-kindergarten-pro' ),
            'section' => 'header_misc_setting',
            'type'    => 'text',
        )
    );

}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_header_misc' );

/**
 * Active Callback
*/
function preschool_and_kindergarten_pro_header_ac( $control ){
    
    $ed_sec_menu = $control->manager->get_setting( 'ed_section_menu' )->value();
    $control_id  = $control->id;
    
    if ( $control_id == 'ed_home_link' && $ed_sec_menu == true ) return true;
    if ( $control_id == 'ed_menu_label' && $ed_sec_menu == true ) return true;    
    
    return false;
}
