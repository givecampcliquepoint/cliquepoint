<?php
/**
 * CPT Archive Header Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_header_archive( $wp_customize ) {
    
    $wp_customize->add_section( 'header_archive_setting', array(
        'title'       => __( 'Archive Header Settings', 'preschool-and-kindergarten-pro' ),
        'description' => __( 'Settings for Archive page title for custom post type. You can change archive page title for  "Event", "Team", "Courses" and "Testimonial" from here.', 'preschool-and-kindergarten-pro' ),
        'priority'    => 35,
        'panel'       => 'header_setting',
    ) );
    
    if( is_rara_toolkit_activated() ){

    /** events Archive Title */
    $wp_customize->add_setting(
        'events_archive_title',
        array(
            'default'           => __( 'Events', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'events_archive_title',
        array(
            'label'       => __( 'Events Archive Title', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Leave blank to use default archive post title.', 'preschool-and-kindergarten-pro' ),
            'section'     => 'header_archive_setting',
            'type'        => 'text',
        )
    );
    
    /** Team Archive Title */
    $wp_customize->add_setting(
        'team_archive_title',
        array(
            'default'           => __( 'Teams', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'team_archive_title',
        array(
            'label'       => __( 'Team Archive Title', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Leave blank to use default archive post title.', 'preschool-and-kindergarten-pro' ),
            'section'     => 'header_archive_setting',
            'type'        => 'text',
        )
    );
    
    /** Testimonail Archive Title */
    $wp_customize->add_setting(
        'testimonial_archive_title',
        array(
            'default'           => __( 'Testimonials', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'testimonial_archive_title',
        array(
            'label'       => __( 'Testimonial Archive Title', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Leave blank to use default archive post title.', 'preschool-and-kindergarten-pro' ),
            'section'     => 'header_archive_setting',
            'type'        => 'text',
        )
    );

    /** Course Archive Title */
    $wp_customize->add_setting(
        'courses_archive_title',
        array(
            'default'           => __( 'Courses', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'courses_archive_title',
        array(
            'label'       => __( 'Courses Archive Title', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Leave blank to use default archive post title.', 'preschool-and-kindergarten-pro' ),
            'section'     => 'header_archive_setting',
            'type'        => 'text',
        )
    );
    
    }else{
        $wp_customize->add_setting(
            'archive_header_note',
            array(
                'sanitize_callback' => 'wp_kses_post'
            )
        );
    
        $wp_customize->add_control(
            new Rara_Controls_Info_Text( 
                $wp_customize,
                'archive_header_note',
                array(
                    'section' => 'header_archive_setting', 
                    'description' => sprintf( __( 'For setting Archive page title for custom post type. Please install/activate the %1$sRara Theme Tool Kit Pro%2$s which you have received along with this theme.', 'preschool-and-kindergarten-pro' ), '<a href="' . admin_url( 'themes.php?page=tgmpa-install-plugins' ) . '" target="_blank">', '</a>' ),
                )
            )
        );
    }    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_header_archive' );