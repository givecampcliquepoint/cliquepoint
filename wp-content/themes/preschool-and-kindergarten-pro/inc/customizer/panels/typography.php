<?php
/**
 * Typography Options 
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_typography( $wp_customize ) {
    
    $wp_customize->add_panel( 'typography_section', array(
        'title'          => __( 'Typography Settings', 'preschool-and-kindergarten-pro' ),
        'priority'       => 65,
        'capability'     => 'edit_theme_options',
    ) );
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_typography' );