<?php
/**
 * Services Page Theme Option.
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_services( $wp_customize ) {
    
    /** Services Page Settings */
    $wp_customize->add_panel( 
        'services_page_settings',
         array(
            'priority'    => 35,
            'capability'  => 'edit_theme_options',
            'title'       => __( 'Services Page Settings', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Customize Services Page Sections', 'preschool-and-kindergarten-pro' ),
        ) 
    );

}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_services' );