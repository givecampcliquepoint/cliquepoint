<?php
/**
 * Post Page Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_postpage( $wp_customize ) {

    $wp_customize->add_section( 'post_page_settings', array(
        'priority'   => 25,
        'capability' => 'edit_theme_options',
        'title'      => __( 'Post Page Settings', 'preschool-and-kindergarten-pro' ),
        'panel'    => 'general_setting',
    ) );
    
    /** Author Bio */
    $wp_customize->add_setting(
        'ed_bio',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
		new Rara_Controls_Toggle_Control( 
			$wp_customize,
			'ed_bio',
			array(
				'section'     => 'post_page_settings',
				'label'       => __( 'Show Author Bio', 'preschool-and-kindergarten-pro' ),
                'description' => __( 'Enable to show Author Bio in Single Post.', 'preschool-and-kindergarten-pro' ),
			)
		)
	);
    
    /** Featured Image */
    $wp_customize->add_setting(
        'ed_featured_image',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
		new Rara_Controls_Toggle_Control( 
			$wp_customize,
			'ed_featured_image',
			array(
				'section'     => 'post_page_settings',
				'label'       => __( 'Show Featured Image', 'preschool-and-kindergarten-pro' ),
                'description' => __( 'Enable to show Featured Image in Single Post/Page.', 'preschool-and-kindergarten-pro' ),
			)
		)
	);
    
    /** Comments */
    $wp_customize->add_setting(
        'ed_comments',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
		new Rara_Controls_Toggle_Control( 
			$wp_customize,
			'ed_comments',
			array(
				'section'     => 'post_page_settings',
				'label'       => __( 'Show Comments', 'preschool-and-kindergarten-pro' ),
                'description' => __( 'Enable to show Comments in Single Post/Page.', 'preschool-and-kindergarten-pro' ),
			)
		)
	);
    
    /** Highlight Author Comment */
    $wp_customize->add_setting(
        'ed_auth_comments',
        array(
            'default'           => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
		new Rara_Controls_Toggle_Control( 
			$wp_customize,
			'ed_auth_comments',
			array(
				'section'     => 'post_page_settings',
				'label'       => __( 'Highlight Author Comments', 'preschool-and-kindergarten-pro' ),
                'description' => __( 'Enable to higlight Author Comments.', 'preschool-and-kindergarten-pro' ),
			)
		)
	);
         
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_postpage' );

/**
 * Active Callback for Related Post Label
*/
function preschool_and_kindergarten_pro_related_post_label_show( $control ){
    $ed_related = $control->manager->get_setting( 'ed_related_post' )->value();
    
    if ( $ed_related ) return true;
    
    return false;
}