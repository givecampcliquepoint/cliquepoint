<?php
/**
 * Blog/Archive Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_blog( $wp_customize ) {

    $wp_customize->add_section( 'blog_page_settings', array(
        'priority'   => 15,
        'capability' => 'edit_theme_options',
        'title'      => __( 'Blog Page Settings', 'preschool-and-kindergarten-pro' ),
        'panel'    => 'general_setting',
    ) );

    /* Blog Layout */
    $wp_customize->add_setting(
        'blog_layout',
        array(
            'default'           => 'default',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
        'blog_layout',
        array(
            'label'       => __( 'Blog Page Layout', 'preschool-and-kindergarten-pro' ),
            'section'     => 'blog_page_settings',
            'type'        => 'radio',
	        'choices'     => array(
	            'default' => __( 'Default', 'preschool-and-kindergarten-pro' ),
	            'square'  => __( 'Square Image', 'preschool-and-kindergarten-pro' ),
	            'round'   => __( 'Round Image', 'preschool-and-kindergarten-pro' ),
	        )
        )
    );
    
    
    $wp_customize->add_setting(
		'exclude_categories', 
		array(
			'default' => '',
			'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_multiple_check',						
		)
	);

	$wp_customize->add_control(
		new Rara_Controls_MultiCheck_Control(
			$wp_customize,
			'exclude_categories',
			array(
				'section'     => 'blog_page_settings',
				'label'       => __( 'Exclude Categories', 'preschool-and-kindergarten-pro' ),
                'description' => __( 'Check multiple categories to exclude from blog and archive page.', 'preschool-and-kindergarten-pro' ),
				'choices'     => preschool_and_kindergarten_pro_get_categories( false )
			)
		)
	);
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_blog' );