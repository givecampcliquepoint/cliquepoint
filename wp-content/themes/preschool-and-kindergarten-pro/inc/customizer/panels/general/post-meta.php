<?php
/**
 * Post Meta Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_postmeta( $wp_customize ) {

    $wp_customize->add_section( 'post_meta_settings', array(
        'title'      => __( 'Post Meta Settings', 'preschool-and-kindergarten-pro' ),
        'priority'   => 20,
        'capability' => 'edit_theme_options',
        'panel'    => 'general_setting',
    ));
    
    /** Post Meta */
    $wp_customize->add_setting(
		'post_meta',
		array(
			'default'			=> array( 'author', 'date', 'cat', 'comment' ),
			'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select'
		)
	);

	$wp_customize->add_control(
		new Rara_Controls_Select_Control(
    		$wp_customize,
    		'post_meta',
    		array(
                'label'	      => __( 'Post Meta', 'preschool-and-kindergarten-pro' ),
                'description' => __( 'Post meta in single post page. You can rearrange the order you want.', 'preschool-and-kindergarten-pro' ),
    			'section'     => 'post_meta_settings',
    			'multiple'    => 5,
                'choices'     => array(    
                    'author'  => __( 'Author', 'preschool-and-kindergarten-pro' ),
                    'date'    => __( 'Date', 'preschool-and-kindergarten-pro' ),
                    'cat'     => __( 'Category', 'preschool-and-kindergarten-pro' ),
                    'comment' => __( 'Comment', 'preschool-and-kindergarten-pro' ),
                ),                	
     		)
		)
	);
    
    /** No. of Character */
    $wp_customize->add_setting( 'excerpt_char', array(
        'default'           => 200,
        'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select'
    ) );
    
    $wp_customize->add_control(
		new Rara_Controls_Slider_Control( 
			$wp_customize,
			'excerpt_char',
			array(
				'section'		=> 'post_meta_settings',
				'label'			=> __( 'Post Excerpt Character', 'preschool-and-kindergarten-pro' ),
				'choices'		=> array(
					'min' 	=> 10,
					'max' 	=> 500,
					'step'	=> 10,
				)
			)
		)
	);
    
    /** Read More Text */
    $wp_customize->add_setting(
        'readmore_text',
        array(
            'default'           => __( 'Read More', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
	   'readmore_text',
		array(
			'section'	  => 'post_meta_settings',
			'label'		  => __( 'Read More Text', 'preschool-and-kindergarten-pro' ),
			'type'        => 'text'
		)		
	);
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_postmeta' );