<?php
/**
 * Social Sharing Options
 *
 * @package preschool_and_kindergarten_pro
 */
 
function preschool_and_kindergarten_pro_customize_register_social_share( $wp_customize ) {
    
    $wp_customize->add_section( 'social_share_settings', array(
        'title'    => __( 'Social Sharing', 'preschool-and-kindergarten-pro' ),
        'priority' => 26,
        'panel'    => 'social_settings',
        'panel'    => 'general_setting',
    ) );
    
    /** Enable Social Sharing Buttons */
    $wp_customize->add_setting(
        'ed_social_sharing',
        array(
            'default'           => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
		new Rara_Controls_Toggle_Control( 
			$wp_customize,
			'ed_social_sharing',
			array(
				'section'     => 'social_share_settings',
				'label'       => __( 'Enable Social Sharing Buttons', 'preschool-and-kindergarten-pro' ),
                'description' => __( 'Enable or disable social sharing buttons on single posts.', 'preschool-and-kindergarten-pro' ),
			)
		)
	);
    
    /** Enable Social Sharing Floating Buttons */
    $wp_customize->add_setting(
        'ed_social_float',
        array(
            'default'           => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
		new Rara_Controls_Toggle_Control( 
			$wp_customize,
			'ed_social_float',
			array(
				'section'     => 'social_share_settings',
				'label'       => __( 'Enable Social Sharing Floating Buttons', 'preschool-and-kindergarten-pro' ),
                'description' => __( 'Make social sharing buttons float on single posts.', 'preschool-and-kindergarten-pro' ),
			)
		)
	);
    
    /** Social Sharing Buttons */
    $wp_customize->add_setting(
		'social_share', 
		array(
			'default' => array( 'facebook', 'twitter', 'linkedin', 'pinterest' ),
			'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_sortable',						
		)
	);

	$wp_customize->add_control(
		new Rara_Control_Sortable(
			$wp_customize,
			'social_share',
			array(
				'section'     => 'social_share_settings',
				'label'       => __( 'Social Sharing Buttons', 'preschool-and-kindergarten-pro' ),
				'description' => __( 'Sort or toggle social sharing buttons.', 'preschool-and-kindergarten-pro' ),
				'choices'     => array(
            		'facebook'  => __( 'Facebook', 'preschool-and-kindergarten-pro' ),
            		'twitter'   => __( 'Twitter', 'preschool-and-kindergarten-pro' ),
            		'linkedin'  => __( 'Linkedin', 'preschool-and-kindergarten-pro' ),
            		'pinterest' => __( 'Pinterest', 'preschool-and-kindergarten-pro' ),
            		'email'     => __( 'Email', 'preschool-and-kindergarten-pro' ),
            		'gplus'     => __( 'Google Plus', 'preschool-and-kindergarten-pro' ),
                    'stumble'   => __( 'StumbleUpon', 'preschool-and-kindergarten-pro' ),
                    'reddit'    => __( 'Reddit', 'preschool-and-kindergarten-pro' ),            
            	),
			)
		)
	);
        
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_social_share' );