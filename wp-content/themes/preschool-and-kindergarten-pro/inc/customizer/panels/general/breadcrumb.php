<?php
/**
 * BreadCrumb Theme Option.
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_breadcrumb( $wp_customize ) {

    /** BreadCrumb Settings */
    $wp_customize->add_section(
        'breadcrumb_settings',
        array(
            'title'      => __( 'Breadcrumb Settings', 'preschool-and-kindergarten-pro' ),
            'priority'   => 10,
            'capability' => 'edit_theme_options',
            'panel'    => 'general_setting',
        )
    );

    /** Enable/Disable BreadCrumb */
    $wp_customize->add_setting(
        'ed_breadcrumb',
        array(
            'default'           => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
		new Rara_Controls_Toggle_Control( 
			$wp_customize,
			'ed_breadcrumb',
			array(
				'section'     => 'breadcrumb_settings',
				'label'       => __( 'Breadcrumb', 'preschool-and-kindergarten-pro' ),
			)
		)
	);
    
    /** Home Text */
    $wp_customize->add_setting(
        'breadcrumb_home_text',
        array(
            'default'           => __( 'Home', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'breadcrumb_home_text',
        array(
            'label'   => __( 'Breadcrumb Home Text', 'preschool-and-kindergarten-pro' ),
            'section' => 'breadcrumb_settings',
            'type'    => 'text',
        )
    );
    
    /** Breadcrumb Separator */
    $wp_customize->add_setting(
        'breadcrumb_separator',
        array(
            'default'           => __( '>', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'breadcrumb_separator',
        array(
            'label'   => __( 'Breadcrumb Separator', 'preschool-and-kindergarten-pro' ),
            'section' => 'breadcrumb_settings',
            'type'    => 'text',
        )
    );
    /** BreadCrumb Settings Ends */
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_breadcrumb' );