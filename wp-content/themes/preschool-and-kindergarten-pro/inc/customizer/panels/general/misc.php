<?php
/**
 * Default Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_general_misc( $wp_customize ) {
	
    $wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
        
    
    $wp_customize->add_section( 'general_settings', array(
        'priority'   => 30,
        'capability' => 'edit_theme_options',
        'title'      => __( 'Misc Settings', 'preschool-and-kindergarten-pro' ),
        'panel'    => 'general_setting',
    ) );
    
    /** Admin Bar */
    $wp_customize->add_setting(
        'ed_adminbar',
        array(
            'default'           => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
		new Rara_Controls_Toggle_Control( 
			$wp_customize,
			'ed_adminbar',
			array(
				'section'		=> 'general_settings',
				'label'			=> __( 'Admin Bar', 'preschool-and-kindergarten-pro' ),
				'description'	=> __( 'Enable to disable Admin Bar in frontend when logged in.', 'preschool-and-kindergarten-pro' ),
			)
		)
	);
    
    /** Lightbox */
    $wp_customize->add_setting(
        'ed_lightbox',
        array(
            'default'           => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
		new Rara_Controls_Toggle_Control( 
			$wp_customize,
			'ed_lightbox',
			array(
				'section'		=> 'general_settings',
				'label'			=> __( 'Lightbox', 'preschool-and-kindergarten-pro' ),
				'description'	=> __( 'A lightbox is a stylized pop-up that allows your visitors to view larger versions of images without leaving the current page. You can enable or disable the lightbox here.', 'preschool-and-kindergarten-pro' ),
			)
		)
	);
    
    /** Ajax Quick Search */
    $wp_customize->add_setting(
        'ed_ajax_search',
        array(
            'default'           => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
		new Rara_Controls_Toggle_Control( 
			$wp_customize,
			'ed_ajax_search',
			array(
				'section'		=> 'general_settings',
				'label'			=> __( 'Ajax Quick Search', 'preschool-and-kindergarten-pro' ),
				'description'	=> __( 'Enable to display search results appearing instantly below the search form.', 'preschool-and-kindergarten-pro' ),
			)
		)
	);

    /** demo content */
    $wp_customize->add_setting(
        'ed_demo',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'ed_demo',
            array(
                'section'       => 'general_settings',
                'label'         => __( 'Demo Content', 'preschool-and-kindergarten-pro' ),
                'description'   => __( 'Disable to hide default demo content displayed in different sections of home page.', 'preschool-and-kindergarten-pro' ),
            )
        )
    );
    
    /** Pagination Type */
    $wp_customize->add_setting(
        'pagination_type',
        array(
            'default'           => 'default',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
        'pagination_type',
        array(
            'label'       => __( 'Pagination Type', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Select pagination type.', 'preschool-and-kindergarten-pro' ),
            'section'     => 'general_settings',
            'type'        => 'radio',
            'choices'     => array(
                'default'         => __( 'Default (Next / Previous)', 'preschool-and-kindergarten-pro' ),
                'numbered'        => __( 'Numbered (1 2 3 4...)', 'preschool-and-kindergarten-pro' ),
                'load_more'       => __( 'AJAX (Load More Button)', 'preschool-and-kindergarten-pro' ),
                'infinite_scroll' => __( 'AJAX (Auto Infinite Scroll)', 'preschool-and-kindergarten-pro' ),
            )
        )
    );
    

    /** Load More Label */
    $wp_customize->add_setting(
        'load_more_label',
        array(
            'default'           => __( 'Load More Posts', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
       'load_more_label',
        array(
            'section' => 'general_settings',
            'label'   => __( 'Load More Label', 'preschool-and-kindergarten-pro' ),
            'type'    => 'text',
            'active_callback' => 'preschool_and_kindergarten_pro_loading_ac' 
        )       
    );
    
    /** Loading Label */
    $wp_customize->add_setting(
        'loading_label',
        array(
            'default'           => __( 'Loading...', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
       'loading_label',
        array(
            'section' => 'general_settings',
            'label'   => __( 'Loading Label', 'preschool-and-kindergarten-pro' ),
            'type'    => 'text',
            'active_callback' => 'preschool_and_kindergarten_pro_loading_ac' 
        )       
    );

     /** Nomore Posts */
    $wp_customize->add_setting(
        'nomore_post_label',
        array(
            'default'           => __( 'No more Post', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
       'nomore_post_label',
        array(
            'section' => 'general_settings',
            'label'   => __( 'No more Post Label', 'preschool-and-kindergarten-pro' ),
            'type'    => 'text',
            'active_callback' => 'preschool_and_kindergarten_pro_loading_ac' 
        )       
    );
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_general_misc' );

/**
 * Active Callback for contact phone
*/
function preschool_and_kindergarten_pro_loading_ac( $control ){
    
    $pagination_type = $control->manager->get_setting( 'pagination_type' )->value();
    
    if ( $pagination_type == 'load_more' ) return true;
    
    return false;
}