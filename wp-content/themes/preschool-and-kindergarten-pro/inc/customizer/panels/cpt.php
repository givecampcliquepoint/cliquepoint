<?php
/**
 * Custom Post Type Page Option.
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_cpt_page( $wp_customize ) {
    
    /** Home Page Settings */
    $wp_customize->add_panel( 
        'custom_post_type_page_settings',
         array(
            'priority'    => 40,
            'capability'  => 'edit_theme_options',
            'title'       => __( 'Custom Post Type Page Settings', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Custom Post Type Page Sections', 'preschool-and-kindergarten-pro' ),
        ) 
    );

}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_cpt_page' );