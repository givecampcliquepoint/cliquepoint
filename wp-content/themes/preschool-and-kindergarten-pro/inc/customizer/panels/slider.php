<?php
/**
 * Slider Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_slider( $wp_customize ) {
    
     $wp_customize->add_panel( 'slider_settings', array(
        'title'          => __( 'Slider Settings', 'preschool-and-kindergarten-pro' ),
        'priority'       => 23,
        'capability'     => 'edit_theme_options',
    ) );
        
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_slider' );