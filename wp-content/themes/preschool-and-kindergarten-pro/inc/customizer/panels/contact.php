<?php
/**
 * Contact Page Theme Option.
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_contact( $wp_customize ) {
    
    /** Contact Page Settings */
    $wp_customize->add_panel( 
        'contact_page_settings',
         array(
            'priority'    => 35,
            'capability'  => 'edit_theme_options',
            'title'       => __( 'Contact Page Settings', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Customize Contact Page Sections', 'preschool-and-kindergarten-pro' ),
        ) 
    );

}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_contact' );