<?php
/**
 * Home Page Theme Option.
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_home( $wp_customize ) {
    
    /** Home Page Settings */
    $wp_customize->add_panel( 
        'home_page_settings',
         array(
            'priority'    => 25,
            'capability'  => 'edit_theme_options',
            'title'       => __( 'Home Page Settings', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Customize Home Page Sections', 'preschool-and-kindergarten-pro' ),
        ) 
    );

}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_home' );