<?php
/**
 * About Page Theme Option.
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_about( $wp_customize ) {
    
    /** About Page Settings */
    $wp_customize->add_panel( 
        'about_page_settings',
         array(
            'priority'    => 35,
            'capability'  => 'edit_theme_options',
            'title'       => __( 'About Page Settings', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Customize About Page Sections', 'preschool-and-kindergarten-pro' ),
        ) 
    );

}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_about' );