<?php
/**
 * Slider Content Section Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_slider_content( $wp_customize ) {
    
    global $preschool_and_kindergarten_pro_option_categories;
 
    $wp_customize->add_section( 
        'slider_content_settings', 
        array(
            'priority'   => 21,
            'capability' => 'edit_theme_options',
            'title'      => __( 'Slider Content', 'preschool-and-kindergarten-pro' ),
            'panel'      => 'slider_settings',
    ) );

    /** Enable/Disable Slider */
    $wp_customize->add_setting(
        'ed_slider',
        array(
            'default'           => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'ed_slider',
            array(
                'section' => 'slider_content_settings',
                'label'   => __( 'Home Page Slider', 'preschool-and-kindergarten-pro' ),
                 )
        )
    );

     /** Select Slider Type */
    $wp_customize->add_setting(
        'slider_type',
        array(
            'default'           => 'post',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Select_Control(
        $wp_customize,
        'slider_type',
        array(
            'label'       => __( 'Choose Slider Type', 'preschool-and-kindergarten-pro' ),
            'section'     => 'slider_content_settings',
            'description' => __( ' You can make slider using Page, category of posts or custom. Choose one options from the dropdown below.', 'preschool-and-kindergarten-pro' ),
            'choices'     => array(
                'post'   => __( 'Page', 'preschool-and-kindergarten-pro' ),
                'cat'    => __( 'Category', 'preschool-and-kindergarten-pro' ),
                'custom' => __( 'Custom', 'preschool-and-kindergarten-pro' ),
            ),
            'active_callback' => 'preschool_and_kindergarten_pro_required'
        )
        )
    ); 
    /** Select Post One */
    $wp_customize->add_setting(
        'slider_page_one',
        array(
            'default' => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Select_Control(
            $wp_customize,
            'slider_page_one',
            array(
                'label'     => __( 'Choose Page One', 'preschool-and-kindergarten-pro'),
                'section'   => 'slider_content_settings',
                'choices'   => preschool_and_kindergarten_pro_get_posts( 'page' ),
                'active_callback' => 'preschool_and_kindergarten_pro_required'
            )
        )
    );
 
    /** Select Post Two */

   $wp_customize->add_setting(
        'slider_page_two',
        array(
            'default' => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Select_Control(
            $wp_customize,
            'slider_page_two',
            array(
                'label'     => __( 'Choose Page Two', 'preschool-and-kindergarten-pro'),
                'section'   => 'slider_content_settings',
                'choices'   => preschool_and_kindergarten_pro_get_posts( 'page' ),
                'active_callback' => 'preschool_and_kindergarten_pro_required'

            )
        )
    );
    /** Select Post Three */
    $wp_customize->add_setting(
        'slider_page_three',
        array(
            'default' => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Select_Control(
            $wp_customize,
            'slider_page_three',
            array(
                'label'     => __( 'Choose Page Three', 'preschool-and-kindergarten-pro'),
                'section'   => 'slider_content_settings',
                'choices'   => preschool_and_kindergarten_pro_get_posts( 'page' ),
                'active_callback' => 'preschool_and_kindergarten_pro_required'
            )
        )
    );
    
    /** Select Post Four */
   $wp_customize->add_setting(
        'slider_page_four',
        array(
            'default' => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Select_Control(
            $wp_customize,
            'slider_page_four',
            array(
                'label'     => __( 'Choose Page Four', 'preschool-and-kindergarten-pro'),
                'section'   => 'slider_content_settings',
                'choices'   => preschool_and_kindergarten_pro_get_posts( 'page' ),
                'active_callback' => 'preschool_and_kindergarten_pro_required'
            )
        )
    );
    
    /** Select Post Five */
    $wp_customize->add_setting(
        'slider_page_five',
        array(
            'default' => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Select_Control(
            $wp_customize,
            'slider_page_five',
            array(
                'label'     => __( 'Choose Page Five', 'preschool-and-kindergarten-pro'),
                'section'   => 'slider_content_settings',
                'choices'   => preschool_and_kindergarten_pro_get_posts( 'page' ),
                'active_callback' => 'preschool_and_kindergarten_pro_required'
            )
        )
    );
    
    /** Select Category */

    $wp_customize->add_setting(
        'slider_category',
        array(
            'default' => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Select_Control(
            $wp_customize,
            'slider_category',
            array(
                'label'     => __( 'Choose Slider Category', 'preschool-and-kindergarten-pro'),
                'section'   => 'slider_content_settings',
                'description' => __( 'You can choose a category of posts to display the post that belongs to that category.', 'preschool-and-kindergarten-pro' ),
                'choices'   => $preschool_and_kindergarten_pro_option_categories,
                'active_callback' => 'preschool_and_kindergarten_pro_required'
            )
        )
    );
    
    /** Add Slides */
    $wp_customize->add_setting( 
        new Rara_Repeater_Setting( 
            $wp_customize, 
            'slider_repeater_settings', 
            array(
                'default' => '',                             
            ) 
        )
    );

    $wp_customize->add_control( 
        new Rara_Control_Repeater(
            $wp_customize, 
            'slider_repeater_settings', 
            array(
                'section' => 'slider_content_settings',                
                'label'   => __( 'Add Slider', 'preschool-and-kindergarten-pro' ),
                'description' => __( 'Click Add New Slide. Then upload image, enter title, description and link for each slides.', 'preschool-and-kindergarten-pro' ),
                'fields'     => array(
                            'thumbnail' => array(
                                'type'  => 'image', 
                                'label' => __( 'Add Image', 'preschool-and-kindergarten-pro' ),                
                            ),
                            'title'     => array(
                                'type'  => 'text',
                                'label' => __( 'Title', 'preschool-and-kindergarten-pro' ),
                            ),
                            'content'     => array(
                                'type'  => 'textarea',
                                'label' => __( 'Description', 'preschool-and-kindergarten-pro' ),
                            ),
                            'link'     => array(
                                'type'  => 'text',
                                'label' => __( 'Link', 'preschool-and-kindergarten-pro' ),
                                
                            )
                        ),
                'active_callback' => 'preschool_and_kindergarten_pro_required',
                'row_label' => array(
                    'type' => 'field', // [default 'text']
                    'value' => __( 'slide', 'preschool-and-kindergarten-pro' ),
                    'field' => 'title',// [only used if type = field, the field-id must exist in fields and be a text field]
                ),                            
            )
        )
    );      
    
    /** Slider Readmore */

     $wp_customize->add_setting(
        'slider_readmore',
        array(
            'default' => __( 'Learn More', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control( 
        'slider_readmore',
        array(
            'label'   => __( 'Readmore Button Label', 'preschool-and-kindergarten-pro' ),
            'section' => 'slider_content_settings',
            'type'    => 'text',
            'active_callback' => 'preschool_and_kindergarten_pro_required'

        )
    );
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_slider_content' );

/**
 * Active Callback
*/
function preschool_and_kindergarten_pro_required( $control ){
    
    $type        = $control->manager->get_setting( 'slider_type' )->value();
    $ed_slider   = $control->manager->get_setting( 'ed_slider' )->value();
    $control_id  = $control->id;

    if ( $control_id == 'slider_type'              && $ed_slider == '1'      ) return true; 
    if ( $control_id == 'slider_page_one'          && $type      == 'post'   && $ed_slider == '1'   ) return true;
    if ( $control_id == 'slider_page_two'          && $type      == 'post'   && $ed_slider == '1'  ) return true;
    if ( $control_id == 'slider_page_three'        && $type      == 'post'   && $ed_slider == '1'  ) return true;
    if ( $control_id == 'slider_page_four'         && $type      == 'post'   && $ed_slider == '1'  ) return true;
    if ( $control_id == 'slider_page_five'         && $type      == 'post'   && $ed_slider == '1'  ) return true;
    if ( $control_id == 'slider_category'          && $type      == 'cat'    && $ed_slider == '1'  ) return true;
    if ( $control_id == 'slider_repeater_settings' && $type      == 'custom' && $ed_slider == '1' ) return true;
    if ( $control_id == 'slider_readmore'          && $ed_slider == '1'      ) return true; 

    
    return false;
} 