<?php
/**
 * Slider Options Section
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_slider_option( $wp_customize ) {
    
    $wp_customize->add_section( 
        'slider_option_settings', 
        array(
            'priority'   => 22,
            'capability' => 'edit_theme_options',
            'title'      => __( 'Slider Options', 'preschool-and-kindergarten-pro' ),
            'panel'      => 'slider_settings',
    ) );
    
    
     /** Enable/Disable Slider Auto Transition */
    $wp_customize->add_setting(
        'ed_auto_transition',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'ed_auto_transition',
            array(
                'section'       => 'slider_option_settings',
                'label'         => __( 'Slider Auto', 'preschool-and-kindergarten-pro' ),
                'description'   => __( 'Enable slider auto transition.', 'preschool-and-kindergarten-pro' ),
             )
        )
    );

    /** Enable/Disable Slider Loop */
    $wp_customize->add_setting(
        'ed_slider_loop',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'ed_slider_loop',
            array(
                'section'       => 'slider_option_settings',
                'label'         => __( 'Slider Loop', 'preschool-and-kindergarten-pro' ),
                'description'   => __( 'Enable slider loop.', 'preschool-and-kindergarten-pro' ),

            )
        )
    );
    
    /** Enable/Disable Slider Caption */
      $wp_customize->add_setting(
        'ed_slider_caption',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'ed_slider_caption',
            array(
                'section'       => 'slider_option_settings',
                'label'         => __( 'Slider Caption', 'preschool-and-kindergarten-pro' ),
                'description'   => __( 'Enable slider caption.', 'preschool-and-kindergarten-pro' ),

            )
        )
    );

    /** Enable/Disable Slider Caption */
    $wp_customize->add_setting(
        'ed_caption_bg',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'ed_caption_bg',
            array(
                'section'       => 'slider_option_settings',
                'label'         => __( 'Slider Caption Background', 'preschool-and-kindergarten-pro' ),
                'description'   => __( 'Enable slider caption Background.', 'preschool-and-kindergarten-pro' ),
            )
        )
    );
    

    /** Use Full Size Image */

        /** Enable/Disable Slider Caption */
    $wp_customize->add_setting(
        'ed_fullsize_img',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'ed_fullsize_img',
            array(
                'section'       => 'slider_option_settings',
                'label'         => __( 'Use Full Size Image', 'preschool-and-kindergarten-pro' ),
                'description'   => __( 'Enable to use full size image in slider.', 'preschool-and-kindergarten-pro' ),
            )
        )
    );

     /** Slider Speed Control */
    $wp_customize->add_setting( 
        'ed_slider_speed_control',
         array(
            'default'           => '3000',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select'
            ) 
         );
    
    $wp_customize->add_control(
        new Rara_Controls_Slider_Control( 
            $wp_customize,
            'ed_slider_speed_control',
            array(
                'section' => 'slider_option_settings',
                'label'   => __( 'Slider Animation Speed', 'preschool-and-kindergarten-pro' ),
                'choices' => array(
                    'min'   => 1000,
                    'max'   => 10000,
                    'step'  => 1000,
                )
            )
        )
    );
    
    /** Slider Animation */
    $wp_customize->add_setting(
        'slider_animation',
        array(
            'default'           => 'slide',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Select_Control(
        $wp_customize,
        'slider_animation',
        array(
            'label'       => __( 'Choose Slider Animation', 'preschool-and-kindergarten-pro' ),
            'section'     => 'slider_option_settings',
            'choices'     => array(
                    'slide'          => __( 'Slide' , 'preschool-and-kindergarten-pro' ),
                    'bounceOut'      => __( 'Bounce Out', 'preschool-and-kindergarten-pro' ),
                    'bounceOutLeft'  => __( 'Bounce Out Left', 'preschool-and-kindergarten-pro' ),
                    'bounceOutRight' => __( 'Bounce Out Right', 'preschool-and-kindergarten-pro' ),
                    'bounceOutUp'    => __( 'Bounce Out Up', 'preschool-and-kindergarten-pro' ),
                    'bounceOutDown'  => __( 'Bounce Out Down', 'preschool-and-kindergarten-pro' ),
                    'fadeOut'        => __( 'Fade Out', 'preschool-and-kindergarten-pro' ),
                    'fadeOutLeft'    => __( 'Fade Out Left', 'preschool-and-kindergarten-pro' ),
                    'fadeOutRight'   => __( 'Fade Out Right', 'preschool-and-kindergarten-pro' ),
                    'fadeOutUp'      => __( 'Fade Out Up', 'preschool-and-kindergarten-pro' ),
                    'fadeOutDown'    => __( 'Fade Out Down', 'preschool-and-kindergarten-pro' ),
                    'flipOutX'       => __( 'Flip OutX', 'preschool-and-kindergarten-pro' ),
                    'flipOutY'       => __( 'Flip OutY', 'preschool-and-kindergarten-pro' ),
                    'hinge'          => __( 'Hinge', 'preschool-and-kindergarten-pro' ),
                    'pulse'          => __( 'Pulse', 'preschool-and-kindergarten-pro' ),
                    'rollOut'        => __( 'Roll Out', 'preschool-and-kindergarten-pro' ),
                    'rollOutLeft'    => __( 'Roll Out Left', 'preschool-and-kindergarten-pro' ),
                    'rollOutRight'   => __( 'Roll Out Right', 'preschool-and-kindergarten-pro' ),
                    'rollOutUp'      => __( 'Roll Out Up', 'preschool-and-kindergarten-pro' ),
                    'rollOutDown'    => __( 'Roll Out Down', 'preschool-and-kindergarten-pro' ),
                    'rotateOut'      => __( 'Rotate Out', 'preschool-and-kindergarten-pro' ),
                    'rotateOutLeft'  => __( 'Rotate Out Left', 'preschool-and-kindergarten-pro' ),
                    'rotateOutRight' => __( 'Rotate Out Right', 'preschool-and-kindergarten-pro' ),
                    'rotateOutUp'    => __( 'Rotate Out Up', 'preschool-and-kindergarten-pro' ),
                    'rotateOutDown'  => __( 'Rotate Out Down', 'preschool-and-kindergarten-pro' ),
                    'rubberBand'     => __( 'Rubber Band', 'preschool-and-kindergarten-pro' ),
                    'shake'          => __( 'Shake', 'preschool-and-kindergarten-pro' ),
                    'slideOutLeft'   => __( 'Slide Out Left', 'preschool-and-kindergarten-pro' ),
                    'slideOutRight'  => __( 'Slide Out Right', 'preschool-and-kindergarten-pro' ),
                    'slideOutUp'     => __( 'Slide Out Up', 'preschool-and-kindergarten-pro' ),
                    'slideOutDown'   => __( 'Slide Out Down', 'preschool-and-kindergarten-pro' ),
                    'swing'          => __( 'Swing', 'preschool-and-kindergarten-pro' ),
                    'tada'           => __( 'Tada', 'preschool-and-kindergarten-pro' ),
                    'zoomOut'        => __( 'Zoom Out', 'preschool-and-kindergarten-pro' ),
                    'zoomOutLeft'    => __( 'Zoom Out Left', 'preschool-and-kindergarten-pro' ),
                    'zoomOutRight'   => __( 'Zoom Out Right', 'preschool-and-kindergarten-pro' ),
                    'zoomOutUp'      => __( 'Zoom Out Up', 'preschool-and-kindergarten-pro' ),
                    'zoomOutDown'    => __( 'Zoom Out Down', 'preschool-and-kindergarten-pro' ),
                ),
        )
        )
    );
  
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_slider_option' );