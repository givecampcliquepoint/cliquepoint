<?php
/**
 * H5 Typography Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_typography_h5( $wp_customize ) {
    
    /** H5 Typography Settings */
    $wp_customize->add_section( 'h5_section', array(
        'title'      => __( 'H5 Settings (Content)', 'preschool-and-kindergarten-pro' ),
        'priority'   => 27,
        'capability' => 'edit_theme_options',
        'panel'      => 'typography_section'
    ) );
    
    /** H5 Font */
    $wp_customize->add_setting( 'h5_font', array(
		'default' => array(                         // Default font styles				
			'font-family' => 'Lato',
			'variant'     => 'regular',
		),
		'sanitize_callback' => array( 'Rara_Fonts', 'sanitize_typography' )
	) );

	$wp_customize->add_control( 
        new Rara_Typography_Control( 
            $wp_customize, 
            'h5_font', 
            array(
        		'label'   => __( 'H5 Font', 'preschool-and-kindergarten-pro' ),
        		'section' => 'h5_section',		
        	) 
         ) 
    );
        
    /** H5 Font Size */
    $wp_customize->add_setting( 'h5_font_size', array(
        'default'           => 20,
        'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select'
    ) );
    
    $wp_customize->add_control(
		new Rara_Controls_Slider_Control( 
			$wp_customize,
			'h5_font_size',
			array(
				'section' => 'h5_section',
				'label'	  => __( 'H5 Font Size', 'preschool-and-kindergarten-pro' ),
				'choices' => array(
					'min' 	=> 10,
					'max' 	=> 30,
					'step'	=> 1,
				)
			)
		)
	);
    
    /** H5 Line Height */
    $wp_customize->add_setting( 'h5_line_height', array(
        'default'           => 24,
        'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select'
    ) );
    
    $wp_customize->add_control(
		new Rara_Controls_Slider_Control( 
			$wp_customize,
			'h5_line_height',
			array(
				'section' => 'h5_section',
				'label'	  => __( 'H5 Line Height', 'preschool-and-kindergarten-pro' ),
				'choices' => array(
					'min' 	=> 10,
					'max' 	=> 35,
					'step'	=> 1,
				)
			)
		)
	);
    
    /** H5 Color */
    $wp_customize->add_setting( 'h5_color', array(
        'default'           => '#313131',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    $wp_customize->add_control( 
        new WP_Customize_Color_Control( 
            $wp_customize, 
            'h5_color', 
            array(
                'label'   => __( 'H5 Color', 'preschool-and-kindergarten-pro' ),
                'section' => 'h5_section',                
            )
        )
    );
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_typography_h5' );