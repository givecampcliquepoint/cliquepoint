<?php
/**
 * H1 Typography Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_typography_h1( $wp_customize ) {
    
    /** H1 Typography Settings */
    $wp_customize->add_section( 'h1_section', array(
        'title'      => __( 'H1 Settings (Content)', 'preschool-and-kindergarten-pro' ),
        'priority'   => 23,
        'capability' => 'edit_theme_options',
        'panel'      => 'typography_section'
    ) );
    
    /** H1 Font */
    $wp_customize->add_setting( 'h1_font', array(
		'default' => array(                         // Default font styles				
			'font-family' => 'Lato',
			'variant'     => 'regular',
		),
		'sanitize_callback' => array( 'Rara_Fonts', 'sanitize_typography' )
	) );

	$wp_customize->add_control( 
        new Rara_Typography_Control( 
            $wp_customize, 
            'h1_font', 
            array(
        		'label'   => __( 'H1 Font', 'preschool-and-kindergarten-pro' ),
        		'section' => 'h1_section',		
        	) 
         ) 
    );
    
    /** H1 Font Size */
    $wp_customize->add_setting( 'h1_font_size', array(
        'default'           => 60,
        'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select'
    ) );
    
    $wp_customize->add_control(
		new Rara_Controls_Slider_Control( 
			$wp_customize,
			'h1_font_size',
			array(
				'section' => 'h1_section',
				'label'	  => __( 'H1 Font Size', 'preschool-and-kindergarten-pro' ),
				'choices' => array(
					'min' 	=> 50,
					'max' 	=> 100,
					'step'	=> 1,
				)
			)
		)
	);
    
    /** H1 Line Height */
    $wp_customize->add_setting( 'h1_line_height', array(
        'default'           => 72,
        'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select'
    ) );
    
    $wp_customize->add_control(
		new Rara_Controls_Slider_Control( 
			$wp_customize,
			'h1_line_height',
			array(
				'section' => 'h1_section',
				'label'	  => __( 'H1 Line Height', 'preschool-and-kindergarten-pro' ),
				'choices' => array(
					'min' 	=> 60,
					'max' 	=> 120,
					'step'	=> 1,
				)
			)
		)
	);
    
    /** H1 Color */
    $wp_customize->add_setting( 'h1_color', array(
        'default'           => '#313131',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    $wp_customize->add_control( 
        new WP_Customize_Color_Control( 
            $wp_customize, 
            'h1_color', 
            array(
                'label'   => __( 'H1 Color', 'preschool-and-kindergarten-pro' ),
                'section' => 'h1_section',                
            )
        )
    );
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_typography_h1' );