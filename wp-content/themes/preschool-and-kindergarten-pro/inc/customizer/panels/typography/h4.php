<?php
/**
 * H4 Typography Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_typography_h4( $wp_customize ) {
    
    /** H4 Typography Settings */
    $wp_customize->add_section( 'h4_section', array(
        'title'      => __( 'H4 Settings (Content)', 'preschool-and-kindergarten-pro' ),
        'priority'   => 26,
        'capability' => 'edit_theme_options',
        'panel'      => 'typography_section'
    ) );
    
    /** H4 Font */
    $wp_customize->add_setting( 'h4_font', array(
		'default' => array(                         // Default font styles				
			'font-family' => 'Lato',
			'variant'     => 'regular',
		),
		'sanitize_callback' => array( 'Rara_Fonts', 'sanitize_typography' )
	) );

	$wp_customize->add_control( 
        new Rara_Typography_Control( 
            $wp_customize, 
            'h4_font', 
            array(
        		'label'   => __( 'H4 Font', 'preschool-and-kindergarten-pro' ),
        		'section' => 'h4_section',		
        	) 
         ) 
    );
    
    /** H4 Font Size */
    $wp_customize->add_setting( 'h4_font_size', array(
        'default'           => 24,
        'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select'
    ) );
    
    $wp_customize->add_control(
		new Rara_Controls_Slider_Control( 
			$wp_customize,
			'h4_font_size',
			array(
				'section' => 'h4_section',
				'label'	  => __( 'H4 Font Size', 'preschool-and-kindergarten-pro' ),
				'choices' => array(
					'min' 	=> 10,
					'max' 	=> 30,
					'step'	=> 1,
				)
			)
		)
	);
    
    /** H4 Line Height */
    $wp_customize->add_setting( 'h4_line_height', array(
        'default'           => 29,
        'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select'
    ) );
    
    $wp_customize->add_control(
		new Rara_Controls_Slider_Control( 
			$wp_customize,
			'h4_line_height',
			array(
				'section' => 'h4_section',
				'label'	  => __( 'H4 Line Height', 'preschool-and-kindergarten-pro' ),
				'choices' => array(
					'min' 	=> 15,
					'max' 	=> 40,
					'step'	=> 1,
				)
			)
		)
	);
    
    /** H4 Color */
    $wp_customize->add_setting( 'h4_color', array(
        'default'           => '#313131',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    $wp_customize->add_control( 
        new WP_Customize_Color_Control( 
            $wp_customize, 
            'h4_color', 
            array(
                'label'   => __( 'H4 Color', 'preschool-and-kindergarten-pro' ),
                'section' => 'h4_section',                
            )
        )
    );
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_typography_h4' );