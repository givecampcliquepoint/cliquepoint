<?php
/**
 * H6 Typography Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_typography_h6( $wp_customize ) {
    
    /** H6 Typography Settings */
    $wp_customize->add_section( 'h6_section', array(
        'title'      => __( 'H6 Settings (Content)', 'preschool-and-kindergarten-pro' ),
        'priority'   => 28,
        'capability' => 'edit_theme_options',
        'panel'      => 'typography_section'
    ) );
    
    /** H6 Font */
    $wp_customize->add_setting( 'h6_font', array(
		'default' => array(                         // Default font styles				
			'font-family' => 'Lato',
			'variant'     => 'regular',
		),
		'sanitize_callback' => array( 'Rara_Fonts', 'sanitize_typography' )
	) );

	$wp_customize->add_control( 
        new Rara_Typography_Control( 
            $wp_customize, 
            'h6_font', 
            array(
        		'label'   => __( 'H6 Font', 'preschool-and-kindergarten-pro' ),
        		'section' => 'h6_section',		
        	) 
         ) 
    );
    
    /** H6 Font Size */
    $wp_customize->add_setting( 'h6_font_size', array(
        'default'           => 16,
        'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select'
    ) );
    
    $wp_customize->add_control(
		new Rara_Controls_Slider_Control( 
			$wp_customize,
			'h6_font_size',
			array(
				'section' => 'h6_section',
				'label'	  => __( 'H6 Font Size', 'preschool-and-kindergarten-pro' ),
				'choices' => array(
					'min' 	=> 10,
					'max' 	=> 30,
					'step'	=> 1,
				)
			)
		)
	);
    
    /** H6 Line Height */
    $wp_customize->add_setting( 'h6_line_height', array(
        'default'           => 19,
        'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select'
    ) );
    
    $wp_customize->add_control(
		new Rara_Controls_Slider_Control( 
			$wp_customize,
			'h6_line_height',
			array(
				'section' => 'h6_section',
				'label'	  => __( 'H6 Line Height', 'preschool-and-kindergarten-pro' ),
				'choices' => array(
					'min' 	=> 10,
					'max' 	=> 35,
					'step'	=> 1,
				)
			)
		)
	);
    
    /** H6 Color */
    $wp_customize->add_setting( 'h6_color', array(
        'default'           => '#313131',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    $wp_customize->add_control( 
        new WP_Customize_Color_Control( 
            $wp_customize, 
            'h6_color', 
            array(
                'label'   => __( 'H6 Color', 'preschool-and-kindergarten-pro' ),
                'section' => 'h6_section',                
            )
        )
    );
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_typography_h6' );