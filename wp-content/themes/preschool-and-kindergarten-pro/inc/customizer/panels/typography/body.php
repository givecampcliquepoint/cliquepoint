<?php
/**
 * Body Typography Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_typography_body( $wp_customize ) {

    /** Body Settings */
    $wp_customize->add_section( 'typography_body_section', array(
        'title'      => __( 'Body Settings', 'preschool-and-kindergarten-pro' ),
        'priority'   => 11,
        'capability' => 'edit_theme_options',
        'panel'      => 'typography_section'
    ) );
    
    /** Body Font */
    $wp_customize->add_setting( 'body_font', array(
		'default' => array(                         // Default font styles				
			'font-family' => 'Lato',
			'variant'     => 'regular',
		),
		'sanitize_callback' => array( 'Rara_Fonts', 'sanitize_typography' )
	) );

	$wp_customize->add_control( 
        new Rara_Typography_Control( 
            $wp_customize, 
            'body_font', 
            array(
        		'label'   => __( 'Body Font', 'preschool-and-kindergarten-pro' ),
                'section' => 'typography_body_section',		
        	) 
         ) 
    );
    
    /** Body Font Size */
    $wp_customize->add_setting( 'body_font_size', array(
        'default'           => 18,
        'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select'
    ) );
    
    $wp_customize->add_control(
		new Rara_Controls_Slider_Control( 
			$wp_customize,
			'body_font_size',
			array(
				'section' => 'typography_body_section',
				'label'	  => __( 'Body Font Size', 'preschool-and-kindergarten-pro' ),
				'choices' => array(
					'min' 	=> 10,
					'max' 	=> 35,
					'step'	=> 1,
				)
			)
		)
	);
    
    /** Body Line Height */
    $wp_customize->add_setting( 'body_line_height', array(
        'default'           => 28,
        'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select'
    ) );
    
    $wp_customize->add_control(
		new Rara_Controls_Slider_Control( 
			$wp_customize,
			'body_line_height',
			array(
				'section' => 'typography_body_section',
				'label'	  => __( 'Body Line Height', 'preschool-and-kindergarten-pro' ),
				'choices' => array(
					'min' 	=> 15,
					'max' 	=> 50,
					'step'	=> 1,
				)
			)
		)
	);    
    
    /** Body Color */
    $wp_customize->add_setting( 'body_color', array(
        'default'           => '#616161',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    $wp_customize->add_control( 
        new WP_Customize_Color_Control( 
            $wp_customize, 
            'body_color', 
            array(
                'label'   => __( 'Body Color', 'preschool-and-kindergarten-pro' ),
                'section' => 'typography_body_section',                
            )
        )
    );
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_typography_body' );