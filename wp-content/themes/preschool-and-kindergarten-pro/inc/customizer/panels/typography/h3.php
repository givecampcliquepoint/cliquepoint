<?php
/**
 * H3 Typography Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_typography_h3( $wp_customize ) {
    
    /** H3 Typography Settings */
    $wp_customize->add_section( 'h3_section', array(
        'title'      => __( 'H3 Settings (Content)', 'preschool-and-kindergarten-pro' ),
        'priority'   => 25,
        'capability' => 'edit_theme_options',
        'panel'      => 'typography_section'
    ) );
    
    /** H3 Font */
    $wp_customize->add_setting( 'h3_font', array(
		'default' => array(                         // Default font styles				
			'font-family' => 'Lato',
			'variant'     => 'regular',
		),
		'sanitize_callback' => array( 'Rara_Fonts', 'sanitize_typography' )
	) );

	$wp_customize->add_control( 
        new Rara_Typography_Control( 
            $wp_customize, 
            'h3_font', 
            array(
        		'label'   => __( 'H3 Font', 'preschool-and-kindergarten-pro' ),
        		'section' => 'h3_section',		
        	) 
         ) 
    );
        
    /** H3 Font Size */
    $wp_customize->add_setting( 'h3_font_size', array(
        'default'           => 36,
        'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select'
    ) );
    
    $wp_customize->add_control(
		new Rara_Controls_Slider_Control( 
			$wp_customize,
			'h3_font_size',
			array(
				'section' => 'h3_section',
				'label'	  => __( 'H3 Font Size', 'preschool-and-kindergarten-pro' ),
				'choices' => array(
					'min' 	=> 15,
					'max' 	=> 50,
					'step'	=> 1,
				)
			)
		)
	);
    
    /** H3 Line Height */
    $wp_customize->add_setting( 'h3_line_height', array(
        'default'           => 43,
        'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select'
    ) );
    
    $wp_customize->add_control(
		new Rara_Controls_Slider_Control( 
			$wp_customize,
			'h3_line_height',
			array(
				'section' => 'h3_section',
				'label'	  => __( 'H3 Line Height', 'preschool-and-kindergarten-pro' ),
				'choices' => array(
					'min' 	=> 20,
					'max' 	=> 60,
					'step'	=> 1,
				)
			)
		)
	);
    
    /** H3 Color */
    $wp_customize->add_setting( 'h3_color', array(
        'default'           => '#313131',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    $wp_customize->add_control( 
        new WP_Customize_Color_Control( 
            $wp_customize, 
            'h3_color', 
            array(
                'label'   => __( 'H3 Color', 'preschool-and-kindergarten-pro' ),
                'section' => 'h3_section',                
            )
        )
    );
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_typography_h3' );