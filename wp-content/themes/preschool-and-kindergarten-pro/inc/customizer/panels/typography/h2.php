<?php
/**
 * H2 Typography Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_typography_h2( $wp_customize ) {
    
    /** H2 Typography Settings */
    $wp_customize->add_section( 'h2_section', array(
        'title'      => __( 'H2 Settings (Content)', 'preschool-and-kindergarten-pro' ),
        'priority'   => 24,
        'capability' => 'edit_theme_options',
        'panel'      => 'typography_section'
    ) );
    
    /** H2 Font */
    $wp_customize->add_setting( 'h2_font', array(
		'default' => array(                         // Default font styles				
			'font-family' => 'Lato',
			'variant'     => 'regular',
		),
		'sanitize_callback' => array( 'Rara_Fonts', 'sanitize_typography' )
	) );

	$wp_customize->add_control( 
        new Rara_Typography_Control( 
            $wp_customize, 
            'h2_font', 
            array(
        		'label'   => __( 'H2 Font', 'preschool-and-kindergarten-pro' ),
        		'section' => 'h2_section',		
        	) 
         ) 
    );
    
    /** H2 Font Size */
    $wp_customize->add_setting( 'h2_font_size', array(
        'default'           => 42,
        'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select'
    ) );
    
    $wp_customize->add_control(
		new Rara_Controls_Slider_Control( 
			$wp_customize,
			'h2_font_size',
			array(
				'section' => 'h2_section',
				'label'	  => __( 'H2 Font Size', 'preschool-and-kindergarten-pro' ),
				'choices' => array(
					'min' 	=> 20,
					'max' 	=> 60,
					'step'	=> 1,
				)
			)
		)
	);
    
    /** H2 Line Height */
    $wp_customize->add_setting( 'h2_line_height', array(
        'default'           => 50,
        'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select'
    ) );
    
    $wp_customize->add_control(
		new Rara_Controls_Slider_Control( 
			$wp_customize,
			'h2_line_height',
			array(
				'section' => 'h2_section',
				'label'	  => __( 'H2 Line Height', 'preschool-and-kindergarten-pro' ),
				'choices' => array(
					'min' 	=> 25,
					'max' 	=> 70,
					'step'	=> 1,
				)
			)
		)
	);
    
    /** H2 Color */
    $wp_customize->add_setting( 'h2_color', array(
        'default'           => '#313131',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    $wp_customize->add_control( 
        new WP_Customize_Color_Control( 
            $wp_customize, 
            'h2_color', 
            array(
                'label'   => __( 'H2 Color', 'preschool-and-kindergarten-pro' ),
                'section' => 'h2_section',                
            )
        )
    );
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_typography_h2' );