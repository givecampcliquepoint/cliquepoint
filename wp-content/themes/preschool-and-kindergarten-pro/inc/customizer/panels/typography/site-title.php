<?php
/**
 * Site Title Typography Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_typography_site_title( $wp_customize ) {
    
    /** Site Title Typography Settings */
    $wp_customize->add_section( 'site_title_section', array(
        'title'      => __( 'Site Title Settings', 'preschool-and-kindergarten-pro' ),
        'priority'   => 23,
        'capability' => 'edit_theme_options',
        'panel'      => 'typography_section'
    ) );
    
    /** Site Title Font */
    $wp_customize->add_setting( 'site_title_font', array(
		'default' => array(                 // Default font styles				
			'font-family' => 'Pacifico',
			'variant'     => 'regular',
		),
		'sanitize_callback' => array( 'Rara_Fonts', 'sanitize_typography' )
	) );

	$wp_customize->add_control( 
        new Rara_Typography_Control( 
            $wp_customize, 
            'site_title_font', 
            array(
        		'label'   => __( 'Site Title Font', 'preschool-and-kindergarten-pro' ),
        		'section' => 'site_title_section',		
        	) 
         ) 
    );

//renaming the Homepage settings to Static Front Page
$wp_customize->get_section( 'static_front_page' )->title    = 'Static Front Page';
      
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_typography_site_title' );