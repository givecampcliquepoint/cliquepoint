<?php
/**
 * Header Theme Option.
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_header( $wp_customize ) {

    $wp_customize->add_panel( 'header_setting', array(
        'title'      => __( 'Header Settings', 'preschool-and-kindergarten-pro' ),
        'priority'   => 21,
        'capability' => 'edit_theme_options',
    ) );
    
    $wp_customize->get_section( 'title_tagline' )->panel = 'header_setting';
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_header' );