<?php
/**
 * General Theme Option.
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_general( $wp_customize ) {

    $wp_customize->add_panel( 'general_setting', array(
        'title'      => __( 'General Settings', 'preschool-and-kindergarten-pro' ),
        'priority'   => 22,
        'capability' => 'edit_theme_options',
    ) );
        
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_general' );