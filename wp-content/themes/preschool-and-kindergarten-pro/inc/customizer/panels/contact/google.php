<?php
/**
 * Google Map Option.
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_contact_google( $wp_customize ) {
    
    /** Zoom Level */
    $zoom = array();
    for($z = 1; $z < 20; $z++){
        $zoom[$z] = $z;    
    }
    
    /** Google Settings */
    $wp_customize->add_section(
        'google_map_settings',
        array(
            'title'    => __( 'Google Map Settings', 'preschool-and-kindergarten-pro' ),
            'priority' => 20,
            'panel'    => 'contact_page_settings',
        )
    );
    
    /** Enable Google Map */
    $wp_customize->add_setting(
        'ed_google_map',
        array(
            'default'           => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
		new Rara_Controls_Toggle_Control( 
			$wp_customize,
			'ed_google_map',
			array(
				'section'	  => 'google_map_settings',
				'label'		  => __( 'Google Map', 'preschool-and-kindergarten-pro' ),
				'description' => __( '', 'preschool-and-kindergarten-pro' ),
			)
		)
	);
    /** Enable/Disable Google map in homepage*/
     $wp_customize->add_setting(
        'ed_google_map_on_home',
        array(
            'default'           => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'ed_google_map_on_home',
            array(
                'section' => 'google_map_settings',
                'label'   => __( 'Google map in HomePage', 'preschool-and-kindergarten-pro' ),
            )
        )
    );
    
    /** Enable Scrolling Wheel */
    $wp_customize->add_setting(
        'ed_map_scroll',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
		new Rara_Controls_Toggle_Control( 
			$wp_customize,
			'ed_map_scroll',
			array(
				'section'	  => 'google_map_settings',
				'label'		  => __( 'Scrolling Wheel', 'preschool-and-kindergarten-pro' ),
				'description' => __( 'Zoom map on Scrolling.', 'preschool-and-kindergarten-pro' ),
			)
		)
	);
    
    /** Enable Map Controls */
    $wp_customize->add_setting(
        'ed_map_controls',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
		new Rara_Controls_Toggle_Control( 
			$wp_customize,
			'ed_map_controls',
			array(
				'section'	  => 'google_map_settings',
				'label'		  => __( 'Map Controls', 'preschool-and-kindergarten-pro' ),
				'description' => __( 'Controls icons that appears above Map.', 'preschool-and-kindergarten-pro' ),
			)
		)
	);
    
    /** Enable Map Marker */
    $wp_customize->add_setting(
        'ed_map_marker',
        array(
            'default'           => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
		new Rara_Controls_Toggle_Control( 
			$wp_customize,
			'ed_map_marker',
			array(
				'section'	  => 'google_map_settings',
				'label'		  => __( 'Map Marker', 'preschool-and-kindergarten-pro' ),
				'description' => __( 'Marker icons that appears above Map.', 'preschool-and-kindergarten-pro' ),
			)
		)
	);
    
    /** Marker Title  */
    $wp_customize->add_setting(
        'marker_title',
        array(
            'default'           => '',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'marker_title',
        array(
            'label'       => __( 'Marker Title', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Enter the Marker Title.', 'preschool-and-kindergarten-pro' ),
            'section'     => 'google_map_settings',
            'type'        => 'text',
            'active_callback' => 'preschool_and_kindergarten_pro_google_map_ac'                        
        )
    );
    
    /** Google Map API Key  */
    $wp_customize->add_setting(
        'map_api',
        array(
            'default'           => '',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'map_api',
        array(
            'label'       => __( 'Google Map API Key', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Enter the google map api key here. You can get API key from here https://developers.google.com/maps/documentation/javascript/get-api-key.', 'preschool-and-kindergarten-pro' ),
            'section'     => 'google_map_settings',
            'type'        => 'text',                        
        )
    );
    
    /** Latitude  */
    $wp_customize->add_setting(
        'latitude',
        array(
            'default'           => '27.7304135',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_number_floatval',
        )
    );
    
    $wp_customize->add_control(
        'latitude',
        array(
            'label'       => __( 'Latitude', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Enter the Latitude of your location.', 'preschool-and-kindergarten-pro' ),
            'section'     => 'google_map_settings',
            'type'        => 'number',                        
        )
    );
    
    /** Longitude  */
    $wp_customize->add_setting(
        'longitude',
        array(
            'default'           => '85.3304937',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_number_floatval',
        )
    );
    
    $wp_customize->add_control(
        'longitude',
        array(
            'label'       => __( 'Longitude', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Enter the Latitude of your location.', 'preschool-and-kindergarten-pro' ),
            'section'     => 'google_map_settings',
            'type'        => 'number',                        
        )
    );
    
    /** Map Height  */
    $wp_customize->add_setting(
        'map_height',
        array(
            'default'           => 430,
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_number_absint',
        )
    );
    
    $wp_customize->add_control(
        'map_height',
        array(
            'label'       => __( 'Map Height', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Enter the height of map, for example: 300.', 'preschool-and-kindergarten-pro' ),
            'section'     => 'google_map_settings',
            'type'        => 'number',                        
        )
    );
    
    /** Zoom Level */
    $wp_customize->add_setting(
		'map_zoom',
		array(
			'default'			=> '17',
			'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select'
		)
	);

	$wp_customize->add_control(
		new Rara_Controls_Select_Control(
    		$wp_customize,
    		'map_zoom',
    		array(
                'label'   => __( 'Zoom Level', 'preschool-and-kindergarten-pro' ),
                'section' => 'google_map_settings',            
    			'choices' => $zoom,
     		)
		)
	);
    
    /** Map Type */
    $wp_customize->add_setting(
		'map_type',
		array(
			'default'			=> 'ROADMAP',
			'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select'
		)
	);

	$wp_customize->add_control(
		new Rara_Controls_Select_Control(
    		$wp_customize,
    		'map_type',
    		array(
                'label'   => __( 'Map Type', 'preschool-and-kindergarten-pro' ),
                'section' => 'google_map_settings',            
    			'choices' => array(
                    'ROADMAP'   => __( 'ROADMAP', 'preschool-and-kindergarten-pro' ),
                    'SATELLITE' => __( 'SATELLITE', 'preschool-and-kindergarten-pro' ),
                    'HYBRID'    => __( 'HYBRID', 'preschool-and-kindergarten-pro' ),
                    'TERRAIN'   => __( 'TERRAIN', 'preschool-and-kindergarten-pro' )
                )
     		)
		)
	);
            
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_contact_google' );

/**
 * Active Callback for display menu label in each home page section
*/
function preschool_and_kindergarten_pro_google_map_ac( $control ){
    
    $ed_marker  = $control->manager->get_setting( 'ed_map_marker' )->value();
    
    if ( $ed_marker == true ) return true;
    
    return false;
}