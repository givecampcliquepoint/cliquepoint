<?php
/**
 * Contact Detail Option.
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_contact_detail( $wp_customize ) {
    
    /** contact Settings */
    $wp_customize->add_section(
        'contact_detail_settings',
        array(
            'title'    => __( 'Contact Details Settings', 'preschool-and-kindergarten-pro' ),
            'priority' => 25,
            'panel'    => 'contact_page_settings',
        )
    );
    
    /**  contact details */
    $wp_customize->add_setting(
        'ed_asin_home_page',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
		new Rara_Controls_Toggle_Control( 
			$wp_customize,
			'ed_asin_home_page',
			array(
				'section'	  => 'contact_detail_settings',
				'label'		  => __( 'Contact details as in Home page', 'preschool-and-kindergarten-pro' ),
				'description' => __( 'Enable to display contact details used in home page.', 'preschool-and-kindergarten-pro' ),
			)
		)
	);
    
    /** Contact Address  */
    $wp_customize->add_setting(
        'contact_address',
        array(
            'default'           => '',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'contact_address',
        array(
            'label'       => __( 'Contact Address', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Enter the contact address.', 'preschool-and-kindergarten-pro' ),
            'section'     => 'contact_detail_settings',
            'type'        => 'text',
            'active_callback'  => 'preschool_and_kindergarten_pro_contact_details'                                    
        )
    );
    
    /** Contact Phone  */
    $wp_customize->add_setting(
        'contact_phone',
        array(
            'default'           => '',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'contact_phone',
        array(
            'label'       => __( 'Contact Phone', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Enter the contact phone. For Multiple Phone numbers seperate with comas. e.g, +977- 9876543210, +977-98410358', 'preschool-and-kindergarten-pro' ),
            'section'     => 'contact_detail_settings',
            'type'        => 'text',
            'active_callback'  => 'preschool_and_kindergarten_pro_contact_details'                                    
        )
    );
    
    /** Contact Email  */
    $wp_customize->add_setting(
        'contact_email',
        array(
            'default'           => '',
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    
    $wp_customize->add_control(
        'contact_email',
        array(
            'label'       => __( 'Contact Email', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Enter the contact email. For Multiple Emails seperate with comas. e.g, info@test.com, info@schoolpro.com', 'preschool-and-kindergarten-pro' ),
            'section'     => 'contact_detail_settings',
            'type'        => 'text', 
            'active_callback'  => 'preschool_and_kindergarten_pro_contact_details'
                                   
        )
    );
        
    /** Drop Down of CF7 only if plugin enabled */
    if( preschool_and_kindergarten_pro_is_cf7_activated() ){
        
        /** Banner Form */
        $wp_customize->add_setting(
    		'contact_form',
    		array(
    			'default'			=> '',
    			'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select'
    		)
    	);
    
    	$wp_customize->add_control(
    		new Rara_Controls_Select_Control(
        		$wp_customize,
        		'contact_form',
        		array(
                    'label'       => __( 'Contact Form', 'preschool-and-kindergarten-pro' ),
                    'section'     => 'contact_detail_settings',
        			'choices'     => preschool_and_kindergarten_pro_get_posts( 'wpcf7_contact_form' ),                    	
         		)
    		)
    	);
        
    }else{
        
        $wp_customize->add_setting(
    		'contact_form_note',
    		array(
    			'sanitize_callback' => 'wp_kses_post'
    		)
    	);
    
    	$wp_customize->add_control(
    		new Rara_Controls_Info_Text( 
    			$wp_customize,
    			'contact_form_note',
    			array(
    				'section' => 'contact_detail_settings', 
                    'label'       => __( 'Contact Form', 'preschool-and-kindergarten-pro' ),   				
                    'description' => __( 'Please install Contact Form 7 Plugin to show form in contact page.', 'preschool-and-kindergarten-pro' ),
    			)
    		)
	   );
        
    }
            
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_contact_detail' );

/**
 * Active Callback for Email and Phone
*/
function preschool_and_kindergarten_pro_contact_details( $control ){
    
    $ed_same    = $control->manager->get_setting( 'ed_asin_home_page' )->value();
    $control_id = $control->id;
    
    if ( $control_id == 'contact_phone' && $ed_same != '1' ) return true;
    if ( $control_id == 'contact_email' && $ed_same != '1' ) return true;
    if ( $control_id == 'contact_address' && $ed_same != '1' ) return true;
    return false;
}