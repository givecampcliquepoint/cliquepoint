<?php
/**
 * Home Page video Section Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_about_video( $wp_customize ) {
    
    global $preschool_and_kindergarten_pro_options_pages;
    
    /** video Section */
    $wp_customize->add_section( 'video_settings', array(
        'title' => __( 'Video Section', 'preschool-and-kindergarten-pro' ),
        'priority' => 10,
        'panel' => 'about_page_settings',
    ) );
    
    /** Select video Post */
    $wp_customize->add_setting(
        'about_video',
        array(
            'default' => '',
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    
    $wp_customize->add_control( 
            'about_video',
            array(
                'label' => __( 'Embed Video Link', 'preschool-and-kindergarten-pro'),
                'description' => __( 'Copy and paste the video links here. e.g. https://vimeo.com/138114461, https://www.youtube.com/watch?v=Jwgf3wmiA04', 'preschool-and-kindergarten-pro' ),               
                'section' => 'video_settings',
                'type'   => 'text',
            )
    );
   
 }
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_about_video' );