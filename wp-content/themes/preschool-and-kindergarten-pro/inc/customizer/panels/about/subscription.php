<?php 
/**
 * About Page subscription Section Options
 *
 * @package preschool_and_kindergarten_pro
 */

    function preschool_and_kindergarten_pro_customize_register_about_subscription_now( $wp_customize ) {
            
        if( is_jetpack_subscription_module_active() ){     
            
            $wp_customize->add_section(
                'about_subscription_settings',
                array(
                    'title' => __( 'Subscription Section', 'preschool-and-kindergarten-pro' ),
                    'priority' => 12,
                    'panel' => 'about_page_settings',
                )
            ); 

            /** subscription section */
            $wp_customize->add_setting(
                'ed_subscription_on_about',
                array(
                    'default'           => '',
                    'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
                )
            );
            
            $wp_customize->add_control(
                new Rara_Controls_Toggle_Control( 
                    $wp_customize,
                    'ed_subscription_on_about',
                    array(
                        'section'       => 'about_subscription_settings',
                        'label'         => __( 'Subscription Form', 'preschool-and-kindergarten-pro' ),
                        'description'   => __( 'Enable to show subscription Form.', 'preschool-and-kindergarten-pro' ),
                    )
                )
            );
        }
    }

    add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_about_subscription_now' );

?>