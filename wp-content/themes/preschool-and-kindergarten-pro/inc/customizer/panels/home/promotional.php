<?php 
/**
* CTA Section Theme Option.
*
*@Package preschool_and_kindergarten_Pro
*/

 function preschool_and_kindergarten_pro_customize_register_home_promotional( $wp_customize ) {
  
     /** cta Section */
    $wp_customize->add_section(
        'promotional_settings',
        array(
            'title' => __( 'Promotional Section', 'preschool-and-kindergarten-pro' ),
            'priority' => 14,
            'panel' => 'home_page_settings',
        )
    );
    
    
    $wp_customize->add_setting(
        'preschool_and_kindergarten_promotional_section_title',
        array(
            'default' => __( 'Summer Camp Special', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'preschool_and_kindergarten_promotional_section_title',
        array(
            'label' => __( 'Section Title', 'preschool-and-kindergarten-pro' ),
            'section' => 'promotional_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting(
        'preschool_and_kindergarten_promotional_section_description',
        array(
            'default' => __( 'The traditional view of a summer camp as a woody place with hiking, canoeing, and campfires is evolving, with greater acceptance of newer summer camps that offer a wide variety of specialized activities.', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    
    $wp_customize->add_control(
        'preschool_and_kindergarten_promotional_section_description',
        array(
            'label' => __( 'Section Description', 'preschool-and-kindergarten-pro' ),
            'section' => 'promotional_settings',
            'type' => 'textarea',
        )
    );


    $wp_customize->add_setting(
        'preschool_and_kindergarten_button_label',
        array(
            'default' => 'View Details',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'preschool_and_kindergarten_button_label',
        array(
            'label' => __( 'CTA Button Label', 'preschool-and-kindergarten-pro' ),
            'section' => 'promotional_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting(
        'preschool_and_kindergarten_button_link',
        array(
            'default' => __( '#', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    
    $wp_customize->add_control(
        'preschool_and_kindergarten_button_link',
        array(
            'label' => __( 'CTA Button Link', 'preschool-and-kindergarten-pro' ),
            'section' => 'promotional_settings',
            'type' => 'text',
        )
    );
    /** Background Image */
    $wp_customize->add_setting(
        'promotional_bg_image',
        array(
            'default'           => get_template_directory_uri() . '/images/demo/img47.jpg',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_image',
        )
    );
    
    $wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'promotional_bg_image',
           array(
               'label'       => __( 'Background Image', 'preschool-and-kindergarten-pro' ),
               'description' => __( 'Upload background image for Promotional section.', 'preschool-and-kindergarten-pro' ),
               'section'     => 'promotional_settings',               
           )
       )
    );
  
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_home_promotional' );
