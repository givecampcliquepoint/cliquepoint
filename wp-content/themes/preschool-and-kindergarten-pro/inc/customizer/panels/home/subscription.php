<?php 
/**
 * Home Page subscription Section Options
 *
 * @package preschool_and_kindergarten_pro
 */

    function preschool_and_kindergarten_pro_customize_register_home_subscription_now( $wp_customize ) {
            
        if( ! is_jetpack_subscription_module_active() ){     
            
            $wp_customize->add_section(
                'subscription_settings',
                array(
                    'title' => __( 'Subscription Section', 'preschool-and-kindergarten-pro' ),
                    'priority' => 12,
                    'panel' => 'home_page_settings',
                )
            ); 

            $wp_customize->add_setting(
                'subscription_section_note',
                array(
                    'sanitize_callback' => 'wp_kses_post'
                )
            );
        
            $wp_customize->add_control(
                new Rara_Controls_Info_Text( 
                    $wp_customize,
                    'subscription_section_note',
                    array(
                        'section'      => 'subscription_settings', 
                        'description'  => sprintf( __( 'Please install/activate the recommended %1$sJetpack by WordPress.com%2$s plugin and enable subscription module. Then goto Appearance > Customize > Home Page Settings > Subscription Section Widget and click on Add a Widget and select Blog Subscriptions(Jetpack) widget. Fill in the details.', 'preschool-and-kindergarten-pro' ), '<a href="' . admin_url( 'themes.php?page=tgmpa-install-plugins' ) . '" target="_blank">', '</a>' ),
                    )
                )
            );
        }
    }

    add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_home_subscription_now' );

?>