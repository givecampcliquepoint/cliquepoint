<?php
/**
 * Home Page Gallery Section Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_home_gallery( $wp_customize ) {
    
    $wp_customize->add_section(
        'gallery_settings',
        array(
            'title' => __( 'Gallery Section', 'preschool-and-kindergarten-pro' ),
            'priority' => 18,
            'panel' => 'home_page_settings',
        )
    );
    /** Menu Label */
    $wp_customize->add_setting(
        'ed_section_menu_gallery',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'ed_section_menu_gallery',
            array(
                'section'       => 'gallery_settings',
                'label'         => __( 'Section Menu', 'preschool-and-kindergarten-pro' ),
                'description'   => __( 'Disable to hide the section in the menu.', 'preschool-and-kindergarten-pro' ),
                'active_callback' => 'preschool_and_kindergarten_pro_section_menu_ac'
            )
        )
    );
    
    /** Menu Label  */
    $wp_customize->add_setting(
        'section_menu_gallery',
        array(
            'default'           => __( 'Gallery', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'section_menu_gallery',
        array(
            'label'       => __( 'Gallery Section Menu Label', 'preschool-and-kindergarten-pro' ),
            'description' => __( '', 'preschool-and-kindergarten-pro' ),
            'section'     => 'gallery_settings',
            'type'        => 'text',
            'active_callback' => 'preschool_and_kindergarten_pro_section_menu_ac'
        )
    ); 

    /** Section Title */
    $wp_customize->add_setting(
        'gallery_section_title',
        array(
            'default'           => __( 'Photo Gallery', 'preschool-and-kindergarten-pro'),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'gallery_section_title',
        array(
            'label'   => __( 'Section Title', 'preschool-and-kindergarten-pro' ),
            'section' => 'gallery_settings',
            'type'    => 'text',
        )
    );

    /** Section Description */
    $wp_customize->add_setting(
        'gallery_section_description',
        array(
            'default'           => __( 'Gallery is a building or space for the exhibition of art, usually visual art.', 'preschool-and-kindergarten-pro'),
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    
    $wp_customize->add_control(
        'gallery_section_description',
        array(
            'label'   => __( 'Section Description', 'preschool-and-kindergarten-pro' ),
            'section' => 'gallery_settings',
            'type'    => 'textarea',
        )
    );

    $wp_customize->add_setting(
        'choose_gallery_images',
        array(
            'default' => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Gallery_Control(
            $wp_customize,
            'choose_gallery_images',
            array(
                'label' => __( 'Add Image Gallery', 'preschool-and-kindergarten-pro'),
                'section' => 'gallery_settings',
            )
        )
    );

    /** Gallery slogan */
    $wp_customize->add_setting(
        'gallery_section_slogan',
        array(
            'default'           => __( 'See Our Kindergarten Photo Gallery', 'preschool-and-kindergarten-pro'),
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    
    $wp_customize->add_control(
        'gallery_section_slogan',
        array(
            'label'   => __( 'Gallery Slogan', 'preschool-and-kindergarten-pro' ),
            'section' => 'gallery_settings',
            'type'    => 'textarea',
        )
    );

    /**  View All gallery Label */
    $wp_customize->add_setting(
        'gallery_viewall_label',
        array(
            'default' => __( 'View Gallery', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control( 
        'gallery_viewall_label',
        array(
            'label'   => __( 'Button Label', 'preschool-and-kindergarten-pro' ),
            'section' => 'gallery_settings',
            'description' => __( 'CTA button label to view gallery page', 'preschool-and-kindergarten-pro'),
            'type'    => 'text',
        )
    );

    /**  View All gallery Link  */
        $wp_customize->add_setting(
        'gallery_viewall_link',
        array(
            'default' => '#',
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    $wp_customize->add_control( 
        'gallery_viewall_link',
        array(
            'label'   => __( 'Button Link', 'preschool-and-kindergarten-pro' ),
            'section' => 'gallery_settings',
            'description' => __( 'CTA button link to view gallery page', 'preschool-and-kindergarten-pro'),
            'type'    => 'text',
        )
    );
      
    /** Gallery Section Ends */  
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_home_gallery' );