<?php
/**
 * Home Page testimonial Section Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_home_testimonial( $wp_customize ) {
        
    $wp_customize->add_section(
        'testimonial_settings',
        array(
            'title' => __( 'Testimonial Section', 'preschool-and-kindergarten-pro' ),
            'priority' => 17,
            'panel' => 'home_page_settings',
        )
    ); 

    /** Menu Label */
    $wp_customize->add_setting(
        'ed_section_menu_testimonial',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'ed_section_menu_testimonial',
            array(
                'section'       => 'testimonial_settings',
                'label'         => __( 'Section Menu', 'preschool-and-kindergarten-pro' ),
                'description'   => __( 'Disable to hide the section in the menu.', 'preschool-and-kindergarten-pro' ),
                'active_callback' => 'preschool_and_kindergarten_pro_section_menu_ac'
            )
        )
    );
    
    /** Menu Label  */
    $wp_customize->add_setting(
        'section_menu_testimonial',
        array(
            'default'           => __( 'Testimonial', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'section_menu_testimonial',
        array(
            'label'       => __( 'Testimonial Section Menu Label', 'preschool-and-kindergarten-pro' ),
            'description' => __( '', 'preschool-and-kindergarten-pro' ),
            'section'     => 'testimonial_settings',
            'type'        => 'text',
            'active_callback' => 'preschool_and_kindergarten_pro_section_menu_ac'
        )
    ); 

    /** Section Title */
    $wp_customize->add_setting(
        'preschool_and_kindergarten_testimonials_section_title',
        array(
            'default'           => __( 'What Parent&rsquo;s Says', 'preschool-and-kindergarten-pro'),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'preschool_and_kindergarten_testimonials_section_title',
        array(
            'label'   => __( 'Section Title', 'preschool-and-kindergarten-pro' ),
            'section' => 'testimonial_settings',
            'type'    => 'text',
        )
    );
    
    /** Section Description */
    $wp_customize->add_setting(
        'preschool_and_kindergarten_testimonials_section_description',
        array(
            'default'           => __( 'Testimonial or show consists of a person&rsquo;s written or spoken statement extolling the virtue of a product.', 'preschool-and-kindergarten-pro'),
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    
    $wp_customize->add_control(
        'preschool_and_kindergarten_testimonials_section_description',
        array(
            'label'   => __( 'Section Description', 'preschool-and-kindergarten-pro' ),
            'section' => 'testimonial_settings',
            'type'    => 'textarea',
        )
    );

    $wp_customize->add_setting(
        'preschool_and_kindergarten_testimonials_autoplay_slider',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'preschool_and_kindergarten_testimonials_autoplay_slider',
            array(
                'section'       => 'testimonial_settings',
                'label'         => __( 'Testimonial Slider Autoplay', 'preschool-and-kindergarten-pro' ),
            )
        )
    );

    if( is_rara_toolkit_activated() ){

    /** number of testimonial */
    $wp_customize->add_setting(
        'number_of_testimonial',
        array(
            'default' => '3',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Slider_Control(
            $wp_customize,
            'number_of_testimonial',
            array(
                'label' => __( 'Testmonials Number', 'preschool-and-kindergarten-pro'),
                'section' => 'testimonial_settings',
                'description' => __( 'Selected number of testimonials will display in testimonial section.', 'preschool-and-kindergarten-pro' ),                
                'choices'  => array(
                        'min'  => 1,
                        'max'  => 10,
                        'step' => 1,
                    )
            )
        )
    );

    /** Testimonial Order  */
    $wp_customize->add_setting(
        'testimonial_order_type',
        array(
            'default'           => 'date',
            'sanitize_callback' => 'esc_attr',
        )
    );
   
    /** Testimonial Order */
    $wp_customize->add_control( 
        new Rara_Controls_Radio_Buttonset_Control( 
            $wp_customize,
            'testimonial_order_type', 
            array(
                'type'       => 'radio',
                'label'      => __( 'Testimonial Order', 'preschool-and-kindergarten-pro' ),
                'help'       => __( 'Choose testimonial order.', 'preschool-and-kindergarten-pro' ),
                'section'    => 'testimonial_settings',
                'default'    => 'date',
                'choices'    => array(
                    'date'       => __( 'Post Date', 'preschool-and-kindergarten-pro' ),
                    'menu_order' => __( 'Menu Order', 'preschool-and-kindergarten-pro' ),
                )
            )  
        )
    );

    /**  View All testimonial Label */
    $wp_customize->add_setting(
        'testimonial_viewall_label',
        array(
            'default' => __( 'View All Testimonials', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control( 
        'testimonial_viewall_label',
        array(
            'label'   => __( 'Button Label', 'preschool-and-kindergarten-pro' ),
            'section' => 'testimonial_settings',
            'description' => __( 'CTA button label to view testimonial listing page', 'preschool-and-kindergarten-pro'),
            'type'    => 'text',
        )
    );
    
    }else{
        $wp_customize->add_setting(
            'testimonial_section_note',
            array(
                'sanitize_callback' => 'wp_kses_post'
            )
        );
    
        $wp_customize->add_control(
            new Rara_Controls_Info_Text( 
                $wp_customize,
                'testimonial_section_note',
                array(
                    'section'      => 'testimonial_settings', 
                    'description' => sprintf( __( 'Please install/activate the %1$sRara Theme Tool Kit Pro%2$s which you have received along with this theme for setting related with Testimonial post type.', 'preschool-and-kindergarten-pro' ), '<a href="' . admin_url( 'themes.php?page=tgmpa-install-plugins' ) . '" target="_blank">', '</a>' ),
                )
            )
        );
    }

    /** testimonial Section Ends */   
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_home_testimonial' );