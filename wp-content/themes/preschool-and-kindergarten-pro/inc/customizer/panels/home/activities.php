<?php 
/*
* Activities Section Theme Option.
*
* @Package  Preschool and Kindergarten Pro
*/

 function preschool_and_kindergarten_pro_customize_register_activities( $wp_customize ) {
    
    /** Activities Section */

    $wp_customize->add_section(
        'activities_settings',
        array(
            'title' => __( 'Activities Section', 'preschool-and-kindergarten-pro' ),
            'priority' => 11,
            'panel' => 'home_page_settings',
        )
    );

    /** Menu Label */
    $wp_customize->add_setting(
        'ed_section_menu_activities',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'ed_section_menu_activities',
            array(
                'section'       => 'activities_settings',
                'label'         => __( 'Section Menu', 'preschool-and-kindergarten-pro' ),
                'description'   => __( 'Disable to hide the section in the menu.', 'preschool-and-kindergarten-pro' ),
                'active_callback' => 'preschool_and_kindergarten_pro_section_menu_ac'
            )
        )
    );
    
    /** Menu Label  */
    $wp_customize->add_setting(
        'section_menu_activities',
        array(
            'default'           => __( 'Activities', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'section_menu_activities',
        array(
            'label'       => __( 'Activities Section Menu Label', 'preschool-and-kindergarten-pro' ),
            'description' => __( '', 'preschool-and-kindergarten-pro' ),
            'section'     => 'activities_settings',
            'type'        => 'text',
            'active_callback' => 'preschool_and_kindergarten_pro_section_menu_ac'
        )
    );
    

   $wp_customize->add_setting(
        'preschool_and_kindergarten_lessons_section_title',
        array(
            'default' => __( 'Lessons thats sparks Creativity', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'preschool_and_kindergarten_lessons_section_title',
        array(
            'label' => __( 'Section Title', 'preschool-and-kindergarten-pro' ),
            'section' => 'activities_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting(
        'preschool_and_kindergarten_lessons_section_description',
        array(
            'default' => __( 'Creativity is a phenomenon whereby something new and different is formed.', 'preschool-and-kindergarten-pro'),
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    
    $wp_customize->add_control(
        'preschool_and_kindergarten_lessons_section_description',
        array(
            'label' => __( 'Section Description', 'preschool-and-kindergarten-pro' ),
            'section' => 'activities_settings',
            'type' => 'textarea',
        )
    );

    $wp_customize->add_setting(
        'preschool_and_kindergarten_lesson_post_one',
        array(
            'default' => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
         new Rara_Controls_Select_Control(
            $wp_customize,
        'preschool_and_kindergarten_lesson_post_one',
            array(
                'label' => __( 'Select Post/Page', 'preschool-and-kindergarten-pro' ),
                'section' => 'activities_settings',
                'choices' => preschool_and_kindergarten_pro_get_posts( array( 'post', 'page' ) ),
            )
        )
    );


    $wp_customize->add_setting(
        'preschool_and_kindergarten_lesson_post_two',
        array(
            'default' => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
         new Rara_Controls_Select_Control(
            $wp_customize,
        'preschool_and_kindergarten_lesson_post_two',
            array(
                'label' => __( 'Select Post/Page', 'preschool-and-kindergarten-pro' ),
                'section' => 'activities_settings',
                'choices' => preschool_and_kindergarten_pro_get_posts( array( 'post', 'page' ) ),
            )
        )
    );


    $wp_customize->add_setting(
        'preschool_and_kindergarten_lesson_post_three',
        array(
            'default' => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
         new Rara_Controls_Select_Control(
            $wp_customize,
        'preschool_and_kindergarten_lesson_post_three',
            array(
                'label' => __( 'Select Post/Page', 'preschool-and-kindergarten-pro' ),
                'section' => 'activities_settings',
                'choices' => preschool_and_kindergarten_pro_get_posts( array( 'post', 'page' ) ),
            )
        )
    );
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_activities');