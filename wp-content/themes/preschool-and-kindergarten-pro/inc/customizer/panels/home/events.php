<?php
/**
 * Home Page events Section Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_home_events( $wp_customize ) {
        
    $wp_customize->add_section(
        'events_settings',
        array(
            'title' => __( 'Events Section', 'preschool-and-kindergarten-pro' ),
            'priority' => 13,
            'panel' => 'home_page_settings',
        )
    );

    /** Menu Label */
    $wp_customize->add_setting(
        'ed_section_menu_events',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'ed_section_menu_events',
            array(
                'section'       => 'events_settings',
                'label'         => __( 'Section Menu', 'preschool-and-kindergarten-pro' ),
                'description'   => __( 'Disable to hide the section in the menu.', 'preschool-and-kindergarten-pro' ),
                'active_callback' => 'preschool_and_kindergarten_pro_section_menu_ac'
            )
        )
    );
    
    /** Menu Label  */
    $wp_customize->add_setting(
        'section_menu_events',
        array(
            'default'           => __( 'Events', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'section_menu_events',
        array(
            'label'       => __( 'Events Section Menu Label', 'preschool-and-kindergarten-pro' ),
            'description' => __( '', 'preschool-and-kindergarten-pro' ),
            'section'     => 'events_settings',
            'type'        => 'text',
            'active_callback' => 'preschool_and_kindergarten_pro_section_menu_ac'
        )
    );
    

    /** Section Title */
    $wp_customize->add_setting(
        'events_section_title',
        array(
            'default'           => __( 'Latest Events', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'events_section_title',
        array(
            'label'   => __( 'Section Title', 'preschool-and-kindergarten-pro' ),
            'section' => 'events_settings',
            'type'    => 'text',
        )
    );
    
    /** Section Description */
    $wp_customize->add_setting(
        'events_section_description',
        array(
            'default'           => __( 'A ceremony is an event of ritual significance, performed on a special occasion.', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    
    $wp_customize->add_control(
        'events_section_description',
        array(
            'label'   => __( 'Section Description', 'preschool-and-kindergarten-pro' ),
            'section' => 'events_settings',
            'type'    => 'textarea',
        )
    );
    if( is_rara_toolkit_activated() ){
    
    /** Post One */
    $wp_customize->add_setting(
        'events_post_one',
        array(
            'default' => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Select_Control(
            $wp_customize,
            'events_post_one',
            array(
                'label' => __( 'Select Event One', 'preschool-and-kindergarten-pro'),
                'description' => __( 'Select events from the dropdown. To create a new event, go to Dashboard >> Events >> Add New.', 'preschool-and-kindergarten-pro' ),                
                'section' => 'events_settings',
                'choices' => preschool_and_kindergarten_pro_get_posts( 'event' ),
            )
        )
    );

    /** Post Two */
    $wp_customize->add_setting(
        'events_post_two',
        array(
            'default' => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
        
    $wp_customize->add_control(
        new Rara_Controls_Select_Control(
            $wp_customize,
            'events_post_two',
            array(
                'label' => __( 'Select Event Two', 'preschool-and-kindergarten-pro'),
                'section' => 'events_settings',
                'choices' => preschool_and_kindergarten_pro_get_posts( 'event' ),
            )
        )
    );

    /** Post Three */
    $wp_customize->add_setting(
        'events_post_three',
        array(
            'default' => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Select_Control(
            $wp_customize,
            'events_post_three',
            array(
                'label' => __( 'Select Event Three', 'preschool-and-kindergarten-pro'),
                'section' => 'events_settings',
                'choices' => preschool_and_kindergarten_pro_get_posts( 'event' ),
            )
        )
    );


    /**  View All events Label */
    $wp_customize->add_setting(
        'events_viewall_label',
        array(
            'default' => __( 'View All', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control( 
        'events_viewall_label',
        array(
            'label'   => __( 'Button Label', 'preschool-and-kindergarten-pro' ),
            'section' => 'events_settings',
            'description' => __( 'CTA button label to view events listing page', 'preschool-and-kindergarten-pro'),
            'type'    => 'text',
        )
    );

    }else{
        $wp_customize->add_setting(
            'event_section_note',
            array(
                'sanitize_callback' => 'wp_kses_post'
            )
        );
    
        $wp_customize->add_control(
            new Rara_Controls_Info_Text( 
                $wp_customize,
                'event_section_note',
                array(
                    'section'      => 'events_settings', 
                    'description' => sprintf( __( 'Please install/activate the %1$sRara Theme Tool Kit Pro%2$s which you have received along with this theme for setting related with Event post type.', 'preschool-and-kindergarten-pro' ), '<a href="' . admin_url( 'themes.php?page=tgmpa-install-plugins' ) . '" target="_blank">', '</a>' ),
                )
            )
        );
    }
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_home_events' );