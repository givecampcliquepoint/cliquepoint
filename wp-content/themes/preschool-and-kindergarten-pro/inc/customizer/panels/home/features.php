<?php 
/*
* features Section Theme Option.
*
* @Package  preschool_and_kindergarten
*/

 function preschool_and_kindergarten_customize_register_features( $wp_customize ) {
    
    /** features Section */
    $wp_customize->add_section(
        'features_settings',
        array(
            'title' => __( 'Features Section', 'preschool-and-kindergarten-pro' ),
            'priority' => 13,
            'panel' => 'home_page_settings',
        )
    );

    /** Menu Label */
    $wp_customize->add_setting(
        'ed_section_menu_features',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'ed_section_menu_features',
            array(
                'section'       => 'features_settings',
                'label'         => __( 'Section Menu', 'preschool-and-kindergarten-pro' ),
                'description'   => __( 'Disable to hide the section in the menu.', 'preschool-and-kindergarten-pro' ),
                'active_callback' => 'preschool_and_kindergarten_pro_section_menu_ac'
            )
        )
    );
    
    /** Menu Label  */
    $wp_customize->add_setting(
        'section_menu_features',
        array(
            'default'           => __( 'Features', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'section_menu_features',
        array(
            'label'       => __( 'features Section Menu Label', 'preschool-and-kindergarten-pro' ),
            'description' => __( '', 'preschool-and-kindergarten-pro' ),
            'section'     => 'features_settings',
            'type'        => 'text',
            'active_callback' => 'preschool_and_kindergarten_pro_section_menu_ac'
        )
    );
    

   $wp_customize->add_setting(
        'preschool_and_kindergarten_features_section_title',
        array(
            'default' => __( 'Relax at your home', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'preschool_and_kindergarten_features_section_title',
        array(
            'label' => __( 'Section Title', 'preschool-and-kindergarten-pro' ),
            'section' => 'features_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting(
        'preschool_and_kindergarten_features_section_description',
        array(
            'default' => __( 'It requires little effort and can be done.', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    
    $wp_customize->add_control(
        'preschool_and_kindergarten_features_section_description',
        array(
            'label' => __( 'Section Description', 'preschool-and-kindergarten-pro' ),
            'section' => 'features_settings',
            'type' => 'textarea',
        )
    );

     /** features Post One */
    $wp_customize->add_setting(
        'preschool_and_kindergarten_features_post',
        array(
            'default' => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Select_Control(
        $wp_customize,
        'preschool_and_kindergarten_features_post',
            array(
                'label' => __( 'Select Post/Page', 'preschool-and-kindergarten-pro' ),
                'section' => 'features_settings',
                'choices' => preschool_and_kindergarten_pro_get_posts( array( 'post', 'page' ) ),
            )
        )
    );

    /**  View All features Label */
    $wp_customize->add_setting(
        'features_viewall_label',
        array(
            'default' => __( 'View Details', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control( 
        'features_viewall_label',
        array(
            'label'   => __( 'Button Label', 'preschool-and-kindergarten-pro' ),
            'section' => 'features_settings',
            'type'    => 'text',
        )
    );

    /**  View All features Link  */
        $wp_customize->add_setting(
        'features_viewall_link',
        array(
            'default' => '#',
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    $wp_customize->add_control( 
        'features_viewall_link',
        array(
            'label'   => __( 'Button Link', 'preschool-and-kindergarten-pro' ),
            'section' => 'features_settings',
            'type'    => 'text',
        )
    );

}
add_action( 'customize_register', 'preschool_and_kindergarten_customize_register_features' );
