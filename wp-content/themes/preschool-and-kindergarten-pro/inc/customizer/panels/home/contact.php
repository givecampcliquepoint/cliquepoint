<?php
/**
 * Home Page Contact Section Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_home_contact( $wp_customize ) {
    
    $wp_customize->add_section(
        'contact_settings',
        array(
            'title' => __( 'Contact Section', 'preschool-and-kindergarten-pro' ),
            'priority' => 21,
            'panel' => 'home_page_settings',
        )
    ); 

    /** Menu Label */
    $wp_customize->add_setting(
        'ed_section_menu_contact',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'ed_section_menu_contact',
            array(
                'section'       => 'contact_settings',
                'label'         => __( 'Section Menu', 'preschool-and-kindergarten-pro' ),
                'description'   => __( 'Disable to hide the section in the menu.', 'preschool-and-kindergarten-pro' ),
                'active_callback' => 'preschool_and_kindergarten_pro_section_menu_ac'
            )
        )
    );
    
    /** Menu Label  */
    $wp_customize->add_setting(
        'section_menu_contact',
        array(
            'default'           => __( 'Contact', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'section_menu_contact',
        array(
            'label'       => __( 'Contact Section Menu Label', 'preschool-and-kindergarten-pro' ),
            'description' => __( '', 'preschool-and-kindergarten-pro' ),
            'section'     => 'contact_settings',
            'type'        => 'text',
            'active_callback' => 'preschool_and_kindergarten_pro_section_menu_ac'
        )
    ); 

    /** Section Title */
    $wp_customize->add_setting(
        'contact_section_title',
        array(
            'default'           => __( 'Contact Us', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'contact_section_title',
        array(
            'label'   => __( 'Section Title', 'preschool-and-kindergarten-pro' ),
            'section' => 'contact_settings',
            'type'    => 'text',
        )
    );
    
    /** Section Description */
    $wp_customize->add_setting(
        'contact_section_description',
        array(
            'default'           => __( 'Your visitors can easily reach you through this contact section.', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    
    $wp_customize->add_control(
        'contact_section_description',
        array(
            'label'   => __( 'Section Description', 'preschool-and-kindergarten-pro' ),
            'section' => 'contact_settings',
            'type'    => 'textarea',
        )
    );
    
     /** contact Title */
    $wp_customize->add_setting(
        'contact_title',
        array(
            'default'           => __( 'Get in touch', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'contact_title',
        array(
            'label'   => __( 'Contact Title', 'preschool-and-kindergarten-pro' ),
            'section' => 'contact_settings',
            'type'    => 'text',
        )
    );
    

    /** Contact Description */
    $wp_customize->add_setting(
        'contact_section_contact_description',
        array(
            'default'           => __( 'Communication is the act of conveying intended meanings from one entity or group to another through the use of mutually understood signs and semiotic rules.', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    
    $wp_customize->add_control(
        'contact_section_contact_description',
        array(
            'label'   => __( 'Contact Description', 'preschool-and-kindergarten-pro' ),
            'section' => 'contact_settings',
            'type'    => 'textarea',
        )
    );

    /* address */
    $wp_customize->add_setting(
        'home_contact_section_address',
        array(
            'default'           => __( 'Address Here', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'home_contact_section_address',
        array(
            'label'   => __( 'Address', 'preschool-and-kindergarten-pro' ),
            'section' => 'contact_settings',
            'type'    => 'text',
        )
    );
    $wp_customize->add_setting(
        'contact_phone_email_as_header',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'contact_phone_email_as_header',
            array(
                'section'       => 'contact_settings',
                'label'         => __( 'Email and Phone number same as in Header section', 'preschool-and-kindergarten-pro' ),
                'description'   => __( 'Enabling will use email and phone number from Header section (Header Settings / Misc Settings)', 'preschool-and-kindergarten-pro' ),
            )
        )
    );

      /**phone number */
    $wp_customize->add_setting(
        'home_contact_section_phone',
        array(
            'default'           => '',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'home_contact_section_phone',
        array(
            'label'   => __( 'Phone Number', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Enter the contact phone. For Multiple Phone numbers seperate with comas. e.g, +977- 9876543210, +977-98410358', 'preschool-and-kindergarten-pro' ),
            'section' => 'contact_settings',
            'type'    => 'text',
            'active_callback' => 'preschool_and_kindergarten_pro_contact_email_phone'
        )
    );

      /** email */
    $wp_customize->add_setting(
        'home_contact_section_email',
        array(
            'default'           => '',
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    
    $wp_customize->add_control(
        'home_contact_section_email',
        array(
            'label'   => __( 'Email', 'preschool-and-kindergarten-pro' ),
            'description' => __( 'Enter the contact email. For Multiple Emails seperate with comas. e.g, info@test.com, info@schoolpro.com', 'preschool-and-kindergarten-pro' ),
            'section' => 'contact_settings',
            'type'    => 'text',
            'active_callback' => 'preschool_and_kindergarten_pro_contact_email_phone'
        )
    );

    /** contact Section Ends */   
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_home_contact' );

/**
 * Active Callback for Email and Phone
*/
function preschool_and_kindergarten_pro_contact_email_phone( $control ){
    
    $ed_same    = $control->manager->get_setting( 'contact_phone_email_as_header' )->value();
    $control_id = $control->id;
    
    if ( $control_id == 'home_contact_section_phone' && $ed_same != '1' ) return true;
    if ( $control_id == 'home_contact_section_email' && $ed_same != '1' ) return true;    
   
    return false;
}