<?php
/**
 * Home Page welcome Section Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_home_welcome( $wp_customize ) {
    
    
    /** welcome Section */
    $wp_customize->add_section( 'welcome_settings', array(
        'title' => __( 'Welcome Section', 'preschool-and-kindergarten-pro' ),
        'priority' => 10,
        'panel' => 'home_page_settings',
    ) );

    /** Menu Label */
    $wp_customize->add_setting(
        'ed_section_menu_about',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'ed_section_menu_about',
            array(
                'section'       => 'welcome_settings',
                'label'         => __( 'Section Menu', 'preschool-and-kindergarten-pro' ),
                'description'   => __( 'Disable to hide the section in the menu.', 'preschool-and-kindergarten-pro' ),
                'active_callback' => 'preschool_and_kindergarten_pro_section_menu_ac'
            )
        )
    );
    
    /** Menu Label  */
    $wp_customize->add_setting(
        'section_menu_about',
        array(
            'default'           => __( 'About', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'section_menu_about',
        array(
            'label'       => __( 'About Section Menu Label', 'preschool-and-kindergarten-pro' ),
            'description' => __( '', 'preschool-and-kindergarten-pro' ),
            'section'     => 'welcome_settings',
            'type'        => 'text',
            'active_callback' => 'preschool_and_kindergarten_pro_section_menu_ac'
        )
    );
    
    /** Select Welcome Post */
    $wp_customize->add_setting(
        'preschool_and_kindergarten_about_page',
        array(
            'default' => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control( 
        new Rara_Controls_Select_Control(
            $wp_customize,
            'preschool_and_kindergarten_about_page',
            array(
                'label' => __( 'Select Welcome Page', 'preschool-and-kindergarten-pro'),
                'section' => 'welcome_settings',
                'choices' => preschool_and_kindergarten_pro_get_posts( 'page' ),
            )
        )
    );
   
    /** Read More Text */
    $wp_customize->add_setting(
        'welcome_read_more',
        array(
            'default'=> __( 'More About Us', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback'=> 'sanitize_text_field'
            )
        );
    
    $wp_customize->add_control(
        'welcome_read_more',
        array(
              'label' => __( 'Read More Text', 'preschool-and-kindergarten-pro' ),
              'section' => 'welcome_settings', 
              'type' => 'text',
            ));
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_home_welcome' );

/**
 * Active Callback for display menu label in each home page section
*/
function preschool_and_kindergarten_pro_section_menu_ac( $control ){
    
    $control_id              = $control->id;
    $ed_sec_menu             = $control->manager->get_setting( 'ed_section_menu' )->value();
    $ed_sec_menu_about       = $control->manager->get_setting( 'ed_section_menu_about' )->value();
    $ed_sec_menu_activities  = $control->manager->get_setting( 'ed_section_menu_activities' )->value();
    $ed_sec_menu_events      = $control->manager->get_setting( 'ed_section_menu_events' )->value();
    $ed_sec_menu_features    = $control->manager->get_setting( 'ed_section_menu_features' )->value();
    $ed_sec_menu_courses     = $control->manager->get_setting( 'ed_section_menu_courses' )->value();
    $ed_sec_menu_testimonial = $control->manager->get_setting( 'ed_section_menu_testimonial' )->value();
    $ed_sec_menu_team        = $control->manager->get_setting( 'ed_section_menu_team' )->value();
    $ed_sec_menu_gallery     = $control->manager->get_setting( 'ed_section_menu_gallery' )->value();
    $ed_sec_menu_blog        = $control->manager->get_setting( 'ed_section_menu_blog' )->value();
    $ed_sec_menu_contact     = $control->manager->get_setting( 'ed_section_menu_contact' )->value();
    
    if ( $control_id == 'ed_section_menu_about' && $ed_sec_menu == true ) return true;
    if ( $control_id == 'section_menu_about' && $ed_sec_menu == true && $ed_sec_menu_about == true ) return true;
    
    if ( $control_id == 'ed_section_menu_activities' && $ed_sec_menu == true ) return true;
    if ( $control_id == 'section_menu_activities' && $ed_sec_menu == true && $ed_sec_menu_activities == true ) return true;
    
    if ( $control_id == 'ed_section_menu_events' && $ed_sec_menu == true ) return true;
    if ( $control_id == 'section_menu_events' && $ed_sec_menu == true && $ed_sec_menu_events == true ) return true;
    
    if ( $control_id == 'ed_section_menu_features' && $ed_sec_menu == true ) return true;
    if ( $control_id == 'section_menu_features' && $ed_sec_menu == true && $ed_sec_menu_features == true ) return true;
    
    if ( $control_id == 'ed_section_menu_courses' && $ed_sec_menu == true ) return true;
    if ( $control_id == 'section_menu_courses' && $ed_sec_menu == true && $ed_sec_menu_courses == true ) return true;
    
    if ( $control_id == 'ed_section_menu_testimonial' && $ed_sec_menu == true ) return true;
    if ( $control_id == 'section_menu_testimonial' && $ed_sec_menu == true && $ed_sec_menu_testimonial == true ) return true;

    if ( $control_id == 'ed_section_menu_team' && $ed_sec_menu == true ) return true;
    if ( $control_id == 'section_menu_team' && $ed_sec_menu == true && $ed_sec_menu_team == true ) return true;

    if ( $control_id == 'ed_section_menu_gallery' && $ed_sec_menu == true ) return true;
    if ( $control_id == 'section_menu_gallery' && $ed_sec_menu == true && $ed_sec_menu_gallery == true ) return true;

    if ( $control_id == 'ed_section_menu_blog' && $ed_sec_menu == true ) return true;
    if ( $control_id == 'section_menu_blog' && $ed_sec_menu == true && $ed_sec_menu_blog == true ) return true;

    if ( $control_id == 'ed_section_menu_contact' && $ed_sec_menu == true ) return true;
    if ( $control_id == 'section_menu_contact' && $ed_sec_menu == true && $ed_sec_menu_contact == true ) return true;
    
    return false;
}