<?php
/**
 * Home Page courses Section Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_home_courses( $wp_customize ) {
        
    $wp_customize->add_section(
        'courses_settings',
        array(
            'title' => __( 'Courses Section', 'preschool-and-kindergarten-pro' ),
            'priority' => 15,
            'panel' => 'home_page_settings',
        )
    );

    /** Menu Label */
    $wp_customize->add_setting(
        'ed_section_menu_courses',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'ed_section_menu_courses',
            array(
                'section'       => 'courses_settings',
                'label'         => __( 'Section Menu', 'preschool-and-kindergarten-pro' ),
                'description'   => __( 'Disable to hide the section in the menu.', 'preschool-and-kindergarten-pro' ),
                'active_callback' => 'preschool_and_kindergarten_pro_section_menu_ac'
            )
        )
    );
    
    /** Menu Label  */
    $wp_customize->add_setting(
        'section_menu_courses',
        array(
            'default'           => __( 'Courses', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'section_menu_courses',
        array(
            'label'       => __( 'Courses Section Menu Label', 'preschool-and-kindergarten-pro' ),
            'description' => __( '', 'preschool-and-kindergarten-pro' ),
            'section'     => 'courses_settings',
            'type'        => 'text',
            'active_callback' => 'preschool_and_kindergarten_pro_section_menu_ac'
        )
    ); 

    /** Section Title */
    $wp_customize->add_setting(
        'preschool_and_kindergarten_program_section_title',
        array(
            'default'           => __( 'Featured Programs', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'preschool_and_kindergarten_program_section_title',
        array(
            'label'   => __( 'Section Title', 'preschool-and-kindergarten-pro' ),
            'section' => 'courses_settings',
            'type'    => 'text',
        )
    );
    
    /** Section Description */
    $wp_customize->add_setting(
        'preschool_and_kindergarten_program_section_description',
        array(
            'default'           => __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit integer risus sem..', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    
    $wp_customize->add_control(
        'preschool_and_kindergarten_program_section_description',
        array(
            'label'   => __( 'Section Description', 'preschool-and-kindergarten-pro' ),
            'section' => 'courses_settings',
            'type'    => 'textarea',
        )
    );
    
    if( is_rara_toolkit_activated() ){

    /** Post One */
    $wp_customize->add_setting(
        'popular_course_course_one',
        array(
            'default' => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Select_Control(
            $wp_customize,
            'popular_course_course_one',
            array(
                'label' => __( 'Select Course One', 'preschool-and-kindergarten-pro'),
                'section' => 'courses_settings',
                'description' => __( 'Select courses from the dropdown. To create a new course, go to Dashboard >> Courses >> Add New.', 'preschool-and-kindergarten-pro' ),
                'choices' => preschool_and_kindergarten_pro_get_posts( 'course' ),
            )
        )
    );

    /** Post Two */
    $wp_customize->add_setting(
        'popular_course_course_two',
        array(
            'default' => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
        
    $wp_customize->add_control(
        new Rara_Controls_Select_Control(
            $wp_customize,
            'popular_course_course_two',
            array(
                'label' => __( 'Select Course Two', 'preschool-and-kindergarten-pro'),
                'section' => 'courses_settings',
                'choices' => preschool_and_kindergarten_pro_get_posts( 'course' ),
            )
        )
    );

    /** Post Three */
    $wp_customize->add_setting(
        'popular_course_course_three',
        array(
            'default' => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Select_Control(
            $wp_customize,
            'popular_course_course_three',
            array(
                'label' => __( 'Select Course Three', 'preschool-and-kindergarten-pro'),
                'section' => 'courses_settings',
                'choices' => preschool_and_kindergarten_pro_get_posts( 'course' ),
            )
        )
    );


    /**  View All Courses Label */
    $wp_customize->add_setting(
        'courses_viewall_label',
        array(
            'default' => __( 'View All', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control( 
        'courses_viewall_label',
        array(
            'label'   => __( 'Button Label', 'preschool-and-kindergarten-pro' ),
            'section' => 'courses_settings',
            'description' => __( 'CTA button label to view courses listing page', 'preschool-and-kindergarten-pro'),
            'type'    => 'text',
        )
    );

    }else{
        $wp_customize->add_setting(
            'course_section_note',
            array(
                'sanitize_callback' => 'wp_kses_post'
            )
        );
    
        $wp_customize->add_control(
            new Rara_Controls_Info_Text( 
                $wp_customize,
                'course_section_note',
                array(
                    'section'      => 'courses_settings', 
                    'description' => sprintf( __( 'Please install/activate the %1$sRara Theme Tool Kit Pro%2$s which you have received along with this theme for setting related with Course post type.', 'preschool-and-kindergarten-pro' ), '<a href="' . admin_url( 'themes.php?page=tgmpa-install-plugins' ) . '" target="_blank">', '</a>' ),
                )
            )
        );
    }
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_home_courses' );