<?php
/**
 * Home Page Team Section Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_home_team( $wp_customize ) {
        
    $wp_customize->add_section(
        'team_settings',
        array(
            'title' => __( 'Team Section', 'preschool-and-kindergarten-pro' ),
            'priority' => 16,
            'panel' => 'home_page_settings',
        )
    ); 

    /** Menu Label */
    $wp_customize->add_setting(
        'ed_section_menu_team',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'ed_section_menu_team',
            array(
                'section'       => 'team_settings',
                'label'         => __( 'Section Menu', 'preschool-and-kindergarten-pro' ),
                'description'   => __( 'Disable to hide the section in the menu.', 'preschool-and-kindergarten-pro' ),
                'active_callback' => 'preschool_and_kindergarten_pro_section_menu_ac'
            )
        )
    );
    
    /** Menu Label  */
    $wp_customize->add_setting(
        'section_menu_team',
        array(
            'default'           => __( 'Team', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'section_menu_team',
        array(
            'label'       => __( 'Team Section Menu Label', 'preschool-and-kindergarten-pro' ),
            'description' => __( '', 'preschool-and-kindergarten-pro' ),
            'section'     => 'team_settings',
            'type'        => 'text',
            'active_callback' => 'preschool_and_kindergarten_pro_section_menu_ac'
        )
    ); 

    /** Section Title */
    $wp_customize->add_setting(
        'preschool_and_kindergarten_staff_section_title',
        array(
            'default'           => __( 'Our Staffs', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'preschool_and_kindergarten_staff_section_title',
        array(
            'label'   => __( 'Section Title', 'preschool-and-kindergarten-pro' ),
            'section' => 'team_settings',
            'type'    => 'text',
        )
    );
    
    /** Section Description */
    $wp_customize->add_setting(
        'preschool_and_kindergarten_staff_section_description',
        array(
            'default' => __( 'Teams are especially appropriate for conducting tasks that are high in complexity and have many interdependent subtasks.', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    
    $wp_customize->add_control(
        'preschool_and_kindergarten_staff_section_description',
        array(
            'label'   => __( 'Section Description', 'preschool-and-kindergarten-pro' ),
            'section' => 'team_settings',
            'type'    => 'textarea',
        )
    );
    
    if( is_rara_toolkit_activated() ){
    
    /** Post One */
    $wp_customize->add_setting(
        'team_one',
        array(
            'default' => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Select_Control(
            $wp_customize,
            'team_one',
            array(
                'label' => __( 'Select Team Member One', 'preschool-and-kindergarten-pro'),
                'section' => 'team_settings',
                'description' => __( 'Choose a team member from the drop down below. To create a team member, go to Dashboard >> Teams >> Add New.', 'preschool-and-kindergarten-pro' ),
                'choices' => preschool_and_kindergarten_pro_get_posts( 'team' ),
            )
        )
    );

    /** Post Two */
    $wp_customize->add_setting(
        'team_two',
        array(
            'default' => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
        
    $wp_customize->add_control(
        new Rara_Controls_Select_Control(
            $wp_customize,
            'team_two',
            array(
                'label' => __( 'Select Team Member Two', 'preschool-and-kindergarten-pro'),
                'section' => 'team_settings',
                'choices' => preschool_and_kindergarten_pro_get_posts( 'team' ),
            )
        )
    );

    /** Post Three */
    $wp_customize->add_setting(
        'team_three',
        array(
            'default' => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_select',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Select_Control(
            $wp_customize,
            'team_three',
            array(
                'label' => __( 'Select Team Member Three', 'preschool-and-kindergarten-pro'),
                'section' => 'team_settings',
                'choices' => preschool_and_kindergarten_pro_get_posts( 'team' ),
            )
        )
    );


    /**  View All team Label */
    $wp_customize->add_setting(
        'team_viewall_label',
        array(
            'default' => __( 'View All', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control( 
        'team_viewall_label',
        array(
            'label'   => __( 'Button Label', 'preschool-and-kindergarten-pro' ),
            'section' => 'team_settings',
            'description' => __( 'CTA button label to view team listing page', 'preschool-and-kindergarten-pro'),
            'type'    => 'text',
        )
    );

    }else{
        $wp_customize->add_setting(
            'team_section_note',
            array(
                'sanitize_callback' => 'wp_kses_post'
            )
        );
    
        $wp_customize->add_control(
            new Rara_Controls_Info_Text( 
                $wp_customize,
                'team_section_note',
                array(
                    'section'      => 'team_settings', 
                    'description' => sprintf( __( 'Please install/activate the %1$sRara Theme Tool Kit Pro%2$s which you have received along with this theme for setting related with Team post type.', 'preschool-and-kindergarten-pro' ), '<a href="' . admin_url( 'themes.php?page=tgmpa-install-plugins' ) . '" target="_blank">', '</a>' ),
                )
            )
        );
    }
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_home_team' );