<?php
/**
 * Sort Home Page Section Option.
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_home_sort( $wp_customize ) {
    
    /** Sort Section */
    $wp_customize->add_section(
        'sort_home_page_settings',
        array(
            'title'    => __( 'Sort Home Page Section', 'preschool-and-kindergarten-pro' ),
            'priority' => 80,
            'panel'    => 'home_page_settings',
        )
    );
    
    /** Sort Home Page Section Section */
    $wp_customize->add_setting(
		'sort_home_section', 
		array(
			'default' => array( 'about', 'activities', 'subscription', 'features', 'events', 'promotional', 'course', 'team', 'testimonial', 'gallery', 'blog', 'CTA', 'contact' ),
			'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_sortable',
        )
	);

	$wp_customize->add_control(
		new Rara_Control_Sortable(
			$wp_customize,
			'sort_home_section',
			array(
				'section'     => 'sort_home_page_settings',
				'label'       => __( 'Sort Sections', 'preschool-and-kindergarten-pro' ),
				'description' => __( 'Sort or toggle home page sections.', 'preschool-and-kindergarten-pro' ),
				'choices'     => array(
            		'about'         => __( 'About Section', 'preschool-and-kindergarten-pro' ),
            		'activities'    => __( 'Activities Section', 'preschool-and-kindergarten-pro' ),
            		'subscription'  => __( 'Subscriptions Section', 'preschool-and-kindergarten-pro' ),
            		'features'      => __( 'Features Section', 'preschool-and-kindergarten-pro' ),
                    'events'        => __( 'Events Section', 'preschool-and-kindergarten-pro' ),
                    'promotional'   => __( 'Promotional Section', 'preschool-and-kindergarten-pro' ),
            		'course'        => __( 'Courses Section', 'preschool-and-kindergarten-pro' ),
                    'team'          => __( 'Team Section', 'preschool-and-kindergarten-pro' ),
                    'testimonial'   => __( 'Testimonial Section', 'preschool-and-kindergarten-pro' ),            
                    'gallery'       => __( 'Gallery Section', 'preschool-and-kindergarten-pro' ),
                    'blog'          => __( 'Blog Section', 'preschool-and-kindergarten-pro' ),
                    'CTA'           => __( 'CTA Section', 'preschool-and-kindergarten-pro' ),
                    'contact'       => __( 'Contact Section', 'preschool-and-kindergarten-pro' ),
            	),
			)
		)
	);
        
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_home_sort' );