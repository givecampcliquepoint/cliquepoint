<?php
/**
 * Home Page Blog Section Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_home_blog( $wp_customize ) {
    
    /** Blog Section */
    $wp_customize->add_section(
        'blog_settings',
        array(
            'title' => __( 'Blog Section', 'preschool-and-kindergarten-pro' ),
            'priority' => 19,
            'panel' => 'home_page_settings',
        )
    ); 
    /** Menu Label */
    $wp_customize->add_setting(
        'ed_section_menu_blog',
        array(
            'default'           => '1',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
        new Rara_Controls_Toggle_Control( 
            $wp_customize,
            'ed_section_menu_blog',
            array(
                'section'       => 'blog_settings',
                'label'         => __( 'Section Menu', 'preschool-and-kindergarten-pro' ),
                'description'   => __( 'Disable to hide the section in the menu.', 'preschool-and-kindergarten-pro' ),
                'active_callback' => 'preschool_and_kindergarten_pro_section_menu_ac'
            )
        )
    );
    
    /** Menu Label  */
    $wp_customize->add_setting(
        'section_menu_blog',
        array(
            'default'           => __( 'Blog', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'section_menu_blog',
        array(
            'label'       => __( 'Blog Section Menu Label', 'preschool-and-kindergarten-pro' ),
            'description' => __( '', 'preschool-and-kindergarten-pro' ),
            'section'     => 'blog_settings',
            'type'        => 'text',
            'active_callback' => 'preschool_and_kindergarten_pro_section_menu_ac'
        )
    ); 

    /** Section Title */
    $wp_customize->add_setting(
        'preschool_and_kindergarten_news_section_title',
        array(
            'default'           => __( 'News and Events', 'preschool-and-kindergarten-pro'),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'preschool_and_kindergarten_news_section_title',
        array(
            'label'   => __( 'Section Title', 'preschool-and-kindergarten-pro' ),
            'section' => 'blog_settings',
            'type'    => 'text',
        )
    );
    
    /** Section Description */
    $wp_customize->add_setting(
        'preschool_and_kindergarten_news_section_description',
        array(
            'default'           => __( 'News is information about current events.', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    
    $wp_customize->add_control(
        'preschool_and_kindergarten_news_section_description',
        array(
            'label'   => __( 'Section Description', 'preschool-and-kindergarten-pro' ),
            'section' => 'blog_settings',
            'type'    => 'textarea',
        )
    );
    
    /**  View All Blog Label */
    $wp_customize->add_setting(
        'blog_viewall_label',
        array(
            'default' => __( 'View All Blog', 'preschool-and-kindergarten-pro' ),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control( 
        'blog_viewall_label',
        array(
            'label'   => __( 'Button Label', 'preschool-and-kindergarten-pro' ),
            'section' => 'blog_settings',
            'description' => __( 'CTA button label to view Blog listing page', 'preschool-and-kindergarten-pro'),
            'type'    => 'text',
        )
    ); 
    
    /** Blog Section Ends */
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_home_blog' );