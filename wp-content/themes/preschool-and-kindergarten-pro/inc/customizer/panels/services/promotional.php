<?php
/**
 * Home Page Promotional Section Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_service_promotional( $wp_customize ) {
    
    
    $wp_customize->add_section(
        'service_promotional_settings',
        array(
            'title' => __( 'Promotional Section', 'preschool-and-kindergarten-pro' ),
            'priority' => 20,
            'panel' => 'services_page_settings',
        )
    );


    /** Section Title */
    $wp_customize->add_setting(
        'service_promotional_section_title',
        array(
            'default'           => '',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'service_promotional_section_title',
        array(
            'label'   => __( 'Section Title', 'preschool-and-kindergarten-pro' ),
            'section' => 'service_promotional_settings',
            'type'    => 'text',
        )
    );
    
    /** Section Description */
    $wp_customize->add_setting(
        'service_promotional_section_description',
        array(
            'default'           => '',
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    
    $wp_customize->add_control(
        'service_promotional_section_description',
        array(
            'label'   => __( 'Section Description', 'preschool-and-kindergarten-pro' ),
            'section' => 'service_promotional_settings',
            'type'    => 'textarea',
        )
    );

    /** First Button Label */
    $wp_customize->add_setting(
        'service_promotional_section_button_label',
        array(
            'default' => '',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'service_promotional_section_button_label',
        array(
            'label' => __( 'First Button Label', 'preschool-and-kindergarten-pro' ),
            'section' => 'service_promotional_settings',
            'type' => 'text',
        )
    );
    
    /** first Button Link */
    $wp_customize->add_setting(
        'service_promotional_section_button_link',
        array(
            'default'           => '',
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    
    $wp_customize->add_control(
        'service_promotional_section_button_link',
        array(
            'label'   => __( 'First Button Link', 'preschool-and-kindergarten-pro' ),
            'section' => 'service_promotional_settings',
            'type'    => 'text',
        )
    );
    
    
   /** Second Button Label */
    $wp_customize->add_setting(
        'service_promotional_section_button_label_two',
        array(
            'default' => '',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'service_promotional_section_button_label_two',
        array(
            'label' => __( 'Second Button Label', 'preschool-and-kindergarten-pro' ),
            'section' => 'service_promotional_settings',
            'type' => 'text',
        )
    );
    
    /** Second Button Link */
    $wp_customize->add_setting(
        'service_promotional_section_button_link_two',
        array(
            'default'           => '',
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    
    $wp_customize->add_control(
        'service_promotional_section_button_link_two',
        array(
            'label'   => __( 'Second Button Link', 'preschool-and-kindergarten-pro' ),
            'section' => 'service_promotional_settings',
            'type'    => 'text',
        )
    );
    
    /** Background Image */
    $wp_customize->add_setting(
        'service_promotional_bg_image',
        array(
            'default'           => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_image',
        )
    );
    
    $wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'service_promotional_bg_image',
            array(
               'label'       => __( 'Background Image', 'preschool-and-kindergarten-pro' ),
               'description' => __( 'Upload background image for promotional section.', 'preschool-and-kindergarten-pro' ),
               'section'     => 'service_promotional_settings',               
            )
       )
    );
    
    /** promotional Section Ends */
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_service_promotional' );