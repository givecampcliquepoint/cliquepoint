<?php 
/*
* facilities Section Theme Option.
*
* @Package  Preschool and Kindergarten Pro
*/

 function preschool_and_kindergarten_pro_customize_register_facilities( $wp_customize ) {
    
    /** facilities Section */

    $wp_customize->add_section(
        'facilities_settings',
        array(
            'title' => __( 'Facilities Section', 'preschool-and-kindergarten-pro' ),
            'priority' => 12,
            'panel' => 'services_page_settings',
        )
    );

   $wp_customize->add_setting(
        'facilities_section_title',
        array(
            'default' => '',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    
    $wp_customize->add_control(
        'facilities_section_title',
        array(
            'label' => __( 'Section Title', 'preschool-and-kindergarten-pro' ),
            'section' => 'facilities_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting(
        'facilities_section_description',
        array(
            'default' => '',
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    
    $wp_customize->add_control(
        'facilities_section_description',
        array(
            'label' => __( 'Section Description', 'preschool-and-kindergarten-pro' ),
            'section' => 'facilities_settings',
            'type' => 'textarea',
        )
    );

     /** Background Image */
    $wp_customize->add_setting(
        'facilities_bg_image',
        array(
            'default'           => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_image',
        )
    );
    
    $wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'facilities_bg_image',
           array(
               'label'       => __( 'Background Image', 'preschool-and-kindergarten-pro' ),
               'description' => __( 'Upload background image for facilities section.', 'preschool-and-kindergarten-pro' ),
               'section'     => 'facilities_settings',               
           )
       )
    );


}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_facilities');