<?php
/**
 * Styling Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_styling( $wp_customize ) {

    $wp_customize->add_section( 'styling_settings', array(
        'priority'   => 21,
        'capability' => 'edit_theme_options',
        'title'      => __( 'Styling Settings', 'preschool-and-kindergarten-pro' ),
    ) );
    
    /** Layout Style */
    $wp_customize->add_setting( 'layout_style', array(
        'default'           => 'right-sidebar',
        'sanitize_callback' => 'esc_attr'
    ) );
    
    $wp_customize->add_control(
		new Rara_Controls_Radio_Image_Control(
			$wp_customize,
			'layout_style',
			array(
				'section'		=> 'styling_settings',
				'label'			=> __( 'Layout Style', 'preschool-and-kindergarten-pro' ),
				'description'	=> __( 'Choose the default sidebar position for your site. The position of the sidebar for individual posts can be set in the post editor.', 'preschool-and-kindergarten-pro' ),
				'choices'		=> array(
					'left-sidebar' => get_template_directory_uri() . '/images/left-sidebar.png',
                    'right-sidebar' => get_template_directory_uri() . '/images/right-sidebar.png',
				)
			)
		)
	);
    
    /** primary Color Scheme */
    $wp_customize->add_setting( 'primary_color_scheme', array(
        'default'           => '#41aad4',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    $wp_customize->add_control( 
        new WP_Customize_Color_Control( 
            $wp_customize, 
            'primary_color_scheme', 
            array(
                'label'       => __( 'Primary Color Scheme', 'preschool-and-kindergarten-pro' ),
                'description' => __( 'The theme comes with unlimited color schemes for your theme styling.', 'preschool-and-kindergarten-pro' ),
                'section'     => 'styling_settings',                
            )
        )
    );

    /** Secondary Color Scheme */
    $wp_customize->add_setting( 'secondary_color_scheme', array(
        'default'           => '#f380b2',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    $wp_customize->add_control( 
        new WP_Customize_Color_Control( 
            $wp_customize, 
            'secondary_color_scheme', 
            array(
                'label'       => __( 'Secondary Color Scheme', 'preschool-and-kindergarten-pro' ),
                'description' => __( 'The theme comes with unlimited color schemes for your theme styling.', 'preschool-and-kindergarten-pro' ),
                'section'     => 'styling_settings',                
            )
        )
    );

    /** Tertiary Color Scheme */
    $wp_customize->add_setting( 'tertiary_color_scheme', array(
        'default'           => '#4fbba9',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    $wp_customize->add_control( 
        new WP_Customize_Color_Control( 
            $wp_customize, 
            'tertiary_color_scheme', 
            array(
                'label'       => __( 'Tertiary Color Scheme', 'preschool-and-kindergarten-pro' ),
                'description' => __( 'The theme comes with unlimited color schemes for your theme styling.', 'preschool-and-kindergarten-pro' ),
                'section'     => 'styling_settings',                
            )
        )
    );
    
    /** Background Color */
    $wp_customize->add_setting( 'bg_color', array(
        'default'           => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    $wp_customize->add_control( 
        new WP_Customize_Color_Control( 
            $wp_customize, 
            'bg_color', 
            array(
                'label'       => __( 'Background Color', 'preschool-and-kindergarten-pro' ),
                'description' => __( 'Pick a color for site background.', 'preschool-and-kindergarten-pro' ),
                'section'     => 'styling_settings',                
            )
        )
    );
    
    /** Body Background */
    $wp_customize->add_setting( 'body_bg', array(
        'default'           => 'image',
        'sanitize_callback' => 'esc_attr'
    ) );
    
    $wp_customize->add_control(
		new Rara_Controls_Radio_Buttonset_Control(
			$wp_customize,
			'body_bg',
			array(
				'section'	  => 'styling_settings',
				'label'       => __( 'Body Background', 'preschool-and-kindergarten-pro' ),
                'description' => __( 'Choose body background as image or pattern.', 'preschool-and-kindergarten-pro' ),
				'choices'	  => array(
					'image'   => __( 'Image', 'preschool-and-kindergarten-pro' ),
                    'pattern' => __( 'Pattern', 'preschool-and-kindergarten-pro' ),
				)
			)
		)
	);
    
    /** Background Image */
    $wp_customize->add_setting(
        'bg_image',
        array(
            'default'           => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_image',
        )
    );
    
    $wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'bg_image',
           array(
               'label'           => __( 'Background Image', 'preschool-and-kindergarten-pro' ),
               'description'     => __( 'Upload your own custom background image or pattern.', 'preschool-and-kindergarten-pro' ),
               'section'         => 'styling_settings',
               'active_callback' => 'preschool_and_kindergarten_pro_body_bg_choice'               
           )
       )
    );    
    
    /** Background Pattern */
    $wp_customize->add_setting( 'bg_pattern', array(
        'default'           => 'nobg',
        'sanitize_callback' => 'esc_attr'
    ) );
    
    $wp_customize->add_control(
		new Rara_Controls_Radio_Image_Control(
			$wp_customize,
			'bg_pattern',
			array(
				'section'		  => 'styling_settings',
				'label'			  => __( 'Background Pattern', 'preschool-and-kindergarten-pro' ),
				'description'	  => __( 'Choose from any of 63 awesome background patterns for your site background.', 'preschool-and-kindergarten-pro' ),
				'choices'         => preschool_and_kindergarten_pro_get_patterns(),
                'active_callback' => 'preschool_and_kindergarten_pro_body_bg_choice'
			)
		)
	);
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_styling' );

/**
 * Active Callback for Body Background
*/
function preschool_and_kindergarten_pro_body_bg_choice( $control ){
    
    $body_bg    = $control->manager->get_setting( 'body_bg' )->value();
    $control_id = $control->id;
         
    if ( $control_id == 'bg_image' && $body_bg == 'image' ) return true;
    if ( $control_id == 'bg_pattern' && $body_bg == 'pattern' ) return true;
    
    return false;
}