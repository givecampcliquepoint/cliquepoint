<?php
/**
 * Footer Options
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customize_register_footer( $wp_customize ) {

    $wp_customize->add_section( 'footer_settings', array(
        'title' => __( 'Footer Settings', 'preschool-and-kindergarten-pro' ),
        'priority' => 130,
        'capability' => 'edit_theme_options',
    ) );
    
    /** Hide Author Link */
    $wp_customize->add_setting(
        'ed_author_link',
        array(
            'default'           => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
		new Rara_Controls_Toggle_Control( 
			$wp_customize,
			'ed_author_link',
			array(
				'section' => 'footer_settings',
				'label'	  => __( 'Hide Author Link', 'preschool-and-kindergarten-pro' ),
			)
		)
	);
    
    /** Hide WordPress Link */
    $wp_customize->add_setting(
        'ed_wp_link',
        array(
            'default'           => '',
            'sanitize_callback' => 'preschool_and_kindergarten_pro_sanitize_checkbox',
        )
    );
    
    $wp_customize->add_control(
		new Rara_Controls_Toggle_Control( 
			$wp_customize,
			'ed_wp_link',
			array(
				'section' => 'footer_settings',
				'label'	  => __( 'Hide WordPress Link', 'preschool-and-kindergarten-pro' ),
			)
		)
	);
    
    /** Footer Copyright*/
    $wp_customize->add_setting(
        'footer_copyright',
        array(
            'default'           => '',
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    
    $wp_customize->add_control(
	   'footer_copyright',
		array(
			'section'	  => 'footer_settings',
			'label'		  => __( 'Footer Copyright', 'preschool-and-kindergarten-pro' ),
            'description' => sprintf( __( 'You can change footer copyright and use your own custom text from here. Use %1$s shortcode to display Current Year and %2$s to display home link.', 'preschool-and-kindergarten-pro' ), '<b>[the-year]</b>', '<b>[home-link]</b>' ),
			'type'        => 'textarea',
		)		
	);
    
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customize_register_footer' );