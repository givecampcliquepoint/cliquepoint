<?php
/**
 * Preschool and Kindergarten Pro Theme Info
 *
 * @package preschool_and_kindergarten_pro
 */

function preschool_and_kindergarten_pro_customizer_theme_info( $wp_customize ) {
	
    $wp_customize->add_section( 'theme_info' , array(
		'title'       => __( 'Getting Started' , 'preschool-and-kindergarten-pro' ),
		'priority'    => 6,
		));
    
    /** Getting Started */
    $wp_customize->add_setting( 'getting_started',
        array(
            'default' => '',
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    
    $getting_started = '<ul>';
    $getting_started .= sprintf( __( '<li>Check WordPress Version and Update it. %1$sHow to?%2$s</li>', 'preschool-and-kindergarten-pro' ),  '<a href="' . esc_url( 'https://raratheme.com/documentation/preschool-and-kindergarten-pro-documentation/#Checking_and_Updating_WordPress_Version' ) . '" target="_blank">', '</a>' );
    $getting_started .= sprintf( __( '<li>Install Necessary Plugins. Some functionality of the theme might not work if you do not install the recommended plugins. %1$sHow to?%2$s</li>', 'preschool-and-kindergarten-pro' ),  '<a href="' . esc_url( 'https://raratheme.com/documentation/preschool-and-kindergarten-pro-documentation/#Install_Recommended_Plugin' ) . '" target="_blank">', '</a>' );
    $getting_started .= sprintf( __( '<li>Setup a Home Page. %1$sHow to?%2$s</li>', 'preschool-and-kindergarten-pro' ),  '<a href="' . esc_url( 'https://raratheme.com/documentation/preschool-and-kindergarten-pro-documentation/#Setting_up_the_FrontLandingHome_page' ) . '" target="_blank">', '</a>' );
    $getting_started .= sprintf( __( '<li>Enable/Disable Demo Content. %1$sHow to?%2$s</li>', 'preschool-and-kindergarten-pro' ),  '<a href="' . esc_url( 'https://raratheme.com/documentation/preschool-and-kindergarten-pro-documentation/#Demo_Content_for_HomeFront_Page' ) . '" target="_blank">', '</a>' );
    $getting_started .= '</ul>';

	$wp_customize->add_control( new Rara_Controls_Info_Text( $wp_customize,
        'getting_started', 
            array(
                'label'       => __( 'Checklist for Getting Started:' , 'preschool-and-kindergarten-pro' ),
                'section'     => 'theme_info',
                'description' => $getting_started
            )
        )
    );
    
    /** Important Links */
	$wp_customize->add_setting( 'theme_info_theme',
        array(
            'default' => '',
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    
    $theme_info = '<ul>';
	$theme_info .= sprintf( __( '<li>View demo: %1$sClick here.%2$s</li>', 'preschool-and-kindergarten-pro' ),  '<a href="' . esc_url( 'http://raratheme.com/previews/?theme=preschool-and-kindergarten-pro' ) . '" target="_blank">', '</a>' );
    $theme_info .= sprintf( __( '<li>View documentation: %1$sClick here.%2$s</li>', 'preschool-and-kindergarten-pro' ),  '<a href="' . esc_url( 'http://raratheme.com/documentation/preschool-and-kindergarten-pro/' ) . '" target="_blank">', '</a>' );
    $theme_info .= sprintf( __( '<li>Theme info: %1$sClick here.%2$s</li>', 'preschool-and-kindergarten-pro' ),  '<a href="' . esc_url( 'https://raratheme.com/wordpress-themes/preschool-and-kindergarten-pro/' ) . '" target="_blank">', '</a>' );
    $theme_info .= sprintf( __( '<li>Support ticket: %1$sClick here.%2$s</li>', 'preschool-and-kindergarten-pro' ),  '<a href="' . esc_url( 'https://raratheme.com/support-ticket/' ) . '" target="_blank">', '</a>' );
    $theme_info .= sprintf( __( '<li>More WordPress Themes: %1$sClick here.%2$s</li>', 'preschool-and-kindergarten-pro' ),  '<a href="' . esc_url( 'https://raratheme.com/wordpress-themes/' ) . '" target="_blank">', '</a>' );
    $theme_info .= '</ul>';

	$wp_customize->add_control( new Rara_Controls_Info_Text( $wp_customize,
        'theme_info_theme', 
            array(
                'label'       => __( 'Important Links' , 'preschool-and-kindergarten-pro' ),
                'section'     => 'theme_info',
                'description' => $theme_info
            )
        )
    );
    
    /** Changing priority for static front page */
    $wp_customize->get_section( 'static_front_page' )->priority = 10;
}
add_action( 'customize_register', 'preschool_and_kindergarten_pro_customizer_theme_info' );