<?php 
 /**
     * Doctype Hook
     * 
     * @see preschool_and_kindergarten_pro_doctype_cb
    */
add_action( 'preschool_and_kindergarten_pro_doctype', 'preschool_and_kindergarten_pro_doctype_cb');

 /**
     * Before wp_head
     * 
     * @see preschool_and_kindergarten_pro_head
    */

add_action( 'preschool_and_kindergarten_pro_before_wp_head', 'preschool_and_kindergarten_pro_head');

 /**
     * Page Start
     * @see preschool_and_kindergarten_pro_fb_page_box -15
     * @see preschool_and_kindergarten_pro_page_start -20
    */
add_action( 'preschool_and_kindergarten_pro_before_page_start', 'preschool_and_kindergarten_pro_fb_page_box', 15 );
add_action( 'preschool_and_kindergarten_pro_before_page_start','preschool_and_kindergarten_pro_page_start', 20 );

    /**
     * Preschool and Kindergarten pro Header
     * 
     * @see preschool_and_kindergarten_pro_dynamic_header  - 20
    */
add_action( 'preschool_and_kindergarten_pro_header', 'preschool_and_kindergarten_pro_dynamic_header', 20 );

 /**
     * Page Header
     * 
     * @see preschool_and_kindergarten_pro_page_header - 10
    */

add_action( 'preschool_and_kindergarten_pro_after_header', 'preschool_and_kindergarten_pro_page_header', 10 );

/**
 * slider
 * 
 * @see preschool_and_kindergarten_pro_slider - 20 
*/
add_action( 'preschool_and_kindergarten_pro_slide', 'preschool_and_kindergarten_pro_slider', 20 );
 /**
     * Breadcrumbs
     * 
     * @see preschool_and_kindergarten_pro_breadcrumb 
    */

add_action( 'preschool_and_kindergarten_pro_breadcrumbs', 'preschool_and_kindergarten_pro_breadcrumb' );

/** Content HOOKS goes here */

/**
 * Before Page entry content
 * 
 * @see preschool_and_kindergarten_pro_page_content_image
*/
add_action( 'preschool_and_kindergarten_pro_before_page_entry_content', 'preschool_and_kindergarten_pro_page_content_image' );

/**
 * Before Post entry content
 * 
 * @see preschool_and_kindergarten_pro_post_content_image - 10
 * @see preschool_and_kindergarten_pro_post_entry_header  - 20
* @see preschool_and_kindergarten_pro_social_share       - 30

*/
add_action( 'preschool_and_kindergarten_pro_before_post_image', 'preschool_and_kindergarten_pro_post_content_image', 10 );
add_action( 'preschool_and_kindergarten_pro_before_post_entry_content', 'preschool_and_kindergarten_pro_post_entry_header', 20 );
add_action( 'preschool_and_kindergarten_pro_before_post_entry_content', 'preschool_and_kindergarten_pro_social_share', 30 );

add_action( 'preschool_and_kindergarten_pro_excerpt_section', 'preschool_and_kindergarten_pro_excerpt_char_to_display' );
/**
 * After post content
 * 
 * @see preschool_and_kindergarten_pro_post_author - 20
*/
add_action( 'preschool_and_kindergarten_pro_after_post_content', 'preschool_and_kindergarten_pro_post_author', 20 );

/**
 * Contact Details
 * @see preschool_and_kindergarten_pro_contact_details_cb
*/ 
add_action( 'preschool_and_kindergarten_pro_contact_details', 'preschool_and_kindergarten_pro_contact_details_cb' );
/**
 * preschool_and_kindergarten_pro Comment
 * 
 * @see preschool_and_kindergarten_pro_get_comment_section 
*/
add_action( 'preschool_and_kindergarten_pro_comment', 'preschool_and_kindergarten_pro_get_comment_section' );

/**
    * Content End
    * 
    * @see preschool_and_kindergarten_pro_content_end -20
*/

add_action( 'preschool_and_kindergarten_pro_after_content', 'preschool_and_kindergarten_pro_content_end', 20 );

 /**
     * Rara Academic Footer
     * 
     * @see preschool_and_kindergarten_pro_footer_start - 10
     * @see preschool_and_kindergarten_pro_footer_top - 20
     * @see preschool_and_kindergarten_pro_footer_bottom - 30
     * @see preschool_and_kindergarten_pro_footer_end - 40
    */

add_action( 'preschool_and_kindergarten_pro_footer', 'preschool_and_kindergarten_pro_footer_start', 10 );
add_action( 'preschool_and_kindergarten_pro_footer', 'preschool_and_kindergarten_pro_footer_top', 20 );
add_action( 'preschool_and_kindergarten_pro_footer', 'preschool_and_kindergarten_pro_footer_bottom', 30 );
add_action( 'preschool_and_kindergarten_pro_footer', 'preschool_and_kindergarten_pro_footer_end', 40 );

 /**
     * page start
     * 
     * @see preschool_and_kindergarten_pro_page_end - 20
    */

add_action( 'preschool_and_kindergarten_pro_after_footer', 'preschool_and_kindergarten_pro_page_end', 20 );