<?php
/**
 * Demo Contents for different sections of Home Page.
 *
 * @package preschool_and_kindergarten_pro
 */
      
if( ! function_exists( 'preschool_and_kindergarten_pro_demo_content' ) ) :
/**
 * Return Demo contents
*/
function preschool_and_kindergarten_pro_demo_content( $section, $header = false ){
    
    switch( $section ){
        case 'banner': ?>
        <div id="banner_section" class="banner">
		<div id="banner-slider" class="owl-carousel owl-theme">
			<div>
				<img class="owl-lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/demo/img31.jpg" alt="">
				<div class="banner-text">
					<div class="container">
						<div class="text-holder">
							<strong class="title">Learning is fun at Kinder School</strong>
							<p>Lorem ipsum dolor lorem sit amet lorem consectetur lorem ipsum</p>
							<a href="#" class="btn-enroll">Enroll Your Child</a>
						</div>
					</div>
				</div>
			</div>
			<div>
				<img class="owl-lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/demo/img31.jpg" alt="">
				<div class="banner-text">
					<div class="container">
						<div class="text-holder">
							<strong class="title">Learning is fun at Kinder School</strong>
							<p>Lorem ipsum dolor lorem sit amet lorem consectetur lorem ipsum</p>
							<a href="#" class="btn-enroll">Enroll Your Child</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
        <?php
        break;
                    
        case 'about':  ?>
            <section id="about_section" class="welcome">
				<div class="container">
					<div class="row">
						<div class="img-holder">
							<img src="<?php echo get_template_directory_uri(); ?>/images/demo/img32.jpg" alt="">
						</div>
						<div class="text-holder">
							<h2 class="title">Welcome to Kinder School</h2>
							<p>Kinderschool Teaches your child to focus on social and emotional growth, the foundation for intellectual learning and democratic living.The strength of this programme lies in the emphasis on the basic motivational factor in human learning.Encouragement is the process of concentrating on a child&rsquo;s assets and strengths, in order to build his/her self confidence and feelings of worth.Preschool systems observe standards for structure administration, class size, student-teacher ratio, services, process quality of classroom environments,, teacher-child interactions, etc.</p>
							<a href="#" class="btn-more">More ABout Us</a>
						</div>
					</div>
				</div>
			</section>

        <?php 
        break;
        
        case 'activities': ?>
			<div class="row">
				<div class="col">
					<div class="img-holder">
						<img src="<?php echo get_template_directory_uri(); ?>/images/demo/img33.jpg" alt="" width="186" height="185">
					</div>
					<div class="text-holder">
						<h3 class="title">Music Class</h3>
						<p>Enhance your child&rsquo;s development and love of music through song, dance, movement games and instruments. With an array of musical styles, our classes help children explore the power of.</p>
					</div>
				</div>
				<div class="col">
					<div class="img-holder">
						<img src="<?php echo get_template_directory_uri(); ?>/images/demo/img34.jpg" alt="" width="186" height="185">
					</div>
					<div class="text-holder">
						<h3 class="title">Active Learning</h3>
						<p>Active learning is a teaching method that strives to more directly involve students in the learning process.The term active learning was introduced by the English scholar R W Revans.</p>
					</div>
				</div>
				<div class="col">
					<div class="img-holder">
						<img src="<?php echo get_template_directory_uri(); ?>/images/demo/img35.jpg" alt="" width="186" height="185">
					</div>
					<div class="text-holder">
						<h3 class="title">Yoga Class</h3>
						<p>Yoga as exercise is a modern phenomenon which has been influenced by the ancient Indian practice of hatha yoga. It involves holding stretches as a kind of low-impact physical exercise.</p>
					</div>
				</div>
			</div>
            <?php    
        break;
        
        case 'subscription': ?>
            <div  id="subscription_section" class="cta-section">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="text">
								<strong>Stay updated with kindergarten</strong>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
							</div>
						</div>
						<div class="col">
							<div class="form-holder">
								<form action="#" class="subscribe-form">
									<input type="email" placeholder="Enter your e-mail...">
									<input type="submit" value="Subscribe">
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
            <?php                
        break;
        
        case 'feature': ?>
            <div class="row">
				<div class="text-holder">
					<p>Service is a non-paying job performed by someone or a group of people for the benefit of the public or its institutions. Performing community service is not the same as volunteering.
                    </p>
					<ul>
						<li>
							<h3 class="title">Summer Camp</h3>
							<p>Summer camp is a supervised program for children or teenagers conducted during the summer</p>
						</li>
						<li>
							<h3 class="title">Personalizing</h3>
							<p>Personalization, sometimes known as customization, consists of tailoring a service or a product</p>
						</li>
						<li>
							<h3 class="title">Sing &amp; Dance</h3>
							<p>Singing is the act of producing musical sounds with the voice, and augments regular speech</p>
						</li>
						<li>
							<h3 class="title">Drawing &amp; Painting</h3>
							<p>Drawing is a form of visual art in which a person uses various drawing instruments.</p>
						</li>
					</ul>
				</div>
			    <div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/demo/img36.jpg" alt=""></div>
			</div>
        <?php 
        break;

        case 'event': ?>

					<div class="row">
						<div class="col">
							<div class="img-holder">
								<a href="#" class="post-thumbnail"><img src="<?php echo get_template_directory_uri(); ?>/images/demo/img89.jpg" alt=""></a>
								<div class="date-holder">
									<span>15</span>&nbsp;March
								</div>
							</div>
							<div class="event-address">
								<i class="fa fa-map-marker"></i>
								<address>New York, US</address>
							</div>
							<div class="text-holder">
								<h2 class="event-title"><a href="#">Art Week &amp; Artists in Action</a></h2>
								<p>Art in Action was an exhibit of artists at work displayed for four months in the summer of 1940 at the Golden Gate International Exposition GGIE held on Treasure Island.Many famous artists took.</p>
							</div>
						</div>
						<div class="col">
							<div class="img-holder">
								<a href="#" class="post-thumbnail"><img src="<?php echo get_template_directory_uri(); ?>/images/demo/img90.jpg" alt=""></a>
								<div class="date-holder">
									<span>15</span>&nbsp;March
								</div>
							</div>
							<div class="event-address">
								<i class="fa fa-map-marker"></i>
								<address>Dallas, US</address>
							</div>
							<div class="text-holder">
								<h2 class="event-title"><a href="#">Celebrating early learning for kids</a></h2>
								<p>Battelle for Kids was established in 2001 through a partnership with the Ohio Business Roundtable and supported by an initial grant from Battelle Memorial Institute to improve student achievement</p>
							</div>
						</div>
						<div class="col">
							<div class="img-holder">
								<a href="#" class="post-thumbnail"><img src="<?php echo get_template_directory_uri(); ?>/images/demo/img91.jpg" alt=""></a>
								<div class="date-holder">
									<span>15</span>&nbsp;March
								</div>
							</div>
							<div class="event-address">
								<i class="fa fa-map-marker"></i>
								<address>Chicago, US</address>
							</div>
							<div class="text-holder">
								<h2 class="event-title"><a href="#">Fun and Games</a></h2>
								<p>A game is a structured form of play, usually undertaken for enjoyment and sometimes used as an educational tool.Games are distinct from work, which is usually carried out for remuneration.</p>
							</div>
						</div>
					</div>
        
        <?php 
        break;
        
        case 'course': ?>
			<div class="row">
				<div class="col">
					<div class="holder">
						<div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/demo/img38.jpg" alt=""></div>
						<div class="text-holder">
							<h3 class="title">Music Class</h3>
							<p>In music, a pitch class is a set of all pitches that are a whole number of octaves apart, e.g. the pitch class C consists of the Cs in all octaves.</p>
							<a href="#" class="btn-detail">View Details</a>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="holder">
						<div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/demo/img39.jpg" alt=""></div>
						<div class="text-holder">
							<h3 class="title">Active Learning</h3>
							<p>Active learning is a teaching method that strives to more directly involve students in the learning process.</p>
							<a href="#" class="btn-detail">View Details</a>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="holder">
						<div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/demo/img40.jpg" alt=""></div>
						<div class="text-holder">
							<h3 class="title">Yoga Class</h3>
							<p>Yoga as exercise is a modern phenomenon which has been influenced by the ancient Indian practice of hatha yoga.</p>
							<a href="#" class="btn-detail">View Details</a>
						</div>
					</div>
				</div>
			</div>
		
            <?php                
        break;
        
        case 'team': ?>
				<div class="row">
					<div class="col">
						<div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/demo/img42.jpg" alt=""></div>
						<div class="text-holder">
							<strong class="name">Michelle Soto</strong>
							<span class="designation">Music Teacher</span>
						</div>
					</div>
					<div class="col">
						<div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/demo/img43.jpg" alt=""></div>
						<div class="text-holder">
							<strong class="name">Danish Jensen</strong>
							<span class="designation">Gym Teacher</span>
						</div>
					</div>
					<div class="col">
						<div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/demo/img44.jpg" alt=""></div>
						<div class="text-holder">
							<strong class="name">Asio Kawasaki</strong>
							<span class="designation">English Teacher</span>
						</div>
					</div>
				</div>
            <?php                
        break;

        case 'testimonial': ?>
            <div id="testimonial-slider" class="owl-theme owl-carousel">
				<div>
					<div class="table">
						<div class="table-row">
							<div class="text-holder">
								<div class="header">
									<h3 class="title">What Parent&rsquo;s Says</h3>
									<p>Take a glance</p>
								</div>
								<p>We can tell you that we are very happy with the school, its employees and the education they provide to our son.We thank the team for the great service they provide, and the skills they teach our son. It is fantastic that he, after half a year, actually reads the books they provide.</p>
								<span class="name">- John Brownstone, Canada</span>
							</div>
							<div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/demo/img41.jpg" alt=""></div>
						</div>
					</div>
				</div>
				<div>
					<div class="table">
						<div class="table-row">
							<div class="text-holder">
								<div class="header">
									<h3 class="title">What Parent&rsquo;s Says</h3>
									<p>Take a glance</p>
								</div>
								<p>When looking at the team of teachers they provide, I can tell you that we are very satisfied. I am, as a business leader, quite impressed on how divttle impact the impressive growth the school has experienced over the last years, has had on the children. Good work.</p>
								<span class="name">- Isabella Abascal, Spain</span>
							</div>
							<div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/demo/img41.jpg" alt=""></div>
						</div>
					</div>
				</div>
				<div>
					<div class="table">
						<div class="table-row">
							<div class="text-holder">
								<div class="header">
									<h3 class="title">What Parent&rsquo;s Says</h3>
									<p>Take a glance</p>
								</div>
								<p>This is the best school for my child. It was an easy decision because it is the best school . The school has innovative and caring teachers which is important to us.It offers high standards of education across a broad range of subjects. My girls love the Art and Music classes! Small class sizes mean my girls are learning at a phenomenal rate..</p>
								<span class="name">- Richa White, Denmark</span>
							</div>
							<div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/demo/img41.jpg" alt=""></div>
						</div>
					</div>
				</div>
			</div>
        <?php 
        break;
        
        case 'gallery': ?>
            <div id="gallery-slider" class="owl-carousel owl-theme" >
			<div><img class="owl-lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/demo/img45.jpg" alt="">
				<div class="caption">Drawing</div></div>
			<div><img class="owl-lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/demo/img46.jpg" alt="">
				<div class="caption">Drawing</div></div>
			<div><img class="owl-lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/demo/img47.jpg" alt="">
			    <div class="caption">A Happy Child</div></div>
			<div><img class="owl-lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/demo/img48.jpg" alt="">
			    <div class="caption">Playing</div></div>
			<div><img class="owl-lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/demo/img49.jpg" alt="">
				<div class="caption">Playing</div></div>
			<div><img class="owl-lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/demo/img45.jpg" alt=""></div>
			<div><img class="owl-lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/demo/img46.jpg" alt=""></div>
			<div><img class="owl-lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/demo/img47.jpg" alt=""></div>
			<div><img class="owl-lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/demo/img48.jpg" alt=""></div>
			<div><img class="owl-lazy" data-src="<?php echo get_template_directory_uri(); ?>/images/demo/img49.jpg" alt=""></div>
		</div>
            <?php                
        break;
        
        case 'blog': ?>
			<div class="row">
				<article class="post">
					<div class="posted-on">
						<strong>15</strong>
						<span>April</span>
					</div>
					<div class="text-holder">
						<header class="entry-header">
							<h3 class="entry-title"><a href="#">Christmas Candle Craft for Kids</a></h3>
						</header>
						<div class="entry-content">
							<p>A candle is an ignitable wick embedded in wax or another flammable solid substance such as tallow that provides light, and in some cases, a fragrance. It can also be used to provide heat, or used as a method of keeping time. A candle manufacturer is traditionally known as a chandler.</p>
						</div>
						<footer class="entry-footer">
							<a href="#" class="readmore">View Details</a>
						</footer>
					</div>
				</article>
				<article class="post">
					<div class="posted-on">
						<strong>15</strong>
						<span>April</span>
					</div>
					<div class="text-holder">
						<header class="entry-header">
							<h3 class="entry-title"><a href="#">Kindergarten Rainbow Art and Sentence Writing</a></h3>
						</header>
						<div class="entry-content">
							<p>A rainbow is a meteorological phenomenon that is caused by reflection, refraction and dispersion of light in water droplets resulting in a spectrum of light appearing in the sky. It takes the form of a multicoloured arc. Rainbows caused by sunlight always appear in the section.</p>
						</div>
						<footer class="entry-footer">
							<a href="#" class="readmore">View Details</a>
						</footer>
					</div>
				</article>
				<article class="post">
					<div class="posted-on">
						<strong>15</strong>
						<span>April</span>
					</div>
					<div class="text-holder">
						<header class="entry-header">
							<h3 class="entry-title"><a href="#">Free Earth Day Craft and Writing Activity</a></h3>
						</header>
						<div class="entry-content">
							<p>Earth Day is an annual event celebrated on April 22. Worldwide, various events are held to demonstrate support for environmental protection. First celebrated in 1970, Earth Day events in more than 193 countries are now coordinated globally by the Earth Day.</p>
						</div>
						<footer class="entry-footer">
							<a href="#" class="readmore">View Details</a>
						</footer>
					</div>
				</article>
			</div>
            <?php                

        break;
        
        case 'contact':
        ?>
			<div class="map-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/demo/map.jpg" alt=""></div>
        <?php                
        break;
    }
    
}
endif;