<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package preschool_and_kindergarten_pro
 */


if( ! function_exists( 'preschool_and_kindergarten_pro_doctype_cb' ) ) :
/**
 * Doctype Declaration
 * 
 * @since 1.0.1
*/
function preschool_and_kindergarten_pro_doctype_cb(){
    ?>
    <!DOCTYPE html>
    <html <?php language_attributes(); ?>>
    <?php
}
endif;



if( ! function_exists( 'preschool_and_kindergarten_pro_head' ) ) :
/**
 * Before wp_head
 * 
 * @since 1.0.1
*/
function preschool_and_kindergarten_pro_head(){
    ?>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php
}
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_fb_page_box' ) ) :
/**
 * Callback to add Facebook Page Plugin JS
*/
function preschool_and_kindergarten_pro_fb_page_box(){
    if( is_active_widget( false, false, 'preschool_and_kindergarten_pro_facebook_page_widget' ) ){ ?>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <?php }
}
endif;


if( ! function_exists( 'preschool_and_kindergarten_pro_page_start' ) ) :
/**
 * Page Start
 * 
 * @since 1.0.1
*/
function preschool_and_kindergarten_pro_page_start(){
    ?>
    <div id="page" class="site">
    <?php
}
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_dynamic_header' ) ) :
/**
 * Dynamic Header 
*/
function preschool_and_kindergarten_pro_dynamic_header(){
    
    $header_array = array( 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine' );
    $header = get_theme_mod( 'header_layout', 'one' );
    if( in_array( $header, $header_array ) ){            
        get_template_part( 'header/' . $header );
    } 
}
endif;


if( ! function_exists( 'preschool_and_kindergarten_pro_page_header' ) ):
/**
 * Page Header 
*/
    function preschool_and_kindergarten_pro_page_header(){
        
        if( ! is_front_page() ){ ?>
            <div class="top-bar">
                <div class="container">
                    <div class="page-header">
                        <h1 class="page-title">
                            <?php 
                            if( is_page()){
                                the_title();
                            }
                                  
                            if(is_search()){ 
                                printf( esc_html__( 'Search Results for: %s', 'preschool-and-kindergarten-pro' ), '<span>' . get_search_query() . '</span>' );
                            }

                            if( is_singular( array( 'event', 'course' ) ) ) the_title();
                                
                            /** For Woocommerce */
                            if( preschool_and_kindergarten_pro_is_woocommerce_activated() && ( is_product_category() || is_product_tag() || is_shop() ) ){
                                if( is_shop() ){
                                    if ( get_option( 'page_on_front' ) == wc_get_page_id( 'shop' ) ) {
                                        return;
                                    }
                                    $_name = wc_get_page_id( 'shop' ) ? get_the_title( wc_get_page_id( 'shop' ) ) : '';
                                
                                    if ( ! $_name ) {
                                        $product_post_type = get_post_type_object( 'product' );
                                        $_name = $product_post_type->labels->singular_name;
                                    }
                                    echo esc_html( $_name );
                                }elseif( is_product_category() || is_product_tag() ){
                                    $current_term = $GLOBALS['wp_query']->get_queried_object();
                                    echo esc_html( $current_term->name );
                                }
                            }else{
                                if( is_archive() ) the_archive_title();    
                            }
                                
                                if(is_404()) {
                                    printf( esc_html__( '404 - Page not found', 'preschool-and-kindergarten-pro' )); 
                                }

                                if ( is_home() && ! is_front_page() ){
                                    single_post_title();
                                }
                            ?>
                        </h1>
                    </div>
                    <?php do_action( 'preschool_and_kindergarten_pro_breadcrumbs' ); ?>  
                </div>
            </div>
        <?php 
        }
        $home_sections = get_theme_mod( 'sort_home_section', array( 'about', 'activities', 'subscription', 'features', 'events', 'promotional', 'course', 'team', 'testimonial', 'gallery', 'blog', 'CTA', 'contact' ) );
        
        if( ! is_page_template( array( 'templates/template-about.php', 'templates/template-contact.php', 'templates/template-services.php' ) ) && !( is_front_page() && ! is_home() && $home_sections ) ):  ?>
                <div class="container">
                    <div id="content" class="site-content">
                        <div class="row">
        <?php  
        endif;
    }
endif;


if( ! function_exists( 'preschool_and_kindergarten_pro_page_content_image' ) ) :
/**
 * Page Featured Image
*/
function preschool_and_kindergarten_pro_page_content_image(){

    if( has_post_thumbnail() && get_theme_mod( 'ed_featured_image', '1' ) ){

        echo '<div class="post-thumbnail">';

        preschool_and_kindergarten_pro_sidebar( true )  ? the_post_thumbnail( 'preschool-and-kindergarten-pro-with-sidebar', array( 'itemprop' => 'image' ) ) : the_post_thumbnail( 'preschool-and-kindergarten-pro-without-sidebar', array( 'itemprop' => 'image' ) );    
        
        echo '</div>';
    }
}
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_post_content_image' ) ) :
/**
 * Post Featured Image
*/
function preschool_and_kindergarten_pro_post_content_image(){

    $blog_layout  = get_theme_mod( 'blog_layout', 'default' ); //From Customizer
    $img_size     = '';
    
    if( $blog_layout == 'round' || $blog_layout == 'square' ) {
        $img_size = 'preschool-and-kindergarten-pro-layout-post';
    }else{
        $img_size = preschool_and_kindergarten_pro_sidebar( true ) ? 'preschool-and-kindergarten-pro-with-sidebar' : 'preschool-and-kindergarten-pro-without-sidebar';
    }

    if( has_post_thumbnail() ){ 
        echo '<a href="' . esc_url( get_permalink() ) . '" class="post-thumbnail">';    
        
            the_post_thumbnail( $img_size, array( 'itemprop' => 'image' ) );   
        
        echo '</a>';
    }        
}
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_post_entry_header' ) ) :
/**
 * Post Entry Header
*/
function preschool_and_kindergarten_pro_post_entry_header(){
    ?>
    <header class="entry-header">
        <?php
            if( is_single() ){
                the_title( '<h1 class="entry-title" itemprop="headline">', '</h1>' );
            }else{
                the_title( '<h2 class="entry-title" itemprop="headline"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
            }
        ?>
        <div class="entry-meta">
            <?php 
            if ( 'post' === get_post_type() ){ 
                preschool_and_kindergarten_pro_meta( true, true, true, true ); 
            } 
            ?>
        </div>
    </header><!-- .entry-header -->
    <?php
}
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_excerpt_char_to_display' ) ):
/**
* Excerpt  function
*
*/
    function preschool_and_kindergarten_pro_excerpt_char_to_display(){
        $excerpt_char      = get_theme_mod( 'excerpt_char', 200 ); //From Cust
        ?>
        <div class="entry-content">
            <?php 
            if( has_excerpt() ){
                the_excerpt();        
            }else{ 
                echo wpautop( wp_kses_post( force_balance_tags( preschool_and_kindergarten_pro_excerpt( get_the_content(), $excerpt_char ) ) ) );        
            } ?>
        </div>
    <?php 
    } 

endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_slider' ) ) : 
/**
 * Callback for Banner Slider 
 */
function preschool_and_kindergarten_pro_slider(){

    if( get_theme_mod( 'ed_slider' ) &&  is_front_page() && !is_home() ){
    
        $slider_caption    = get_theme_mod( 'ed_slider_caption', '1' );
        $slider_type       = get_theme_mod( 'slider_type', 'post' ); 
        $slider_post_one   = get_theme_mod( 'slider_page_one' );
        $slider_post_two   = get_theme_mod( 'slider_page_two' );
        $slider_post_three = get_theme_mod( 'slider_page_three' );
        $slider_post_four  = get_theme_mod( 'slider_page_four' );
        $slider_post_five  = get_theme_mod( 'slider_page_five' );
        $slider_cat        = get_theme_mod( 'slider_category' );
        $slider_slides     = get_theme_mod( 'slider_repeater_settings' );
        $slider_readmore   = get_theme_mod( 'slider_readmore', __( 'Learn More', 'preschool-and-kindergarten-pro' ) );
        $slider_full_img   = get_theme_mod( 'ed_fullsize_img' );
        $demo_content      = get_theme_mod( 'ed_demo', '1' );
                
        $slider_posts      = array( $slider_post_one, $slider_post_two, $slider_post_three, $slider_post_four, $slider_post_five );
        $slider_posts      = array_diff( array_unique( $slider_posts ), array('') );
        
        if( $slider_full_img ){
            $img_size = 'full';
        }else{
            $img_size = 'preschool-and-kindergarten-pro-banner-thumb';
        }?>
         
        <?php     
        if( $slider_type == 'post' || $slider_type == 'cat' ){
            
                $args =  array( 
                    'post_status'         => 'publish',
                    'posts_per_page'      => -1,                    
                    'ignore_sticky_posts' => true
                );

                if( $slider_type == 'post' ){
                $args['post_type'] =  'page';
                $args['post__in']  = $slider_posts;
                $args['orderby']   = 'post__in';
                }elseif( $slider_type == 'cat' ){
                    $args['post_type'] = 'post';
                    $args['cat']       = $slider_cat;
                }
                
                $qry = new WP_Query( $args );
            
                if( ( ( $slider_posts && $slider_type == 'post' ) || ( $slider_cat && $slider_type == 'cat' ) ) && $qry->have_posts() ){ ?>
                    <div id="banner_section" class="banner">
                        <div id="banner-slider" class="owl-carousel owl-theme">
                            <?php
                            while( $qry->have_posts() ){
                                $qry->the_post();
                                $image = wp_get_attachment_image_src( get_post_thumbnail_id(), $img_size );
                            ?>
                                <?php if( has_post_thumbnail() ){ ?>
                                <div>
                                    <img class="owl-lazy" data-src="<?php echo esc_url( $image[0] ); ?>"  alt="<?php the_title_attribute(); ?>" />
                                    <?php if( $slider_caption ){ ?>
                                    <div class="banner-text">
                                        <div class="container">
                                            <div class="text-holder">                                              
                                                <strong class="title"><?php the_title(); ?></strong>
                                                <?php if( has_excerpt() ){
                                                        the_excerpt();        
                                                      }else{
                                                     echo wpautop( wp_kses_post( force_balance_tags( preschool_and_kindergarten_pro_excerpt( get_the_content(), 90, '...', false, false ) ) ) );        
                                                      }
                                                if( $slider_readmore ){ ?>
                                                <a class="btn-enroll" href="<?php the_permalink(); ?>"><?php echo esc_html( $slider_readmore );?></a>
                                                <?php } ?>                                        
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                            <?php
                            } ?>
                        </div>
                    </div>  
                <?php 
                    wp_reset_postdata();       
                }elseif( $demo_content ){
           /** Default Content */
           preschool_and_kindergarten_pro_demo_content( 'banner' ); 
        } // end of custom slider   
            
        }elseif( $slider_type == 'custom' && $slider_slides ){ //end of post and cat slider ?>
            <div id="banner_section" class="banner">
                <div id="banner-slider" class="owl-carousel owl-theme">
                    <?php
                    foreach( $slider_slides as $slides ){
                        if( $slides['thumbnail'] ){
                            $image = wp_get_attachment_image_src( $slides['thumbnail'], $img_size );
                            if( $image ){
                            ?>

                            <div>
                                <img class="owl-lazy" data-src="<?php echo esc_url( $image[0] ); ?>"  alt="<?php echo esc_attr( $slides['title'] ); ?>" />
                                <?php if( $slider_caption && ( $slides['title'] || ( $slides['link'] && $slider_readmore ) ) ){ ?>
                                <div class="banner-text">
                                    <div class="container">
                                        <div class="text-holder">
                                            <?php 
                                            if( $slides['title'] ) echo '<strong class="title">' . esc_html( $slides['title'] ) . '</strong>';
                                            echo '<p>' . esc_html( $slides['content'] ) . '</p>';
                                            if( $slides['link'] && $slider_readmore ){ ?>
                                            <a href="<?php echo esc_url( $slides['link'] ); ?>" class="btn-enroll"><?php echo esc_html( $slider_readmore ); ?></a>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>                                
                                <?php }?>
                            </div>
                            <?php        
                            }        
                        }
                    } ?>
                </div>
            </div>
        <?php }elseif( $demo_content ){
           /** Default Content */
           preschool_and_kindergarten_pro_demo_content( 'banner' ); 
        } // end of custom slider  
        
    }
   
}
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_social_share' ) ) :
/**
 * Social Sharing
*/
function preschool_and_kindergarten_pro_social_share(){
    $ed_share = get_theme_mod( 'ed_social_sharing' );
    $ed_float = get_theme_mod( 'ed_social_float' );
    $shares   = get_theme_mod( 'social_share', array( 'facebook', 'twitter', 'linkedin', 'pinterest' ) );
    
    if( $ed_share && ( 'post' === get_post_type() ) && is_single() ){   
    ?>
    <div class="social-share<?php if( $ed_float ) echo esc_attr( ' floating-share' ); ?>">
        <ul class="share-links">
            <?php
            foreach( $shares as $share ){
                preschool_and_kindergarten_pro_get_social_share( $share );                    
            }
            ?>
        </ul>
    </div>
    <?php
    }
}
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_post_author' ) ) :
/**
 * Author Bio
 * 
*/
function preschool_and_kindergarten_pro_post_author(){
    if( get_theme_mod( 'ed_bio', '1' ) && get_the_author_meta( 'description' ) ){
    ?>
    <section class="author-section">
        <div class="img-holder">
            <?php echo get_avatar( get_the_author_meta( 'ID' ), 105 ); ?>
        </div>
        <div class="text-holder">
            <span class="name"><?php echo esc_html( get_the_author_meta( 'display_name' ) ); ?></span>              
            <?php echo wpautop( wp_kses_post( get_the_author_meta( 'description' ) ) ); ?>
        </div>
    </section>
    <?php  
    }  
}
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_contact_details_cb' ) ) :

/**
 * Contact Details in Contact Page
*/
function preschool_and_kindergarten_pro_contact_details_cb(){
         
        // In contact page
        $as_in_header    = get_theme_mod( 'contact_phone_email_as_header', '1' );
        $home_phone      = $as_in_header ? get_theme_mod( 'preschool_and_kindergarten_phone', __( '123 456 1234', 'preschool-and-kindergarten-pro' ) ) : get_theme_mod( 'home_contact_section_phone' );
        $home_email      = $as_in_header ? get_theme_mod( 'preschool_and_kindergarten_email_address', __( 'contact@kinderschool.com', 'preschool-and-kindergarten-pro' ) ) : get_theme_mod( 'home_contact_section_email' );
        $as_in_home      = get_theme_mod( 'ed_asin_home_page', '1' );                
        $contact_address = $as_in_home ? get_theme_mod( 'home_contact_section_address' ) : get_theme_mod( 'contact_address' );
        $contact_phone   = $as_in_home ? $home_phone : get_theme_mod( 'contact_phone' );
        $contact_email   = $as_in_home ? $home_email : get_theme_mod( 'contact_email' );
        $phones          = explode( ',', $contact_phone);
        $emails          = explode( ',', $contact_email);
        if( $contact_address || $contact_phone || $contact_email ){
        ?>            
            <div class="right">
                <div class="contact-info">
                <?php
                    if( $contact_address ){
                        echo '<div class="address"><div class="icon-holder"><i class="fa fa-map-marker"></i></div>';
                        echo '<div class="text-holder">';
                        echo '<strong>' . esc_html__( 'Address', 'preschool-and-kindergarten-pro' ) . '</strong>';
                        echo '<address>' . esc_html( $contact_address ) . '</address>';
                        echo '</div>';
                        echo '</div>';
                    }
                    if( $contact_phone ){
                        echo '<div class="phone"><div class="icon-holder"><i class="fa fa-mobile"></i></div>';
                        echo '<div class="text-holder">';
                        echo '<strong>' . esc_html__( 'Phone', 'preschool-and-kindergarten-pro' ) . '</strong>';
                        if( $phones ){
                            foreach ( $phones as $phone ) {
                                echo '<a href="tel:' .  preg_replace('/\D/', '', $phone) . '">' . esc_html( $phone ) . '</a>';
                            }
                        }
                        echo '</div>';
                        echo '</div>';
                    }
                    if( $contact_email ){
                        echo '<div class="email"><div class="icon-holder"><i class="fa fa-envelope"></i></div>';
                        echo '<div class="text-holder">';
                        echo '<strong>' . esc_html__( 'E-mail', 'preschool-and-kindergarten-pro' ) . '</strong>';
                        if( $emails ){
                            foreach ( $emails as $email ) {
                                echo '<a href="mailto:' . sanitize_email( $email ) . '">' . esc_html( $email ) . '</a>';
                            }
                        }
                        echo '</div>';
                        echo '</div>';
                    }
                ?>
                </div>
            </div>
        <?php 
        }
}
endif;


if( ! function_exists( 'preschool_and_kindergarten_pro_get_comment_section' ) ) :
/**
 * Comment template
 * 
 * @since 1.0.1
*/
function preschool_and_kindergarten_pro_get_comment_section(){
    // If comments are open or we have at least one comment, load up the comment template.
    if ( get_theme_mod( 'ed_comments', '1' ) && ( comments_open() || get_comments_number() ) ) : ?>
        <hr>
        <?php comments_template();
    endif;
}

endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_content_end' ) ) :
/**
 * Content End
 * 
 * @since 1.0.1
*/
    function preschool_and_kindergarten_pro_content_end(){
         $home_sections = get_theme_mod( 'sort_home_section', array( 'about', 'activities', 'subscription', 'features', 'events', 'promotional', 'course', 'team', 'testimonial', 'gallery', 'blog', 'CTA', 'contact' ) );
        
        if( ! is_page_template( array( 'templates/template-about.php', 'templates/template-contact.php', 'templates/template-services.php' ) ) && !( is_front_page() && ! is_home() && $home_sections ) ):  ?>
                    </div><!-- row -->
                </div><!-- .container -->
            </div><!-- #content -->
            
        <?php
        endif;
    }
    endif;



if( ! function_exists( 'preschool_and_kindergarten_pro_footer_start' ) ) :
/**
 * Footer Start
 * 
 * @since 1.0.1
*/
    function preschool_and_kindergarten_pro_footer_start(){
        ?>
        <footer id="colophon" class="site-footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
            <div class="container">
        <?php
    }
endif;



if( ! function_exists( 'preschool_and_kindergarten_pro_footer_top' ) ) :
/**
 * Footer Top
 * 
 * @since 1.0.1
*/
    function preschool_and_kindergarten_pro_footer_top(){    
        if( is_active_sidebar( 'footer-one' ) || is_active_sidebar( 'footer-two' ) || is_active_sidebar( 'footer-three' ) || is_active_sidebar( 'footer-four' ) ){
        ?>
           <div class="footer-t">
                <div class="row">
                    
                    <?php if( is_active_sidebar( 'footer-one' ) ){ ?>
                        <div class="column">
                           <?php dynamic_sidebar( 'footer-one' ); ?>    
                        </div>
                    <?php } ?>
                    
                    <?php if( is_active_sidebar( 'footer-two' ) ){ ?>
                        <div class="column">
                           <?php dynamic_sidebar( 'footer-two' ); ?>    
                        </div>
                    <?php } ?>
                    
                    <?php if( is_active_sidebar( 'footer-three' ) ){ ?>
                        <div class="column">
                           <?php dynamic_sidebar( 'footer-three' ); ?>  
                        </div>
                    <?php } ?>
                    
                </div>
            </div>
        <?php 
        }   
    }
endif;



if( ! function_exists( 'preschool_and_kindergarten_pro_footer_bottom' ) ) :
/**
 * Footer Bottom
 * 
 * @since 1.0.1 
*/
    function preschool_and_kindergarten_pro_footer_bottom(){

        $footer_copyright = get_theme_mod( 'footer_copyright' );
        $ed_author_link   = get_theme_mod( 'ed_author_link' );
        $ed_wp_link       = get_theme_mod( 'ed_wp_link' ); 
        $text = ''; 
    ?>
        <div class="site-info">
        
            <?php 
            if( $footer_copyright ){
                $text .=  wp_kses_post( preschool_and_kindergarten_pro_apply_theme_shortcode( $footer_copyright ) );
            }else{
                $text .=  esc_html__( 'Copyright &copy;', 'preschool-and-kindergarten-pro' ) . date_i18n( esc_html__( 'Y', 'preschool-and-kindergarten-pro' ) ); 
                $text .= ' <a href="' . esc_url( home_url( '/' ) ) . '">' . esc_html( get_bloginfo( 'name' ) ) . '</a>.';
            }            
            if( ! $ed_author_link ){
            $text .= '<a href="' . esc_url( 'http://raratheme.com/wordpress-themes/preschool-and-kindergarten-pro/' ) .'" rel="author" target="_blank">' . esc_html__( ' Preschool And Kindergarten Pro by Rara Theme', 'preschool-and-kindergarten-pro' ) . '</a>. ';
            }            
            if( ! $ed_wp_link ){
                $text .= sprintf( esc_html__( 'Powered by %s', 'preschool-and-kindergarten-pro' ), '<a href="'. esc_url( __( 'https://wordpress.org/', 'preschool-and-kindergarten-pro' ) ) .'" target="_blank">WordPress</a>' );
            }
            if ( function_exists( 'the_privacy_policy_link' ) ) {
                  $text .= get_the_privacy_policy_link();
            }
            echo apply_filters( 'preschool_and_kindergarten_pro_footer_text', $text );  ?>
           
        </div>
    <?php }
endif;



if( ! function_exists( 'preschool_and_kindergarten_pro_footer_end' ) ) :
/**
 * Footer End
 * 
 * @since 1.0.1 
*/
    function preschool_and_kindergarten_pro_footer_end(){
        ?>
        </div>
        </footer><!-- #colophon -->
        <div id="rara-top"><i class="fa fa-angle-up"></i></div>
        <?php
    }
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_page_end' ) ) :
/**
 * Page End
 * 
 * @since 1.0.1
*/
    function preschool_and_kindergarten_pro_page_end(){
        ?>
        </div><!-- #page -->
        <?php
    }
endif;
