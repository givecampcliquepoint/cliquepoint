<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * @package preschool_and_kindergarten_pro
 */

if ( ! function_exists( 'preschool_and_kindergarten_pro_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function preschool_and_kindergarten_pro_posted_on() {
    
    echo '<span class="posted-on">';
    printf( '<a href="%1$s" rel="bookmark"><time class="entry-date published updated" datetime="%2$s">%3$s</time></a>', esc_url( get_permalink() ), esc_attr( get_the_date( 'c' ) ), esc_html( get_the_date() ) );
    
    echo '</span>';

}
endif; 

if( ! function_exists( 'preschool_and_kindergarten_pro_posted_by' ) ) :
/**
 * Post posted by
*/
function preschool_and_kindergarten_pro_posted_by(){
    
    printf( '<span class="byline authors vcard" itemprop="author" itemscope itemtype="https://schema.org/Person"><a class="url fn n" href="%1$s">%2$s</a></span>', esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ), esc_html( get_the_author() ) );
}
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_comment_count' ) ) :
/**
 * Post Comment count
*/
function preschool_and_kindergarten_pro_comment_count(){
    
    if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
        echo '<span class="comments-link">';
        comments_popup_link( esc_html__( 'Leave a comment', 'preschool-and-kindergarten-pro' ), esc_html__( '1 Comment', 'preschool-and-kindergarten-pro' ), esc_html__( '% Comments', 'preschool-and-kindergarten-pro' ) );
        echo '</span>';
    }
    
}
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_categories' ) ) :
/**
 * Post Categories
*/
function preschool_and_kindergarten_pro_categories(){
    // Hide category text for pages.
    if ( 'post' === get_post_type() ) {
        /* translators: used between list items, there is a space after the comma */
        $categories_list = get_the_category_list( esc_html__( ', ', 'preschool-and-kindergarten-pro' ) );
        if ( $categories_list && preschool_and_kindergarten_pro_categorized_blog() ) {
            printf( '<span class="cat-links">%1$s</span>', $categories_list ); // WPCS: XSS OK.
        }
    }
}
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_meta' ) ) :
/**
 * Post Meta Data
*/
function preschool_and_kindergarten_pro_meta( $date = false, $author = false, $comment = false, $cat = false ){
    if( $date || $comment || $author || $cat ){
        
        $post_meta = get_theme_mod( 'post_meta', array( 'author', 'date', 'cat', 'comment' ) );
        
        echo '<div class="entry-meta">';
        
        foreach( $post_meta as $meta ){
            if( $meta === 'author' && $author ) preschool_and_kindergarten_pro_posted_by();
            if( $meta === 'date' && $date ) preschool_and_kindergarten_pro_posted_on();
            if( $meta === 'cat' && $cat ) preschool_and_kindergarten_pro_categories();
            if( $meta === 'comment' && $comment ) preschool_and_kindergarten_pro_comment_count();              
        }
        
        echo '</div>';
    }
}
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_get_social_share' ) ) :
/**
 * Get list of social sharing icons
 * http://www.sharelinkgenerator.com/
 * 
*/
function preschool_and_kindergarten_pro_get_social_share( $share ){
    global $post;
    
    switch( $share ){
        case 'facebook':
        echo '<li><a href="' . esc_url( 'https://www.facebook.com/sharer/sharer.php?u=' . get_the_permalink( $post->ID ) ) . '" rel="nofollow" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>';
        
        break;
        
        case 'twitter':
        echo '<li><a href="' . esc_url( 'https://twitter.com/home?status=' . get_the_title( $post->ID ) ) . '&nbsp;' . get_the_permalink( $post->ID ) . '" rel="nofollow" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>';
        
        break;
        
        case 'linkedin':
        echo '<li><a href="' . esc_url( 'https://www.linkedin.com/shareArticle?mini=true&url=' . get_the_permalink( $post->ID ) . '&title=' . get_the_title( $post->ID ) ) . '" rel="nofollow" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>';
        
        break;
        
        case 'pinterest':
        echo '<li><a href="' . esc_url( 'https://pinterest.com/pin/create/button/?url=' . get_the_permalink( $post->ID ) . '&description=' . get_the_title( $post->ID )  ) . '" rel="nofollow" target="_blank"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>';
        
        break;
        
        case 'email':
        echo '<li><a href="' . esc_url( 'mailto:?Subject=' . get_the_title( $post->ID ) . '&Body=' . get_the_permalink( $post->ID ) ) . '" rel="nofollow" target="_blank"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>';
        
        break;
        
        case 'gplus':
        echo '<li><a href="' . esc_url( 'https://plus.google.com/share?url=' . get_the_permalink( $post->ID ) ) . '" rel="nofollow" target="_blank"><i class="fa fa-google" aria-hidden="true"></i></a></li>';
        
        break;
        
        case 'stumble':
        echo '<li><a href="' . esc_url( 'http://www.stumbleupon.com/submit?url=' . get_the_permalink( $post->ID ) . '&title=' . get_the_title( $post->ID ) ) . '" rel="nofollow" target="_blank"><i class="fa fa-stumbleupon" aria-hidden="true"></i></a></li>';
        
        break;
        
        case 'reddit':
        echo '<li><a href="' . esc_url( 'http://www.reddit.com/submit?url=' . get_the_permalink( $post->ID ) . '&title=' . get_the_title( $post->ID ) ) . '" rel="nofollow" target="_blank"><i class="fa fa-reddit" aria-hidden="true"></i></a></li>';
        
        break;                
    }
}
endif;


if ( ! function_exists( 'preschool_and_kindergarten_pro_entry_footer' ) ) :
/**
 * Prints edit links
 */
function preschool_and_kindergarten_pro_entry_footer() {   
    edit_post_link(
        sprintf(
            /* translators: %s: Name of current post */
            esc_html__( 'Edit %s', 'preschool-and-kindergarten-pro' ),
            the_title( '<span class="screen-reader-text">"', '"</span>', false )
        ),
        '<span class="edit-link">',
        '</span>'
    );
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function preschool_and_kindergarten_pro_categorized_blog() {
    if ( false === ( $all_the_cool_cats = get_transient( 'preschool_and_kindergarten_pro_categories' ) ) ) {
        // Create an array of all the categories that are attached to posts.
        $all_the_cool_cats = get_categories( array(
            'fields'     => 'ids',
            'hide_empty' => 1,
            // We only need to know if there is more than one category.
            'number'     => 2,
        ) );

        // Count the number of categories that are attached to the posts.
        $all_the_cool_cats = count( $all_the_cool_cats );

        set_transient( 'preschool_and_kindergarten_pro_categories', $all_the_cool_cats );
    }

    if ( $all_the_cool_cats > 1 ) {
        // This blog has more than 1 category so preschool_and_kindergarten_pro_categorized_blog should return true.
        return true;
    } else {
        // This blog has only 1 category so preschool_and_kindergarten_pro_categorized_blog should return false.
        return false;
    }
}

if( ! function_exists( 'preschool_and_kindergarten_pro_excerpt' ) ):  
/**
 * preschool_and_kindergarten_pro_excerpt can truncate a string up to a number of characters while preserving whole words and HTML tags
 *
 * @param string $text String to truncate.
 * @param integer $length Length of returned string, including ellipsis.
 * @param string $ending Ending to be appended to the trimmed string.
 * @param boolean $exact If false, $text will not be cut mid-word
 * @param boolean $considerHtml If true, HTML tags would be handled correctly
 *
 * @return string Trimmed string.
 * 
 * @link http://alanwhipple.com/2011/05/25/php-truncate-string-preserving-html-tags-words/
 */
function preschool_and_kindergarten_pro_excerpt($text, $length = 100, $ending = '...', $exact = false, $considerHtml = true) {
    $text = strip_shortcodes( $text );
    $text = preschool_and_kindergarten_pro_strip_single( 'img', $text );
    $text = preschool_and_kindergarten_pro_strip_single( 'a', $text );
    $text = preschool_and_kindergarten_pro_strip_single( 'b', $text );
    $text = preschool_and_kindergarten_pro_strip_single( 'i', $text );
    
    if ($considerHtml) {
        // if the plain text is shorter than the maximum length, return the whole text
        if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
            return $text;
        }
        // splits all html-tags to scanable lines
        preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
        $total_length = strlen($ending);
        $open_tags = array();
        $truncate = '';
        foreach ($lines as $line_matchings) {
            // if there is any html-tag in this line, handle it and add it (uncounted) to the output
            if (!empty($line_matchings[1])) {
                // if it's an "empty element" with or without xhtml-conform closing slash
                if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
                    // do nothing
                // if tag is a closing tag
                } else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
                    // delete tag from $open_tags list
                    $pos = array_search($tag_matchings[1], $open_tags);
                    if ($pos !== false) {
                    unset($open_tags[$pos]);
                    }
                // if tag is an opening tag
                } else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
                    // add tag to the beginning of $open_tags list
                    array_unshift($open_tags, strtolower($tag_matchings[1]));
                }
                // add html-tag to $truncate'd text
                $truncate .= $line_matchings[1];
            }
            // calculate the length of the plain text part of the line; handle entities as one character
            $content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
            if ($total_length+$content_length> $length) {
                // the number of characters which are left
                $left = $length - $total_length;
                $entities_length = 0;
                // search for html entities
                if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
                    // calculate the real length of all entities in the legal range
                    foreach ($entities[0] as $entity) {
                        if ($entity[1]+1-$entities_length <= $left) {
                            $left--;
                            $entities_length += strlen($entity[0]);
                        } else {
                            // no more characters left
                            break;
                        }
                    }
                }
                $truncate .= substr($line_matchings[2], 0, $left+$entities_length);
                // maximum lenght is reached, so get off the loop
                break;
            } else {
                $truncate .= $line_matchings[2];
                $total_length += $content_length;
            }
            // if the maximum length is reached, get off the loop
            if($total_length>= $length) {
                break;
            }
        }
    } else {
        if (strlen($text) <= $length) {
            return $text;
        } else {
            $truncate = substr($text, 0, $length - strlen($ending));
        }
    }
    // if the words shouldn't be cut in the middle...
    if (!$exact) {
        // ...search the last occurance of a space...
        $spacepos = strrpos($truncate, ' ');
        if (isset($spacepos)) {
            // ...and cut the text in this position
            $truncate = substr($truncate, 0, $spacepos);
        }
    }
    // add the defined ending to the text
    $truncate .= $ending;
    if($considerHtml) {
        // close all unclosed html-tags
        foreach ($open_tags as $tag) {
            $truncate .= '</' . $tag . '>';
        }
    }
    return $truncate;
}
endif; // End function_exists

/**
 * Strip specific tags from string
 * @link http://www.altafweb.com/2011/12/remove-specific-tag-from-php-string.html
*/
function preschool_and_kindergarten_pro_strip_single( $tag, $string ){
    $string = preg_replace('/<'.$tag.'[^>]*>/i', '', $string);
    $string = preg_replace('/<\/'.$tag.'>/i', '', $string);
    return $string;
} 

if( ! function_exists( 'preschool_and_kindergarten_pro_breadcrumb' ) ) :
/**
 * Breadcrumb 
*/
function preschool_and_kindergarten_pro_breadcrumb(){
    
   global $post;
    
    $post_page   = get_option( 'page_for_posts' ); //The ID of the page that displays posts.
    $show_front  = get_option( 'show_on_front' ); //What to show on the front page
    $delimiter   = get_theme_mod( 'breadcrumb_separator', __( '>', 'preschool-and-kindergarten-pro' ) ); // delimiter between crumbs
    $home        = get_theme_mod( 'breadcrumb_home_text', __( 'Home', 'preschool-and-kindergarten-pro' ) ); // text for the 'Home' link
    $before      = '<span class="current">'; // tag before the current crumb
    $after       = '</span>'; // tag after the current crumb
    
    if( get_theme_mod( 'ed_breadcrumb' ) && ! is_front_page() ){
        
        echo '<div id="crumbs"><a href="' . esc_url( home_url() ) . '">' . esc_html( $home ) . '</a> <span class="separator">' . esc_html( $delimiter ) . '</span> ';
        
        if( is_home() ){
            
            echo $before . esc_html( single_post_title( '', false ) ) . $after;
            
        }elseif( is_category() ){
            
            $thisCat = get_category( get_query_var( 'cat' ), false );
            
            if( $show_front === 'page' && $post_page ){ //If static blog post page is set
                $p = get_post( $post_page );
                echo ' <a href="' . esc_url( get_permalink( $post_page ) ) . '">' . esc_html( $p->post_title ) . '</a> <span class="separator">' . esc_html( $delimiter ) . '</span> ';  
            }
            
            if ( $thisCat->parent != 0 ) echo get_category_parents( $thisCat->parent, TRUE, ' <span class="separator">' . $delimiter . '</span> ' );
            echo $before .  esc_html( single_cat_title( '', false ) ) . $after;
        
        }elseif(preschool_and_kindergarten_pro_is_woocommerce_activated() && ( is_product_category() || is_product_tag() ) ){ //For Woocommerce archive page
        
            $current_term = $GLOBALS['wp_query']->get_queried_object();
            if( is_product_category() ){
                $ancestors = get_ancestors( $current_term->term_id, 'product_cat' );
                $ancestors = array_reverse( $ancestors );
                foreach ( $ancestors as $ancestor ) {
                    $ancestor = get_term( $ancestor, 'product_cat' );    
                    if ( ! is_wp_error( $ancestor ) && $ancestor ) {
                        echo ' <a href="' . esc_url( get_term_link( $ancestor ) ) . '">' . esc_html( $ancestor->name ) . '</a> <span class="separator">' . esc_html( $delimiter ) . '</span> ';
                    }
                }
            }           
            echo $before . esc_html( $current_term->name ) . $after;
            
        } elseif(preschool_and_kindergarten_pro_is_woocommerce_activated() && is_shop() ){ //Shop Archive page
            if ( get_option( 'page_on_front' ) == wc_get_page_id( 'shop' ) ) {
                return;
            }
            $_name = wc_get_page_id( 'shop' ) ? get_the_title( wc_get_page_id( 'shop' ) ) : '';
    
            if ( ! $_name ) {
                $product_post_type = get_post_type_object( 'product' );
                $_name = $product_post_type->labels->singular_name;
            }
            echo $before . esc_html( $_name ) . $after;
            
        }elseif( is_tag() ){
            
            echo $before . esc_html( single_tag_title( '', false ) ) . $after;
     
        }elseif( is_author() ){
            
            global $author;
            $userdata = get_userdata( $author );
            echo $before . esc_html( $userdata->display_name ) . $after;
     
        }elseif( is_search() ){
            
            echo $before . esc_html__( 'Search Results for "', 'preschool-and-kindergarten-pro' ) . esc_html( get_search_query() ) . esc_html__( '"', 'preschool-and-kindergarten-pro' ) . $after;
        
        }elseif( is_day() ){
            
            echo '<a href="' . esc_url( get_year_link( get_the_time( __( 'Y', 'preschool-and-kindergarten-pro' ) ) ) ) . '">' . esc_html( get_the_time( __( 'Y', 'preschool-and-kindergarten-pro' ) ) ) . '</a> <span class="separator">' . esc_html( $delimiter ) . '</span> ';
            echo '<a href="' . esc_url( get_month_link( get_the_time( __( 'Y', 'preschool-and-kindergarten-pro' ) ), get_the_time( __( 'm', 'preschool-and-kindergarten-pro' ) ) ) ) . '">' . esc_html( get_the_time( __( 'F', 'preschool-and-kindergarten-pro' ) ) ) . '</a> <span class="separator">' . esc_html( $delimiter ) . '</span> ';
            echo $before . esc_html( get_the_time( __( 'd', 'preschool-and-kindergarten-pro' ) ) ) . $after;
        
        }elseif( is_month() ){
            
            echo '<a href="' . esc_url( get_year_link( get_the_time( __( 'Y', 'preschool-and-kindergarten-pro' ) ) ) ) . '">' . esc_html( get_the_time( __( 'Y', 'preschool-and-kindergarten-pro' ) ) ) . '</a> <span class="separator">' . esc_html( $delimiter ) . '</span> ';
            echo $before . esc_html( get_the_time( __( 'F', 'preschool-and-kindergarten-pro' ) ) ) . $after;
        
        }elseif( is_year() ){
            
            echo $before . esc_html( get_the_time( __( 'Y', 'preschool-and-kindergarten-pro' ) ) ) . $after;
    
        }elseif( is_single() && !is_attachment() ){
            
            if( preschool_and_kindergarten_pro_is_woocommerce_activated() && 'product' === get_post_type() ){ //For Woocommerce single product
                /** NEED TO CHECK THIS PORTION WHILE INTEGRATION WITH WOOCOMMERCE */
                if ( $terms = wc_get_product_terms( $post->ID, 'product_cat', array( 'orderby' => 'parent', 'order' => 'DESC' ) ) ) {
                    $main_term = apply_filters( 'woocommerce_breadcrumb_main_term', $terms[0], $terms );
                    $ancestors = get_ancestors( $main_term->term_id, 'product_cat' );
                    $ancestors = array_reverse( $ancestors );
                    foreach ( $ancestors as $ancestor ) {
                        $ancestor = get_term( $ancestor, 'product_cat' );    
                        if ( ! is_wp_error( $ancestor ) && $ancestor ) {
                            echo ' <a href="' . esc_url( get_term_link( $ancestor ) ) . '">' . esc_html( $ancestor->name ) . '</a> <span class="separator">' . esc_html( $delimiter ) . '</span> ';
                        }
                    }
                    echo ' <a href="' . esc_url( get_term_link( $main_term ) ) . '">' . esc_html( $main_term->name ) . '</a> <span class="separator">' . esc_html( $delimiter ) . '</span> ';
                }
                
                echo $before . esc_html( get_the_title() ) . $after;
                
            }elseif ( get_post_type() != 'post' ){
                
                $post_type = get_post_type_object( get_post_type() );
                
                if( $post_type->has_archive == true ){// For CPT Archive Link
                   
                   // Add support for a non-standard label of 'archive_title' (special use case).
                   $label = !empty( $post_type->labels->archive_title ) ? $post_type->labels->archive_title : $post_type->labels->name;
                   printf( '<a href="%1$s">%2$s</a>', esc_url( get_post_type_archive_link( get_post_type() ) ), $label );
                   echo '<span class="separator">' . esc_html( $delimiter ) . '</span> ';
    
                }
                echo $before . esc_html( get_the_title() ) . $after;
                
            }else{ //For Post
                
                $cat_object       = get_the_category();
                $potential_parent = 0;
                
                if( $show_front === 'page' && $post_page ){ //If static blog post page is set
                    $p = get_post( $post_page );
                    echo ' <a href="' . esc_url( get_permalink( $post_page ) ) . '">' . esc_html( $p->post_title ) . '</a> <span class="separator">' . esc_html( $delimiter ) . '</span> ';  
                }
                
                if( is_array( $cat_object ) ){ //Getting category hierarchy if any
        
                    //Now try to find the deepest term of those that we know of
                    $use_term = key( $cat_object );
                    foreach( $cat_object as $key => $object )
                    {
                        //Can't use the next($cat_object) trick since order is unknown
                        if( $object->parent > 0  && ( $potential_parent === 0 || $object->parent === $potential_parent ) ){
                            $use_term = $key;
                            $potential_parent = $object->term_id;
                        }
                    }
                    
                    $cat = $cat_object[$use_term];
              
                    $cats = get_category_parents( $cat, TRUE, ' <span class="separator">' . esc_html( $delimiter ) . '</span> ' );
                    $cats = preg_replace( "#^(.+)\s$delimiter\s$#", "$1", $cats ); //NEED TO CHECK THIS
                    echo $cats;
                }
    
                echo $before . esc_html( get_the_title() ) . $after;
                
            }
        
        }elseif( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ){
            
            $post_type = get_post_type_object(get_post_type());
            if( get_query_var('paged') ){
                echo '<a href="' . esc_url( get_post_type_archive_link( $post_type->name ) ) . '">' . esc_html( $post_type->label ) . '</a>';
                echo ' <span class="separator">' . esc_html( $delimiter ) . '</span> ' . $before . sprintf( __('Page %s', 'preschool-and-kindergarten-pro'), get_query_var('paged') ) . $after;
            }elseif( is_archive() ){
                //echo $before . esc_html( $post_type->label ) . $after;
                echo $before . esc_html( post_type_archive_title() ) . $after;
            }else{
                echo $before . esc_html( $post_type->label ) . $after;
            }
    
        }elseif( is_attachment() ){
            
            $parent = get_post( $post->post_parent );
            $cat = get_the_category( $parent->ID ); 
            if( $cat ){
                $cat = $cat[0];
                echo get_category_parents( $cat, TRUE, ' <span class="separator">' . esc_html( $delimiter ) . '</span> ');
                echo '<a href="' . esc_url( get_permalink( $parent ) ) . '">' . esc_html( $parent->post_title ) . '</a>' . ' <span class="separator">' . esc_html( $delimiter ) . '</span> ';
            }
            echo  $before . esc_html( get_the_title() ) . $after;
        
        }elseif( is_page() && !$post->post_parent ){
            
            echo $before . esc_html( get_the_title() ) . $after;
    
        }elseif( is_page() && $post->post_parent ){
            
            $parent_id  = $post->post_parent;
            $breadcrumbs = array();
            
            while( $parent_id ){
                $page = get_post( $parent_id );
                $breadcrumbs[] = '<a href="' . esc_url( get_permalink( $page->ID ) ) . '">' . esc_html( get_the_title( $page->ID ) ) . '</a>';
                $parent_id  = $page->post_parent;
            }
            $breadcrumbs = array_reverse( $breadcrumbs );
            for ( $i = 0; $i < count( $breadcrumbs) ; $i++ ){
                echo $breadcrumbs[$i];
                if ( $i != count( $breadcrumbs ) - 1 ) echo ' <span class="separator">' . esc_html( $delimiter ) . '</span> ';
            }
            echo ' <span class="separator">' . esc_html( $delimiter ) . '</span> ' . $before . esc_html( get_the_title() ) . $after;
        
        }elseif( is_404() ){
            echo $before . esc_html__( '404 Error - Page Not Found', 'preschool-and-kindergarten-pro' ) . $after;
        }
        
        if( get_query_var('paged') ) echo __( ' (Page', 'preschool-and-kindergarten-pro' ) . ' ' . get_query_var('paged') . __( ')', 'preschool-and-kindergarten-pro' );
        
        echo '</div>';
        
    }
}
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_pagination' ) ) :
/**
 * Pagination
*/
function preschool_and_kindergarten_pro_pagination(){
    
    if( is_single() ){
        the_post_navigation();
    }else{        
        $pagination = get_theme_mod( 'pagination_type', 'default' );
        
        switch( $pagination ){
            case 'default': // Default Pagination
            
            the_posts_navigation();
            
            break;
            
            case 'numbered': // Numbered Pagination
            
            the_posts_pagination( array(
                'prev_text'          => __( '<', 'preschool-and-kindergarten-pro' ),
                'next_text'          => __( '>', 'preschool-and-kindergarten-pro' ),
                'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'preschool-and-kindergarten-pro' ) . ' </span>',
             ) );
            
            break;
            
            case 'load_more': // Load More Button
            case 'infinite_scroll': // Auto Infinite Scroll
            
            echo '<div class="pagination"></div>';
            
            break;
            
            default:
            
            the_posts_navigation();
            
            break;
        }
    }
}
endif;

/**
 * Returns Section header
*/
function preschool_and_kindergarten_pro_get_section_header( $title, $description ){
?>
    <header class="header">
        <?php 
        echo '<h2 class="title">';
        echo esc_html( $title );
        echo '</h2>';
        echo '<p>';
        echo wp_kses_post( $description );
        echo '</p>'; 
        ?>
    </header>
    <?php    
}

/**
 * Function to list Custom Pattern
*/
function preschool_and_kindergarten_pro_get_patterns(){
    $patterns = array();
    $patterns['nobg'] = get_template_directory_uri() . '/images/patterns_thumb/' . 'nobg.png';
    for( $i=0; $i<38; $i++ ){
        $patterns['pattern'.$i] = get_template_directory_uri() . '/images/patterns_thumb/' . 'pattern' . $i .'.png';
    }
    for( $j=1; $j<26; $j++ ){
        $patterns['hbg'.$j] = get_template_directory_uri() . '/images/patterns_thumb/' . 'hbg' . $j . '.png';
    }
    return $patterns;
}

/**
 * Function to list dynamic sidebar
*/
function preschool_and_kindergarten_pro_get_dynamnic_sidebar( $nosidebar = false, $sidebar = false, $default = false ){
    $sidebar_arr = array();
    $sidebars = get_theme_mod( 'sidebar' );
    
    if( $default ) $sidebar_arr['default-sidebar'] = __( 'Default Sidebar', 'preschool-and-kindergarten-pro' );
    if( $sidebar ) $sidebar_arr['sidebar'] = __( 'Sidebar', 'preschool-and-kindergarten-pro' );
    
    if( $sidebars ){        
        foreach( $sidebars as $sidebar ){            
            $id = $sidebar['name'] ? sanitize_title( $sidebar['name'] ) : 'rara-sidebar-one';
            $sidebar_arr[$id] = $sidebar['name'];
        }
    }
    
    if( $nosidebar ) $sidebar_arr['no-sidebar'] = __( 'No Sidebar', 'preschool-and-kindergarten-pro' );
    
    return $sidebar_arr;
}

if( ! function_exists( 'preschool_and_kindergarten_pro_sidebar' ) ) :
/**
 * Function to retrive page specific sidebar and corresponding body class
 * 
 * @param boolean $sidebar
 * @param boolean $class
 * 
 * @return string dynamic sidebar id / classname
*/
function preschool_and_kindergarten_pro_sidebar( $sidebar = false, $class = false ){
    
    global $post;
    $return = false;
    $layout = get_theme_mod( 'layout_style', 'right-sidebar' ); //Default Layout Style for Styling Settings
    
    if( ( is_front_page() && is_home() ) || is_home() ){
        //blog/home page 
        $blog_sidebar = get_theme_mod( 'blog_page_sidebar', 'sidebar' );
        
        if( is_active_sidebar( $blog_sidebar ) ){            
            if( $sidebar ) $return = $blog_sidebar; //With Sidebar
            if( $class && $layout == 'right-sidebar' ) $return = 'rightsidebar'; 
            if( $class && $layout == 'left-sidebar' )  $return = 'leftsidebar';
        }else{
            if( $sidebar ) $return = false; //Fullwidth
            if( $class ) $return = 'full-width';
        }        
    }
    
    if( is_archive() ){
        //archive page
        $archive_sidebar = get_theme_mod( 'archive_page_sidebar', 'sidebar' );
        $cat_sidebar     = get_theme_mod( 'cat_archive_page_sidebar', 'default-sidebar' );
        $tag_sidebar     = get_theme_mod( 'tag_archive_page_sidebar', 'default-sidebar' );
        $date_sidebar    = get_theme_mod( 'date_archive_page_sidebar', 'default-sidebar' );
        $author_sidebar  = get_theme_mod( 'author_archive_page_sidebar', 'default-sidebar' );        
        
        if( is_category() ){
            
            if( $cat_sidebar == 'no-sidebar' || ( $cat_sidebar == 'default-sidebar' && $archive_sidebar == 'no-sidebar' ) ){
                if( $sidebar ) $return = false; //Fullwidth
                if( $class ) $return = 'full-width';
            }elseif( $cat_sidebar == 'default-sidebar' && $archive_sidebar != 'no-sidebar' && is_active_sidebar( $archive_sidebar ) ){
                if( $sidebar ) $return = $archive_sidebar;
                if( $class && $layout == 'right-sidebar' ) $return = 'rightsidebar'; //With Sidebar
                if( $class && $layout == 'left-sidebar' ) $return = 'leftsidebar';
            }elseif( is_active_sidebar( $cat_sidebar ) ){
                if( $sidebar ) $return = $cat_sidebar;
                if( $class && $layout == 'right-sidebar' ) $return = 'rightsidebar'; //With Sidebar
                if( $class && $layout == 'left-sidebar' ) $return = 'leftsidebar';
            }else{
                if( $sidebar ) $return = false; //Fullwidth
                if( $class ) $return = 'full-width';
            }
                
        }elseif( is_tag() ){
            
            if( $tag_sidebar == 'no-sidebar' || ( $tag_sidebar == 'default-sidebar' && $archive_sidebar == 'no-sidebar' ) ){
                if( $sidebar ) $return = false; //Fullwidth
                if( $class ) $return = 'full-width';
            }elseif( ( $tag_sidebar == 'default-sidebar' && $archive_sidebar != 'no-sidebar' && is_active_sidebar( $archive_sidebar ) ) ){
                if( $sidebar ) $return = $archive_sidebar;
                if( $class && $layout == 'right-sidebar' ) $return = 'rightsidebar'; //With Sidebar
                if( $class && $layout == 'left-sidebar' ) $return = 'leftsidebar';
            }elseif( is_active_sidebar( $tag_sidebar ) ){
                if( $sidebar ) $return = $tag_sidebar;
                if( $class && $layout == 'right-sidebar' ) $return = 'rightsidebar'; //With Sidebar
                if( $class && $layout == 'left-sidebar' ) $return = 'leftsidebar';              
            }else{
                if( $sidebar ) $return = false; //Fullwidth
                if( $class ) $return = 'full-width';
            }
            
        }elseif( is_author() ){
            
            if( $author_sidebar == 'no-sidebar' || ( $author_sidebar == 'default-sidebar' && $archive_sidebar == 'no-sidebar' ) ){
                if( $sidebar ) $return = false; //Fullwidth
                if( $class ) $return = 'full-width';
            }elseif( ( $author_sidebar == 'default-sidebar' && $archive_sidebar != 'no-sidebar' && is_active_sidebar( $archive_sidebar ) ) ){
                if( $sidebar ) $return = $archive_sidebar;
                if( $class && $layout == 'right-sidebar' ) $return = 'rightsidebar'; //With Sidebar
                if( $class && $layout == 'left-sidebar' ) $return = 'leftsidebar';
            }elseif( is_active_sidebar( $author_sidebar ) ){
                if( $sidebar ) $return = $author_sidebar;
                if( $class && $layout == 'right-sidebar' ) $return = 'rightsidebar'; //With Sidebar
                if( $class && $layout == 'left-sidebar' ) $return = 'leftsidebar';
            }else{
                if( $sidebar ) $return = false; //Fullwidth
                if( $class ) $return = 'full-width';
            }
            
        }elseif( is_date() ){
            
            if( $date_sidebar == 'no-sidebar' || ( $date_sidebar == 'default-sidebar' && $archive_sidebar == 'no-sidebar' ) ){
                if( $sidebar ) $return = false; //Fullwidth
                if( $class ) $return = 'full-width';
            }elseif( ( $date_sidebar == 'default-sidebar' && $archive_sidebar != 'no-sidebar' && is_active_sidebar( $archive_sidebar ) ) ){
                if( $sidebar ) $return = $archive_sidebar;
                if( $class && $layout == 'right-sidebar' ) $return = 'rightsidebar'; //With Sidebar
                if( $class && $layout == 'left-sidebar' ) $return = 'leftsidebar';
            }elseif( is_active_sidebar( $date_sidebar ) ){
                if( $sidebar ) $return = $date_sidebar;
                if( $class && $layout == 'right-sidebar' ) $return = 'rightsidebar'; //With Sidebar
                if( $class && $layout == 'left-sidebar' ) $return = 'leftsidebar';
            }else{
                if( $sidebar ) $return = false; //Fullwidth
                if( $class ) $return = 'full-width';
            }                         
            
        }elseif( preschool_and_kindergarten_pro_is_woocommerce_activated() && ( is_shop() || is_product_category() || is_product_tag() ) ){ //For Woocommerce
            
            if( is_active_sidebar( 'shop-sidebar' ) ){
                if( $class && $layout == 'right-sidebar' ) $return = 'rightsidebar'; //With Sidebar
                if( $class && $layout == 'left-sidebar' ) $return = 'leftsidebar';
            }else{
                if( $class ) $return = 'full-width';
            }            
                    
        }else{
            if( $archive_sidebar != 'no-sidebar' && is_active_sidebar( $archive_sidebar ) ){
                if( $sidebar ) $return = $archive_sidebar;
                if( $class && $layout == 'right-sidebar' ) $return = 'rightsidebar'; //With Sidebar
                if( $class && $layout == 'left-sidebar' ) $return = 'leftsidebar';
            }else{
                if( $sidebar ) $return = false; //Fullwidth
                if( $class ) $return = 'full-width';
            }                      
        }
        
    }
    
    if( is_singular() ){
        $post_sidebar = get_theme_mod( 'single_post_sidebar', 'sidebar' );
        $page_sidebar = get_theme_mod( 'single_page_sidebar', 'sidebar' );
        
        if( get_post_meta( $post->ID, '_preschool_and_kindergarten_pro_sidebar', true ) ){
            $single_sidebar = get_post_meta( $post->ID, '_preschool_and_kindergarten_pro_sidebar', true );
        }else{
            $single_sidebar = 'default-sidebar';
        }

        if( get_post_meta( $post->ID, '_preschool_and_kindergarten_pro_sidebar_layout', true ) ){
            $sidebar_layout = get_post_meta( $post->ID, '_preschool_and_kindergarten_pro_sidebar_layout', true );
        }else{
            $sidebar_layout = 'default-sidebar';
        }
        
        if( is_page() ){
            
            if( ( $single_sidebar == 'no-sidebar' ) || ( ( $single_sidebar == 'default-sidebar' ) && ( $page_sidebar == 'no-sidebar' ) ) ){
                if( $sidebar ) $return = false; //Fullwidth
                if( $class ) $return = 'full-width';
            }elseif( $single_sidebar == 'default-sidebar' && $page_sidebar != 'no-sidebar' && is_active_sidebar( $page_sidebar ) ){
                if( $sidebar ) $return = $page_sidebar;
                if( $class && ( ( $sidebar_layout == 'default-sidebar' && $layout == 'right-sidebar' ) || ( $sidebar_layout == 'right-sidebar' ) ) ) $return = 'rightsidebar';
                if( $class && ( ( $sidebar_layout == 'default-sidebar' && $layout == 'left-sidebar' ) || ( $sidebar_layout == 'left-sidebar' ) ) ) $return = 'leftsidebar';
            }elseif( is_active_sidebar( $single_sidebar ) ){
                if( $sidebar ) $return = $single_sidebar;
                if( $class && ( ( $sidebar_layout == 'default-sidebar' && $layout == 'right-sidebar' ) || ( $sidebar_layout == 'right-sidebar' ) ) ) $return = 'rightsidebar';
                if( $class && ( ( $sidebar_layout == 'default-sidebar' && $layout == 'left-sidebar' ) || ( $sidebar_layout == 'left-sidebar' ) ) ) $return = 'leftsidebar';
            }else{
                if( $sidebar ) $return = false; //Fullwidth
                if( $class ) $return = 'full-width';
            }
            
        }elseif( is_single() ){
            if( preschool_and_kindergarten_pro_is_woocommerce_activated() && 'product' === get_post_type() ){
                if( is_active_sidebar( 'shop-sidebar' ) ){
                    if( $class && $layout == 'right-sidebar' ) $return = 'rightsidebar'; //With Sidebar
                    if( $class && $layout == 'left-sidebar' ) $return = 'leftsidebar';
                }else{
                    if( $class ) $return = 'full-width';
                }
            }elseif( ( $single_sidebar == 'no-sidebar' ) || ( ( $single_sidebar == 'default-sidebar' ) && ( $post_sidebar == 'no-sidebar' ) ) ){
                if( $sidebar ) $return = false; //Fullwidth
                if( $class ) $return = 'full-width';
            }elseif( $single_sidebar == 'default-sidebar' && $post_sidebar != 'no-sidebar' && is_active_sidebar( $post_sidebar ) ){
                if( $sidebar ) $return = $post_sidebar;
                if( $class && ( ( $sidebar_layout == 'default-sidebar' && $layout == 'right-sidebar' ) || ( $sidebar_layout == 'right-sidebar' ) ) ) $return = 'rightsidebar';
                if( $class && ( ( $sidebar_layout == 'default-sidebar' && $layout == 'left-sidebar' ) || ( $sidebar_layout == 'left-sidebar' ) ) ) $return = 'leftsidebar';
            }elseif( is_active_sidebar( $single_sidebar ) ){
                if( $sidebar ) $return = $single_sidebar;
                if( $class && ( ( $sidebar_layout == 'default-sidebar' && $layout == 'right-sidebar' ) || ( $sidebar_layout == 'right-sidebar' ) ) ) $return = 'rightsidebar';
                if( $class && ( ( $sidebar_layout == 'default-sidebar' && $layout == 'left-sidebar' ) || ( $sidebar_layout == 'left-sidebar' ) ) ) $return = 'leftsidebar';
            }else{
                if( $sidebar ) $return = false; //Fullwidth
                if( $class ) $return = 'full-width';
            }
        }
    }
    
    if( is_search() ){
        $search_sidebar = get_theme_mod( 'search_page_sidebar', 'sidebar' );
                
        if( $search_sidebar != 'no-sidebar' && is_active_sidebar( $search_sidebar ) ){
            if( $sidebar ) $return = $search_sidebar;
            if( $class && $layout == 'right-sidebar' ) $return = 'rightsidebar'; //With Sidebar
            if( $class && $layout == 'left-sidebar' ) $return = 'leftsidebar';
        }else{
            if( $sidebar ) $return = false; //Fullwidth
            if( $class ) $return = 'full-width';
        }
        
    }
    
    return $return;        
}
endif;


if( ! function_exists( 'preschool_and_kindergarten_pro_get_categories' ) ) :
/**
 * Function to list post categories in customizer options
*/
function preschool_and_kindergarten_pro_get_categories( $select = true, $taxonomy = 'category', $slug = false ){
    
    /* Option list of all categories */
    $categories = array();
    
    $args = array( 
        'hide_empty' => false,
        'taxonomy'   => $taxonomy 
    );
    
    $catlists = get_terms( $args );
    if( $select ) $categories[''] = __( 'Choose Category', 'preschool-and-kindergarten-pro' );
    foreach( $catlists as $category ){
        if( $slug ){
            $categories[$category->slug] = $category->name;
        }else{
            $categories[$category->term_id] = $category->name;    
        }        
    }
    
    return $categories;
}
endif;

/**
 * Function to list Custom Post Type
*/
function preschool_and_kindergarten_pro_get_posts( $post_type = 'post' ){
    
    $args = array(
        'posts_per_page'   => -1,
        'post_type'        => $post_type,
        'post_status'      => 'publish',
        'suppress_filters' => true 
    );
    $posts_array = get_posts( $args );
    
    // Initate an empty array
    $post_options = array();
    $post_options[''] = __( ' -- Choose -- ', 'preschool-and-kindergarten-pro' );
    if ( ! empty( $posts_array ) ) {
        foreach ( $posts_array as $posts ) {
            $post_options[ $posts->ID ] = $posts->post_title;
        }
    }
    return $post_options;
    wp_reset_postdata();
}

if( ! function_exists( 'preschool_and_kindergarten_pro_create_post' ) ) :

/**
 * A function used to programmatically create a post and assign a page template in WordPress. 
 *
 * @link https://tommcfarlin.com/programmatically-create-a-post-in-wordpress/
 * @link https://tommcfarlin.com/programmatically-set-a-wordpress-template/
 */
    function preschool_and_kindergarten_pro_create_post( $title, $slug, $template ) {

        // Setup the author, page
        $author_id = 1;
        
        // Look for the page by the specified title. Set the ID to -1 if it doesn't exist.
        // Otherwise, set it to the page's ID.
        $page = get_page_by_title( $title, OBJECT, 'page' );
        $page_id = ( null == $page ) ? -1 : $page->ID;
        
        // If the page doesn't already exist, then create it
        if( $page_id == -1 ) {

            // Set the post ID so that we know the post was created successfully
            $post_id = wp_insert_post(
                array(
                    'comment_status' => 'closed',
                    'ping_status'    => 'closed',
                    'post_author'    => $author_id,
                    'post_name'      => $slug,
                    'post_title'     => $title,
                    'post_status'    => 'publish',
                    'post_type'      => 'page'
                )
            );
            
            if( $post_id ) update_post_meta( $post_id, '_wp_page_template', $template );

        // Otherwise, we'll stop
        }else{
           update_post_meta( $page_id, '_wp_page_template', $template );
        } // end if

    } // end programmatically_create_post
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_site_branding' ) ) :

    /**
     * Site Branding
    */
    function preschool_and_kindergarten_pro_site_branding(){ ?>
        <div class="site-branding" itemscope itemtype="http://schema.org/Organization">
            <?php 
                if( function_exists( 'has_custom_logo' ) && has_custom_logo() ){
                    the_custom_logo();
                } 
            ?>
            <div class="text-logo">
                <?php if ( is_front_page() ) : ?>
                    <h1 class="site-title" itemprop="name"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" itemprop="url"><?php bloginfo( 'name' ); ?></a></h1>
                <?php else : ?>
                    <p class="site-title" itemprop="name"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" itemprop="url"><?php bloginfo( 'name' ); ?></a></p>
                <?php endif;
                $description = get_bloginfo( 'description', 'display' );
                if ( $description || is_customize_preview() ) : ?>
                    <p class="site-description" itemprop="description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
                <?php
                endif; ?>
            </div>
        </div><!-- .site-branding -->
        <?php       
    }

endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_header_phone' ) ) :

    function preschool_and_kindergarten_pro_header_phone( $layout ){
        $phone_number  = get_theme_mod( 'preschool_and_kindergarten_phone', __( '843 123 45678', 'preschool-and-kindergarten-pro' ) ); 
        $phone_label   = get_theme_mod( 'phone_label', __( 'Call Us Anytime', 'preschool-and-kindergarten-pro' ) );
        
        $opening_time       = get_theme_mod( 'opening_time' ); 
        $opening_time_label = get_theme_mod( 'opening_time_label', __( 'Opening Time', 'preschool-and-kindergarten-pro' ) );
        
        $layout_array  = array( 'one', 'two', 'three', 'four', 'five', 'six', 'eight' );
        $layout_array1 = array( 'one', 'two' );
        $layout_array2 = array( 'three', 'four' );
        $layout_array3 = array( 'three' );
        $layout_array4 = array( 'five' );

       
        $phones = explode( ',', $phone_number);

            if( in_array( $layout, $layout_array ) ){

                if( $phone_number ){
                    if( $layout !== 'five' ) echo '<li class="phone">';
                    if( in_array( $layout, $layout_array2 ) ) echo '<span>' . $phone_label . '</span>'; ?>
                    <?php foreach ($phones as $phone ) { ?>
                        <a href="tel:<?php echo preg_replace('/\D/', '', $phone); ?>" class="tel-link" >
                        <?php 
                        if( in_array( $layout, $layout_array1 ) ) echo'<span class="fa fa-phone"></span>';
                            echo esc_html( $phone ); ?>
                        </a>
                    <?php } ?>
            <?php 
                    if( $layout !== 'five' ) echo '</li>';            }

                if( in_array( $layout, $layout_array3 ) && $opening_time ){ ?>
                    <li class="opening-time">
                        <span><?php echo esc_html( $opening_time_label ); ?></span>
                        <strong><?php echo esc_html( $opening_time ); ?></strong>
                    </li>
                <?php 
                } 
            }
    }
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_header_email' ) ) :

    /**
     * Header email
    */
    function preschool_and_kindergarten_pro_header_email( $layout ){

        $email_address = get_theme_mod( 'preschool_and_kindergarten_email_address', __( 'contact@kinderschool.com', 'preschool-and-kindergarten-pro' ) ); 
        $email_label   = get_theme_mod( 'email_label', __( 'Mail Us', 'preschool-and-kindergarten-pro' ) );
        
        $layout_array  = array( 'one', 'two', 'three', 'four', 'six', 'eight' );
        $layout_array1 = array( 'one', 'two' );
        $layout_array2 = array( 'three', 'four' );
        $emails        = explode( ',', $email_address);

        if( in_array( $layout, $layout_array ) ){
            
            if( $email_address ){ 
                echo '<li class="email">';
                if( in_array( $layout, $layout_array2 ) ) echo '<span>' . $email_label . '</span>'; ?>
                <?php if( $emails ){ 
                    foreach ($emails as $email ) {
                     ?>
                        <a href="mailto:<?php echo sanitize_email( $email ); ?>" class="email-link">
                            <?php 
                            if( in_array( $layout, $layout_array1 ) ) echo'<span class="fa fa-envelope"></span>';
                                echo esc_html( $email ); ?>
                            </a>
                   <?php
                    }
                } 
                echo '</li>';
            } 

        }
    }

endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_header_social' ) ) :

    /**
     * Header Social Icons
    */
    function preschool_and_kindergarten_pro_header_social(){
        if( is_active_sidebar( 'header-social' ) ) dynamic_sidebar( 'header-social' );
    }

endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_primary_nav' ) ) :

    /**
     * Primary Navigation
    */
    function preschool_and_kindergarten_pro_primary_nav(){ ?>

        <div id="mobile-header">
            <a id="responsive-menu-button" href="#sidr-main">
                <span class="fa fa-navicon"></span>
            </a>
        </div>
        <?php 

        $enabled_section = array();
        $ed_section_menu = get_theme_mod( 'ed_section_menu' );
        $ed_home_link    = get_theme_mod( 'ed_home_link', '1' );
        $home_sections   = get_theme_mod( 'sort_home_section', array( 'about', 'activities', 'features', 'events', 'course', 'team', 'testimonial', 'gallery', 'blog', 'contact' ) );
        
        $menu_about       = get_theme_mod( 'section_menu_about', __( 'About', 'preschool-and-kindergarten-pro' ) );
        $menu_activities  = get_theme_mod( 'section_menu_activities', __( 'Activities', 'preschool-and-kindergarten-pro' ) );
        $menu_features    = get_theme_mod( 'section_menu_features', __( 'Features', 'preschool-and-kindergarten-pro' ) );
        $menu_events      = get_theme_mod( 'section_menu_events', __( 'Events', 'preschool-and-kindergarten-pro' ) );
        $menu_courses     = get_theme_mod( 'section_menu_courses', __( 'Courses', 'preschool-and-kindergarten-pro' ) );
        $menu_team        = get_theme_mod( 'section_menu_team', __( 'Team', 'preschool-and-kindergarten-pro' ) );
        $menu_testimonial = get_theme_mod( 'section_menu_testimonial', __( 'Testimonials', 'preschool-and-kindergarten-pro' ) );
        $menu_gallery     = get_theme_mod( 'section_menu_gallery', __( 'Gallery', 'preschool-and-kindergarten-pro' ) );
        $menu_blog        = get_theme_mod( 'section_menu_blog', __( 'Blog', 'preschool-and-kindergarten-pro' ) );
        $menu_contact     = get_theme_mod( 'section_menu_contact', __( 'Contact', 'preschool-and-kindergarten-pro' ) );
        
        $menu_label = array();
        if( get_theme_mod( 'ed_section_menu_about', '1' ) )       $menu_label['about']       =  $menu_about;
        if( get_theme_mod( 'ed_section_menu_activities', '1' ) )  $menu_label['activities']  = $menu_activities;     
        if( get_theme_mod( 'ed_section_menu_features', '1' ) )    $menu_label['features']    = $menu_features;       
        if( get_theme_mod( 'ed_section_menu_events', '1' ) )      $menu_label['events']      = $menu_events;    
        if( get_theme_mod( 'ed_section_menu_courses', '1' ) )     $menu_label['course']      = $menu_courses;    
        if( get_theme_mod( 'ed_section_menu_team', '1' ) )        $menu_label['team']        = $menu_team; 
        if( get_theme_mod( 'ed_section_menu_testimonial', '1' ) ) $menu_label['testimonial'] = $menu_testimonial;
        if( get_theme_mod( 'ed_section_menu_gallery', '1' ) )     $menu_label['gallery']     = $menu_gallery; 
        if( get_theme_mod( 'ed_section_menu_blog' , '1' ) )       $menu_label['blog']        = $menu_blog;  
        if( get_theme_mod( 'ed_section_menu_contact', '1' ) )     $menu_label['contact']     = $menu_contact;         
        
        foreach( $home_sections as $section ){
            if( array_key_exists( $section, $menu_label ) ){
                $enabled_section[] = array(
                    'id'    => $section . '_section',
                    'label' => $menu_label[$section],
                );
            }
        }
        if( $ed_section_menu && ( 'page' == get_option( 'show_on_front' ) ) && $enabled_section ){ ?>
            <nav id="site-navigation" class="main-navigation" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
                <ul>
                    <?php if( $ed_home_link ){ ?>
                    <li class="<?php if( is_front_page() ) echo esc_attr( 'current-menu-item' ); ?>"><a href="<?php echo esc_url( home_url( '#banner_section' ) ); ?>"><?php esc_html_e( 'Home', 'preschool-and-kindergarten-pro' ); ?></a></li>
                <?php }
                    foreach( $enabled_section as $section ){ 
                        if( $section['label'] ){
                ?>
                        <li><a href="<?php echo esc_url( home_url( '#' . esc_attr( $section['id'] ) ) ); ?>"><?php echo esc_html( $section['label'] );?></a></li>                        
                <?php 
                        } 
                    }
                ?>
                </ul>
            </nav>
            <?php
        }else{
        ?>
        <nav id="site-navigation" class="main-navigation" role="navigation">
           <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
        </nav>
        <?php
        }

    }
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_search_form' ) ) :

    /**
     * Header Search Form
    */
    function preschool_and_kindergarten_pro_search_form(){
        $ed_search_form = get_theme_mod( 'ed_search', '1' );
        
        if( $ed_search_form ){ 
        echo '<div  class="form-holder">';  
            get_search_form();
        echo '</div>';

        }
    }

endif;

if( !function_exists( 'preschool_and_kindergarten_pro_cpt_has_archive' ) ) :
/**
 * Check if post type supports an archive
 *
 * @param string $post_type post type name
 * @uses get_post_type
 * @global object $post
 * @returns boolean
 * @link https://joshuadnelson.com/code/check-if-post-type-supports-archives/
 */
function preschool_and_kindergarten_pro_cpt_has_archive( $post_type ) {
    if( !is_string( $post_type ) || !isset( $post_type ) )
        return false;
    
    // find custom post types with archvies
    $args = array(
        'has_archive'   => true,
        '_builtin'  => false,
    );
    $output = 'names';
    $archived_custom_post_types = get_post_types( $args, $output );
    
    // if there are no custom post types, then the current post can't be one
    if( empty( $archived_custom_post_types ) )
        return false;
    
    // check if post type is a supports archives
    if ( in_array( $post_type, $archived_custom_post_types ) ) {
            return true;
    } else {
            return false;
    }
    
    // if all else fails, return false
    return false;   
    
}
endif;

/**
 * Query Contact Form 7
 */
function preschool_and_kindergarten_pro_is_cf7_activated() {
    return class_exists( 'WPCF7' ) ? true : false;
}

/**
 * Query Jetpack activation
*/
function preschool_and_kindergarten_pro_is_jetpack_activated( $gallery = false ){
    if( $gallery ){
        return ( class_exists( 'jetpack' ) && Jetpack::is_module_active( 'tiled-gallery' ) ) ? true : false;
    }else{
        return class_exists( 'jetpack' ) ? true : false;
    } 

}

function is_jetpack_subscription_module_active(){
    return( class_exists( 'jetpack' ) && Jetpack::is_module_active( 'subscriptions' ) ) ? true : false;
}

/**
 * Query WooCommerce activation
 */
function preschool_and_kindergarten_pro_is_woocommerce_activated() {
    return class_exists( 'woocommerce' ) ? true : false;
}

/**
 * Query for Rara theme toolkit 
 */
function is_rara_toolkit_activated(){
    return class_exists( 'Rara_Theme_Toolkit_Pro' ) ? true : false;
}

/**
 * Custom functions that act independently of the theme templates.
 */

if ( ! function_exists( 'preschool_and_kindergarten_pro_custom_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function preschool_and_kindergarten_pro_custom_posted_on() {
    
    echo '<span class="posted-on">';
    echo esc_html__( 'on ', 'preschool-and-kindergarten-pro' );
    printf( '<a href="%1$s" rel="bookmark"><time class="entry-date published updated" datetime="%2$s">%3$s</time></a>', esc_url( get_permalink() ), esc_attr( get_the_date( 'c' ) ), esc_html( get_the_date() ) );
    
    echo '</span>';

}
endif; 

/**
 * Retrieves the Attachment ID from the file URL
 *  
 * @link https://pippinsplugins.com/retrieve-attachment-id-from-image-url/
 */
function preschool_and_kindergarten_pro_get_image_id( $image_url ){
    $attachment_id = 0;
        $dir = wp_upload_dir();
        if ( false !== strpos( $image_url, $dir['baseurl'] . '/' ) ) { // Is URL in uploads directory?
            $file = basename( $image_url );
            $query_args = array(
                'post_type'   => 'attachment',
                'post_status' => 'inherit',
                'fields'      => 'ids',
                'meta_query'  => array(
                    array(
                        'value'   => $file,
                        'compare' => 'LIKE',
                        'key'     => '_wp_attachment_metadata',
                    ),
                )
            );
            $query = new WP_Query( $query_args );
            if ( $query->have_posts() ) {
                foreach ( $query->posts as $post_id ) {
                    $meta = wp_get_attachment_metadata( $post_id );
                    $original_file       = basename( $meta['file'] );
                    $cropped_image_files = wp_list_pluck( $meta['sizes'], 'file' );
                    if ( $original_file === $file || in_array( $file, $cropped_image_files ) ) {
                        $attachment_id = $post_id;
                        break;
                    }
                }
            }
        }
        return $attachment_id; 
}

/**
 * List out font awesome icon list
*/
function preschool_and_kindergarten_pro_get_icon_list(){
    include get_template_directory() . '/inc/fontawesome.php';
    echo '<div class="rara-font-awesome-list"><ul class="rara-font-group">';
    foreach( $fontawesome as $font ){
        echo '<li><i class="fa ' . esc_attr( $font ) . '"></i></li>';
    }
    echo '</ul></div>';
    
}

/**
 * Helper Function for Image widget
*/
function preschool_and_kindergarten_pro_get_image_field( $id, $name, $image, $label ){
    
    $output = '';
    
    $output .= '<div class="widget-upload">';
    
    $output .= '<label for="' . $id . '">' . esc_html( $label ) . '</label><br/>';
    $output .= '<input id="' . $id . '" class="rara-upload" type="text" name="' . $name . '" value="' . $image . '" placeholder="' . __('No file chosen', 'preschool-and-kindergarten-pro') . '" />' . "\n";
    if ( function_exists( 'wp_enqueue_media' ) ) {
        if ( $image == '' ) {
            $output .= '<input id="upload-' . $id . '" class="rara-upload-button button" type="button" value="' . __('Upload', 'preschool-and-kindergarten-pro') . '" />' . "\n";
        } else {
            $output .= '<input id="upload-' . $id . '" class="rara-upload-button button" type="button" value="' . __('Change', 'preschool-and-kindergarten-pro') . '" />' . "\n";
        }
    } else {
        $output .= '<p><i>' . __('Upgrade your version of WordPress for full media support.', 'preschool-and-kindergarten-pro') . '</i></p>';
    }

    $output .= '<div class="rara-screenshot" id="' . $id . '-image">' . "\n";

    if ( $image != '' ) {
        $remove = '<a class="rara-remove-image"></a>';
        $attachment_id = $image;
        $image_array = wp_get_attachment_image_src( $attachment_id, 'full');
        if ( $image ) {
            $output .= '<img src="' . esc_url( $image_array[0] ) . '" alt="" />' . $remove;
        } else {
            // Standard generic output if it's not an image.
            $output .= '<small>' . __( 'Please upload valid image file.', 'preschool-and-kindergarten-pro' ) . '</small>';
        }     
    }
    $output .= '</div></div>' . "\n";
    
    echo $output;
}

if( ! function_exists( 'preschool_and_kindergarten_pro_truncate' ) ):  
/**
 * Return Striptags from the content.
 */
function preschool_and_kindergarten_pro_truncate( $content, $letter_count ) {
	
    $striped_content = strip_shortcodes( $content );
    $striped_content = strip_tags( $striped_content );
    $excerpt         = mb_substr( $striped_content, 0, $letter_count );
    
    if( $striped_content > $excerpt ){
        $excerpt .= '...';
    }
    
    return $excerpt;

}
endif; // End function_exists

if ( ! function_exists( 'preschool_and_kindergarten_pro_apply_theme_shortcode' ) ) :
    /**
     * Footer Shortcode
    */
    function preschool_and_kindergarten_pro_apply_theme_shortcode( $string ) {
        if ( empty( $string ) ) {
        return $string;
    }
    $search = array( '[the-year]', '[home-link]' );
    $replace = array(
        date_i18n( esc_html__( 'Y', 'preschool-and-kindergarten-pro' ) ),
        '<a href="'. esc_url( home_url( '/' ) ) .'">'. esc_html( get_bloginfo( 'name', 'display' ) ) . '</a>',
    );
    $string = str_replace( $search, $replace, $string );
    return $string;
    }
endif;

if( ! function_exists( 'preschool_and_kindergarten_pro_escape_text_tags' ) ) :
/**
 * Remove new line tags from string
 *
 * @param $text
 * @return string
 */
function preschool_and_kindergarten_pro_escape_text_tags( $text ) {
    return (string) str_replace( array( "\r", "\n" ), '', strip_tags( $text ) );
}
endif;