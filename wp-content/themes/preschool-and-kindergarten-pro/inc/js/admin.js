jQuery(document).ready(function($) {
    
    // set var
    var in_customizer = false;

    // check for wp.customize return boolean
    if ( typeof wp !== 'undefined' ) {
        in_customizer =  typeof wp.customize !== 'undefined' ? true : false;
    }

    /** Icon Text Widget */
    $(document).on('click', '.rara-font-group li', function(){
	   	var id = $(this).parents('.widget').attr('id');
        $( '#' + id ).find('.rara-font-group li').removeClass();
        $( '#' + id ).find('.icon-receiver').siblings('a').remove('.rara-remove-icon');
        $(this).addClass('selected');
        var aa = $(this).parents('.rara-font-awesome-list').find('.rara-font-group li.selected').children('i').attr('class');
        $(this).parents('.rara-font-awesome-list').siblings('p').find('.hidden-icon-input').val(aa);
    	$(this).parents('.rara-font-awesome-list').siblings('p').find('.icon-receiver').html('<i class="'+aa+'"></i>');
	    $( '#' + id ).find('.icon-receiver').after('<a class="rara-remove-icon"></a>');        
        
        if( in_customizer ){
            $('.hidden-icon-input').trigger('change');
        }
        return false;
   });
   
   /** Remove icon function */
   $(document).on('click', '.rara-remove-icon', function(){
        var id = $(this).parents('.widget').attr('id');
        $( '#' + id ).find('.rara-font-group li').removeClass();
        $( '#' + id ).find('.hidden-icon-input').val('');
        $( '#' + id ).find('.icon-receiver').html('<i class=""></i>').siblings('a').remove('.rara-remove-icon');
        if( in_customizer ){
            $('.hidden-icon-input').trigger('change');
        }
   });
   
   /** To add remove button if icon is selected in widget update event */
   $(document).on( 'widget-updated', function( e, widget ){
    // "widget" represents jQuery object of the affected widget's DOM element
        var $this = $( '#' + widget[0].id ).find('.icon-receiver');
        if( $this.find('i').hasClass('fa') ){
            $this.after('<a class="rara-remove-icon"></a>');
        }        
    });
    
    preschool_and_kindergarten_pro_check_icon();
    
    /** function to check if icon is selected and saved when loading in widget.php */
    function preschool_and_kindergarten_pro_check_icon(){        
        $('.icon-receiver').each(function(){
            var id = $(this).parents('.widget').attr('id');    
            if( $('#'+id).find('.icon-receiver').find('i').hasClass('fa') ){
                $('#'+id).find('.icon-receiver').after('<a class="rara-remove-icon"></a>');                 
            }
        });           
    }
    
    /** Append remove button in customizer preview */
    if( in_customizer ){
        $( document ).on( 'click', '.widget-top', function() {
            if( $(this).siblings('.widget-inside').find('.icon-receiver').find('i').hasClass('fa') ){
                $(this).siblings('.widget-inside').find('.icon-receiver').after('<a class="rara-remove-icon"></a>');  
            }
        });        
    }
    /** Icon Text Widget Ends */
                   
});

jQuery(document).ready(function($){
   
    function check_page_templates(){
        //$('#preschool_and_kindergarten_pro_sidebar_layout').hide();
        $('.inside #page_template').each(function(i,e){
            if( ( $(this).val() === "templates/template-about.php" )|| ( $(this).val() === "templates/template-contact.php" ) || ( $(this).val() === "templates/template-services.php" )){
                $('#preschool_and_kindergarten_pro_sidebar_layout').hide();
            }else{
                $('#preschool_and_kindergarten_pro_sidebar_layout').show();
            }
        });
    }
    
    $('.inside #page_template').on( 'change', check_page_templates );
    
    check_page_templates();
   
});