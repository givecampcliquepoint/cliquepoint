jQuery(document).ready(function($){

	$('#preschool_and_kindergarten_pro_event_start_date').datepicker({ 
        dateFormat: 'yy-mm-dd',
        onSelect: function(date) {
            $("#preschool_and_kindergarten_pro_event_end_date").datepicker('option', 'minDate', date);
        }
    });

    $("#preschool_and_kindergarten_pro_event_end_date").datepicker({dateFormat: 'yy-mm-dd'});

});