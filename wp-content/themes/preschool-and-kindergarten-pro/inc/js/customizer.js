jQuery(document).ready(function($) {
	/* Move widgets to their respective sections */
     
    if( preschool_and_kindergarten_pro_form_data.subscribe == true ){
    	
        wp.customize.section( 'sidebar-widgets-newsletter-form' ).panel( 'home_page_settings' );
    	wp.customize.section( 'sidebar-widgets-newsletter-form' ).priority( '12' );
    }

	wp.customize.section( 'sidebar-widgets-about-icon-text' ).panel( 'about_page_settings' );
	wp.customize.section( 'sidebar-widgets-about-icon-text' ).priority( '12' );

	wp.customize.section( 'sidebar-widgets-service-icon-text' ).panel( 'services_page_settings' );
	wp.customize.section( 'sidebar-widgets-service-icon-text' ).priority( '11' );

	wp.customize.section( 'sidebar-widgets-facilities-icon-text' ).panel( 'services_page_settings' );
	wp.customize.section( 'sidebar-widgets-facilities-icon-text' ).priority( '13' );

	wp.customize.section( 'sidebar-widgets-featured-icon-text' ).panel( 'services_page_settings' );
	wp.customize.section( 'sidebar-widgets-featured-icon-text' ).priority( '14' );

    wp.customize.section( 'sidebar-widgets-header-social' ).panel( 'header_setting' );
    wp.customize.section( 'sidebar-widgets-header-social' ).priority( '25' );

	//Scroll to section
    $('body').on('click', '#sub-accordion-panel-home_page_settings .control-subsection .accordion-section-title', function(event) {
        var section_id = $(this).parent('.control-subsection').attr('id');
        scrollToSection( section_id );
    });

});

    
function scrollToSection( section_id ){
    var preview_section_id = "banner_section";

    var $contents = jQuery('#customize-preview iframe').contents();

    switch ( section_id ) {
        
        case 'accordion-section-welcome_settings':
        preview_section_id = "about_section";
        break;

        case 'accordion-section-activities_settings':
        preview_section_id = "activities_section";
        break;
        
        case 'accordion-section-sidebar-widgets-newsletter-form':
        preview_section_id = "subscription_section";
        break;

        case 'accordion-section-features_settings':
        preview_section_id = "features_section";
        break;

        case 'accordion-section-events_settings':
        preview_section_id = "events_section";
        break;
        
        case 'accordion-section-promotional_settings':
        preview_section_id = "promotional_section";
        break;

        case 'accordion-section-courses_settings':
        preview_section_id = "course_section";
        break;

        case 'accordion-section-team_settings':
        preview_section_id = "team_section";
        break;

        case 'accordion-section-testimonial_settings':
        preview_section_id = "testimonial_section";
        break;

        case 'accordion-section-gallery_settings':
        preview_section_id = "gallery_section";
        break;

        case 'accordion-section-blog_settings':
        preview_section_id = "blog_section";
        break;
        
        case 'accordion-section-CTA_settings':
        preview_section_id = "CTA_section";
        break;

        case 'accordion-section-contact_settings':
        preview_section_id = "contact_section";
        break;
        
        case 'accordion-section-sort_home_page_settings':
        preview_section_id = "banner_section";
        break;
        
    }

    if( $contents.find('#'+preview_section_id).length > 0 && $contents.find('.home').length > 0 ){
        $contents.find("html, body").animate({
        scrollTop: $contents.find( "#" + preview_section_id ).offset().top
        }, 1000);
    }
}
