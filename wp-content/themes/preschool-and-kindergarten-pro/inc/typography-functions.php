<?php
/**
 * Typography Related Functions
 *
 * @package preschool_and_kindergarten_pro
 */
 
function preschool_and_kindergarten_pro_fonts_url() {
    $fonts_url = '';
    
    $body_font    = get_theme_mod( 'body_font', array( 'font-family'=>'Lato', 'variant'=>'regular' ) );
    $ig_body_font = preschool_and_kindergarten_pro_is_google_font( $body_font['font-family'] );
    
    $site_title    = get_theme_mod( 'site_title_font', array( 'font-family'=>'Pacifico', 'variant'=>'regular' ) );
    $ig_site_title = preschool_and_kindergarten_pro_is_google_font( $site_title['font-family'] );
    
    $h1_font    = get_theme_mod( 'h1_font', array( 'font-family'=>'Lato', 'variant'=>'regular' ) );
    $ig_h1_font = preschool_and_kindergarten_pro_is_google_font( $h1_font['font-family'] );
    $h2_font    = get_theme_mod( 'h2_font', array( 'font-family'=>'Lato', 'variant'=>'regular' ) );
    $ig_h2_font = preschool_and_kindergarten_pro_is_google_font( $h2_font['font-family'] ); 
    $h3_font    = get_theme_mod( 'h3_font', array( 'font-family'=>'Lato', 'variant'=>'regular' ) );
    $ig_h3_font = preschool_and_kindergarten_pro_is_google_font( $h3_font['font-family'] );
    $h4_font    = get_theme_mod( 'h4_font', array( 'font-family'=>'Lato', 'variant'=>'regular' ) );
    $ig_h4_font = preschool_and_kindergarten_pro_is_google_font( $h4_font['font-family'] );
    $h5_font    = get_theme_mod( 'h5_font', array( 'font-family'=>'Lato', 'variant'=>'regular' ) );
    $ig_h5_font = preschool_and_kindergarten_pro_is_google_font( $h5_font['font-family'] );
    $h6_font    = get_theme_mod( 'h6_font', array( 'font-family'=>'Lato', 'variant'=>'regular' ) );
    $ig_h6_font = preschool_and_kindergarten_pro_is_google_font( $h6_font['font-family'] );
    
    /* Translators: If there are characters in your language that are not
    * supported by respective fonts, translate this to 'off'. Do not translate
    * into your own language.
    */
    $body     = _x( 'on', 'Body Font: on or off', 'preschool-and-kindergarten-pro' );
    $h1       = _x( 'on', 'H1 Content Font: on or off', 'preschool-and-kindergarten-pro' );
    $h2       = _x( 'on', 'H2 Content Font: on or off', 'preschool-and-kindergarten-pro' );
    $h3       = _x( 'on', 'H3 Content Font: on or off', 'preschool-and-kindergarten-pro' );
    $h4       = _x( 'on', 'H4 Content Font: on or off', 'preschool-and-kindergarten-pro' );
    $h5       = _x( 'on', 'H5 Content Font: on or off', 'preschool-and-kindergarten-pro' );
    $h6       = _x( 'on', 'H6 Content Font: on or off', 'preschool-and-kindergarten-pro' );
    $pacifico = _x( 'on', 'Site Title font: on or off', 'preschool-and-kindergarten-pro' );

    
    if ( 'off' !== $body || 'off' !== $h1 || 'off' !== $h2 || 'off' !== $h3 || 'off' !== $h4 || 'off' !== $h5 || 'off' !== $h6 || 'off' !== $pacifico ) {
        
        $font_families = array();
     
        if ( 'off' !== $body && $ig_body_font ) {
            if( ! empty( $body_font['variant'] ) ){
                $body_var = ':' . preschool_and_kindergarten_pro_check_varient( $body_font['font-family'], $body_font['variant'], true );
            }else{
                $body_var = '';    
            }            
            $font_families[] = $body_font['font-family'] . $body_var;
        }
         
        if ( 'off' !== $h1 && $ig_h1_font ) {
            if( ! empty( $h1_font['variant'] ) ){
                $h1_var = ':' . preschool_and_kindergarten_pro_check_varient( $h1_font['font-family'], $h1_font['variant'] );    
            }else{
                $h1_var = '';
            }
            $font_families[] = $h1_font['font-family'] . $h1_var;
        }
        
        if ( 'off' !== $h2 && $ig_h2_font ) {
            if( ! empty( $h2_font['variant'] ) ){
                $h2_var = ':' . preschool_and_kindergarten_pro_check_varient( $h2_font['font-family'], $h2_font['variant'] );    
            }else{
                $h2_var = '';
            }
            $font_families[] = $h2_font['font-family'] . $h2_var;
        }
        
        if ( 'off' !== $h3 && $ig_h3_font ) {
            if( ! empty( $h3_font['variant'] ) ){
                $h3_var = ':' . preschool_and_kindergarten_pro_check_varient( $h3_font['font-family'], $h3_font['variant'] );    
            }else{
                $h3_var = '';
            }
            $font_families[] = $h3_font['font-family'] . $h3_var;
        }
        
        if ( 'off' !== $h4 && $ig_h4_font ) {
            if( ! empty( $h4_font['variant'] ) ){
                $h4_var = ':' . preschool_and_kindergarten_pro_check_varient( $h4_font['font-family'], $h4_font['variant'] );    
            }else{
                $h4_var = '';
            }
            $font_families[] = $h4_font['font-family'] . $h4_var;
        }
        
        if ( 'off' !== $h5 && $ig_h5_font ) {
            if( ! empty( $h5_font['variant'] ) ){
                $h5_var = ':' . preschool_and_kindergarten_pro_check_varient( $h5_font['font-family'], $h5_font['variant'] );    
            }else{
                $h5_var = '';
            }
            $font_families[] = $h5_font['font-family'] . $h5_var;
        }
        
        if ( 'off' !== $h6 && $ig_h6_font ) {
            if( ! empty( $h6_font['variant'] ) ){
                $h6_var = ':' . preschool_and_kindergarten_pro_check_varient( $h6_font['font-family'], $h6_font['variant'] );    
            }else{
                $h6_var = '';
            }
            $font_families[] = $h6_font['font-family'] . $h6_var;
        }
        if( 'off' !== $pacifico && $ig_site_title ){
            if( ! empty( $site_title['variant'] ) ){
                $site_title_var = ':' . preschool_and_kindergarten_pro_check_varient( $site_title['font-family'], $site_title['variant'] );    
            }else{
                $site_title_var = '';
            }
            $font_families[] = $site_title['font-family'] . $site_title_var;

        }
        
        $font_families = array_diff( array_unique( $font_families ), array('') );
        
        $query_args = array(
            'family' => urlencode( implode( '|', $font_families ) ),            
        );
        
        $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
    }
     
    return esc_url_raw( $fonts_url );
}

/**
 * Get Google Fonts
*/
function preschool_and_kindergarten_pro_get_google_fonts(){
    $fonts = include wp_normalize_path( get_template_directory() . '/inc/custom-controls/typography/webfonts.php' );
    $google_fonts = array();
    if ( is_array( $fonts ) ) {
        foreach ( $fonts['items'] as $font ) {
            $google_fonts[ $font['family'] ] = array(
                'variants' => $font['variants'],
            );
        }
    }    
    return $google_fonts;
}

/**
 * Checks for matched varients in google fonts for typography fields
*/
function preschool_and_kindergarten_pro_check_varient( $font_family = 'serif', $font_variants = 'regular', $body = false ){
    $variant = '';
    $var     = array();
    $google_fonts  = preschool_and_kindergarten_pro_get_google_fonts(); //Google Fonts
    $websafe_fonts = preschool_and_kindergarten_pro_get_websafe_font(); //Standard Web Safe Fonts
    
    if( array_key_exists( $font_family, $google_fonts ) ){
        $variants = $google_fonts[ $font_family ][ 'variants' ];

        if( in_array( $font_variants, $variants ) ){
            if( $body ){ //LOAD ALL VARIANTS FOR BODY FONT
                foreach( $variants as $v ){
                    $var[] = $v;
                }
                $variant = implode( ',', $var );
            }else{                
                $variant = $font_variants;
            }
        }else{
            $variant = 'regular';
        }
        
    }else{ //Standard Web Safe Fonts
        if( array_key_exists( $font_family, $websafe_fonts ) ){
            $variants = $websafe_fonts[ $font_family ][ 'variants' ];
            if( in_array( $font_variants, $variants ) ){
                $variant = $font_variants;
            }else{
                $variant = 'regular';
            }    
        }
    }
    return $variant;
}

/**
 * Returns font weight and font style to use in dynamic styles.
*/
function preschool_and_kindergarten_pro_get_css_variant( $font_variant ){
    $v_array = array(
        '100'       => array(
            'weight'    => '100',
            'style'     => 'normal'
            ),
        '100italic' => array(
            'weight'    => '100',
            'style'     => 'italic'
            ),
        '200'       => array(
            'weight'    => '200',
            'style'     => 'normal'
            ),
        '200italic' => array(
            'weight'    => '200',
            'style'     => 'italic'
            ),
        '300'       => array(
            'weight'    => '300',
            'style'     => 'normal'
            ),
        '300italic' => array(
            'weight'    => '300',
            'style'     => 'italic'
            ),
        'regular'   => array(
            'weight'    => '400',
            'style'     => 'normal'
            ),
        'italic'    => array(
            'weight'    => '400',
            'style'     => 'italic'
            ),
        '500'       => array(
            'weight'    => '500',
            'style'     => 'normal'
            ),
        '500italic' => array(
            'weight'    => '500',
            'style'     => 'italic'
            ),
        '600'       => array(
            'weight'    => '600',
            'style'     => 'normal'
            ),
        '600italic' => array(
            'weight'    => '600',
            'style'     => 'italic'
            ),
        '700'       => array(
            'weight'    => '700',
            'style'     => 'normal'
            ),
        '700italic' => array(
            'weight'    => '700',
            'style'     => 'italic'
            ),
        '800'       => array(
            'weight'    => '800',
            'style'     => 'normal'
            ),
        '800italic' => array(
            'weight'    => '800',
            'style'     => 'italic'
            ),
        '900'       => array(
            'weight'    => '900',
            'style'     => 'normal'
            ),
        '900italic' => array(
            'weight'    => '900',
            'style'     => 'italic'
            ),
    );
    
    if( array_key_exists( $font_variant, $v_array ) ){
        return $v_array[ $font_variant ];
    }
}

/**
 * Function listing WebSafe Fonts and its attributes
*/
function preschool_and_kindergarten_pro_get_websafe_font(){
    $standard_fonts = array(
        'serif' => array(
            'variants' => array( 'regular', 'italic', '700', '700italic' ),
            'fonts' => 'Georgia,Times,"Times New Roman",serif',
        ),
        'sans-serif' => array(
            'variants'  => array( 'regular', 'italic', '700', '700italic' ),
            'fonts'  => 'Helvetica,Arial,sans-serif',
        ),
        'monospace' => array(
            'variants' => array( 'regular', 'italic', '700', '700italic' ),
            'fonts' => 'Monaco,"Lucida Sans Typewriter","Lucida Typewriter","Courier New",Courier,monospace',
        ),
    );
    $standard_fonts = apply_filters( 'pak_pro_get_websafe_font', $standard_fonts );
    return $standard_fonts;
}

/**
 * Function to check if it's a google font
*/
function preschool_and_kindergarten_pro_is_google_font( $font ){
    $return = false;
    $websafe_fonts = preschool_and_kindergarten_pro_get_websafe_font();
    if( $font ){
        if( array_key_exists( $font, $websafe_fonts ) ){
            //Web Safe Font
            $return = false;
        }else{
            //Google Font
            $return = true;
        }
    }
    return $return; 
}

/**
 * Function to get valid font, weight and style
*/
function preschool_and_kindergarten_pro_get_fonts( $font_family, $font_variant ){
    
    $fonts = array();
    $websafe_fonts = preschool_and_kindergarten_pro_get_websafe_font(); //Web Safe Font
    
    if( $font_family ){
        if( preschool_and_kindergarten_pro_is_google_font( $font_family ) ){
            $fonts['font'] = esc_attr( $font_family ); //Google Font
            if( $font_variant ){
                $weight_style    = preschool_and_kindergarten_pro_get_css_variant( preschool_and_kindergarten_pro_check_varient( $font_family, $font_variant ) );
                $fonts['weight'] = $weight_style['weight'];
                $fonts['style']  = $weight_style['style'];
            }else{
                $fonts['weight'] = '400';
                $fonts['style']  = 'normal';
            }
        }else{
            if( array_key_exists( $font_family, $websafe_fonts ) ){
                $fonts['font'] = $websafe_fonts[ $font_family ]['fonts']; //Web Safe Font
                if( $font_variant ){
                    $weight_style    = preschool_and_kindergarten_pro_get_css_variant( preschool_and_kindergarten_pro_check_varient( $font_family, $font_variant ) );
                    $fonts['weight'] = $weight_style['weight'];
                    $fonts['style']  = $weight_style['style'];
                }else{
                    $fonts['weight'] = '400';
                    $fonts['style']  = 'normal';
                }
            }
        }   
    }else{
        $fonts['font']   = '"Times New Roman", Times, serif';
        $fonts['weight'] = '400';
        $fonts['style']  = 'normal';
    }
    
    return $fonts;
}