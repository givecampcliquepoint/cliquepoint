<?php
/**
 * Font Awesome Control.
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if( ! class_exists( 'Rara_Fontawesome_Icon_Chooser' ) ) {
    
    class Rara_Fontawesome_Icon_Chooser extends WP_Customize_Control{
    	public $type = 'icon';
        
        public function enqueue() {            
            wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/css/font-awesome.css' );
            wp_enqueue_style( 'rara-fontawesome', get_template_directory_uri() . '/inc/custom-controls/fontawesome/font.css', null );
            wp_enqueue_script( 'rara-fontawesome', get_template_directory_uri() . '/inc/custom-controls/fontawesome/font.js', array( 'jquery', 'customize-controls' ), false, true ); //for slider        
        }        
        
    	public function render_content(){
    		?>
                <label>
                    <span class="customize-control-title">
                    <?php echo esc_html( $this->label ); ?>
                    </span>
    
                    <?php if($this->description){ ?>
    	            <span class="description customize-control-description">
    	            	<?php echo wp_kses_post($this->description); ?>
    	            </span>
    	            <?php } ?>
    
                    <div class="rara-selected-icon">
                    	<i class="fa <?php echo esc_attr($this->value()); ?>"></i>
                    	<span><i class="fa fa-angle-down"></i></span>
                    </div>
    
                    <ul class="rara-icon-list clearfix">
                    	<?php
                    	$font_awesome_icon_array = include wp_normalize_path( get_template_directory() . '/inc/custom-controls/fontawesome/fonts.php' );
                    	foreach( $font_awesome_icon_array as $font_awesome_icon ){
				            $icon_class = $this->value() == $font_awesome_icon ? 'icon-active' : '';
    						echo '<li class=' . $icon_class . '><i class="' . $font_awesome_icon . '"></i></li>';
                        }
                    	?>
                    </ul>
                    <input type="hidden" value="<?php $this->value(); ?>" <?php $this->link(); ?> />
                </label>
    		<?php
    	}
    }
    
}