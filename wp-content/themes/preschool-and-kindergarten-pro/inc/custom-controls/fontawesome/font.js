jQuery(document).ready(function($) {
    "use strict";

    //FontAwesome Icon Control JS
    $('body').on('click', '.rara-icon-list li', function(){
        var icon_class = $(this).find('i').attr('class');
        $(this).addClass('icon-active').siblings().removeClass('icon-active');
        $(this).parent('.rara-icon-list').prev('.rara-selected-icon').children('i').attr('class','').addClass(icon_class);
        $(this).parent('.rara-icon-list').next('input').val(icon_class).trigger('change');
    });

    $('body').on('click', '.rara-selected-icon', function(){
        $(this).next().slideToggle();
    });
    
});