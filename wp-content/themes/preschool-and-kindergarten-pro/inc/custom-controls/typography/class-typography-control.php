<?php

/**
 * Customizer Typography Control
 *
 * Taken from Kirki.
 *
 * @package   theme-slug
 * @copyright Copyright (c) 2016, Nose Graze Ltd.
 * @license   GPL2+
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if( ! class_exists( 'Rara_Typography_Control' ) ) {
    
    class Rara_Typography_Control extends WP_Customize_Control {
    
    	public $tooltip = '';
    	public $js_vars = array();
    	public $output = array();
    	public $option_type = 'theme_mod';
    	public $type = 'rara-typography';
    
    	/**
    	 * Refresh the parameters passed to the JavaScript via JSON.
    	 *
    	 * @access public
    	 * @return void
    	 */
    	public function to_json() {
    		parent::to_json();
    
    		if ( isset( $this->default ) ) {
    			$this->json['default'] = $this->default;
    		} else {
    			$this->json['default'] = $this->setting->default;
    		}
    		$this->json['js_vars'] = $this->js_vars;
    		$this->json['output']  = $this->output;
    		$this->json['value']   = $this->value();
    		$this->json['choices'] = $this->choices;
    		$this->json['link']    = $this->get_link();
    		$this->json['tooltip'] = $this->tooltip;
    		$this->json['id']      = $this->id;
    		$this->json['l10n']    = apply_filters( 'rara-typography-control/il8n/strings', array(
    			'on'                 => esc_attr__( 'ON', 'preschool-and-kindergarten-pro' ),
    			'off'                => esc_attr__( 'OFF', 'preschool-and-kindergarten-pro' ),
    			'all'                => esc_attr__( 'All', 'preschool-and-kindergarten-pro' ),
    			'cyrillic'           => esc_attr__( 'Cyrillic', 'preschool-and-kindergarten-pro' ),
    			'cyrillic-ext'       => esc_attr__( 'Cyrillic Extended', 'preschool-and-kindergarten-pro' ),
    			'devanagari'         => esc_attr__( 'Devanagari', 'preschool-and-kindergarten-pro' ),
    			'greek'              => esc_attr__( 'Greek', 'preschool-and-kindergarten-pro' ),
    			'greek-ext'          => esc_attr__( 'Greek Extended', 'preschool-and-kindergarten-pro' ),
    			'khmer'              => esc_attr__( 'Khmer', 'preschool-and-kindergarten-pro' ),
    			'latin'              => esc_attr__( 'Latin', 'preschool-and-kindergarten-pro' ),
    			'latin-ext'          => esc_attr__( 'Latin Extended', 'preschool-and-kindergarten-pro' ),
    			'vietnamese'         => esc_attr__( 'Vietnamese', 'preschool-and-kindergarten-pro' ),
    			'hebrew'             => esc_attr__( 'Hebrew', 'preschool-and-kindergarten-pro' ),
    			'arabic'             => esc_attr__( 'Arabic', 'preschool-and-kindergarten-pro' ),
    			'bengali'            => esc_attr__( 'Bengali', 'preschool-and-kindergarten-pro' ),
    			'gujarati'           => esc_attr__( 'Gujarati', 'preschool-and-kindergarten-pro' ),
    			'tamil'              => esc_attr__( 'Tamil', 'preschool-and-kindergarten-pro' ),
    			'telugu'             => esc_attr__( 'Telugu', 'preschool-and-kindergarten-pro' ),
    			'thai'               => esc_attr__( 'Thai', 'preschool-and-kindergarten-pro' ),
    			'serif'              => _x( 'Serif', 'font style', 'preschool-and-kindergarten-pro' ),
    			'sans-serif'         => _x( 'Sans Serif', 'font style', 'preschool-and-kindergarten-pro' ),
    			'monospace'          => _x( 'Monospace', 'font style', 'preschool-and-kindergarten-pro' ),
    			'font-family'        => esc_attr__( 'Font Family', 'preschool-and-kindergarten-pro' ),
    			'font-size'          => esc_attr__( 'Font Size', 'preschool-and-kindergarten-pro' ),
    			'font-weight'        => esc_attr__( 'Font Weight', 'preschool-and-kindergarten-pro' ),
    			'line-height'        => esc_attr__( 'Line Height', 'preschool-and-kindergarten-pro' ),
    			'font-style'         => esc_attr__( 'Font Style', 'preschool-and-kindergarten-pro' ),
    			'letter-spacing'     => esc_attr__( 'Letter Spacing', 'preschool-and-kindergarten-pro' ),
    			'text-align'         => esc_attr__( 'Text Align', 'preschool-and-kindergarten-pro' ),
    			'text-transform'     => esc_attr__( 'Text Transform', 'preschool-and-kindergarten-pro' ),
    			'none'               => esc_attr__( 'None', 'preschool-and-kindergarten-pro' ),
    			'uppercase'          => esc_attr__( 'Uppercase', 'preschool-and-kindergarten-pro' ),
    			'lowercase'          => esc_attr__( 'Lowercase', 'preschool-and-kindergarten-pro' ),
    			'top'                => esc_attr__( 'Top', 'preschool-and-kindergarten-pro' ),
    			'bottom'             => esc_attr__( 'Bottom', 'preschool-and-kindergarten-pro' ),
    			'left'               => esc_attr__( 'Left', 'preschool-and-kindergarten-pro' ),
    			'right'              => esc_attr__( 'Right', 'preschool-and-kindergarten-pro' ),
    			'center'             => esc_attr__( 'Center', 'preschool-and-kindergarten-pro' ),
    			'justify'            => esc_attr__( 'Justify', 'preschool-and-kindergarten-pro' ),
    			'color'              => esc_attr__( 'Color', 'preschool-and-kindergarten-pro' ),
    			'select-font-family' => esc_attr__( 'Select a font-family', 'preschool-and-kindergarten-pro' ),
    			'variant'            => esc_attr__( 'Variant', 'preschool-and-kindergarten-pro' ),
    			'style'              => esc_attr__( 'Style', 'preschool-and-kindergarten-pro' ),
    			'size'               => esc_attr__( 'Size', 'preschool-and-kindergarten-pro' ),
    			'height'             => esc_attr__( 'Height', 'preschool-and-kindergarten-pro' ),
    			'spacing'            => esc_attr__( 'Spacing', 'preschool-and-kindergarten-pro' ),
    			'ultra-light'        => esc_attr__( 'Ultra-Light 100', 'preschool-and-kindergarten-pro' ),
    			'ultra-light-italic' => esc_attr__( 'Ultra-Light 100 Italic', 'preschool-and-kindergarten-pro' ),
    			'light'              => esc_attr__( 'Light 200', 'preschool-and-kindergarten-pro' ),
    			'light-italic'       => esc_attr__( 'Light 200 Italic', 'preschool-and-kindergarten-pro' ),
    			'book'               => esc_attr__( 'Book 300', 'preschool-and-kindergarten-pro' ),
    			'book-italic'        => esc_attr__( 'Book 300 Italic', 'preschool-and-kindergarten-pro' ),
    			'regular'            => esc_attr__( 'Normal 400', 'preschool-and-kindergarten-pro' ),
    			'italic'             => esc_attr__( 'Normal 400 Italic', 'preschool-and-kindergarten-pro' ),
    			'medium'             => esc_attr__( 'Medium 500', 'preschool-and-kindergarten-pro' ),
    			'medium-italic'      => esc_attr__( 'Medium 500 Italic', 'preschool-and-kindergarten-pro' ),
    			'semi-bold'          => esc_attr__( 'Semi-Bold 600', 'preschool-and-kindergarten-pro' ),
    			'semi-bold-italic'   => esc_attr__( 'Semi-Bold 600 Italic', 'preschool-and-kindergarten-pro' ),
    			'bold'               => esc_attr__( 'Bold 700', 'preschool-and-kindergarten-pro' ),
    			'bold-italic'        => esc_attr__( 'Bold 700 Italic', 'preschool-and-kindergarten-pro' ),
    			'extra-bold'         => esc_attr__( 'Extra-Bold 800', 'preschool-and-kindergarten-pro' ),
    			'extra-bold-italic'  => esc_attr__( 'Extra-Bold 800 Italic', 'preschool-and-kindergarten-pro' ),
    			'ultra-bold'         => esc_attr__( 'Ultra-Bold 900', 'preschool-and-kindergarten-pro' ),
    			'ultra-bold-italic'  => esc_attr__( 'Ultra-Bold 900 Italic', 'preschool-and-kindergarten-pro' ),
    			'invalid-value'      => esc_attr__( 'Invalid Value', 'preschool-and-kindergarten-pro' ),
    		) );
    
    		$defaults = array( 'font-family'=> false );
    
    		$this->json['default'] = wp_parse_args( $this->json['default'], $defaults );
    	}
    
    	/**
    	 * Enqueue scripts and styles.
    	 *
    	 * @access public
    	 * @return void
    	 */
    	public function enqueue() {
    		wp_enqueue_style( 'rara-typography-css', get_template_directory_uri() . '/inc/custom-controls/typography/typography.css', null );
            /*
    		 * JavaScript
    		 */
            wp_enqueue_script( 'jquery-ui-core' );
    		wp_enqueue_script( 'jquery-ui-tooltip' );
    		wp_enqueue_script( 'jquery-stepper-min-js' );
    		
    		// Selectize
    		wp_enqueue_script( 'selectize', get_template_directory_uri() . '/inc/custom-controls/select/selectize.js', array( 'jquery' ), false, true );
    
    		// Typography
    		wp_enqueue_script( 'rara-typography-control', get_template_directory_uri() . '/inc/custom-controls/typography/typography.js', array(
    			'jquery',
    			'selectize'
    		), false, true );
    
    		$google_fonts   = Rara_Fonts::get_google_fonts();
    		$standard_fonts = Rara_Fonts::get_standard_fonts();
    		$all_variants   = Rara_Fonts::get_all_variants();
    
    		$standard_fonts_final = array();
    		foreach ( $standard_fonts as $key => $value ) {
    			$standard_fonts_final[] = array(
    				'family'      => $value['stack'],
    				'label'       => $value['label'],
    				'is_standard' => true,
    				'variants'    => array(
    					array(
    						'id'    => 'regular',
    						'label' => $all_variants['regular'],
    					),
    					array(
    						'id'    => 'italic',
    						'label' => $all_variants['italic'],
    					),
    					array(
    						'id'    => '700',
    						'label' => $all_variants['700'],
    					),
    					array(
    						'id'    => '700italic',
    						'label' => $all_variants['700italic'],
    					),
    				),
    			);
    		}
    
    		$google_fonts_final = array();
    
    		if ( is_array( $google_fonts ) ) {
    			foreach ( $google_fonts as $family => $args ) {
    				$label    = ( isset( $args['label'] ) ) ? $args['label'] : $family;
    				$variants = ( isset( $args['variants'] ) ) ? $args['variants'] : array( 'regular', '700' );
    
    				$available_variants = array();
    				foreach ( $variants as $variant ) {
    					if ( array_key_exists( $variant, $all_variants ) ) {
    						$available_variants[] = array( 'id' => $variant, 'label' => $all_variants[ $variant ] );
    					}
    				}
    
    				$google_fonts_final[] = array(
    					'family'   => $family,
    					'label'    => $label,
    					'variants' => $available_variants
    				);
    			}
    		}
    
    		$final = array_merge( $standard_fonts_final, $google_fonts_final );
    		wp_localize_script( 'rara-typography-control', 'RaraAllFonts', $final );
    	}
    
    	/**
    	 * Render the control's content.
    	 *
    	 * Allows the content to be overriden without having to rewrite the wrapper in $this->render().
    	 *
    	 * Supports basic input types `text`, `checkbox`, `textarea`, `radio`, `select` and `dropdown-pages`.
    	 * Additional input types such as `email`, `url`, `number`, `hidden` and `date` are supported implicitly.
    	 *
    	 * Control content can alternately be rendered in JS. See {@see WP_Customize_Control::print_template()}.
    	 *
    	 * @access public
    	 * @return void
    	 */
    	public function render_content() {
    
    		// intentionally empty
    	}
    
    	/**
    	 * An Underscore (JS) template for this control's content (but not its container).
    	 *
    	 * Class variables for this control class are available in the `data` JS object;
    	 * export custom variables by overriding {@see WP_Customize_Control::to_json()}.
    	 *
    	 * I put this in a separate file because PhpStorm didn't like it and it fucked with my formatting.
    	 *
    	 * @see    WP_Customize_Control::print_template()
    	 *
    	 * @access protected
    	 * @return void
    	 */
    	protected function content_template() { ?>
    		<# if ( data.tooltip ) { #>
                <a href="#" class="tooltip hint--left" data-hint="{{ data.tooltip }}"><span class='dashicons dashicons-info'></span></a>
            <# } #>
            
            <label class="customizer-text">
                <# if ( data.label ) { #>
                    <span class="customize-control-title">{{{ data.label }}}</span>
                <# } #>
                <# if ( data.description ) { #>
                    <span class="description customize-control-description">{{{ data.description }}}</span>
                <# } #>
            </label>
            
            <div class="wrapper">
                <# if ( data.default['font-family'] ) { #>
                    <# if ( '' == data.value['font-family'] ) { data.value['font-family'] = data.default['font-family']; } #>
                    <# if ( data.choices['fonts'] ) { data.fonts = data.choices['fonts']; } #>
                    <div class="font-family">
                        <h5>{{ data.l10n['font-family'] }}</h5>
                        <select id="rara-typography-font-family-{{{ data.id }}}" placeholder="{{ data.l10n['select-font-family'] }}"></select>
                    </div>
                    <div class="variant rara-variant-wrapper">
                        <h5>{{ data.l10n['style'] }}</h5>
                        <select class="variant" id="rara-typography-variant-{{{ data.id }}}"></select>
                    </div>
                <# } #>   
                
            </div>
            <?php
    	}
    
    }
}