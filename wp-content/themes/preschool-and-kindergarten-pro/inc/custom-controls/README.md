This is a collection of Custom Control for Customizer.

1. Font Awesome Icon derived from Hash Themes.
NOTE : To use this control you have to include fontawesome.css in theme css folder.

2. Gallery Control derived from XWP https://github.com/xwp/wp-customize-image-gallery-control

3. Rest of the control is derived from Kirki.