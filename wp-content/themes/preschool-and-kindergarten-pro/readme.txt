=== Preschool and Kindergarten Pro ===
Author: Rara Theme (http://raratheme.com)

Tags: translation-ready, custom-background, theme-options, custom-menu, custom-logo, post-formats, threaded-comments, blog, two-columns, right-sidebar, footer-widgets, education, e-commerce, one-column, full-width-template

Requires at least: 4.6
Tested up to: 4.9.7
Stable tag: 2.1.9
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


== Description ==

Preschool and Kindergarten Pro is a beautiful educational WordPress theme suitable for Kindergarten, Schools, Elementary, Primary Schools, Universities, Academy, Secondary School, LMS, Training Center and Educational institutions. Preschool and Kindergarten is also suitable for education and children activities. It is mobile responsive and translation ready. The theme is very flexible and versatile. In other words, it is multipurpose theme. It can be used to build various types of websites for business, corporate, charity, food, recipe, travel, photography, feminine, lifestyle, events, etc. The theme is highly customizable with various features and personalization options. Preschool and Kindergarten comes with several features to make a user-friendly, interactive and visually stunning webssite. Such features include custom menu, attractive slider, about section, courses section, testimonial section, featured programs, Banner with Call to Action Button (CTA), and social media. The theme is SEO friendly with optimized codes, which make it easy for your site to rank on Google and other search engines. The theme is properly tested and optimized for speed and faster page load time and has a secure and clean code. The theme is also translation ready. Designed with visitor engagement in mind, Preschool and Kindergarten theme helps you to easily and intuitively create professional and appealing websites. If your preschool, kindergarten, school, college or university needs an online home that is dynamic and multi-functional, Preschool and Kindergarten them is a good place to start. Check the demo at https://raratheme.com/preview/preschool-and-kindergarten/, documentation at https://raratheme.com/wordpress-themes/preschool-and-kindergarten/, and get support at http://raratheme.com/support-ticket/.


== Installation ==
        
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.


== Copyrights and License ==

Unless otherwise specified, all the theme files, scripts and images are licensed under GPLv2 or later


External resources linked to the theme.

* Pacifico Font by Google Font 
https://fonts.google.com/specimen/Pacifico

* Lato Font by Google Font 
https://www.google.com/fonts/specimen/Lato

== Font Awesome ==
https://fortawesome.github.io/Font-Awesome/


# Images
All images are under Creative Commons Public Domain deed CC0.

https://pixabay.com/en/boy-graduation-kindergarten-526222/
https://pixabay.com/en/child-kid-play-tranquil-study-865116/
https://pixabay.com/en/pre-primary-school-education-1272291/


# JS
All the JS are licensed under GPLv2 or later

https://www.berriart.com/sidr/
http://sachinchoolur.github.io/lightslider/



* Based on Underscores http://underscores.me/, (C) 2012-2015 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)


* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2015 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)


All other resources and theme elements are licensed under the GPLv2 or later


Preschool and Kindergarten Pro WordPress Theme, Copyright Rara Theme 2015, Raratheme.com
Preschool and Kindergarten Pro WordPress Theme is distributed under the terms of the GPLv2 or later


*  This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

== Change Log ==
    2.1.9
    * Icon Text widget issue fixed.

    2.1.8
    * Multiple phones options added in the header and footer.

    2.1.7
    * Added performance features
    * Added minified files for all js and css
    * Added schema for SEO

    2.1.6
    * Removed demo files from theme and updated imported hooks.

    2.1.5
    * Alt tag in the slider issue solved.
    * Rara theme toolkit pro updated to 1.1.3.
    
    2.1.4
    * Iframe issue of height and width in post solved.
    * Rara Theme Toolkit Pro updated to 1.1.2 version.

    2.1.3
    * Privacy Policy link and WP Comment Cookies Consent features were added after 4.9.6 wp installation.

    2.1.2
    * Issue fixed that were opened in gitlab.
    * Events date issue fixed.
    * Course Post type warning issue fixed.

    2.1.1
    * Rara Theme Toolkit Pro updated to 1.0.9 version.
    
    2.1.0
    * Added filter for typography
    * Fixed minor issues

    2.0.9
    * Added features of renaming slug to any post type
    * Added Homepage Panel Lanel after 4.9 wp version
    * Author description issue were fixed

    2.0.8
    * Added menu order sorting options for custom post type archive page and also in homepage testimonial section.
    * Theme updater code updated. 

    2.0.7
    * Added option for slider animation speed.
    * Added option in testimonial slider for autoplay.

    2.0.6
    * Updated rara theme toolkit pro plugin.

    2.0.5
    * Added archive description on event and testimonial archive pages. 
    * Fixed slider images issue in mobile devices. 

    2.0.4
    * Fixed Shortcode issue on custom post type.

    2.0.3
    * Added typography option for site title.

    2.0.2
    * Added Open Graph Tags for social sharing.
    * Disable previous dates of a datepicker in event end date according to the selected date in event start date.
    * Fixed minor issues.
    * Fixed issue on demo import.
    
    2.0.1
    * Fixed issue on google map section.

    2.0.0
    * Initial Release