<?php
   /**
    * The template for displaying all single events
    *
    * @package Rara_Theme_Toolkit_Pro
    * @subpackage Rara_Theme_Toolkit_Pro/includes/rara-toolkit-pro-templates
    * @since 1.0.0
    */
    get_header();
        global $post; 
        $event_img_size    = apply_filters('event_img_size','full'); 
        $post_thumbnail_id = get_post_thumbnail_id( $post->ID );
        $src               = wp_get_attachment_image_src( $post_thumbnail_id, $event_img_size );
        $cats              = get_the_terms($post->ID,'event_categories');
        $rttk_setting      = get_post_meta( $post->ID, '_rttk_setting', true );

        $start_date        = get_post_meta( $post->ID, '_preschool_and_kindergarten_pro_event_start_date',true );
        $end_date          = get_post_meta( $post->ID, '_preschool_and_kindergarten_pro_event_end_date', true );
        $address           = isset($rttk_setting['event']['address'])   ? esc_attr($rttk_setting['event']['address'])  : '';
        $time              = isset($rttk_setting['event']['time'])      ? esc_attr($rttk_setting['event']['time'])     : '';
        $start_time        = isset($rttk_setting['event']['stime'])     ? esc_attr($rttk_setting['event']['stime'])    : '';
        $end_time          = isset($rttk_setting['event']['etime'])     ? esc_attr($rttk_setting['event']['etime'])    : '';
        $cost              = isset($rttk_setting['event']['cost'])      ? esc_attr($rttk_setting['event']['cost'])     : '';
        $organizer         = isset($rttk_setting['event']['organizer']) ? esc_attr($rttk_setting['event']['organizer']): '';
        $phone             = isset($rttk_setting['event']['phone'])     ? esc_attr($rttk_setting['event']['phone'])    : '';
        $email             = isset($rttk_setting['event']['email'])     ? esc_attr($rttk_setting['event']['email'])    : '';
        $website           = isset($rttk_setting['event']['website'])   ? esc_attr($rttk_setting['event']['website'])  : '';
        $venue             = isset($rttk_setting['event']['venue'])     ? esc_attr($rttk_setting['event']['venue'])    : '';
        $map_iframe        = isset($rttk_setting['event']['map'])       ? esc_attr($rttk_setting['event']['map'])      : '';

        ?>
    <div id="primary" class="content-area">
    	<main id="main" class="site-main" role="main">
            <?php while ( have_posts() ) : the_post(); ?>
            	<article class="page event-detail-page">
                    <?php do_action('rttk_event_before_image'); ?>
                    <div class="holder">
                        
                        <div class="post-thumbnail">
                        	<img src="<?php echo esc_url($src[0]);?>" alt="">
                        </div>
                       
                        <?php do_action('rttk_event_before_image');
                        if( $start_date || $end_date || $address || $time ){
                        ?>
                            <div class="event-info">
                            <?php 
                                if( $start_date || $end_date ){ ?>
                                    <div class="col">
                                        <div class="date">
                                        <?php echo esc_attr( $start_date ); ?> - <?php echo esc_attr( $end_date ); ?>
                                        </div>
                                    </div>
                            <?php }
                                if( $address ){?>
                                    <div class="col">
                                        <address><?php echo esc_attr( $address ); ?></address>
                                    </div>
                            <?php }
                                if( $time ){ ?>
                                    <div class="col">
                                        <div class="time"><?php echo esc_attr( $time ); ?></div>
                                    </div>
                            <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                    <?php 
                    if( $start_date || $end_date || $cost || $cats || $phone || $email || $website ){ ?>
                        <div class="detail-info">
                            <?php 
                            if( $start_date || $end_date || $start_time || $end_time || $cost || $cats ){ ?>
                                <div class="col">
                                    <h3 class="title"><?php _e('Detail','preschool-and-kindergarten-pro');?></h3>
                                    <ul class="information-list">
                                        <?php 
                                        if( $start_date || $start_time ){ ?>
                                            <li>
                                                <strong><?php _e('Start Date','preschool-and-kindergarten-pro');?></strong>
                                                <span><?php  echo esc_attr( $start_date ); ?></span>
                                                <span><?php  echo esc_attr( $start_time ); ?></span>
                                            </li>
                                        <?php }
                                        if( $end_date || $end_time ){ ?>
                                            <li>
                                                <strong><?php _e('End Date:','preschool-and-kindergarten-pro');?></strong>
                                                <span><?php  echo esc_attr( $end_date ); ?></span>
                                                <span><?php  echo esc_attr( $end_time ); ?></span>
                                            </li>
                                        <?php } 
                                        if( $cost ){ ?>
                                            <li>
                                                <strong><?php _e('Cost:','preschool-and-kindergarten-pro');?></strong>
                                                <span><?php echo esc_attr( $cost ); ?></span>
                                            </li>
                                        <?php } 
                                        if( $cats ){ ?>
                                            <li>
                                                <strong><?php _e('Event Category:','preschool-and-kindergarten-pro');?></strong>
                                                <?php
                                                $out = array();
                                                foreach ($cats as $cat) { 
                                                    $out[] = sprintf( '<span><a href="%1$s">%2$s</a></span>', esc_url( get_term_link( $cat->term_id,'event_categories' ) ), esc_html( $cat->name ) );
                                                }
                                                echo implode( ", ", $out );
                                                ?>
                                            </li>
                                        <?php 
                                        } ?>
                                    </ul>
                                </div>
                            <?php } 
                            if( $organizer || $phone || $email || $website ){ ?>
                                <div class="col">
                                    <h3 class="title"><?php _e('Organizer','preschool-and-kindergarten-pro');?></h3>
                                    <ul class="information-list">
                                        <?php  
                                        if( $organizer ){ ?>
                                            <li>
                                                <strong><?php echo esc_attr( $organizer ); ?></strong>
                                            </li>
                                        <?php } 
                                        if( $phone ){ ?>                                   
                                            <li>
                                                <strong><?php _e('Phone','preschool-and-kindergarten-pro');?></strong>
                                                <span><?php echo esc_attr( $phone ); ?></span>
                                            </li>
                                        <?php } 
                                        if( $email ){ ?>
                                            <li>
                                                <strong><?php _e('Email','preschool-and-kindergarten-pro');?></strong>
                                                <span><?php echo esc_attr( $email ); ?></span>
                                            </li>
                                        <?php } 
                                        if( $website ){ ?>
                                            <li>
                                                <strong><?php _e('Website','preschool-and-kindergarten-pro');?></strong>
                                                <a href="<?php echo esc_url( $website ); ?>" target="_blank"><?php echo esc_attr( $website ); ?></a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            <?php 
                            } ?>
                        </div>
                    <?php } ?>
                    <div class="entry-content">
                        <?php the_content(); ?>
                    </div>
                    <?php 
                    if( $venue || $map_iframe){ ?>
                        <div class="venue">
                            <?php
                            if( $map_iframe){ 
                            $obj = new Rara_Theme_Toolkit_Pro_Functions; 
                            ?>
                            <div class="map-holder">
                                <?php 
                                if( class_exists( 'jetpack' ) && Jetpack::is_module_active( 'shortcodes' ) ){
                                    echo do_shortcode( $rttk_setting['event']['map'] );
                                }else{
                                echo $obj->rrtk_sanitize_iframe( $rttk_setting['event']['map'] );
                                } ?>
                            </div>
                            <?php }
                            if( $venue ){ ?>
                                <div class="text-holder">
                                    <h3 class="title"><?php _e('Venue','preschool-and-kindergarten-pro');?></h3>
                                    <address>
                                        <?php echo esc_attr( $venue ); ?>   
                                    </address>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>

                    <div class="btn-holder">
                        <?php do_action('rrtk_third_party_button_holder'); ?>
                    </div>
                </article>
            <?php endwhile; ?>
    	</main>
    </div>
    <?php get_sidebar(); ?>
    <?php get_footer(); ?>