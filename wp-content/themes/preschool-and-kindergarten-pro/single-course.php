<?php
   /**
    * The template for displaying all single course
    *
    * @package Preschool_Kindergarten_Pro
    * 
    */
   
$course_text = get_theme_mod( 'preschool_and_kindergarten_pro_course_text', __('Course Information','preschool-and-kindergarten-pro') );

get_header();?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">
        <?php while ( have_posts() ) : the_post(); ?>
            <article class="page course-detail-page">           
            <?php
                global $post;
                $rttk_course_images = get_post_meta($post->ID, '_rttk_images_course_id', true);
                $rttk_setting       = get_post_meta( $post->ID, '_rttk_setting', true );
                $sdate              = isset( $rttk_setting['course']['sdate'] ) ? $rttk_setting['course']['sdate']: '';
                $staff              = isset( $rttk_setting['course']['staff'] ) ? $rttk_setting['course']['staff']: '';
                $size               = isset( $rttk_setting['course']['size'] ) ? $rttk_setting['course']['size']: '';
                $transportation     = isset( $rttk_setting['course']['transportation'] ) ? $rttk_setting['course']['transportation']: '';
                $old                = isset( $rttk_setting['course']['old'] ) ? $rttk_setting['course']['old']: '';
                $duration           = isset( $rttk_setting['course']['duration'] ) ? $rttk_setting['course']['duration']: '';
                $join               = isset( $rttk_setting['course']['join'] ) ? $rttk_setting['course']['join']: '';
                if( isset( $rttk_course_images ) && $rttk_course_images!='' ): ?>
                    <ul id="class-detail-slider">
                    <?php
                        foreach ($rttk_course_images as $image) { 
                            $course_img_size = apply_filters('course_image_size','full');
                            $link = wp_get_attachment_image_src($image, $course_img_size); 
                            echo '<li data-thumb="'.$link[0].'" data-src=""><img src="'.$link[0].'" alt=""></li>';
                        }
                    ?> 
                    </ul>                                           
                <?php else:       
                the_post_thumbnail();
                endif; ?>
            
                <?php do_action('rttk_course_after_image');
                
                if( !empty( $sdate ) || !empty( $staff ) || !empty( $size ) || !empty( $transportation ) || !empty( $old )  || !empty( $duration ) ){  
                    ?>
                    <div class="course-information">
                        <h3 class="title"><?php echo esc_html( $course_text );?></h3>
                        <ul class="information-list">
                            <?php 
                            if( isset( $sdate ) && ! empty( $sdate  ) ): ?>
                                <li>
                                    <strong><?php _e('Start Date:','preschool-and-kindergarten-pro');?></strong>
                                    <span><?php echo isset( $sdate ) ? esc_attr( $sdate ): ''; ?></span>
                                </li>
                            <?php endif;
                            if( isset( $staff ) && ! empty( $staff  ) ): ?>
                                <li>
                                    <strong><?php _e('Class Staff:','preschool-and-kindergarten-pro');?></strong>
                                    <span><?php echo isset( $staff ) ? esc_attr( $staff ): ''; ?></span>
                                </li>
                            <?php endif;
                            if( isset( $size ) && ! empty( $size  ) ): ?>
                                <li>
                                    <strong><?php _e('Class Size:','preschool-and-kindergarten-pro');?></strong>
                                    <span><?php echo isset( $size ) ? esc_attr( $size ): ''; ?></span>
                                </li>
                            <?php endif;
                            if( isset( $transportation ) && ! empty( $transportation  ) ): ?>
                                <li>
                                    <strong><?php _e('Transportation:','preschool-and-kindergarten-pro');?></strong>
                                    <span><?php echo isset( $transportation ) ? esc_attr( $transportation ): ''; ?></span>
                                </li>
                            <?php endif;
                            if( isset( $old ) && ! empty( $old  ) ): ?>
                                <li>
                                    <strong><?php _e('Years old:','preschool-and-kindergarten-pro');?></strong>
                                    <span><?php echo isset( $old ) ? esc_attr( $old ): ''; ?></span>
                                </li>
                            <?php endif;
                            if( isset( $duration ) && ! empty( $duration  ) ): ?>
                                <li>
                                    <strong><?php _e('Class Duration:','preschool-and-kindergarten-pro');?></strong>
                                    <span><?php echo isset( $duration ) ? esc_attr( $duration ): ''; ?></span>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <?php 
                } ?>
        
                <div class="entry-content">
                    <?php  
                    the_content();

                    if( isset( $join ) && ! empty( $join  ) ): ?>
                        <a href="<?php echo esc_url( $join ); ?>" class="btn-join">
                            <?php 
                            $join_now_txt = __('Join Now','preschool-and-kindergarten-pro'); 
                            $join_now_label = apply_filters( 'course_join_now_label', $join_now_txt ); 
                            echo $join_now_label;
                            ?>
                        </a>
                    <?php 
                    endif; ?>
                </div>
            </article>
        <?php endwhile; ?>
        </main>
    </div>
    <?php get_sidebar(); ?>
            
    <?php do_action('rttk_sidebar_action');?>
    <?php get_footer();