<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://raratheme.com
 * @since             1.0.0
 * @package           Rara_Theme_Toolkit_Pro
 *
 * @wordpress-plugin
 * Plugin Name:       Rara Theme Toolkit Pro
 * Plugin URI:        
 * Description:       The plugin generates multiple custom post types, number of Rara Theme exclusive widgets, and some useful shortcodes.
 * Version:           1.1.3
 * Author:            Rara Theme
 * Author URI:        https://raratheme.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       rara-theme-toolkit-pro
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
define( 'RTTKPRO_BASE_PATH', dirname( __FILE__ ) );
define( 'RTTKPRO_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'RTTKPRO_TEMPLATE_PATH', RTTKPRO_BASE_PATH.'/includes/rara-toolkit-pro-templates' );
add_image_size( 'rttk-thumb', 600, 600 );

// this is the URL our updater / license checker pings. This should be the URL of the site with EDD installed
define( 'EDD_PLUGIN_STORE_URL', 'http://raratheme.com' ); // you should use your own CONSTANT name, and be sure to replace it throughout this file

// the name of your product. This should match the download name in EDD exactly
define( 'EDD_PLUGIN_NAME', 'Rara Theme Toolkit Pro' ); // you should use your own CONSTANT name, and be sure to replace it throughout this file

// the name of the settings page for the license input to be displayed
define( 'EDD_RTTK_PLUGIN_LICENSE_PAGE', 'pluginname-license' );


/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-rara-theme-toolkit-pro-activator.php
 */
function activate_rara_theme_toolkit_pro() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-rara-theme-toolkit-pro-activator.php';
	Rara_Theme_Toolkit_Pro_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-rara-theme-toolkit-pro-deactivator.php
 */
function deactivate_rara_theme_toolkit_pro() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-rara-theme-toolkit-pro-deactivator.php';
	Rara_Theme_Toolkit_Pro_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_rara_theme_toolkit_pro' );
register_deactivation_hook( __FILE__, 'deactivate_rara_theme_toolkit_pro' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-rara-theme-toolkit-pro.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_rara_theme_toolkit_pro() {

	$plugin = new Rara_Theme_Toolkit_Pro();
	$plugin->run();

}
run_rara_theme_toolkit_pro();
