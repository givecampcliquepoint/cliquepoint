#, fuzzy
msgid ""
msgstr ""
"POT-Creation-Date: 2017-04-10 10:26+0000\n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: Loco - https://localise.biz/"

#: admin/class-rara-theme-toolkit-pro-admin.php:152
msgid "Are you sure?"
msgstr ""

#: admin/class-rara-theme-toolkit-pro-admin.php:155
#: includes/class-rara-theme-toolkit-pro-functions.php:68
msgid "Upload"
msgstr ""

#: admin/class-rara-theme-toolkit-pro-admin.php:156
#: includes/class-rara-theme-toolkit-pro-functions.php:70
msgid "Change"
msgstr ""

#: admin/class-rara-theme-toolkit-pro-admin.php:157
#: includes/class-rara-theme-toolkit-pro-functions.php:87
msgid "Please upload valid image file."
msgstr ""

#: admin/class-rara-theme-toolkit-pro-admin.php:293
msgid "Add New"
msgstr ""

#: admin/class-rara-theme-toolkit-pro-admin.php:300
msgid "Not found"
msgstr ""

#: admin/class-rara-theme-toolkit-pro-admin.php:301
msgid "Not found in Trash"
msgstr ""

#: admin/class-rara-theme-toolkit-pro-admin.php:302
msgid "Featured Image"
msgstr ""

#: admin/class-rara-theme-toolkit-pro-admin.php:303
msgid "Set featured image"
msgstr ""

#: admin/class-rara-theme-toolkit-pro-admin.php:304
msgid "Remove featured image"
msgstr ""

#: admin/class-rara-theme-toolkit-pro-admin.php:305
msgid "Use as featured image"
msgstr ""

#: admin/class-rara-theme-toolkit-pro-admin.php:348
msgid "Search Categories"
msgstr ""

#: admin/class-rara-theme-toolkit-pro-admin.php:349
msgid "All Categories"
msgstr ""

#: admin/class-rara-theme-toolkit-pro-admin.php:350
msgid "Parent Categories"
msgstr ""

#: admin/class-rara-theme-toolkit-pro-admin.php:351
msgid "Parent Categories:"
msgstr ""

#: admin/class-rara-theme-toolkit-pro-admin.php:352
msgid "Edit Categories"
msgstr ""

#: admin/class-rara-theme-toolkit-pro-admin.php:353
msgid "Update Categories"
msgstr ""

#: admin/class-rara-theme-toolkit-pro-admin.php:354
msgid "Add New Categories"
msgstr ""

#: admin/class-rara-theme-toolkit-pro-admin.php:355
msgid "New Categories Name"
msgstr ""

#: admin/class-rara-theme-toolkit-pro-admin.php:413
msgid " plugin needs following theme(s) to be Activated: "
msgstr ""

#: includes/class-rara-theme-toolkit-pro-functions.php:65
msgid "No file chosen"
msgstr ""

#: includes/class-rara-theme-toolkit-pro-functions.php:73
msgid "Upgrade your version of WordPress for full media support."
msgstr ""

#: includes/class-rara-theme-toolkit-pro-functions.php:79
msgid "Remove Image"
msgstr ""

#: includes/class-rara-theme-toolkit-pro-meta.php:34
msgid "Working Details"
msgstr ""

#: includes/class-rara-theme-toolkit-pro-meta.php:77
msgid "Member Details"
msgstr ""

#: includes/class-rara-theme-toolkit-pro-meta.php:120
msgid "Event Details"
msgstr ""

#: includes/class-rara-theme-toolkit-pro-meta.php:163
msgid "Course Details"
msgstr ""

#: includes/class-rara-theme-toolkit-pro-meta.php:206
msgid "Portfolio Details"
msgstr ""

#: includes/shortcodes.php:206
msgid "Call to Action Title"
msgstr ""

#: includes/shortcodes.php:207
msgid "View"
msgstr ""

#: includes/meta-parts/rttk-teams-template.php:78
msgid "Add Icon"
msgstr ""

#: includes/meta-parts/rttk-teams-template.php:79
#: includes/widgets/widget-social-links.php:251
msgid "Click the above button to add social icons. You can sort them as well."
msgstr ""

#: includes/rara-toolkit-pro-templates/archive-event.php:48
msgid "Find out more"
msgstr ""

#: includes/rara-toolkit-pro-templates/archive-team.php:55
#: includes/rara-toolkit-pro-templates/single-team.php:41
msgid ", Tel: "
msgstr ""

#: includes/rara-toolkit-pro-templates/single-course.php:44
msgid "Course Information"
msgstr ""

#: includes/rara-toolkit-pro-templates/single-course.php:47
msgid "Start Date:"
msgstr ""

#: includes/rara-toolkit-pro-templates/single-course.php:51
msgid "Class Staff:"
msgstr ""

#: includes/rara-toolkit-pro-templates/single-course.php:55
msgid "Class Size:"
msgstr ""

#: includes/rara-toolkit-pro-templates/single-course.php:59
msgid "Transportation:"
msgstr ""

#: includes/rara-toolkit-pro-templates/single-course.php:63
msgid "Years old:"
msgstr ""

#: includes/rara-toolkit-pro-templates/single-course.php:67
msgid "Class Duration:"
msgstr ""

#: includes/rara-toolkit-pro-templates/single-course.php:78
msgid "Join Now"
msgstr ""

#: includes/rara-toolkit-pro-templates/single-event.php:41
msgid "Detail"
msgstr ""

#: includes/rara-toolkit-pro-templates/single-event.php:44
msgid "Start Date"
msgstr ""

#: includes/rara-toolkit-pro-templates/single-event.php:48
msgid "End:"
msgstr ""

#: includes/rara-toolkit-pro-templates/single-event.php:52
msgid "Cost:"
msgstr ""

#: includes/rara-toolkit-pro-templates/single-event.php:56
msgid "Event Category:"
msgstr ""

#: includes/rara-toolkit-pro-templates/single-event.php:71
msgid "Organizer"
msgstr ""

#: includes/rara-toolkit-pro-templates/single-event.php:77
#: includes/widgets/widget-contact.php:100
msgid "Phone"
msgstr ""

#: includes/rara-toolkit-pro-templates/single-event.php:81
#: includes/widgets/widget-contact.php:105
msgid "Email"
msgstr ""

#: includes/rara-toolkit-pro-templates/single-event.php:85
msgid "Website"
msgstr ""

#: includes/rara-toolkit-pro-templates/single-event.php:103
msgid "Venue"
msgstr ""

#. %s: Name of current post
#. %s: Name of current post
#. %s: Name of current post
#. %s: Name of current post
#. %s: Name of current post
#. %s: Name of current post
#. %s: Name of current post
#. %s: Name of current post
#. %s: Name of current post
#. %s: Name of current post
#: includes/rara-toolkit-pro-templates/single-portfolio.php:45
#: includes/rara-toolkit-pro-templates/taxonomy-course_categories.php:78
#: includes/rara-toolkit-pro-templates/taxonomy-course_categories.php:142
#: includes/rara-toolkit-pro-templates/taxonomy-course_categories.php:216
#: includes/rara-toolkit-pro-templates/taxonomy-event_categories.php:78
#: includes/rara-toolkit-pro-templates/taxonomy-event_categories.php:142
#: includes/rara-toolkit-pro-templates/taxonomy-event_categories.php:216
#: includes/rara-toolkit-pro-templates/taxonomy-portfolio_categories.php:78
#: includes/rara-toolkit-pro-templates/taxonomy-portfolio_categories.php:142
#: includes/rara-toolkit-pro-templates/taxonomy-portfolio_categories.php:216
#, php-format
msgid "Continue reading %s"
msgstr ""

#: includes/rara-toolkit-pro-templates/single-portfolio.php:50
msgid "Pages:"
msgstr ""

#: includes/rara-toolkit-pro-templates/single-portfolio.php:54
msgid "Page"
msgstr ""

#: includes/rara-toolkit-pro-templates/single-portfolio.php:69
msgid "Edit"
msgstr ""

#. Base ID
#: includes/widgets/widget-ad.php:26
msgid "RARA: AD Widget"
msgstr ""

#: includes/widgets/widget-ad.php:27
msgid "A widget for AD."
msgstr ""

#: includes/widgets/widget-ad.php:101
#: includes/widgets/widget-author-bio.php:103
#: includes/widgets/widget-author-post.php:117
#: includes/widgets/widget-cat-post.php:110
#: includes/widgets/widget-contact.php:85 includes/widgets/widget-cta.php:117
#: includes/widgets/widget-facebook-page.php:108
#: includes/widgets/widget-icon-text.php:112
#: includes/widgets/widget-instagram.php:158
#: includes/widgets/widget-popular-post.php:179
#: includes/widgets/widget-recent-post.php:109
#: includes/widgets/widget-social-links.php:210
#: includes/widgets/widget-stat-counter.php:121
#: includes/widgets/widget-twitter-feeds.php:95
msgid "Title"
msgstr ""

#: includes/widgets/widget-ad.php:106
msgid "Ad Code"
msgstr ""

#: includes/widgets/widget-ad.php:110 includes/widgets/widget-icon-text.php:128
#: includes/widgets/widget-stat-counter.php:132
msgid "or"
msgstr ""

#: includes/widgets/widget-ad.php:112
#: includes/widgets/widget-author-bio.php:112
#: includes/widgets/widget-icon-text.php:126
#: includes/widgets/widget-stat-counter.php:130
msgid "Upload Image"
msgstr ""

#: includes/widgets/widget-ad.php:115
msgid "Link URL"
msgstr ""

#. Base ID
#: includes/widgets/widget-author-bio.php:26
msgid "RARA: Author Bio"
msgstr ""

#: includes/widgets/widget-author-bio.php:27
msgid "An Author Bio Widget"
msgstr ""

#: includes/widgets/widget-author-bio.php:108
#: includes/widgets/widget-contact.php:90 includes/widgets/widget-cta.php:122
#: includes/widgets/widget-icon-text.php:117
msgid "Content"
msgstr ""

#: includes/widgets/widget-author-bio.php:115
msgid "Button Label"
msgstr ""

#: includes/widgets/widget-author-bio.php:120
#: includes/widgets/widget-cta.php:132
msgid "Button Link"
msgstr ""

#. Base ID
#: includes/widgets/widget-author-post.php:25
msgid "RARA: Author Post"
msgstr ""

#: includes/widgets/widget-author-post.php:26
msgid ""
"A Author Post Widget display other posts written by the author. Will appear "
"on single posts only."
msgstr ""

#: includes/widgets/widget-author-post.php:84
#: includes/widgets/widget-author-post.php:84
#: includes/widgets/widget-cat-post.php:76
#: includes/widgets/widget-cat-post.php:76
#: includes/widgets/widget-recent-post.php:76
#: includes/widgets/widget-recent-post.php:76
#, php-format
msgid "%1$s"
msgstr ""

#: includes/widgets/widget-author-post.php:122
#: includes/widgets/widget-cat-post.php:132
#: includes/widgets/widget-popular-post.php:184
#: includes/widgets/widget-recent-post.php:114
msgid "Number of Posts"
msgstr ""

#: includes/widgets/widget-author-post.php:128
#: includes/widgets/widget-cat-post.php:138
#: includes/widgets/widget-featured-post.php:118
#: includes/widgets/widget-popular-post.php:198
#: includes/widgets/widget-recent-post.php:120
msgid "Show Post Thumbnail"
msgstr ""

#: includes/widgets/widget-author-post.php:133
#: includes/widgets/widget-cat-post.php:143
#: includes/widgets/widget-popular-post.php:203
#: includes/widgets/widget-recent-post.php:125
msgid "Show Post Date"
msgstr ""

#. Base ID
#: includes/widgets/widget-cat-post.php:25
msgid "RARA: Category Post"
msgstr ""

#: includes/widgets/widget-cat-post.php:26
msgid "A Category Recent Post Widget"
msgstr ""

#: includes/widgets/widget-cat-post.php:115
msgid "Category:"
msgstr ""

#. Base ID
#: includes/widgets/widget-contact.php:25
msgid "RARA: Contact"
msgstr ""

#: includes/widgets/widget-contact.php:26
msgid "A Contact Widget"
msgstr ""

#: includes/widgets/widget-contact.php:95
msgid "Street Address"
msgstr ""

#. Base ID
#: includes/widgets/widget-cta.php:32
msgid "RARA: Call To Action Widget"
msgstr ""

#: includes/widgets/widget-cta.php:33
msgid "A Call To Action Text Widget."
msgstr ""

#: includes/widgets/widget-cta.php:127
msgid "Button Text"
msgstr ""

#: includes/widgets/widget-cta.php:138
msgid "Background Color"
msgstr ""

#: includes/widgets/widget-cta.php:143
msgid "Background Image"
msgstr ""

#. Base ID
#: includes/widgets/widget-facebook-page.php:25
msgid "RARA: Facebook Page Plugin"
msgstr ""

#: includes/widgets/widget-facebook-page.php:26
msgid "A widget that shows Facebook Page Box"
msgstr ""

#: includes/widgets/widget-facebook-page.php:113
msgid "Facebook Page URL"
msgstr ""

#: includes/widgets/widget-facebook-page.php:118
msgid "Width"
msgstr ""

#: includes/widgets/widget-facebook-page.php:123
msgid "Height"
msgstr ""

#: includes/widgets/widget-facebook-page.php:129
msgid "Adapt to plugin container width"
msgstr ""

#: includes/widgets/widget-facebook-page.php:134
msgid "Show Friend's Faces"
msgstr ""

#: includes/widgets/widget-facebook-page.php:139
msgid "Use Small Header"
msgstr ""

#: includes/widgets/widget-facebook-page.php:144
msgid "Hide Cover Photo"
msgstr ""

#: includes/widgets/widget-facebook-page.php:149
msgid "Show Timeline Tab"
msgstr ""

#: includes/widgets/widget-facebook-page.php:154
msgid "Show Event Tab"
msgstr ""

#: includes/widgets/widget-facebook-page.php:159
msgid "Show Message Tab"
msgstr ""

#. Base ID
#: includes/widgets/widget-featured-post.php:25
msgid "RARA: Featured Post"
msgstr ""

#: includes/widgets/widget-featured-post.php:26
msgid "A Featured Post Widget"
msgstr ""

#: includes/widgets/widget-featured-post.php:39
#: includes/widgets/widget-featured-post.php:97
#: includes/widgets/widget-featured-post.php:137
msgid "Read More"
msgstr ""

#: includes/widgets/widget-featured-post.php:85
msgid "--Choose--"
msgstr ""

#: includes/widgets/widget-featured-post.php:102
msgid "Posts"
msgstr ""

#: includes/widgets/widget-featured-post.php:112
msgid "Read More Text"
msgstr ""

#: includes/widgets/widget-flickr.php:17
msgid "RARA: Flickr Widget"
msgstr ""

#: includes/widgets/widget-flickr.php:20
msgid "Display your latest Flickr photos."
msgstr ""

#: includes/widgets/widget-flickr.php:113
msgid "Flickr Photos"
msgstr ""

#: includes/widgets/widget-flickr.php:123
msgid "Title:"
msgstr ""

#: includes/widgets/widget-flickr.php:130
msgid "Your Flickr User ID:"
msgstr ""

#: includes/widgets/widget-flickr.php:134
#, php-format
msgid "Find your Flickr user ID here  %s."
msgstr ""

#: includes/widgets/widget-flickr.php:139
msgid "Number of photos to show:"
msgstr ""

#: includes/widgets/widget-flickr.php:143
msgid ""
"Flickr seems to limit its feeds to 20. So you can use maximum 20 photos."
msgstr ""

#: includes/widgets/widget-flickr.php:148
msgid "Number of photos per row:"
msgstr ""

#: includes/widgets/widget-flickr.php:152
msgid "You can use minimum 1 photo and maximum 6 photos."
msgstr ""

#. Base ID
#: includes/widgets/widget-icon-text.php:25
msgid "RARA: Icon Text Widget"
msgstr ""

#: includes/widgets/widget-icon-text.php:26
msgid "An Icon Text Widget."
msgstr ""

#: includes/widgets/widget-icon-text.php:122
msgid "Featured Link"
msgstr ""

#: includes/widgets/widget-icon-text.php:131
#: includes/widgets/widget-stat-counter.php:135
msgid "Icons"
msgstr ""

#. Base ID
#: includes/widgets/widget-instagram.php:25
msgid "RARA: Instagram"
msgstr ""

#: includes/widgets/widget-instagram.php:26
msgid "A Instagram Widget that displays your latest Instagram photos."
msgstr ""

#: includes/widgets/widget-instagram.php:113
msgid "Connection Error!"
msgstr ""

#: includes/widgets/widget-instagram.php:139
msgid "Instagram"
msgstr ""

#: includes/widgets/widget-instagram.php:142
msgid "Follow Me!"
msgstr ""

#: includes/widgets/widget-instagram.php:163
msgid "User Id"
msgstr ""

#: includes/widgets/widget-instagram.php:166
#, php-format
msgid "Find your Instagram User Id %s."
msgstr ""

#: includes/widgets/widget-instagram.php:171
msgid "Access Token"
msgstr ""

#: includes/widgets/widget-instagram.php:174
#, php-format
msgid "Find your Instagram Access-token %s."
msgstr ""

#: includes/widgets/widget-instagram.php:180
msgid "Number of photos"
msgstr ""

#: includes/widgets/widget-instagram.php:185
msgid "Open links in"
msgstr ""

#: includes/widgets/widget-instagram.php:187
msgid "Current window (_self)"
msgstr ""

#: includes/widgets/widget-instagram.php:188
msgid "New window (_blank)"
msgstr ""

#: includes/widgets/widget-instagram.php:193
msgid "Link text"
msgstr ""

#: includes/widgets/widget-instagram.php:198
msgid "Photo size"
msgstr ""

#: includes/widgets/widget-instagram.php:200
msgid "Thumbnail"
msgstr ""

#: includes/widgets/widget-instagram.php:201
msgid "Small"
msgstr ""

#: includes/widgets/widget-instagram.php:202
msgid "Large"
msgstr ""

#: includes/widgets/widget-instagram.php:203
msgid "Original"
msgstr ""

#. Base ID
#: includes/widgets/widget-popular-post.php:28
msgid "RARA: Popular Post"
msgstr ""

#: includes/widgets/widget-popular-post.php:29
msgid "A Popular Post Widget"
msgstr ""

#: includes/widgets/widget-popular-post.php:66
msgid "0 View"
msgstr ""

#: includes/widgets/widget-popular-post.php:68
msgid " View"
msgstr ""

#: includes/widgets/widget-popular-post.php:70
msgid " Views"
msgstr ""

#: includes/widgets/widget-popular-post.php:189
msgid "Popular based on:"
msgstr ""

#: includes/widgets/widget-popular-post.php:191
msgid "Post Views"
msgstr ""

#: includes/widgets/widget-popular-post.php:192
msgid "Comment Count"
msgstr ""

#: includes/widgets/widget-popular-post.php:209
msgid "Show number of comments"
msgstr ""

#: includes/widgets/widget-popular-post.php:216
msgid "Show number of views"
msgstr ""

#. Base ID
#: includes/widgets/widget-recent-post.php:25
msgid "RARA: Recent Post"
msgstr ""

#: includes/widgets/widget-recent-post.php:26
msgid "A Recent Post Widget"
msgstr ""

#: includes/widgets/widget-recent-post.php:40
#: includes/widgets/widget-recent-post.php:101
#: includes/widgets/widget-recent-post.php:144
msgid "Recent Posts"
msgstr ""

#. Base ID
#: includes/widgets/widget-social-links.php:44
msgid "RARA: Social Links"
msgstr ""

#: includes/widgets/widget-social-links.php:45
msgid "A Social Links Widget"
msgstr ""

#: includes/widgets/widget-social-links.php:183
msgid "Subscribe and Follow"
msgstr ""

#: includes/widgets/widget-social-links.php:214
msgid "Open in New Tab"
msgstr ""

#: includes/widgets/widget-social-links.php:250
msgid "Add Social Icon"
msgstr ""

#. Base ID
#: includes/widgets/widget-stat-counter.php:25
msgid "RARA: Stat Counter Widget"
msgstr ""

#: includes/widgets/widget-stat-counter.php:26
msgid "Widget for stat counter."
msgstr ""

#: includes/widgets/widget-stat-counter.php:126
msgid "Counter"
msgstr ""

#. Base ID
#: includes/widgets/widget-twitter-feeds.php:43
msgid "RARA: Latest Tweets"
msgstr ""

#: includes/widgets/widget-twitter-feeds.php:44
msgid "A widget that shows latest tweets"
msgstr ""

#: includes/widgets/widget-twitter-feeds.php:99
msgid "Theme"
msgstr ""

#: includes/widgets/widget-twitter-feeds.php:100
#: includes/widgets/widget-twitter-feeds.php:101
msgid "Choose a theme&hellip;"
msgstr ""

#: includes/widgets/widget-twitter-feeds.php:113
msgid "Twitter Username"
msgstr ""

#: includes/widgets/widget-twitter-feeds.php:118
msgid "Border Color"
msgstr ""

#: includes/widgets/widget-twitter-feeds.php:123
msgid "Link Color"
msgstr ""

#: includes/widgets/widget-twitter-feeds.php:128
msgid "Widget Width"
msgstr ""

#: includes/widgets/widget-twitter-feeds.php:133
msgid "Number of tweets"
msgstr ""

#. Name of the plugin
msgid "Rara Theme Toolkit Pro"
msgstr ""

#. Description of the plugin
msgid ""
"The plugin generates seven custom post types (FAQs, Logos, Practices, "
"Services, Portfolios, Team Members, and Testimonials),16 Rara Theme "
"exclusive widgets, and some useful shortcodes."
msgstr ""

#. Author of the plugin
msgid "Rara Theme"
msgstr ""

#. Author URI of the plugin
msgid "https://raratheme.com"
msgstr ""
