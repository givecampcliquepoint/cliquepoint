<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://raratheme.com
 * @since      1.0.0
 *
 * @package    Rara_Theme_Toolkit_Pro
 * @subpackage Rara_Theme_Toolkit_Pro/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Rara_Theme_Toolkit_Pro
 * @subpackage Rara_Theme_Toolkit_Pro/admin
 * @author     Rara Theme <test@testmail.com>
 */
class Rara_Theme_Toolkit_Pro_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		register_activation_hook( __FILE__, 'rrtk_register_post_types' );
	}

	/**
	 * Get the allowed socicon lists.
	 * @return array
	 */
	function rttk_get_allowed_socicons() {
		return apply_filters( 'rttk_social_icons_allowed_socicon', array( 'modelmayhem', 'mixcloud', 'drupal', 'swarm', 'istock', 'yammer', 'ello', 'stackoverflow', 'persona', 'triplej', 'houzz', 'rss', 'paypal', 'odnoklassniki', 'airbnb', 'periscope', 'outlook', 'coderwall', 'tripadvisor', 'appnet', 'goodreads', 'tripit', 'lanyrd', 'slideshare', 'buffer', 'disqus', 'vk', 'whatsapp', 'patreon', 'storehouse', 'pocket', 'mail', 'blogger', 'technorati', 'reddit', 'dribbble', 'stumbleupon', 'digg', 'envato', 'behance', 'delicious', 'deviantart', 'forrst', 'play', 'zerply', 'wikipedia', 'apple', 'flattr', 'github', 'renren', 'friendfeed', 'newsvine', 'identica', 'bebo', 'zynga', 'steam', 'xbox', 'windows', 'qq', 'douban', 'meetup', 'playstation', 'android', 'snapchat', 'twitter', 'facebook', 'google-plus', 'pinterest', 'foursquare', 'yahoo', 'skype', 'yelp', 'feedburner', 'linkedin', 'viadeo', 'xing', 'myspace', 'soundcloud', 'spotify', 'grooveshark', 'lastfm', 'youtube', 'vimeo', 'dailymotion', 'vine', 'flickr', '500px', 'instagram', 'wordpress', 'tumblr', 'twitch', '8tracks', 'amazon', 'icq', 'smugmug', 'ravelry', 'weibo', 'baidu', 'angellist', 'ebay', 'imdb', 'stayfriends', 'residentadvisor', 'google', 'yandex', 'sharethis', 'bandcamp', 'itunes', 'deezer', 'medium', 'telegram', 'openid', 'amplement', 'viber', 'zomato', 'quora', 'draugiem', 'endomodo', 'filmweb', 'stackexchange', 'wykop', 'teamspeak', 'teamviewer', 'ventrilo', 'younow', 'raidcall', 'mumble', 'bebee', 'hitbox', 'reverbnation', 'formulr', 'battlenet', 'chrome', 'diablo', 'discord', 'issuu', 'macos', 'firefox', 'heroes', 'hearthstone', 'overwatch', 'opera', 'warcraft', 'starcraft', 'keybase', 'alliance', 'livejournal', 'googlephotos', 'horde', 'etsy', 'zapier', 'google-scholar', 'researchgate' ) );
	}

	/**
	 * Get the icon from supported URL lists.
	 * @return array
	 */
	function rttk_get_supported_url_icon() {
		return apply_filters( 'rttk_social_icons_get_supported_url_icon', array(
			'feed'                  => 'rss',
			'ok.ru'                 => 'odnoklassniki',
			'vk.com'                => 'vk',
			'last.fm'               => 'lastfm',
			'youtu.be'              => 'youtube',
			'battle.net'            => 'battlenet',
			'blogspot.com'          => 'blogger',
			'play.google.com'       => 'play',
			'plus.google.com'       => 'google-plus',
			'photos.google.com'     => 'googlephotos',
			'chrome.google.com'     => 'chrome',
			'scholar.google.com'    => 'google-scholar',
			'feedburner.google.com' => 'mail',
		) );
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Rara_Theme_Toolkit_Pro_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Rara_Theme_Toolkit_Pro_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_style( $this->plugin_name.'fontawesome', plugin_dir_url( __FILE__ ) . 'css/font-awesome.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/rara-theme-toolkit-pro-admin.min.css', array(), $this->version, 'all' );
    	$screen = get_current_screen();
    	$jquery_ui_cpt = array( 'event'=>'event','course'=>'course' );
    	$myarr = apply_filters( 'rttk_jquery_ui_cpt',$jquery_ui_cpt );
    	foreach ($myarr as $key => $value) {
			if( $screen->post_type== $key )
			{
				wp_enqueue_style('jquery-style', plugin_dir_url( __FILE__ ) . 'css/jquery-ui.min.css', array(), $this->version, 'all' );
				wp_enqueue_style('jquery-timepicker-style', plugin_dir_url( __FILE__ ) . 'css/jquery.timepicker.min.css', array(), $this->version, 'all' );
			}
    	} 
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Rara_Theme_Toolkit_Pro_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Rara_Theme_Toolkit_Pro_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
    	wp_enqueue_script( 'jquery-ui-sortable' );    
		wp_enqueue_media();
		wp_enqueue_script( 'jquery-ui-datepicker' );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/rara-theme-toolkit-pro-admin.min.js', array( 'jquery','jquery-ui-sortable','jquery-ui-datepicker' ), $this->version, false );
		$screen = get_current_screen(); 
		
		$rttk_media_upload_cpt = array( 'course'=>'course' );
    	$myarr = apply_filters( 'rttk_media_upload_cpt',$rttk_media_upload_cpt );
    	foreach ($myarr as $key => $value) {
			if( $screen->post_type== $key )
			{
				
				wp_enqueue_script('jquery-media', plugin_dir_url( __FILE__ ) . 'js/media-upload.min.js', array('jquery'), $this->version, false );
			}
    	} 
		// Localize socicons.
		$socicons_params = array(
			'allowed_socicons'   => $this->rttk_get_allowed_socicons(),
			'supported_url_icon' => $this->rttk_get_supported_url_icon(),
		);
		$confirming = array( 
					'are_you_sure'       => __( 'Are you sure?', 'rara-theme-toolkit-pro' ),
					);
		wp_localize_script( $this->plugin_name, 'rara_theme_toolkit_pro_uploader', array(
        	'upload' => __( 'Upload', 'rara-theme-toolkit-pro' ),
        	'change' => __( 'Change', 'rara-theme-toolkit-pro' ),
        	'msg'    => __( 'Please upload valid image file.', 'rara-theme-toolkit-pro' )
    	));
		wp_localize_script( $this->plugin_name, 'social_icons_admin_widgets', $socicons_params );
		wp_localize_script( $this->plugin_name, 'confirming', $confirming );
		// print_r($screen);
		if( $screen->parent_base =='edit' )
		{
			wp_enqueue_script( $this->plugin_name.'-shortcodes', plugin_dir_url( __FILE__ ) . 'js/shortcodes.min.js', array( 'jquery' ), $this->version, false );
		}
	}
	// Declare script for new button
	function rara_theme_toolkit_pro_add_tinymce_plugin( $plugin_array ) {
		$plugin_array['rara_theme_toolkit_pro_mce_button'] = plugins_url( '/js/shortcodes.min.js', __FILE__ );
		return $plugin_array;
	}

  /**
	* Get post types for templates
	*
	* @return array of default settings
	*/
	public function rttk_get_posttype_array() {

		$posts = array(
			'course' => array( 
						'singular_name'			=> 'Course',
						'general_name'			=> 'Courses',
						'dashicon'				=> 'dashicons-list-view',
						'taxonomy'				=> 'course_categories',
						'taxonomy_slug'			=> 'courses',
						'has_archive'           => true,		
						'exclude_from_search'   => false,
						'show_in_nav_menus'		=> true,
						'supports' 			 	=> array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
						'path'					=> RTTKPRO_BASE_PATH.'/includes/meta-parts/'
						),
			'event' => array(  
						'singular_name'			=> 'Event',
						'general_name'			=> 'Events',
						'dashicon'				=> 'dashicons-calendar',
						'taxonomy'				=> 'event_categories',
						'taxonomy_slug'			=> 'events',
						'has_archive'           => true,		
						'exclude_from_search'   => false,
						'show_in_nav_menus'		=> true,
						'supports' 			 	=> array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
						'path'					=> RTTKPRO_BASE_PATH.'/includes/meta-parts/'
						),
			'portfolio' => array( 
						'singular_name'			=> 'Portfolio',
						'general_name'			=> 'Portfolios',
						'dashicon'				=> 'dashicons-portfolio',
						'taxonomy'				=> 'portfolio_categories',
						'taxonomy_slug'			=> 'portfolios',
						'has_archive'           => true,		
						'exclude_from_search'   => false,
						'show_in_nav_menus'		=> true,
						'supports' 			 	=> array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
						'path'					=> RTTKPRO_BASE_PATH.'/includes/meta-parts/'
						),
			'team' => array( 
						'singular_name'			=> 'Team Member',
						'general_name'			=> 'Team Members',
						'dashicon'	    		=> 'dashicons-groups',
						'has_archive'           => true,		
						'exclude_from_search'   => false,
						'show_in_nav_menus'		=> true,
						'supports' 			 	=> array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
						'path'					=> RTTKPRO_BASE_PATH.'/includes/meta-parts/'
						),
			'testimonial' => array( 
						'singular_name'			=>'Testimonial',
						'general_name'			=>'Testimonials',
						'dashicon'				=>'dashicons-testimonial',
						'has_archive'           => true,		
						'exclude_from_search'   => false,
						'show_in_nav_menus'		=> true,
						'supports' 			 	=> array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
						'path'					=> RTTKPRO_BASE_PATH.'/includes/meta-parts/'
						),
			);
		$posts = apply_filters( 'rttk_get_posttype_array', $posts );
		return $posts;
	}

	/**
	 * Register post types.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/register_post_type
	 */
	function rrtk_register_post_types() {
		$myarray = $this->rttk_get_posttype_array();
		foreach ($myarray as $key => $value) {
			$labels = array(
		'name'                  => _x( $value['general_name'], 'Post Type General Name', 'rara-theme-toolkit-pro' ),
		'singular_name'         => _x( $value['singular_name'], 'Post Type Singular Name', 'rara-theme-toolkit-pro' ),
		'menu_name'             => __( $value['general_name'], 'rara-theme-toolkit-pro' ),
		'name_admin_bar'        => __( $value['singular_name'], 'rara-theme-toolkit-pro' ),
		'archives'              => __( $value['singular_name'].' Archives', 'rara-theme-toolkit-pro' ),
		'attributes'            => __( $value['singular_name'].' Attributes', 'rara-theme-toolkit-pro' ),
		'parent_item_colon'     => __( 'Parent '. $value['singular_name'].':', 'rara-theme-toolkit-pro' ),
		'all_items'             => __( 'All '. $value['general_name'], 'rara-theme-toolkit-pro' ),
		'add_new_item'          => __( 'Add New '. $value['singular_name'], 'rara-theme-toolkit-pro' ),
		'add_new'               => __( 'Add New', 'rara-theme-toolkit-pro' ),
		'new_item'              => __( 'New '. $value['singular_name'], 'rara-theme-toolkit-pro' ),
		'edit_item'             => __( 'Edit '. $value['singular_name'], 'rara-theme-toolkit-pro' ),
		'update_item'           => __( 'Update '. $value['singular_name'], 'rara-theme-toolkit-pro' ),
		'view_item'             => __( 'View '. $value['singular_name'], 'rara-theme-toolkit-pro' ),
		'view_items'            => __( 'View '. $value['general_name'], 'rara-theme-toolkit-pro' ),
		'search_items'          => __( 'Search '. $value['singular_name'], 'rara-theme-toolkit-pro' ),
		'not_found'             => __( 'Not found', 'rara-theme-toolkit-pro' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'rara-theme-toolkit-pro' ),
		'featured_image'        => __( 'Featured Image', 'rara-theme-toolkit-pro' ),
		'set_featured_image'    => __( 'Set featured image', 'rara-theme-toolkit-pro' ),
		'remove_featured_image' => __( 'Remove featured image', 'rara-theme-toolkit-pro' ),
		'use_featured_image'    => __( 'Use as featured image', 'rara-theme-toolkit-pro' ),
		'insert_into_item'      => __( 'Insert into '.$value['singular_name'], 'rara-theme-toolkit-pro' ),
		'uploaded_to_this_item' => __( 'Uploaded to this '.$value['singular_name'], 'rara-theme-toolkit-pro' ),
		'items_list'            => __( $value['general_name'] .' list', 'rara-theme-toolkit-pro' ),
		'items_list_navigation' => __( $value['general_name'] .' list navigation', 'rara-theme-toolkit-pro' ),
		'filter_items_list'     => __( 'Filter '. $value['general_name'] .'list', 'rara-theme-toolkit-pro' ),
	);
	$args = array(
		'label'                 => __( $value['singular_name'].'', 'rara-theme-toolkit-pro' ),
		'description'           => __( $value['singular_name'].' Post Type', 'rara-theme-toolkit-pro' ),
		'labels'                => $labels,
		'supports'              =>  $value['supports'],
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_icon'             => $value['dashicon'],
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => $value['show_in_nav_menus'],
		'can_export'            => true,
		'has_archive'           => $value['has_archive'],		
		'exclude_from_search'   => $value['exclude_from_search'],
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
		register_post_type( $key, $args );
	    flush_rewrite_rules();
		}
	}

	/**
	 * Register a taxonomy, post_types_categories for the post types.
	 *
	 * @link https://codex.wordpress.org/Function_Reference/register_taxonomy
	 */
	function rttk_create_post_type_taxonomies() {
		// Add new taxonomy, make it hierarchical
		$myarray = $this->rttk_get_posttype_array();
		foreach ($myarray as $key => $value) {
			if(isset($value['taxonomy']))
			{
				$labels = array(
					'name'              => _x( $value['singular_name'].' Categories', 'taxonomy general name', 'rara-theme-toolkit-pro' ),
					'singular_name'     => _x( $value['singular_name'].' Categories', 'taxonomy singular name', 'rara-theme-toolkit-pro' ),
					'search_items'      => __( 'Search Categories', 'rara-theme-toolkit-pro' ),
					'all_items'         => __( 'All Categories', 'rara-theme-toolkit-pro' ),
					'parent_item'       => __( 'Parent Categories', 'rara-theme-toolkit-pro' ),
					'parent_item_colon' => __( 'Parent Categories:', 'rara-theme-toolkit-pro' ),
					'edit_item'         => __( 'Edit Categories', 'rara-theme-toolkit-pro' ),
					'update_item'       => __( 'Update Categories', 'rara-theme-toolkit-pro' ),
					'add_new_item'      => __( 'Add New Categories', 'rara-theme-toolkit-pro' ),
					'new_item_name'     => __( 'New Categories Name', 'rara-theme-toolkit-pro' ),
					'menu_name'         => __( $value['singular_name'].' Categories', 'rara-theme-toolkit-pro' ),
				);

				$args = array(
					'hierarchical'      => true,
					'labels'            => $labels,
					'show_ui'           => true,
					'show_admin_column' => true,
					'show_in_nav_menus' => true,
					'rewrite'           => array( 'slug' => $value['taxonomy_slug'], 'hierarchical' => true ),
				);
				register_taxonomy( $value['taxonomy'], array( $key ), $args );
			}
		}
	} 

	/**
    * 
    * Social icon template for team.
    *
    * @since 1.0.0
    */
    function rara_theme_tool_kit_team_social_template() { ?>
        
        <div class="rttk-social-team-template">
            <li class="rttk-social-icon-wrap">
                <p>
                    <span class="rttk-social-icons-field-handle dashicons dashicons-plus"></span>
                    <input class="team-social-length" name="rttk_setting[team][social][{{indexed}}]" type="text" value="" />
                    <span class="del-team-icon dashicons-no" style="font-family: 'dashicons'"></span>
                </p>
            </li>
        </div>
    <?php
        echo 
        '<style>
        	.rttk-social-team-template{
        		display: none;
        	}
        </style>';
        }


	function rara_theme_tool_kit_test() { 
        $supported_themes = array('Lawyer Landing Page Pro', 'Preschool and Kindergarten Pro', 'Bakes and Cakes Pro');
        $supported_templates = array('lawyer-landing-page-pro', 'preschool-and-kindergarten-pro', 'bakes-and-cakes-pro');
        $supported_themes = apply_filters( 'rrtk_supported_themes', $supported_themes );
        $supported_templates = apply_filters( 'rrtk_supported_templates', $supported_themes );

        $get_theme = wp_get_theme();
        $current_template = $get_theme->get( 'Template' );
	    foreach ( $supported_themes as $key ) {
	    	if ( strpos( $get_theme, $key ) === false ) 
	    	{
		    	$a = 'false';
			}
			else{
				$a = 'true';
				return;
			}
		}

		foreach ( $supported_templates as $key ) {
			if( isset($current_template) && $current_template!='' )
			{
		    	if ( strpos( $current_template, $key ) === false ) 
		    	{
			    	$a = 'false';
				}
				else{
					
					$a = 'true';
					return;
				}
			}
		}

		if($a == 'false')
		{
			add_action( 'admin_notices', function() {
				$get_theme = wp_get_theme();
				$installed_themes = wp_get_themes();
	       		$supported_themes = array('Lawyer Landing Page Pro', 'Preschool and Kindergarten Pro', 'Bakes and Cakes Pro');
    			$supported_themes = apply_filters( 'rrtk_supported_themes', $supported_themes );
	       		echo '<div class="notice notice-error is-dismissible"><p><strong><a href="#">Rara Theme Toolkit Pro</a></strong>'.__(' plugin recommends following theme(s) to be Activated: ','rara-theme-toolkit-pro');
	       		$var='';
	       		foreach ( $installed_themes as $key=>$value ) {
	       			if( $get_theme != $value['Name'] && in_array( $value['Name'], $supported_themes ) )
	       			{
	       				$var='1';
	       				echo '<strong class="installed-themes">'.$value['Name'].'</strong>';
	       			}
	       		}
	       		
	       		if( $var!='1' )
	       		{
	       			foreach ( $supported_themes as $supported_theme ) {
	       				echo '<strong class="required-themes">'.$supported_theme.'</strong>';
	       			}
	       		}
	       		echo '</p></div>';
	    	} );
		}
		
		return;
	}

	/**
	 * return array of post types that should use the Post Type Archive Description
	 * @return array post types to use description with (default, all non-built-in with archive)
	 */
	function rrtk_get_post_types() {
		// $args = array(
		// 	'_builtin' => false,
		// 	'has_archive' => true
		// );
		$post_types = array( 'event' => 'event', 'course' => 'course', 'testimonial' => 'testimonial', 'portfolio' => 'portfolio' );
		$post_types = apply_filters( 'rrtk_post_types_archive_desc', $post_types );

		return $post_types;
	}

	/**
	 * Output filterable name of Settings page
	 * 
	 * @param string $post_type name of post type for the page
	 * @param 'label'|'name' $pt_val whether $post_type is the label (default) or name
	 * @return name for settings page
	 */
	function rrtk_settings_page_title( $post_type, $pt_val = 'label' ) {
		if( $pt_val == 'name' ) {
			$post_type_info = get_post_types( array( 'name' => $post_type ), 'objects' );
			$post_type = $post_type_info[$post_type]->labels->name;
		}
		$settings_page_title = sprintf( __( 'Description for the %1$s Archive', 'rara-theme-toolkit-pro' ), $post_type );
		/**
		 * filter for admin menu label
		 * 
		 * @var string $settings_page_menu_label label text (default: "Description for the {Post Type} Archive")
		 * @var string $post_type post_type name if needed
		 */
		$settings_page_title = apply_filters( 'rrtk_admin_title', $settings_page_title, $post_type );
		return $settings_page_title;
	}

	/**
	 * Output filterable menu label for a post type's description settings page.
	 * @param  string $post_type post_type to create label for
	 * @param 'label'|'name' $pt_val whether $post_type is the label (default) or name
	 * @return string            admin menu label
	 */
	function rrtk_settings_menu_label( $post_type, $pt_val = 'label' ) {
		if( $pt_val == 'name' ) {
			$post_type_info = get_post_types( array( 'name' => $post_type ), 'objects' );
			$post_type = $post_type_info[$post_type]->labels->name;
		}
		$settings_page_menu_label = __( 'Archive Description', 'rara-theme-toolkit-pro' );
		/**
		 * filter for admin menu label
		 * 
		 * @var string $settings_page_menu_label label text (default: "Description")
		 * @var string $post_type post_type name if needed
		 */
		$settings_page_menu_label = apply_filters( 'rrtk_menu_label', $settings_page_menu_label, $post_type );
		return $settings_page_menu_label;
	}

	/****************************************************
	 * 
	 * Register Menu Pages, Settings, and Callbacks
	 * 
	 ****************************************************/

	/**
	 * Register admin pages for description field
	 */

	function rrtk_enable_pages() {

		$post_types = $this->rrtk_get_post_types();

		foreach ( $post_types as $post_type ) {

			if( post_type_exists( $post_type ) ) {

				add_submenu_page(
					'edit.php?post_type=' . $post_type, // $parent_slug
					$this->rrtk_settings_page_title( $post_type, 'name' ), // $page_title
					$this->rrtk_settings_menu_label( $post_type, 'name' ), // $menu_label
					'edit_posts', // $capability
					$post_type . '-description', // $menu_slug
					array( $this, 'rrtk_settings_page' )// $function
				);

			} // end if

		} // end foreach

	}

	/**
	 * Register Setting, Settings Section, and Settings Field
	 */

	function rrtk_register_settings() {

		$post_types = $this->rrtk_get_post_types();

		// A single option will hold all our descriptions


		// add a settings section and field for each $post_type
		foreach ( $post_types as $post_type ) {

			if( post_type_exists( $post_type ) ) {

				register_setting(
					'rrtk_descriptions', // $option_group
					'rrtk_descriptions', // $option_name
					array( $this, 'rrtk_sanitize_inputs' ) // $sanitize_callback
				);
				// Register settings and call sanitization functions
				add_settings_section(
					'rrtk_settings_section_' . $post_type, // $id
					'', // $title
					array( $this, 'rrtk_settings_section_callback' ), // $callback
					$post_type . '-description' // $page
				);

				// Field for our setting
				add_settings_field(
					'rrtk_setting_' . $post_type, // $id
					__( 'Description Text', 'rara-theme-toolkit-pro' ), // $title
					array( $this, 'rrtk_editor_field' ), // $callback
					$post_type . '-description', // $page
					'rrtk_settings_section_' . $post_type, // $section
					array( // $args
						'post_type' => $post_type,
						'field_name' => 'rrtk_descriptions[' . $post_type . ']',
						'label_for' => 'rrtk_descriptions[' . $post_type . ']'
					)
				);

			} // endif

		} // end foreach

	}

	// There is no need for this function at this time.
	function rrtk_settings_section_callback() {}

	/**
	 * Output a wp_editor instance for use by settings fields
	 */
	function rrtk_editor_field( $args ) {

		$post_type = $args['post_type'];
		$descriptions = (array) get_option( 'rrtk_descriptions' );

		if( array_key_exists($post_type, $descriptions) ) {
			$description = $descriptions[$post_type];
		} else {
			$description = '';
		}
		$editor_settings = array(
			'textarea_name' => $args['field_name'],
			'textarea_rows' => 15,
			'media_buttons' => true,
			'classes' 		=> 'wp-editor-area wp-editor'
		);
		$editor_settings = apply_filters( 'rrtk_wp_editor_settings', $editor_settings, $args, $description );

		wp_editor( $description, 'rrtkeditor', $editor_settings );
		
		if ( ! defined( 'QTX_VERSION' ) ) {
			add_filter( 'the_editor', 'qtranslate_admin_loadConfig' );
		}
		
	}

	/**
	 * Output settings pages
	 */
	function rrtk_settings_page() {
		$screen = get_current_screen();
		$post_type = $screen->post_type;
		?>
		<div class="wrap">
			<?php screen_icon(); ?>
			<h2><?php echo $this->rrtk_settings_page_title( $post_type, 'name' ); ?></h2>
			<form action="options.php" method="POST">
					<?php settings_fields( 'rrtk_descriptions' ); ?>
					<?php do_settings_sections( $post_type . '-description' ); ?>
					<?php submit_button(); ?>
			</form>
		</div> <?php
	}

	/**
	 * sanitize description inputs before saving option
	 */
	function rrtk_sanitize_inputs( $input ) {
		// get all descriptions
		$all_descriptions = (array) get_option( 'rrtk_descriptions' );
		// sanitize input
		foreach( $input as $post_type => $description ) {
			$sanitized_input[$post_type] = wp_kses_post( $description );
		}
		// merge with other descriptions into array setting
		$input = array_merge( $all_descriptions, $sanitized_input );

		return $input;
	}

	/**
	 * Return capability that's allowed to edit posts
	 * 
	 * See: http://core.trac.wordpress.org/ticket/14365
	 */
	function rrtk_allow_edit_posts() {
		$capability = 'edit_posts';
		/**
		 * filter the capability for who can edit descriptions
		 * 
		 * @var string $capability capability required to edit post type descriptions (default: edit_posts)
		 */
		$capability = apply_filters( 'rrtk_description_capability', $capability );
		
		return esc_attr( $capability );
	}
	/* Set options page permissions to honor specific permissions for editing the description */

	/**
	 * add links to view/edit archive in the admin bar
	 */
	function rrtk_admin_bar_links( $admin_bar ) {

		if(
			!is_admin()
			&& is_post_type_archive( $this->rrtk_get_post_types() )
			&& current_user_can( $this->rrtk_allow_edit_posts() )
		 ) {
			global $post_type;
			$post_type_object = get_post_type_object( $post_type );
			$post_type_name = $post_type_object->labels->name;

			$link_text = sprintf( __( 'Edit %1$s Description', 'rara-theme-toolkit-pro' ), $post_type_name );
			/**
			 * filter the "Edit {Post Type} Description" link
			 * @var $link_text string default test
			 * @var $post_type_name string name of post type for targeting specific type
			 */
			$link_text = apply_filters( 'rrtk_edit_description_link', $link_text, $post_type_name );

			$args = array(
				'id'    => 'wp-admin-bar-edit',
				'title' => $link_text,
				'href'  => admin_url( 'edit.php?post_type=' . $post_type . '&page=' . $post_type . '-description' )
			);
			$admin_bar->add_menu( $args );
		}

		if( is_admin() ) {

			$screen = get_current_screen();
			$post_type = $screen->post_type;
			$description_page = $post_type . '_page_' . $post_type . '-description';

			if( $screen->base == $description_page ) {
				$post_type_object = get_post_type_object( $post_type );
				$post_type_name = $post_type_object->labels->name;

				$link_text = sprintf( __( 'View %1$s Archive', 'rara-theme-toolkit-pro' ), $post_type_name );
				/**
				 * filter the "View {Post Type} Archive" link
				 * @var $link_text string default test
				 * @var $post_type_name string name of post type for targeting specific type
				 */
				$link_text = apply_filters( 'rrtk_view_archive_link', $link_text, $post_type_name );

				$post_type_object = get_post_type_object( $post_type );
				$args = array(
					'id'    => 'wp-admin-bar-edit',
					'title' => $link_text,
					'href'  => get_post_type_archive_link( $post_type )
				);
				$admin_bar->add_menu( $args );
			}
		}

	}

	/****************************************************
	 * 
	 * Automatically display content if using *_archive_description() introduced in 4.1!
	 * 
	 ****************************************************/
	/**
	 * filter the_archive_description & get_the_archive_description to show post type archive
	 * @param  string $description original description
	 * @return string              post type description if on post type archive
	 */
	function rrtk_archive_description( $description ) {
		if( is_post_type_archive( $this->rrtk_get_post_types() ) ) {
			$description = $this->rrtk_get_post_type_description();
		}
		return wp_kses_post( $description );
	}

	/****************************************************
	 * 
	 * Functions to get Description Page Content
	 * 
	 ****************************************************/
	/**
	 * echo post type archive description
	 * 
	 * if on a post type archive, automatically grabs current post type
	 * 
	 * @param  string $post_type slug for post type to show description for (optional)
	 * @return string            post type description
	 */
	function rrtk_the_post_type_description( $post_type = '' ) {
		echo $this->rrtk_get_post_type_description( $post_type );
	}

	/**
	 * return post type archive description
	 * 
	 * if on a post type archive, automatically grabs current post type
	 * 
	 * @param  string $post_type slug for post type to show description for (optional)
	 * @return string            post type description
	 */
	function rrtk_get_post_type_description( $post_type = '' ) {
		
		// get global $post_type if not specified
		if ( '' == $post_type ) {
			global $post_type;
		}

		$all_descriptions = (array) get_option( 'rrtk_descriptions' );
		if( array_key_exists($post_type, $all_descriptions) ) {
			$post_type_description = $all_descriptions[$post_type];
		} else {
			$post_type_description = '';
		}
		$description = apply_filters( 'the_content', $post_type_description );

		return wp_kses_post( $description );

	}


	/**
	 * filter editor settings to add necessary text editor classes for support
	 * @param  array $editor_settings tinymce settings array
	 * @return array                  filtered settings
	 */
	function rrtk_qtranslate_editor_args( $editor_settings ) {
		 $editor_settings['classes'] = $editor_settings['classes'] . ' multilanguage-input qtranxs-translatable';

		 return $editor_settings;
	}
	
	/**
	 * filter qtranslate so it knows to pay attention on archive description editor pages
	 */
	function rrtk_qtranslate_support( $page_configs ) {

			//edit.php?post_type=$post_type&page=
		$page_config = array();
		
		//get post types
		$post_types = $this->rrtk_get_post_types();

		// add a settings section and field for each $post_type
		foreach ( $post_types as $post_type ) {

			if( post_type_exists( $post_type ) ) {
				$page_config['pages'] = array( 'edit.php' => 'post_type=' . $post_type . '&page=' );
			}
			
		}

		$page_config['forms'] = array();

		$f = array();

		$f['fields'] = array();
		$fields = &$f['fields'];

		//textarea support
		$fields[] = array( 'tag' => 'textarea' );

		$page_config['forms'][] = $f;
		$page_configs[] = $page_config;

		return $page_configs;
	}

	/**
	 * Adds metabox for testimonials. 
	 * 
	 * @since 1.0.0
	 */
	function rttk_create_boxes(){
		$myarray = $this->rttk_get_posttype_array();
		foreach ($myarray as $key => $value) {
			add_meta_box(
				'rttk_'.$key.'_id', //assuming each key is different
				__( 'Details', 'rara-theme-toolkit-pro' ),
				array($this,'rttk_testimonials_metabox_callback'),
				$key, // WP_Screen
				'side',
				'high',
				array('key' => $key,'value' => $value) // This is what you need
			);
		}
	}

	public function rttk_testimonials_metabox_callback( $post, $callback_args ){
		$key = $callback_args['args']['key'];
		$value = $callback_args['args']['value'];
		include $value['path'].'rttk-'.$key.'-template.php';
	}
	/**
	 * When the post is saved, saves our custom data.
	 *
	 * @param int $post_id The ID of the post being saved.
	 */
	function rttk_save_meta_box_data( $post_id ) {
		
	    /*
	     * We need to verify this came from our screen and with proper authorization,
	     * because the save_post action can be triggered at other times.
	     */
	    // Sanitize user input.
	    if(isset($_POST['rttk_setting']))
	    {
		    $settings = $_POST['rttk_setting'];
		    update_post_meta( $post_id, '_rttk_setting', $settings );
	    }  
	}

}
