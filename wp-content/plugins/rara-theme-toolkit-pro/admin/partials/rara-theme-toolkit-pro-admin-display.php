<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://raratheme.com
 * @since      1.0.0
 *
 * @package    Rara_Theme_Toolkit_Pro
 * @subpackage Rara_Theme_Toolkit_Pro/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
