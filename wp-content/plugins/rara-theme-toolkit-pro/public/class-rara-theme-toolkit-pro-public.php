<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://raratheme.com
 * @since      1.0.0
 *
 * @package    Rara_Theme_Toolkit_Pro
 * @subpackage Rara_Theme_Toolkit_Pro/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Rara_Theme_Toolkit_Pro
 * @subpackage Rara_Theme_Toolkit_Pro/public
 * @author     Rara Theme <test@testmail.com>
 */
class Rara_Theme_Toolkit_Pro_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Rara_Theme_Toolkit_Pro_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Rara_Theme_Toolkit_Pro_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name.'fontawesome', plugin_dir_url( __FILE__ ) . 'css/font-awesome.min.css', array(), $this->version, 'all' );
		// wp_enqueue_style( $this->plugin_name.'social-icons', plugin_dir_url( __FILE__ ) . 'css/css/social-icons.min.css', array(), $this->version, 'all' );
		
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/rara-theme-toolkit-pro-public.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'lightslider', plugin_dir_url( __FILE__ ). 'css/lightslider.min.css', array(), $this->version, 'all' );
    	wp_enqueue_style( $this->plugin_name.'lightsidr', plugin_dir_url( __FILE__ ). 'css/jquery.sidr.light.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'magnific-popup', plugin_dir_url( __FILE__ ) . 'css/magnific-popup.css', array(), $this->version, 'all' );
    	
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Rara_Theme_Toolkit_Pro_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Rara_Theme_Toolkit_Pro_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_script( 'odometer', plugin_dir_url( __FILE__ ) . 'js/odometer.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'waypoint', plugin_dir_url( __FILE__ ) . 'js/waypoint.min.js', array( 'jquery' ), $this->version, false );
		
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/rara-theme-toolkit-pro-public.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'isotope', plugin_dir_url( __FILE__ ) . 'js/jquery.isotope.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'rttk-portfolio-isotope', plugin_dir_url( __FILE__ ) . 'js/rttk-portfolio-isotope.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'lightslider', plugin_dir_url( __FILE__ ) . 'js/lightslider.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'rttk-portfolio-settings', plugin_dir_url( __FILE__ ) . 'js/settings.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'magnific-popup', plugin_dir_url( __FILE__ ) . 'js/jquery.magnific-popup.min.js', array( 'jquery' ), $this->version, false );
	}

}
