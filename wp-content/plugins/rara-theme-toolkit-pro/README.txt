=== Plugin Name ===
Contributors: raratheme
Donate link: https://raratheme.com
Tags: comments, spam
Requires at least: 4.3
Tested up to: 4.9
Stable tag: 1.1.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The plugin generates multiple custom post types, number of Rara Theme exclusive widgets, and some useful shortcodes.

== Description ==

Rara Theme Toolkit Pro is a lightweight and safe premium plugin available exclusively with Premium Rara Theme themes. The plugin generates seven custom post types (FAQs, Logos, Practices, Services, Portfolios, Team Members, and Testimonials),16 Rara Theme exclusive widgets, and some useful shortcodes.

== Installation ==
**From your WordPress dashboard**

1. Visit 'Plugins > Add New',
2. Search for 'Rara One Click Demo Import' and install the plugin.
3. Activate 'Rara One Click Demo Import' from your Plugins page.


== Changelog ==

= 1.0.0 =
* Initial release

= 1.0.1 =
* Support for child theme

= 1.0.2 =
* Permalink flush
* Activation notice modified

= 1.0.3 =
* Flexibility for custom pot types
* Flexibility for meta boxes

= 1.0.4 =
* Flexibility for archive description added

= 1.0.5 =
* Recent post widget error fix
* OK, XING and VK social link background color added

= 1.0.6 =
* Social Icon Widget issue fixed

= 1.0.7 =
* Social Template

= 1.0.8 =
* Instagram default thumb size

= 1.0.9 =
* Instagram widgets

= 1.1.0 =
* Image widget url to id

= 1.1.1 =
* Instagram widget closing

= 1.1.2 =
* Stat counter Widget

= 1.1.3 =
* Instagram username check


