<?php
/**
 * Call To Action Widget
 *
 * @package Rttk_Pro
 */

// register Rttk_Pro_Cta widget
function rttk_pro_register_cta_widget(){
    register_widget( 'Rttk_Pro_Cta' );
}
add_action('widgets_init', 'rttk_pro_register_cta_widget');

//load wp color picker
function rttk_load_cta_colorpicker() {    
    wp_enqueue_style( 'wp-color-picker' );        
    wp_enqueue_script( 'wp-color-picker' );    
}
add_action( 'load-widgets.php', 'rttk_load_cta_colorpicker' );

 /**
 * Adds Rttk_Pro_Cta widget.
 */
class Rttk_Pro_Cta extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    public function __construct() {
        parent::__construct(
            'rttk_pro_cta_widget', // Base ID
            __( 'RARA: Call To Action Widget', 'rara-theme-toolkit-pro' ), // Name
            array( 'description' => __( 'A Call To Action Text Widget.', 'rara-theme-toolkit-pro' ), ) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
        
        $obj = new Rara_Theme_Toolkit_Pro_Functions();
        $title       = ! empty( $instance['title'] ) ? $instance['title'] : '' ;        
        $content     = ! empty( $instance['content'] ) ? $instance['content'] : '';
        $bg_image    = ! empty( $instance['bg_image'] ) ? $instance['bg_image'] : '';
        $bg_color    = ! empty( $instance['bg_color'] ) ? $instance['bg_color'] : '#686868';
        $button_text = ! empty( $instance['button_text'] ) ? $instance['button_text'] : '' ;
        $button_url  = ! empty( $instance['button_url'] ) ? $instance['button_url'] : '' ;
        
        
        if( $bg_image ){
            /** Added to work for demo content compatible */
            $attachment_id = $bg_image;
            if ( !filter_var( $bg_image, FILTER_VALIDATE_URL ) === false ) {
                $attachment_id = $obj->rttk_pro_get_attachment_id( $bg_image );
            }

            $icon_img_size = apply_filters('icon_img_size','rttk-thumb');
            $image_array   = wp_get_attachment_image_src( $attachment_id, $icon_img_size);
            $image         = preg_match('/(^.*\.jpg|jpeg|png|gif|ico*)/i', $image_array[0]);
            $fimg_url      = $image_array[0];     
            $ctaclass = 'cta-bg';
            $bg = 'style="background:url(' . esc_url( $fimg_url ) . ') no-repeat; background-size: cover; background-position: center"';
        }else{
            $ctaclass = 'text';
            $bg = 'style="background:' . sanitize_hex_color( $bg_color ) . '"';
        }
        
        echo $args['before_widget']; 
        ?>
        
        <div class="<?php echo $ctaclass;?>">
            <div class="rttk-cta-container">
                <div class="text-holder">
                    <?php if( $title ) echo $args['before_title'] . apply_filters( 'widget_title', $title, $instance, $this->id_base ) . $args['after_title']; ?>
                    
                    <div class="widget-content" <?php echo $bg; ?>>
                        <?php
                        if( $content ) echo wpautop( wp_kses_post( $content ) ); 
                        
                        if( $button_text && $button_url ) echo '<a href="' . esc_url( $button_url ) . '" class="btn-donate">' . esc_html( $button_text ) . '</a>';
                        ?>
                    </div>                
                </div>
            </div>
        </div>
        <?php 
        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {
        
        $obj = new Rara_Theme_Toolkit_Pro_Functions();
        $title       = ! empty( $instance['title'] ) ? $instance['title'] : '' ;        
        $content     = ! empty( $instance['content'] ) ? $instance['content'] : '';
        $bg_image    = ! empty( $instance['bg_image'] ) ? $instance['bg_image'] : '';
        $bg_color    = ! empty( $instance['bg_color'] ) ? $instance['bg_color'] : '#686868';
        $button_text = ! empty( $instance['button_text'] ) ? $instance['button_text'] : '' ;
        $button_url  = ! empty( $instance['button_url'] ) ? $instance['button_url'] : '' ;
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'rara-theme-toolkit-pro' ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />            
        </p>
        
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'content' ) ); ?>"><?php esc_html_e( 'Content', 'rara-theme-toolkit-pro' ); ?></label>
            <textarea name="<?php echo esc_attr( $this->get_field_name( 'content' ) ); ?>" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'content' ) ); ?>"><?php print $content; ?></textarea>
        </p>
        
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'button_text' ) ); ?>"><?php esc_html_e( 'Button Text', 'rara-theme-toolkit-pro' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'button_text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'button_text' ) ); ?>" type="text" value="<?php echo esc_attr( $button_text ); ?>" />
        </p>
        
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'button_url' ) ); ?>"><?php esc_html_e( 'Button Link', 'rara-theme-toolkit-pro' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'button_url' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'button_url' ) ); ?>" type="text" value="<?php echo esc_url( $button_url ); ?>" />
        </p>
        
        <div class="cta-bg-color">
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'bg_color' ) ); ?>"><?php esc_html_e( 'Background Color', 'rara-theme-toolkit-pro' ); ?></label><br />
            <input class="rrtk-widget-color-field" type="text" data-default-color="#686868" id="<?php echo esc_attr( $this->get_field_id( 'bg_color' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'bg_color' ) ); ?>" value="<?php echo esc_attr( $bg_color ); ?>" />
        </p>
        </div>
        
        <?php $obj->rttk_pro_get_image_field( $this->get_field_id( 'bg_image' ), $this->get_field_name( 'bg_image' ), $bg_image, __( 'Background Image', 'rara-theme-toolkit-pro' ) ); ?>
                        
        <?php
    }
    
    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        
        $instance['title']       = ! empty( $new_instance['title'] ) ? sanitize_text_field( $new_instance['title'] ) : '' ;
        $instance['content']     = ! empty( $new_instance['content'] ) ? wp_kses_post( $new_instance['content'] ) : '';
        $instance['bg_image']    = ! empty( $new_instance['bg_image'] ) ? esc_attr( $new_instance['bg_image'] ) : '';
        $instance['bg_color']    = ! empty( $new_instance['bg_color'] ) ? sanitize_hex_color( $new_instance['bg_color'] ) : '#686868';
        $instance['button_url']  = ! empty( $new_instance['button_url'] ) ? esc_url_raw( $new_instance['button_url'] ) : '';
        $instance['button_text'] = ! empty( $new_instance['button_text'] ) ? sanitize_text_field( $new_instance['button_text'] ) : '';
        
        return $instance;
    }
    
}  // class Rttk_Pro_Cta / class Rttk_Pro_Cta 