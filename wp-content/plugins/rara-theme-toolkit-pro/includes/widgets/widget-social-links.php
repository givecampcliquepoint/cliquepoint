<?php
/**
 * Widget Social Links
 *
 * @package Rttk
 */

// register Rttk_Social_Links widget 
function rttk_register_social_links_widget() {
    register_widget( 'Rttk_Social_Links' );
}
add_action( 'widgets_init', 'rttk_register_social_links_widget' );


//load wp sortable
function rttk_load_sortable() {    
    wp_enqueue_script( 'jquery-ui-core' );    
    wp_enqueue_script( 'jquery-ui-sortable' );    
}
add_action( 'load-widgets.php', 'rttk_load_sortable' );

//allow skype
function rttk_allowed_social_protocols( $protocols ) {
    $social_protocols = array(
        'skype'
    );
    return array_merge( $protocols, $social_protocols );    
}
add_filter( 'kses_allowed_protocols' ,'rttk_allowed_social_protocols' );

 /**
 * Adds Rttk_Social_Links widget.
 */
class Rttk_Social_Links extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        add_action( 'admin_print_footer_scripts', array( $this,'rara_theme_tool_kit_social_template' ) );
        
        parent::__construct(
            'rttk_social_links', // Base ID
            esc_html__( 'RARA: Social Links', 'rara-theme-toolkit-pro' ), // Name
            array( 'description' => esc_html__( 'A Social Links Widget', 'rara-theme-toolkit-pro' ), ) // Args
        );
    }

    /**
    * 
    * Social icon template.
    *
    * @since 1.0.0
    */
    function rara_theme_tool_kit_social_template() { ?>
        
        <div class="rttk-social-template">
            <li class="rttk-social-icon-wrap no">
                <p>
                    <span class="rttk-social-icons-field-handle dashicons dashicons-plus"></span>
                    <input class="social-length" placeholder="E.g: facebook.com" name="<?php echo esc_attr( $this->get_field_name( 'social[{{indexes}}]' ) ); ?>" type="text" value="" />
                    <span class="del-icon dashicons-no" style="font-family: 'dashicons'"></span>
                </p>
            </li>
        </div>
    <?php
        }

    /**
     * Get the allowed socicon lists.
     * @return array
     */
    function rttk_allowed_socicons() {
        return apply_filters( 'rttk_social_icons_allowed_socicon', array( 'modelmayhem', 'mixcloud', 'drupal', 'swarm', 'istock', 'yammer', 'ello', 'stackoverflow', 'persona', 'triplej', 'houzz', 'rss', 'paypal', 'odnoklassniki', 'airbnb', 'periscope', 'outlook', 'coderwall', 'tripadvisor', 'appnet', 'goodreads', 'tripit', 'lanyrd', 'slideshare', 'buffer', 'disqus', 'vk', 'whatsapp', 'patreon', 'storehouse', 'pocket', 'mail', 'blogger', 'technorati', 'reddit', 'dribbble', 'stumbleupon', 'digg', 'envato', 'behance', 'delicious', 'deviantart', 'forrst', 'play', 'zerply', 'wikipedia', 'apple', 'flattr', 'github', 'renren', 'friendfeed', 'newsvine', 'identica', 'bebo', 'zynga', 'steam', 'xbox', 'windows', 'qq', 'douban', 'meetup', 'playstation', 'android', 'snapchat', 'twitter', 'facebook', 'google-plus', 'pinterest', 'foursquare', 'yahoo', 'skype', 'yelp', 'feedburner', 'linkedin', 'viadeo', 'xing', 'myspace', 'soundcloud', 'spotify', 'grooveshark', 'lastfm', 'youtube', 'vimeo', 'dailymotion', 'vine', 'flickr', '500px', 'instagram', 'wordpress', 'tumblr', 'twitch', '8tracks', 'amazon', 'icq', 'smugmug', 'ravelry', 'weibo', 'baidu', 'angellist', 'ebay', 'imdb', 'stayfriends', 'residentadvisor', 'google', 'yandex', 'sharethis', 'bandcamp', 'itunes', 'deezer', 'medium', 'telegram', 'openid', 'amplement', 'viber', 'zomato', 'quora', 'draugiem', 'endomodo', 'filmweb', 'stackexchange', 'wykop', 'teamspeak', 'teamviewer', 'ventrilo', 'younow', 'raidcall', 'mumble', 'bebee', 'hitbox', 'reverbnation', 'formulr', 'battlenet', 'chrome', 'diablo', 'discord', 'issuu', 'macos', 'firefox', 'heroes', 'hearthstone', 'overwatch', 'opera', 'warcraft', 'starcraft', 'keybase', 'alliance', 'livejournal', 'googlephotos', 'horde', 'etsy', 'zapier', 'google-scholar', 'researchgate' ) );
    }

    /**
     * Get the icon from supported URL lists.
     * @return array
     */
    function rttk_supported_url_icon() {
        return apply_filters( 'social_icons_supported_url_icon', array(
            'feed'                  => 'rss',
            'ok.ru'                 => 'odnoklassniki',
            'vk.com'                => 'vk',
            'last.fm'               => 'lastfm',
            'youtu.be'              => 'youtube',
            'battle.net'            => 'battlenet',
            'blogspot.com'          => 'blogger',
            'play.google.com'       => 'play',
            'plus.google.com'       => 'google-plus',
            'photos.google.com'     => 'googlephotos',
            'chrome.google.com'     => 'chrome',
            'scholar.google.com'    => 'google-scholar',
            'feedburner.google.com' => 'mail',
        ) );
    }

    /**
     * Get the social icon name for given website url.
     *
     * @param  string $url Social site link.
     * @return string
     */
    function rttk_get_social_icon_name( $url ) {
        $icon = '';
        // $obj = new Rara_Theme_Toolkit_Pro_Admin;
        if ( $url = strtolower( $url ) ) {
            foreach ( $this->rttk_supported_url_icon() as $link => $icon_name ) {
                if ( strstr( $url, $link ) ) {
                    $icon = $icon_name;
                }
            }

            if ( ! $icon ) {
                foreach ( $this->rttk_allowed_socicons() as $icon_name ) {
                    if ( strstr( $url, $icon_name ) ) {
                        $icon = $icon_name;
                    }
                }
            }
        }

        return apply_filters( 'rttk_social_icons_get_icon_name', $icon, $url );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
       
        $title  = ! empty( $instance['title'] ) ? $instance['title'] : '';        
        $size   = isset($instance['size'])?esc_attr($instance['size']):'20';
        $allowed_socicon = $this->rttk_allowed_socicons();
        
        echo $args['before_widget'];
        if( $title ) echo $args['before_title'] . apply_filters( 'widget_title', $title, $instance, $this->id_base ) . $args['after_title'];
        if( isset( $instance['social'] ) && !empty($instance['social']) ){ 
            $icons = $instance['social'];
        ?>
            <ul class="social-networks">
                <?php
                    $arr_keys  = array_keys( $icons );
                    foreach ($arr_keys as $key => $value)
                    { 
                        if ( array_key_exists( $value,$instance['social'] ) )
                        { 
                            $icon = $this->rttk_get_social_icon_name( $instance['social'][$value] );
                            if( in_array( $this->rttk_get_social_icon_name( $instance['social'][$value] ), $allowed_socicon ) )
                            {
                            ?>
                            <li class="rttk-social-icon-wrap">
                                <a <?php if(isset($instance['target']) && $instance['target']=='1'){ echo "target=_blank"; } ?> href="<?php echo esc_url($instance['social'][$value]);?>"><span class="rttk-social-icons-field-handle fontawesome fa-<?php echo esc_attr($icon);?>" style="font-family: 'FontAwesome';"></span></a>
                            </li>
                        <?php
                            }
                        }
                    }
                ?>
            </ul>
        <?php
        }
        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {
        if( isset( $instance['title'] ) )
        {
            $title  = $instance['title'];       
        } 
        else{
            $title = __('Subscribe and Follow','rara-theme-toolkit-pro');
        }
        ?>
        <script type='text/javascript'>
            jQuery(document).ready(function($) {
                $('.rttk-sortable-icons').sortable({
                    cursor: 'move',
                    update: function (event, ui) {
                        $('ul.rttk-sortable-icons input').trigger('change');
                    }
                });
            });
        </script>
            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'rara-theme-toolkit-pro' ); ?></label> 
                <input class="widefat rttk-social-title-test" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
            </p>
            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'target' ) ); ?>"><?php esc_html_e( 'Open in New Tab', 'rara-theme-toolkit-pro' ); ?></label> 
                <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'target' ) ); ?>" 
                <?php $j='0'; if( isset( $instance['target'] ) ){ $j='1'; } ?> value="1" <?php checked( $j, true ); ?> name="<?php echo esc_attr( $this->get_field_name( 'target' ) ); ?>" type="checkbox" />
            </p>

        <ul class="rttk-sortable-icons" id="<?php echo esc_attr( $this->get_field_id( 'rttk-social-icons' ) ); ?>">
        <?php
        if(isset($instance['social']))
        {
            $icons  = $instance['social'];
            $arr_keys  = array_keys( $icons );
            $allowed_socicon = $this->rttk_allowed_socicons();
            if(isset($arr_keys)){
                foreach ($arr_keys as $key => $value)
                { 
                    if ( array_key_exists( $value,$instance['social'] ) )
                    { 
                        $icon = $this->rttk_get_social_icon_name( $instance['social'][$value] );
                        
                        ?>
                            <li class="rttk-social-icon-wrap">
                                <p>
                                    <span class="rttk-social-icons-field-handle fontawesome fa-<?php echo esc_attr( $icon )?>" style="font-family: 'FontAwesome'"></span>
                                    <input class="social-length" name="<?php echo esc_attr( $this->get_field_name( 'social['.$value.']' ) ) ?>" type="text" value="<?php echo esc_url($instance['social'][$value]);?>" />
                                    <span class="del-icon dashicons-no" style="font-family: 'dashicons'"></span>
                                </p>
                            </li>
                    <?php
                    }
                }
            }
        }
        ?>

            <div class="rttk-social-icon-holder"></div>
        </ul>
        <input class="rttk-social-add button-secondary" type="button" value="<?php _e('Add Social Icon','rara-theme-toolkit-pro');?>">
        <span class="widget-note"><?php _e('Click the above button to add social icons. You can sort them as well.','rara-theme-toolkit-pro');?></span>
        <?php 
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ! empty( $new_instance['title'] ) ? sanitize_text_field( $new_instance['title'] ) : '';
        $instance['target'] = $new_instance['target'];
        $instance['size'] = $new_instance['size'];
       
        if(isset($new_instance['social']) && !empty($new_instance['social']))
        {
            $arr_keys  = array_keys( $new_instance['social'] );
                    
            foreach ($arr_keys as $key => $value)
            { 
                if ( array_key_exists( $value,$new_instance['social'] ) )
                { 
                    
                    $instance['social'][$value] =  $new_instance['social'][$value];
                    
                }
            }
        }
        return $instance;            
    }
} 