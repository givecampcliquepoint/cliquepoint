<?php
/**
 * Widget Contact
 *
 * @package Rttk_Pro
 */
 
// register Rttk_Pro_Contact widget
function rttk_pro_register_contact_widget() {
    register_widget( 'Rttk_Pro_Contact' );
}
add_action( 'widgets_init', 'rttk_pro_register_contact_widget' );
 
 /**
 * Adds Rttk_Pro_Contact widget.
 */
class Rttk_Pro_Contact extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'rttk_pro_contact', // Base ID
			__( 'RARA: Contact', 'rara-theme-toolkit-pro' ), // Name
			array( 'description' => __( 'A Contact Widget', 'rara-theme-toolkit-pro' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
        
        $obj = new Rara_Theme_Toolkit_Pro_Functions();
        $title       = ! empty( $instance['title'] ) ? $instance['title'] : '';
        $content     = ! empty( $instance['content'] ) ? $instance['content'] : '';
        $street_add  = ! empty( $instance['street_add'] ) ? $instance['street_add'] : '';
        $phone       = ! empty( $instance['phone'] ) ? $instance['phone'] : '';
        $email       = ! empty( $instance['email'] ) ? $instance['email'] : '';
        
        echo $args['before_widget'];
        
        if( $title ) echo $args['before_title'] . apply_filters( 'widget_title', $title, $instance, $this->id_base ) . $args['after_title'];
        
        echo '<div class="widget-holder">';
        
        if( $content ) echo wpautop( wp_kses_post( $content ) );
        
        if( $street_add ) echo '<address class="address">' . esc_html( $street_add ) . '</address>';
        
        if( $phone ) echo '<span><a href="tel:' . preg_replace( '/\D/', '', $phone ) . '" class="tel-link">' . esc_html( $phone ) . '</a></span>';
        
        if( $email ) echo '<span><a href="mailto:' . sanitize_email( $email ) . '" class="email-link">' . esc_html( $email ) . '</a></span>';
        
        echo '</div>';
        
        echo $args['after_widget'];   
        
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
        
        $title       = ! empty( $instance['title'] ) ? $instance['title'] : '';
        $content     = ! empty( $instance['content'] ) ? $instance['content'] : '';
        $street_add  = ! empty( $instance['street_add'] ) ? $instance['street_add'] : '';
        $phone       = ! empty( $instance['phone'] ) ? $instance['phone'] : '';
        $email       = ! empty( $instance['email'] ) ? $instance['email'] : '';
        
        ?>
		
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'rara-theme-toolkit-pro' ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
        
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'content' ) ); ?>"><?php esc_html_e( 'Content', 'rara-theme-toolkit-pro' ); ?></label>
            <textarea name="<?php echo esc_attr( $this->get_field_name( 'content' ) ); ?>" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'content' ) ); ?>"><?php echo esc_attr( $content ); ?></textarea>
        </p>
        
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'street_add' ) ); ?>"><?php esc_html_e( 'Street Address', 'rara-theme-toolkit-pro' ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'street_add' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'street_add' ) ); ?>" type="text" value="<?php echo esc_attr( $street_add ); ?>" />
		</p>
        
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'phone' ) ); ?>"><?php esc_html_e( 'Phone', 'rara-theme-toolkit-pro' ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'phone' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'phone' ) ); ?>" type="text" value="<?php echo esc_attr( $phone ); ?>" />
		</p>        
        
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'email' ) ); ?>"><?php esc_html_e( 'Email', 'rara-theme-toolkit-pro' ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'email' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'email' ) ); ?>" type="text" value="<?php echo esc_attr( $email ); ?>" />
		</p>
        
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		
        $instance['title']       = ! empty( $new_instance['title'] ) ? sanitize_text_field( $new_instance['title'] ) : '';
        $instance['content']     = ! empty( $new_instance['content'] ) ? wp_kses_post( $new_instance['content'] ) : '';
        $instance['street_add']  = ! empty( $new_instance['street_add'] ) ? sanitize_text_field( $new_instance['street_add'] ) : '';
        $instance['phone']       = ! empty( $new_instance['phone'] ) ? sanitize_text_field( $new_instance['phone'] ) : '';
        $instance['email']       = ! empty( $new_instance['email'] ) ? sanitize_email( $new_instance['email'] ) : '';
                
        return $instance;
	}

} // class Rttk_Pro_Contact / class Rttk_Pro_Contact 