<?php
/**
 * Icon Text Widget
 *
 * @package Rttk_Pro
 */

// register Rttk_Pro_Icon_Text_Widget widget
function rttk_pro_register_icon_text_widget(){
    register_widget( 'Rttk_Pro_Icon_Text_Widget' );
}
add_action('widgets_init', 'rttk_pro_register_icon_text_widget');
 
 /**
 * Adds Rttk_Pro_Icon_Text_Widget widget.
 */
class Rttk_Pro_Icon_Text_Widget extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    public function __construct() {
        parent::__construct(
			'rttk_pro_icon_text_widget', // Base ID
			__( 'RARA: Icon Text Widget', 'rara-theme-toolkit-pro' ), // Name
			array( 'description' => __( 'An Icon Text Widget.', 'rara-theme-toolkit-pro' ), ) // Args
		);
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
        
        $obj = new Rara_Theme_Toolkit_Pro_Functions();
        $title   = ! empty( $instance['title'] ) ? $instance['title'] : '' ;		
        $content = ! empty( $instance['content'] ) ? $instance['content'] : '';
        $icon    = ! empty( $instance['icon'] ) ? $instance['icon'] : '';
        $image   = ! empty( $instance['image'] ) ? $instance['image'] : '';
        $link   = ! empty( $instance['link'] ) ? $instance['link'] : '';
        
        if( $image ){
            $attachment_id = $image;
            if ( !filter_var( $image, FILTER_VALIDATE_URL ) === false ) {
                $attachment_id = $obj->rttk_pro_get_attachment_id( $image );
            }

            $icon_img_size = apply_filters('icon_img_size','rttk-thumb');
            $image_array   = wp_get_attachment_image_src( $attachment_id, $icon_img_size);
            $image         = preg_match('/(^.*\.jpg|jpeg|png|gif|ico*)/i', $image_array[0]);
            $fimg_url      = $image_array[0];   
        }
        echo $args['before_widget']; 
        ?>
        <div class="rttk-itw-holder">
            <div class="rttk-itw-inner-holder">
                <?php if( $image ){ ?>
                    <div class="icon-holder">
                        <img src="<?php echo esc_url( $fimg_url ); ?>" alt="<?php echo esc_attr( $title ); ?>" />
                    </div>
                <?php }elseif( $icon ){ ?>
                    <div class="icon-holder">
                        <span class="fa <?php echo esc_attr( $icon ); ?>"></span>
                    </div>
                <?php }?>
    
                <div class="text-holder">
                <?php 
                    if( $title ) { echo $args['before_title']; }
                    if( isset( $link ) && $link!='' ){
                        echo '<a href="'.esc_url($link).'" target="_blank">'.apply_filters( 'widget_title', $title, $instance, $this->id_base ) .'</a>';
                    }
                    else{
                        echo apply_filters( 'widget_title', $title, $instance, $this->id_base );
                    }
                    echo $args['after_title'];                
                    if( $content ) echo wpautop( wp_kses_post( $content ) );
                ?>								
				</div>
            </div>
		</div>
        <?php    
        echo $args['after_widget'];
    }

    /**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
        
        $obj = new Rara_Theme_Toolkit_Pro_Functions();
        $title   = ! empty( $instance['title'] ) ? $instance['title'] : '' ;		
        $content = ! empty( $instance['content'] ) ? $instance['content'] : '';
        $icon    = ! empty( $instance['icon'] ) ? $instance['icon'] : '';
        $image   = ! empty( $instance['image'] ) ? $instance['image'] : '';
        $link   = ! empty( $instance['link'] ) ? $instance['link'] : '';

        ?>
		
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'rara-theme-toolkit-pro' ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />            
		</p>
        
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'content' ) ); ?>"><?php esc_html_e( 'Content', 'rara-theme-toolkit-pro' ); ?></label>
            <textarea name="<?php echo esc_attr( $this->get_field_name( 'content' ) ); ?>" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'content' ) ); ?>"><?php print $content; ?></textarea>
        </p>
        
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'link' ) ); ?>"><?php esc_html_e( 'Featured Link', 'rara-theme-toolkit-pro' ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'link' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'link' ) ); ?>" type="text" value="<?php echo esc_url( $link ); ?>" />            
        </p>
        
        <?php $obj->rttk_pro_get_image_field( $this->get_field_id( 'image' ), $this->get_field_name( 'image' ), $image, __( 'Upload Image', 'rara-theme-toolkit-pro' ) ); ?>
        
        <p><strong><?php esc_html_e( 'or', 'rara-theme-toolkit-pro' ); ?></strong></p>
        
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'icon' ) ); ?>"><?php esc_html_e( 'Icons', 'rara-theme-toolkit-pro' ); ?></label><br />
            <span class="icon-receiver"><i class="<?php echo esc_attr( $icon ); ?>"></i></span>
            <input class="hidden-icon-input" name="<?php echo esc_attr( $this->get_field_name( 'icon' ) ); ?>" type="hidden" id="<?php echo esc_attr( $this->get_field_id( 'icon' ) ); ?>" value="<?php echo esc_attr( $icon ); ?>" />            
        </p>

        
        <?php $obj->rttk_pro_get_icon_list(); ?>
                        
        <?php
	}
    
    /**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		
        $instance['title']   = ! empty( $new_instance['title'] ) ? sanitize_text_field( $new_instance['title'] ) : '' ;
        $instance['content'] = ! empty( $new_instance['content'] ) ? wp_kses_post( $new_instance['content'] ) : '';
        $instance['image']   = ! empty( $new_instance['image'] ) ? esc_attr( $new_instance['image'] ) : '';
        $instance['icon']    = ! empty( $new_instance['icon'] ) ? esc_attr( $new_instance['icon'] ) : '';
        $instance['link']    = ! empty( $new_instance['link'] ) ? esc_attr( $new_instance['link'] ) : '';
        
        return $instance;
	}
    
}  // class Rttk_Pro_Icon_Text_Widget / class Rttk_Pro_Icon_Text_Widget 