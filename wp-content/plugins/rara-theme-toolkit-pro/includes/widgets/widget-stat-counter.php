<?php
/**
 * Stat Counter Widget
 *
 * @package Rttk_Pro
 */

// register Rttk_Pro_Stat_Counter_Widget widget
function rttk_pro_register_stat_counter_widget(){
    register_widget( 'Rttk_Pro_Stat_Counter_Widget' );
}
add_action('widgets_init', 'rttk_pro_register_stat_counter_widget');
 
 /**
 * Adds Rttk_Pro_Stat_Counter_Widget widget.
 */
class Rttk_Pro_Stat_Counter_Widget extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    public function __construct() {
        parent::__construct(
            'rttk_pro_stat_counter_widget', // Base ID
            __( 'RARA: Stat Counter Widget', 'rara-theme-toolkit-pro' ), // Name
            array( 'description' => __( 'Widget for stat counter.', 'rara-theme-toolkit-pro' ), ) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
        
        $obj = new Rara_Theme_Toolkit_Pro_Functions();
        $title   = ! empty( $instance['title'] ) ? $instance['title'] : '' ;        
        $counter = ! empty( $instance['counter'] ) ? $instance['counter'] : '';
        $icon    = ! empty( $instance['icon'] ) ? $instance['icon'] : '';
        $image   = ! empty( $instance['image'] ) ? $instance['image'] : '';
        if( $image ){
            $attachment_id = $image;
            if ( !filter_var( $image, FILTER_VALIDATE_URL ) === false ) {
                $attachment_id = $obj->rttk_pro_get_attachment_id( $image );
            }

            $icon_img_size = apply_filters('icon_img_size','rttk-thumb');
            $image_array   = wp_get_attachment_image_src( $attachment_id, $icon_img_size);
            $image         = preg_match('/(^.*\.jpg|jpeg|png|gif|ico*)/i', $image_array[0]);
            $fimg_url      = $image_array[0];   
        }
        echo $args['before_widget']; 
        $ran = rand(1,100); $ran++;?>
        
            <div class="col">
                <div class="rttk-sc-holder">
                  <?php if( $image ){ ?>
                      <div class="icon-holder">
                          <?php do_action('widget_stat_counter_before_icon')?><img src="<?php echo esc_url( $fimg_url ); ?>" alt="<?php echo esc_attr( $title ); ?>" /><?php do_action('widget_stat_counter_after_icon')?>
                      </div>
                  <?php }elseif( $icon ){ ?>
                      <div class="icon-holder">
                          <?php do_action('widget_stat_counter_before_icon')?><i class="fa <?php echo esc_attr( $icon ); ?>"></i><?php do_action('widget_stat_counter_after_icon')?>
                      </div>
                  <?php } ?>
                  <?php
                  if( $counter ) { $delay = ($ran/1000)*100;?>
                      <?php do_action('widget_stat_counter_before_outer_wrapper')?>
                      <div class="hs-counter hs-counter<?php echo $ran;?> wow fadeInDown" data-wow-duration="<?php echo $delay/100; echo 's';?>" data-wow-delay="<?php echo $delay.'s'?>">
                          <?php do_action('widget_stat_counter_before_inner_wrapper')?>
                          <div class="hs-counter-count odometer odometer<?php echo $ran;?>" data-count="<?php echo absint($counter); ?>">
                              99
                          </div>
                          <?php do_action('widget_stat_counter_after_inner_wrapper')?>
                      </div>
                      <?php if( $title ) echo $args['before_title'] . apply_filters( 'widget_title', $title, $instance, $this->id_base ) . $args['after_title']; ?>
                      <?php do_action('widget_stat_counter_after_outer_wrapper')?>
                  <?php } ?>
                </div>
            </div>
            <?php 
            echo
            '<script>
            jQuery( document ).ready(function($) {
                $(".odometer'.$ran.'").waypoint(function() {
                   setTimeout(function() {
                      $(".odometer'.$ran.'").html($(".odometer'.$ran.'").data("count"));
                    }, 500);
                  }, {
                    offset: 800,
                    triggerOnce: true
                });
            });</script>';       
        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {
        
        $obj = new Rara_Theme_Toolkit_Pro_Functions();
        $title   = ! empty( $instance['title'] ) ? $instance['title'] : '' ;        
        $counter = ! empty( $instance['counter'] ) ? $instance['counter'] : '';
        $icon    = ! empty( $instance['icon'] ) ? $instance['icon'] : '';
        $image   = ! empty( $instance['image'] ) ? $instance['image'] : '';
        
        ?>
        
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'rara-theme-toolkit-pro' ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />            
        </p>
        
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'counter' ) ); ?>"><?php esc_html_e( 'Counter', 'rara-theme-toolkit-pro' ); ?></label>
            <input name="<?php echo esc_attr( $this->get_field_name( 'counter' ) ); ?>" type="number" step="1" min="1" id="<?php echo esc_attr( $this->get_field_id( 'content' ) ); ?>" value="<?php echo absint( $counter ); ?>" class="small-text" />         
        </p>

        <?php $obj->rttk_pro_get_image_field( $this->get_field_id( 'image' ), $this->get_field_name( 'image' ), $image, __( 'Upload Image', 'rara-theme-toolkit-pro' ) ); ?>
        
        <p><strong><?php esc_html_e( 'or', 'rara-theme-toolkit-pro' ); ?></strong></p>
        
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'icon' ) ); ?>"><?php esc_html_e( 'Icons', 'rara-theme-toolkit-pro' ); ?></label><br />
            <span class="icon-receiver"><i class="<?php echo esc_attr( $icon ); ?>"></i></span>
            <input class="hidden-icon-input" name="<?php echo esc_attr( $this->get_field_name( 'icon' ) ); ?>" type="hidden" id="<?php echo esc_attr( $this->get_field_id( 'icon' ) ); ?>" value="<?php echo esc_attr( $icon ); ?>" />            
        </p>
        
        <?php $obj->rttk_pro_get_icon_list(); ?>
                        
        <?php
    }
    
    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        
        $instance['title']   = ! empty( $new_instance['title'] ) ? sanitize_text_field( $new_instance['title'] ) : '' ;
        $instance['counter'] = ! empty( $new_instance['counter'] ) ? absint( $new_instance['counter'] ) : '';
        $instance['image']   = ! empty( $new_instance['image'] ) ? esc_attr( $new_instance['image'] ) : '';
        $instance['icon']    = ! empty( $new_instance['icon'] ) ? esc_attr( $new_instance['icon'] ) : '';
       
        return $instance;
    }
    
}  // class Rttk_Pro_Stat_Counter_Widget / class Rttk_Pro_Stat_Counter_Widget 