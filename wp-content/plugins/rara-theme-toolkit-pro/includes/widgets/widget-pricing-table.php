<?php
/**
 * Pricing Table Widget
 *
 * @package Rttk_Pro
 */

// register Rttk_Pro_Pricing_Table_Widget widget
function rttk_pro_register_pricing_table_widget(){
    register_widget( 'Rttk_Pro_Pricing_Table_Widget' );
}
add_action('widgets_init', 'rttk_pro_register_pricing_table_widget');
 
 /**
 * Adds Rttk_Pro_Pricing_Table_Widget widget.
 */
class Rttk_Pro_Pricing_Table_Widget extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    public function __construct() {
        add_action( 'admin_print_footer_scripts', array( $this,'rara_theme_tool_kit_item_template' ) );
        parent::__construct(
			'rttk_pro_pricing_table__widget', // Base ID
			__( 'RARA: Pricing Table Widget', 'rara-theme-toolkit-pro' ), // Name
			array( 'description' => __( 'A Pricing Table Widget.', 'rara-theme-toolkit-pro' ), ) // Args
		);
    }

    /**
    * 
    * Items template.
    *
    * @since 1.0.0
    */
    function rara_theme_tool_kit_item_template() { ?>
        
        <div class="rttk-item-template">
            <li class="rttk-items-wrap">
                <p>
                    <input class="items-length" name="<?php echo esc_attr( $this->get_field_name( 'items[{{indexed}}]' ) ); ?>" type="text" value="" />
                    <span class="del-item dashicons-no" style="font-family: 'dashicons'"></span>
                </p>
            </li>
        </div>
    <?php
        }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
        
        $obj = new Rara_Theme_Toolkit_Pro_Functions();
        $title   = ! empty( $instance['title'] ) ? $instance['title'] : '' ;		
        $subtitle = ! empty( $instance['subtitle'] ) ? $instance['subtitle'] : '';
        $icon    = ! empty( $instance['icon'] ) ? $instance['icon'] : '';
        $image   = ! empty( $instance['image'] ) ? $instance['image'] : '';
        $link   = ! empty( $instance['link'] ) ? $instance['link'] : '';
        $label   = ! empty( $instance['label'] ) ? $instance['label'] : '';


        if( $image ){
            $attachment_id = $image;
            if ( !filter_var( $image, FILTER_VALIDATE_URL ) === false ) {
                $attachment_id = $obj->rttk_pro_get_attachment_id( $image );
            }

            $icon_img_size = apply_filters('icon_img_size','rttk-thumb');
            $image_array   = wp_get_attachment_image_src( $attachment_id, $icon_img_size);
            $image         = preg_match('/(^.*\.jpg|jpeg|png|gif|ico*)/i', $image_array[0]);
            $fimg_url      = $image_array[0];   
        }
        
        echo $args['before_widget'];
        ?>
            <div class="rttk-pt-holder">
                <div class="rttk-pt-top-header">
                    <?php if( $image ){ ?>
                        <div class="icon-holder">
                            <img src="<?php echo esc_url( $fimg_url ); ?>" alt="<?php echo esc_attr( $title ); ?>" />
                        </div>
                    <?php }elseif( $icon ){ ?>
                        <div class="icon-holder">
                            <span class="fa <?php echo esc_attr( $icon ); ?>"></span>
                        </div>
                    <?php }?>
                </div>
                <div class="text-holder">
                <?php 
                    echo '<div class="rttk-pt-title-header">';
                    if( $title ) echo $args['before_title'] . apply_filters( 'widget_title', $title, $instance, $this->id_base ) . $args['after_title'];
                    if( $subtitle ) echo '<span class="rttk-pt-price">'.wp_kses_post( $subtitle ).'</span>';
                    echo '</div>';
                    if( isset( $instance['items'] ) && !empty($instance['items']) ){ 
                        ?>
                        <ul class="pricing-table">
                            <?php
                                $items  = $instance['items'];
                                $arr_keys  = array_keys( $items );
                                foreach ( $items as $key => $value )
                                { 
                                    if ( array_key_exists( $key,$instance['items'] ) )
                                    { 
                                        ?>
                                        <li class="rttk-items-wrap">
                                            <?php echo esc_attr($instance['items'][$key]);?>
                                        </li>
                                    <?php
                                    }
                                }
                            ?>
                        </ul> 
                    <?php
                    }
                    if( isset($link) && $link!='' && isset($label) && $label!='' ) echo '<a href="'.esc_url($link).'" class="rttk-pt-buy-btn" target="_blank">'.esc_attr($label) .'</a>';
                ?>                              
                </div>
            </div>
        <?php            
        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
        
        $obj = new Rara_Theme_Toolkit_Pro_Functions();
        $title   = ! empty( $instance['title'] ) ? $instance['title'] : '' ;		
        $subtitle = ! empty( $instance['subtitle'] ) ? $instance['subtitle'] : '';
        $icon    = ! empty( $instance['icon'] ) ? $instance['icon'] : '';
        $image   = ! empty( $instance['image'] ) ? $instance['image'] : '';
        $link   = ! empty( $instance['link'] ) ? $instance['link'] : '';
        $label   = ! empty( $instance['label'] ) ? $instance['label'] : '';
        ?>
		
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'rara-theme-toolkit-pro' ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />            
		</p>
        
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'subtitle' ) ); ?>"><?php esc_html_e( 'Price/Subtitle', 'rara-theme-toolkit-pro' ); ?></label>
            <input type="text" name="<?php echo esc_attr( $this->get_field_name( 'subtitle' ) ); ?>" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'subtitle' ) ); ?>" value="<?php print $subtitle; ?>">
        </p>
        
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'link' ) ); ?>"><?php esc_html_e( 'Featured Link', 'rara-theme-toolkit-pro' ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'link' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'link' ) ); ?>" type="text" value="<?php echo esc_url( $link ); ?>" />            
        </p>

         <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'label' ) ); ?>"><?php esc_html_e( 'Label', 'rara-theme-toolkit-pro' ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'label' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'label' ) ); ?>" type="text" value="<?php echo esc_attr( $label ); ?>" />            
        </p>

        <script type='text/javascript'>
            jQuery(document).ready(function($) {
                $('.rttk-sortable-items').sortable({
                    cursor: 'move',
                    update: function (event, ui) {
                        $('ul.rttk-sortable-items input').trigger('change');
                    }
                });
            });
        </script>

        <ul class="rttk-sortable-items" id="<?php echo esc_attr( $this->get_field_id( 'rttk-items' ) ); ?>">
            <?php
            if(isset($instance['items']))
            {
                $items  = $instance['items'];
                $arr_keys  = array_keys( $items );
                if(isset($arr_keys)){
                    foreach ($arr_keys as $key => $value)
                    { 
                        if ( array_key_exists( $value,$instance['items'] ) )
                        { 
                            ?>
                                <li class="rttk-items-wrap">
                                    <p>
                                        <input class="items-length" name="<?php echo esc_attr( $this->get_field_name( 'items['.$value.']' ) ) ?>" type="text" value="<?php echo esc_attr($instance['items'][$value]);?>" />
                                        <span class="del-item dashicons-no" style="font-family: 'dashicons'"></span>
                                    </p>
                                </li>
                        <?php
                        }
                    }
                }
            }
            ?>
            <div class="rttk-items-holder"></div>
        </ul>
        <input class="rttk-items-add button-secondary" type="button" value="<?php _e('Add Item','rara-theme-toolkit-pro');?>">
        <span class="widget-note"><?php _e('Click the above button to add items. You can sort them as well.','rara-theme-toolkit-pro');?></span>
        
        <?php $obj->rttk_pro_get_image_field( $this->get_field_id( 'image' ), $this->get_field_name( 'image' ), $image, __( 'Upload Image', 'rara-theme-toolkit-pro' ) ); ?>
        
        <p><strong><?php esc_html_e( 'or', 'rara-theme-toolkit-pro' ); ?></strong></p>
        
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'icon' ) ); ?>"><?php esc_html_e( 'Icons', 'rara-theme-toolkit-pro' ); ?></label><br />
            <span class="icon-receiver"><i class="<?php echo esc_attr( $icon ); ?>"></i></span>
            <input class="hidden-icon-input" name="<?php echo esc_attr( $this->get_field_name( 'icon' ) ); ?>" type="hidden" id="<?php echo esc_attr( $this->get_field_id( 'icon' ) ); ?>" value="<?php echo esc_attr( $icon ); ?>" />            
        </p>
        <?php $obj->rttk_pro_get_icon_list(); ?>
        <?php
	}
    
    /**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		
        $instance['title']   = ! empty( $new_instance['title'] ) ? sanitize_text_field( $new_instance['title'] ) : '' ;
        $instance['subtitle'] = ! empty( $new_instance['subtitle'] ) ? wp_kses_post( $new_instance['subtitle'] ) : '';
        $instance['image']   = ! empty( $new_instance['image'] ) ? esc_attr( $new_instance['image'] ) : '';
        $instance['icon']    = ! empty( $new_instance['icon'] ) ? esc_attr( $new_instance['icon'] ) : '';
        $instance['link']    = ! empty( $new_instance['link'] ) ? esc_attr( $new_instance['link'] ) : '';
        $instance['label']    = ! empty( $new_instance['label'] ) ? esc_attr( $new_instance['label'] ) : '';

        if(isset($new_instance['items']) && !empty($new_instance['items']))
        {
            $arr_keys  = array_keys( $new_instance['items'] );
                    
            foreach ($arr_keys as $key => $value)
            { 
                if ( array_key_exists( $value,$new_instance['items'] ) )
                { 
                    $instance['items'][$value] =  $new_instance['items'][$value];
                }
            }
        }
        // print_r($new_instance);
        // die;
        return $instance;
	}
    
}  // class Rttk_Pro_Pricing_Table_Widget / class Rttk_Pro_Pricing_Table_Widget 