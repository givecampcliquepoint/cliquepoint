<?php
/**
 * Widget Flickr
 *
 * @package Rttk_Pro
 */
class Rttk_Pro_Flickr_Widget extends WP_Widget {

    private $widget_id;

    /**
     * Register widget with WordPress.
     */
    public function __construct()
    {
        $this->widget_id    = 'flickr_widget';
        $widget_name        = __( 'RARA: Flickr Widget', 'rara-theme-toolkit-pro' );
        $widget_options     = array(
            'classname'     => 'rttk_flickr_widget',
            'description'   => __( 'Display your latest Flickr photos.', 'rara-theme-toolkit-pro' ),
        );

        parent::__construct( $this->widget_id, $widget_name, $widget_options );

    }

    public function flush_widget_cache() {
        wp_cache_delete( $this->widget_id );
    }


    function widget( $args, $instance ) {

        $cache = wp_cache_get( $this->widget_id );

        if ( $cache ){
            echo $cache;
            return;
        }

        ob_start();

        extract( $args );
        
        $title          = apply_filters( 'widget_title', $instance['title'] );
        $flickr_id      = esc_attr($instance['flickr_id']);
        $number         = absint($instance['number']);
        $row_number     = absint($instance['row_number']);
        
        include_once(ABSPATH . WPINC . '/feed.php');

        $rss = fetch_feed('http://api.flickr.com/services/feeds/photos_public.gne?ids='.$flickr_id.'&lang=en-us&format=rss_200');
        
        add_filter( 'wp_feed_cache_transient_lifetime', function(){
            return 1800;
        });

        if( ! is_wp_error( $rss ) ){
            $items = $rss->get_items( 0, $rss->get_item_quantity( $number ) );
        }

        echo $before_widget;

        if ( $title ) echo $before_title . $title . $after_title;
        ?>
        <div id="flickr" class="rttk_flickr_widget-row col-<?php echo esc_attr($row_number);?>"></div>
        <?php
        echo 
            '<script type="text/javascript">
            jQuery(function($){       
            var id="'.$flickr_id.'";
            var limit ="'.$number.'";

            // Flickr Photostream feed link.
            $.getJSON("http://api.flickr.com/services/feeds/photos_public.gne?id=" + id + "&lang=en-us&format=json&jsoncallback=?", 

            function(data){$.each(data.items, 

            function(i,item){
                // Number of thumbnails to show.            
                if(i < limit){
                    var wrap=`<a href=${item.media.m.replace("_m", "_n") }name=${item.link}title=${item.title}></a>`;
                    $("<img/>").attr("src", item.media.m.replace("_m", "_z")).appendTo("#flickr").wrap(wrap);
                // Create images and append to div id flickr and wrap link around the image.
                }
            }); 

            }); 

            });
            </script>';
        echo $after_widget;
        $content = ob_get_clean();
        wp_cache_set( $this->widget_id, $content );
        echo $content;
    }

    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        
        $instance['title']      = sanitize_text_field( $new_instance['title'] );
        $instance['flickr_id']  = sanitize_text_field( $new_instance['flickr_id'] );
        $instance['number']     = absint( $new_instance['number'] );
        $instance['row_number'] = absint( $new_instance['row_number'] );

        $this->flush_widget_cache();

        return $instance;
    }

    function form( $instance ){
        $defaults = array(
            'title'         => __( 'Flickr Photos', 'rara-theme-toolkit-pro' ),
            'flickr_id'     => '',
            'number'        => 9,
            'row_number'    => 3,
        );
        $instance = wp_parse_args( (array) $instance, $defaults );

        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">
                <?php _e( 'Title:', 'rara-theme-toolkit-pro' ); ?>
            </label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>">
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('flickr_id'); ?>">
                <?php _e( 'Your Flickr User ID:', 'rara-theme-toolkit-pro' ); ?>
            </label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('flickr_id'); ?>" name="<?php echo $this->get_field_name('flickr_id'); ?>" value="<?php echo $instance['flickr_id']; ?>">
            <span class="description">
                <?php echo sprintf( __( 'Find your Flickr user ID here  %s.', 'rara-theme-toolkit-pro' ), '<a href="//idgettr.com" target="_blank" rel="nofollow">idgettr</a>' ); ?>
            </span>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>">
                <?php _e( 'Number of photos to show:', 'rara-theme-toolkit-pro' ); ?>
            </label>
            <input type="number" class="widefat" id="<?php echo $this->get_field_id('number'); ?>" max="20" min="1" name="<?php echo $this->get_field_name('number'); ?>" value="<?php echo $instance['number']; ?>">
            <span class="description">
                <?php echo __( 'Flickr seems to limit its feeds to 20. So you can use maximum 20 photos.', 'rara-theme-toolkit-pro' ); ?>
            </span>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('row_number'); ?>">
                <?php _e( 'Number of photos per row:', 'rara-theme-toolkit-pro' ); ?>
            </label>
            <input type="number" class="widefat" max="6" id="<?php echo $this->get_field_id('row_number'); ?>" name="<?php echo $this->get_field_name('row_number'); ?>" value="<?php echo $instance['row_number']; ?>">
            <span class="description">
                <?php echo __( 'You can use minimum 1 photo and maximum 6 photos.', 'rara-theme-toolkit-pro' ); ?>
            </span>
        </p>
        <?php
    }
}

add_action( 'widgets_init', function(){ register_widget( "Rttk_Pro_Flickr_Widget" );});