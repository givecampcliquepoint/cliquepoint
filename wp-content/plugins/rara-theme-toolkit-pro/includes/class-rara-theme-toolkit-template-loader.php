<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * The template loader of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Travel_Triping
 * @subpackage Travel_Triping/admin
 * @author     Code Wing <test@test.com>
 */
class RRTK_Load_Templates {

	/**
	 * Hook in methods.
	 */
	public function __construct() {
		// add_filter( 'template_include', array( $this, 'rttk_include_template_function' ) );
	}

	
	/**
	 * Template over-ride for single trip.
	 *
	 * @since    1.0.0
	 */
	function rttk_include_template_function( $template_path ) {

	    if ( get_post_type() == 'event' ) {
	        if ( is_single() ) {
	            if ( $theme_file = locate_template( array ( 'single-event.php' ) ) ) {
	                $template_path = $theme_file;
	            } else {
	                $template_path = RTTKPRO_TEMPLATE_PATH . '/single-event.php';
	            }
	        }
	        if( is_archive() ) {

	        	if ( $theme_file = locate_template( array ( 'archive-event.php' ) ) ) {
	                $template_path = $theme_file;
	            } else {
	                $template_path = RTTKPRO_TEMPLATE_PATH . '/archive-event.php';
	            }
	        }
	        if( is_tax( 'event_categories' ) ){
	        	
	        	if ( $theme_file = locate_template( array ( 'taxonomy-event_categories.php' ) ) ) {
	                $template_path = $theme_file;
	            } else {
	                $template_path = RTTKPRO_TEMPLATE_PATH . '/taxonomy-event_categories.php';
	            }
	        }
	    }

	    if ( get_post_type() == 'course' ) {
	        if ( is_single() ) {
	            if ( $theme_file = locate_template( array ( 'single-course.php' ) ) ) {
	                $template_path = $theme_file;
	            } else {
	                $template_path = RTTKPRO_TEMPLATE_PATH . '/single-course.php';
	            }
	        }
	        if( is_archive() ) {

	        	if ( $theme_file = locate_template( array ( 'archive-course.php' ) ) ) {
	                $template_path = $theme_file;
	            } else {
	                $template_path = RTTKPRO_TEMPLATE_PATH . '/archive-course.php';
	            }
	        }
	        if( is_tax( 'course_categories' ) ){
	        	
	        	if ( $theme_file = locate_template( array ( 'taxonomy-course_categories.php' ) ) ) {
	                $template_path = $theme_file;
	            } else {
	                $template_path = RTTKPRO_TEMPLATE_PATH . '/taxonomy-course_categories.php';
	            }
	        }
	    }

	    if ( get_post_type() == 'team' ) {
	        if ( is_single() ) {
	            if ( $theme_file = locate_template( array ( 'single-team.php' ) ) ) {
	                $template_path = $theme_file;
	            } else {
	                $template_path = RTTKPRO_TEMPLATE_PATH . '/single-team.php';
	            }
	        }
	        if( is_archive() ) {

	        	if ( $theme_file = locate_template( array ( 'archive-team.php' ) ) ) {
	                $template_path = $theme_file;
	            } else {
	                $template_path = RTTKPRO_TEMPLATE_PATH . '/archive-team.php';
	            }
	        }
	        if( is_tax( 'team_categories' ) ){
	        	
	        	if ( $theme_file = locate_template( array ( 'taxonomy-team_categories.php' ) ) ) {
	                $template_path = $theme_file;
	            } else {
	                $template_path = RTTKPRO_TEMPLATE_PATH . '/taxonomy-team_categories.php';
	            }
	        }
	    }

	    if ( get_post_type() == 'testimonial' ) {
	        if ( is_single() ) {
	            if ( $theme_file = locate_template( array ( 'single-testimonial.php' ) ) ) {
	                $template_path = $theme_file;
	            } else {
	                $template_path = RTTKPRO_TEMPLATE_PATH . '/single-testimonial.php';
	            }
	        }
	        if( is_archive() ) {

	        	if ( $theme_file = locate_template( array ( 'archive-testimonial.php' ) ) ) {
	                $template_path = $theme_file;
	            } else {
	                $template_path = RTTKPRO_TEMPLATE_PATH . '/archive-testimonial.php';
	            }
	        }
	        if( is_tax( 'testimonial_categories' ) ){
	        	
	        	if ( $theme_file = locate_template( array ( 'taxonomy-testimonial_categories.php' ) ) ) {
	                $template_path = $theme_file;
	            } else {
	                $template_path = RTTKPRO_TEMPLATE_PATH . '/taxonomy-testimonial_categories.php';
	            }
	        }
	    }

	    if ( get_post_type() == 'portfolio' ) {
	        if ( is_single() ) {
	            if ( $theme_file = locate_template( array ( 'single-portfolio.php' ) ) ) {
	                $template_path = $theme_file;
	            } else {
	                $template_path = RTTKPRO_TEMPLATE_PATH . '/single-portfolio.php';
	            }
	        }
	        if( is_archive() ) {

	        	if ( $theme_file = locate_template( array ( 'archive-portfolio.php' ) ) ) {
	                $template_path = $theme_file;
	            } else {
	                $template_path = RTTKPRO_TEMPLATE_PATH . '/archive-portfolio.php';
	            }
	        }
	        if( is_tax( 'portfolio_categories' ) ){
	        	
	        	if ( $theme_file = locate_template( array ( 'taxonomy-portfolio_categories.php' ) ) ) {
	                $template_path = $theme_file;
	            } else {
	                $template_path = RTTKPRO_TEMPLATE_PATH . '/taxonomy-portfolio_categories.php';
	            }
	        }
	    }

	    return $template_path;
	}
}

new RRTK_Load_Templates();
