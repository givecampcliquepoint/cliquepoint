<?php
  /**
    * The template for displaying portfolio categories page
    *
    * @package Rara_Theme_Toolkit_Pro
    * @subpackage Rara_Theme_Toolkit_Pro/includes/rara-toolkit-pro-templates
    * @since 1.0.0
    */
    get_header();?>
    <div class="container">
        <div id="content" class="site-content">
            <div class="row">
                <div id="primary" class="content-area">
                    <?php
                    $termID = get_queried_object()->term_id; // Parent A ID
                    $taxonomyName = 'portfolio_categories';
                    $termchildren = get_term_children( $termID, $taxonomyName );
                    if($termchildren) {
                        $rttk_portfolio_cat_slug = get_queried_object()->slug;
                        $rttk_portfolio_cat_name = get_queried_object()->name;
                        ?>
                        <div class="page-header">
                            <h2 class="page-title"><?php echo esc_attr( $rttk_portfolio_cat_name ); ?></h2>
                        </div>
                        <?php 
                        $term_description = term_description( $termID, 'portfolio_categories' ); ?>
                        <div class="parent-desc"><?php echo isset( $term_description ) ?  $term_description:'';?></div>
                        <?php
                        $rttk_portfolio_tax_post_args = array(
                            'post_type' => 'portfolio', // Your Post type Name that You Registered
                            'posts_per_page' => -1,
                            'order' => 'ASC',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'portfolio_categories',
                                    'field' => 'slug',
                                    'terms' => $rttk_portfolio_cat_slug,
                                    'include_children' => false
                                )
                            )
                        );
                        $rttk_portfolio_tax_post_qry = new WP_Query($rttk_portfolio_tax_post_args);
                        global $post;
                        if($rttk_portfolio_tax_post_qry->have_posts()) :
                            while($rttk_portfolio_tax_post_qry->have_posts()) :
                                $rttk_portfolio_tax_post_qry->the_post(); 
                                // Start the Loop.
                                // while ( have_posts() ) : the_post();
                                    /*
                                     * Include the Post-Format-specific template for the content.
                                     * If you want to override this in a child theme, then include a file
                                     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                     */
                            
                            $rttk_setting = get_post_meta( $post->ID,'rttk_setting',true );
                            ?>                            
                            
                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                <?php
                                    // Post thumbnail.
                                    the_post_thumbnail();
                                ?>

                                <header class="entry-header">
                                    <?php
                                        if ( is_single() ) :
                                            the_title( '<h1 class="entry-title">', '</h1>' );
                                        else :
                                            the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
                                        endif;
                                    ?>
                                </header><!-- .entry-header -->

                                <div class="entry-content">
                                    <?php
                                        /* translators: %s: Name of current post */
                                        the_content( sprintf(
                                            __( 'Continue reading %s', 'rara-theme-toolkit-pro' ),
                                            the_title( '<span class="screen-reader-text">', '</span>', false )
                                        ) );
                                    ?>
                                </div><!-- .entry-content -->

                                <?php
                                    // Author bio.
                                    if ( is_single() && get_the_author_meta( 'description' ) ) :
                                        do_action('rttk_portfolio_author_bio'); 
                                    endif;
                                ?>
                            </article><!-- #post-## -->
                            <?php
                        endwhile; endif;

                        foreach ($termchildren as $child) {
                            $term = get_term_by( 'id', $child, 'portfolio_categories' ); 
                            $term_link = get_term_link( $term );
                            $child_term_description = term_description( $term, 'portfolio_categories' ); ?>
                            <h2 class="child-title"><a href="<?php echo esc_url( $term_link );?>"><?php echo esc_attr( $term->name );?></a></h2>
                            <div class="child-desc"><?php echo isset( $child_term_description ) ?  $child_term_description:'';?></div>

                            <?php             
                            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                            $my_query = new WP_Query( array(
                                'post_type' => 'portfolio', 
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => $taxonomyName,
                                        'field' => 'slug',
                                        'terms' => $term->slug
                                    )
                                ),
                                'posts_per_page' => -1,
                                'orderby' => 'menu_order',
                                'order' => 'ASC',
                                'paged'=> $paged

                                )); 
                            while ($my_query->have_posts()) : $my_query->the_post();
                                global $post;
                                $rttk_setting = get_post_meta( $post->ID,'rttk_setting',true );
                                ?>
                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                <?php
                                    // Post thumbnail.
                                    the_post_thumbnail();
                                ?>

                                <header class="entry-header">
                                    <?php
                                        if ( is_single() ) :
                                            the_title( '<h1 class="entry-title">', '</h1>' );
                                        else :
                                            the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
                                        endif;
                                    ?>
                                </header><!-- .entry-header -->

                                <div class="entry-content">
                                    <?php
                                        /* translators: %s: Name of current post */
                                        the_content( sprintf(
                                            __( 'Continue reading %s', 'rara-theme-toolkit-pro' ),
                                            the_title( '<span class="screen-reader-text">', '</span>', false )
                                        ) );
                                    ?>
                                </div><!-- .entry-content -->

                                <?php
                                    // Author bio.
                                    if ( is_single() && get_the_author_meta( 'description' ) ) :
                                        do_action('rttk_portfolio_author_bio'); 
                                    endif;
                                ?>
                            </article><!-- #post-## -->
                        <?php
                         endwhile;
                        } 
                    }
                    else{
                        $rttk_portfolio_cat_slug = get_queried_object()->slug;
                        $rttk_portfolio_cat_name = get_queried_object()->name;
                        ?>
                        <div class="page-header">
                            <h2 class="page-title"><?php echo esc_attr( $rttk_portfolio_cat_name ); ?></h2>
                        </div>
                        <?php 
                            $term_description = term_description( $termID, 'portfolio_categories' ); ?>
                            <div class="parent-desc"><?php echo isset( $term_description ) ?  $term_description:'';?></div>
                        <?php
                        $rttk_portfolio_tax_post_args = array(
                            'post_type' => 'portfolio', // Your Post type Name that You Registered
                            'posts_per_page' => -1,
                            'order' => 'ASC',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'portfolio_categories',
                                    'field' => 'slug',
                                    'terms' => $rttk_portfolio_cat_slug
                                )
                            )
                        );
                        $rttk_portfolio_tax_post_qry = new WP_Query($rttk_portfolio_tax_post_args);
                        global $post;
                        if($rttk_portfolio_tax_post_qry->have_posts()) :
                            while($rttk_portfolio_tax_post_qry->have_posts()) :
                                $rttk_portfolio_tax_post_qry->the_post(); 
                                // Start the Loop.
                                // while ( have_posts() ) : the_post();
                                    /*
                                     * Include the Post-Format-specific template for the content.
                                     * If you want to override this in a child theme, then include a file
                                     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                     */
                            $rttk_setting = get_post_meta( $post->ID,'rttk_setting',true );
                            ?>
                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                <?php
                                    // Post thumbnail.
                                    the_post_thumbnail();
                                ?>

                                <header class="entry-header">
                                    <?php
                                        if ( is_single() ) :
                                            the_title( '<h1 class="entry-title">', '</h1>' );
                                        else :
                                            the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
                                        endif;
                                    ?>
                                </header><!-- .entry-header -->

                                <div class="entry-content">
                                    <?php
                                        /* translators: %s: Name of current post */
                                        the_content( sprintf(
                                            __( 'Continue reading %s', 'rara-theme-toolkit-pro' ),
                                            the_title( '<span class="screen-reader-text">', '</span>', false )
                                        ) );
                                    ?>
                                </div><!-- .entry-content -->

                                <?php
                                    // Author bio.
                                    if ( is_single() && get_the_author_meta( 'description' ) ) :
                                        do_action('rttk_portfolio_author_bio'); 
                                    endif;
                                ?>
                            </article><!-- #post-## -->
                        <?php
                        endwhile; endif;
                    } 
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php do_action('rttk_sidebar_action');?>
<?php get_footer(); ?>