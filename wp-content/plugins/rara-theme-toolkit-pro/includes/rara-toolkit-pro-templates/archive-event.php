<?php
   /**
    * The template for displaying archive page for events
    *
    * @package Rara_Theme_Toolkit_Pro
    * @subpackage Rara_Theme_Toolkit_Pro/includes/rara-toolkit-pro-templates
    * @since 1.0.0
    */
   	get_header(); $all_descriptions = (array) get_option( 'rrtk_descriptions' ); print_r($all_descriptions['event']);?>
      <div class="container">
        <div id="content" class="site-content">
            <div class="row">
                <div id="primary" class="content-area">
                    <div class="event-list-page">
                        <?php
		                global $post; 
		                $rttk_event_post_args = array(
		                    'post_type' => 'event', // Your Post type Name that You Registered
		                    'posts_per_page' => -1,
		                    'order' => 'ASC',
		                );
		                $rttk_event_post_qry = new WP_Query($rttk_event_post_args);

		                if($rttk_event_post_qry->have_posts()) :
		                   while($rttk_event_post_qry->have_posts()) :
		                        $rttk_event_post_qry->the_post(); 
		                        $event_img_size = apply_filters('event_img_size','thumbnail'); 
		                        $post_thumbnail_id = get_post_thumbnail_id( $post->ID );
		                        $src = wp_get_attachment_image_src( $post_thumbnail_id, $event_img_size );
		                        $rttk_setting = get_post_meta( $post->ID, '_rttk_setting', true );
                    	?>
                        <div class="event-holder">
                            <div class="img-holder">
                                <a href="<?php the_permalink();?>"><img src="<?php echo esc_url($src[0]);?>" alt=""></a>
                            </div>
                            <div class="text-holder">
                                <header class="entry-header">
                                    <h2 class="entry-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
                                    <div class="entry-meta">
                                        <span class="date"><?php echo isset($rttk_setting['event']['sdate']) ? esc_attr($rttk_setting['event']['sdate']): ''; ?> - <?php echo isset($rttk_setting['event']['edate']) ? esc_attr($rttk_setting['event']['edate']): ''; ?></span>
                                        <address><?php echo isset($rttk_setting['event']['venue']) ? esc_attr($rttk_setting['event']['venue']): ''; ?></address>
                                    </div>
                                </header>
                                <div class="entry-content">
                                    <?php the_excerpt();?>
                                </div>
                                <div class="footer">
                                    <a href="<?php the_permalink();?>" class="btn-more"><?php _e('Find out more','rara-theme-toolkit-pro');?></a>
                                </div>
                            </div>        
                        </div>
                    	<?php
                    	endwhile;
                    	endif;
                    	?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php do_action('rttk_sidebar_action');?>  
	<?php get_footer(); ?>
