<?php
  /**
    * The template for displaying all single testimonial and testimonial listing
    *
    * @package Rara_Theme_Toolkit_Pro
    * @subpackage Rara_Theme_Toolkit_Pro/includes/rara-toolkit-pro-templates
    * @since 1.0.0
    */
    get_header(); ?>    
    <div id="primary" class="content-area" style="width: 100%;">
        <main id="main" class="site-main">
            <article class="page testimonial-page">
                <div class="entry-content">
                    <?php do_action('rttk_testimonial_description');?>
                </div>
                <div class="testimonial-holder">
                    <?php
                    global $post; 
                    $rttk_testimonial_post_args = array(
                        'post_type' => 'testimonial', // Your Post type Name that You Registered
                        'posts_per_page' => -1,
                        'order' => 'ASC',
                    );
                    $rttk_testimonial_post_qry = new WP_Query($rttk_testimonial_post_args);

                    if($rttk_testimonial_post_qry->have_posts()) :
                       while($rttk_testimonial_post_qry->have_posts()) :
                            $rttk_testimonial_post_qry->the_post(); 
                            $testimonial_img_size = apply_filters('testimonial_img_size','thumbnail'); 
                            $post_thumbnail_id = get_post_thumbnail_id( $post->ID );
                            $src = wp_get_attachment_image_src( $post_thumbnail_id, $testimonial_img_size );
                            $rttk_setting = get_post_meta( $post->ID, '_rttk_setting', true );
                        ?>
                    <div class="testimonial-item">
                        <div class="img-holder">
                            <img src="<?php echo esc_url($src[0]);?>" alt="">
                            <strong class="name"><?php the_title();?></strong>
                            <span class="designation"><?php echo isset($rttk_setting['testimonial']['title']) ? esc_attr($rttk_setting['testimonial']['title']): ''; echo isset($rttk_setting['testimonial']['position']) ? esc_attr(', '.$rttk_setting['testimonial']['position']): ''; ?></span>
                        </div>
                        <div class="text-holder">
                            <div class="entry-content">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                    <?php
                    endwhile;
                    endif;
                    ?>
                </div>
            </article>
        </main>
    </div>
    <?php do_action('rttk_sidebar_action');?>
    <?php
    get_footer();
    ?>