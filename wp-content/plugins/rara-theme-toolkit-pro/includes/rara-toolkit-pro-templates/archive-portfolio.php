	<?php
	/**
    * The template for displaying all single portfolio and portfolio listing
    *
    * @package Rara_Theme_Toolkit_Pro
    * @subpackage Rara_Theme_Toolkit_Pro/includes/rara-toolkit-pro-templates
    * @since 1.0.0
    */
    get_header(); ?>    
	<div id="primary" class="content-area" style="width: 100%;">
        <main id="main" class="site-main">
            <article class="page course-list">
                <div class="entry-content">
                    <?php do_action('rttk_portfolio_description');?>
                </div>
				<div id="filters" class="button-group">
			  		
			  		<button class="btn btn-primary" data-filter="*">show all</button>
			  		<?php
					$args = array('orderby'=>'asc','hide_empty'=>true);
					$rttk_custom_terms = get_terms('portfolio_categories', $args);
					foreach($rttk_custom_terms as $term){
					    echo '<button class="btn btn-primary" data-filter=".'.$term->slug.'">'.$term->name.'</button>';
					}
			  		?>
			  			
				</div>
				<div class="container-fluid no-gutter">

				    <div id="posts" class="row">
				        <?php				
						$i=1;
						foreach($rttk_custom_terms as $term){ 
							$rttk_get_posts = get_posts(array(
							    'showposts' => -1,
							    'post_type' => 'portfolio',
							    'tax_query' => array(
							        array(
							        'taxonomy' => 'portfolio_categories',
							        'field' => 'slug',
							        'terms' => array($term->slug))
							    ))
							);
				        	foreach ($rttk_get_posts as $mypost) {?>
						        <div id="<?php echo $i; ?>" class="item <?php echo esc_attr($term->slug);?> col-sm-3">
						            <div class="item-wrap">
						            	<?php
						            		$portfolio_img_size = apply_filters('portfolio_img_size','thumbnail'); 
			                                $post_thumbnail_id = get_post_thumbnail_id( $mypost->ID );
			                                $src = wp_get_attachment_image_src( $post_thumbnail_id, $portfolio_img_size );
			                            ?>
						                <img class="img-responsive" src="<?php echo esc_url($src[0]);?>">
					                	<div class="text-holder">
											<div class="table">
												<div class="table-row">
													<div class="table-cell">
														<a href="<?php the_permalink();?>"><i class="fa fa-link" aria-hidden="true"></i></a>
													</div>
												</div>
											</div>
										</div>
						            </div>
						        </div>
						<?php 
							}
						$i++;
						} ?>
				    </div>
				</div>
			</article>
		</main>
	</div>
	<?php do_action('rttk_sidebar_action');?>
<?php get_footer(); ?>