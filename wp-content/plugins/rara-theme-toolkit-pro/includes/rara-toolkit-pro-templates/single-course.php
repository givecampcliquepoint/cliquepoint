<?php
   /**
    * The template for displaying all single course
    *
    * @package Rara_Theme_Toolkit_Pro
    * @subpackage Rara_Theme_Toolkit_Pro/includes/rara-toolkit-pro-templates
    * @since 1.0.0
    */
   
    get_header(); ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <article class="page course-detail-page">           
                <div class="container">
                    <?php do_action('rttk_course_before_image'); ?>
                    <div id="content" class="site-content">
                        <div class="row">
                            <ul id="class-detail-slider">
                                <?php
                                    global $post;
                                    $rttk_course_images = get_post_meta($post->ID, '_rttk_images_course_id', true);
                                    $rttk_setting = get_post_meta( $post->ID, '_rttk_setting', true );
                                    if( isset($rttk_course_images) && $rttk_course_images!='' ): ?>
                                                    <?php
                                                        foreach ($rttk_course_images as $image) { 
                                                            $course_img_size = apply_filters('course_image_size','full');
                                                            $link = wp_get_attachment_image_src($image, $course_img_size); 
                                                            echo '<li data-thumb="'.$link[0].'" data-src=""><img src="'.$link[0].'" alt=""></li>';
                                                        }
                                                    ?>                                            
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                    <?php do_action('rttk_course_after_image'); ?>
                    <header class="entry-header">
                        <h2 class="entry-title"><?php the_title();?></h2>
                        <!-- <div class="entry-meta">
                            <span class="byline">by <a href="#">venisha</a></span>
                            <span class="posted-on">on <a href="#"><time>April 20, 2016</time></a></span>
                        </div> -->
                    </header>
                    <div class="course-information">
                        <h3 class="title"><?php _e('Course Information','rara-theme-toolkit-pro');?></h3>
                        <ul class="information-list">
                            <li>
                                <strong><?php _e('Start Date:','rara-theme-toolkit-pro');?></strong>
                                <span><?php echo isset($rttk_setting['course']['sdate']) ? esc_attr($rttk_setting['course']['sdate']): ''; ?></span>
                            </li>
                            <li>
                                <strong><?php _e('Class Staff:','rara-theme-toolkit-pro');?></strong>
                                <span><?php echo isset($rttk_setting['course']['staff']) ? esc_attr($rttk_setting['course']['staff']): ''; ?></span>
                            </li>
                            <li>
                                <strong><?php _e('Class Size:','rara-theme-toolkit-pro');?></strong>
                                <span><?php echo isset($rttk_setting['course']['size']) ? esc_attr($rttk_setting['course']['size']): ''; ?></span>
                            </li>
                            <li>
                                <strong><?php _e('Transportation:','rara-theme-toolkit-pro');?></strong>
                                <span><?php echo isset($rttk_setting['course']['transportation']) ? esc_attr($rttk_setting['course']['transportation']): ''; ?></span>
                            </li>
                            <li>
                                <strong><?php _e('Years old:','rara-theme-toolkit-pro');?></strong>
                                <span><?php echo isset($rttk_setting['course']['old']) ? esc_attr($rttk_setting['course']['old']): ''; ?></span>
                            </li>
                            <li>
                                <strong><?php _e('Class Duration:','rara-theme-toolkit-pro');?></strong>
                                <span><?php echo isset($rttk_setting['course']['duration']) ? esc_attr($rttk_setting['course']['duration']): ''; ?></span>
                            </li>
                        </ul>
                    </div>
                    <div class="entry-content">
                        <?php  
                        $content = $post->post_content;
                        echo $content; ?>
                        <a href="<?php echo isset($rttk_setting['course']['join']) ? esc_url($rttk_setting['course']['join']): '#'; ?>" class="btn-join">
                            <?php 
                            $join_now_txt = __('Join Now','rara-theme-toolkit-pro'); 
                            $join_now_label = apply_filters( 'course_join_now_label', $join_now_txt ); 
                            echo $join_now_label;
                            ?>
                        </a>
                    </div>
                </div>
            </article>
        </main>
    </div>
    <?php do_action('rttk_sidebar_action');?>
    <?php get_footer(); ?>