<?php
  /**
    * The template for displaying all single team and team listing
    *
    * @package Rara_Theme_Toolkit_Pro
    * @subpackage Rara_Theme_Toolkit_Pro/includes/rara-toolkit-pro-templates
    * @since 1.0.0
    */
    get_header(); ?>    
    <div id="primary" class="content-area" style="width: 100%;">
        <main id="main" class="site-main">
            <article class="page team-page">
                <div class="entry-content">
                    <?php do_action('rttk_team_description');?>
                </div>
                <div class="team-holder">
                    <?php
                        global $post; 
                        $rttk_team_post_args = array(
                            'post_type' => 'team', // Your Post type Name that You Registered
                            'posts_per_page' => -1,
                            'order' => 'ASC',
                        );
                        $rttk_team_post_qry = new WP_Query($rttk_team_post_args);

                        if($rttk_team_post_qry->have_posts()) :
                           while($rttk_team_post_qry->have_posts()) :
                                $rttk_team_post_qry->the_post(); 
                                $team_img_size = apply_filters('team_img_size','thumbnail'); 
                                $post_thumbnail_id = get_post_thumbnail_id( $post->ID );
                                $src = wp_get_attachment_image_src( $post_thumbnail_id, $team_img_size );
                                $rttk_setting = get_post_meta( $post->ID, '_rttk_setting', true );
                            ?>
                                <div class="single-team">
                                    <div class="team">
                                        <div class="row">
                                            <div class="col large">
                                                <div class="image-holder">
                                                    <?php
                                                        $team_img_size = apply_filters('archive_team_img_size','thumbnail'); 
                                                        $post_thumbnail_id = get_post_thumbnail_id( $post->ID );
                                                        $src = wp_get_attachment_image_src( $post_thumbnail_id, $team_img_size );
                                                        $rttk_setting = get_post_meta( $post->ID, '_rttk_setting', true );
                                                    ?>
                                                    <img src="<?php echo esc_url($src[0]); ?>" alt="">
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="top">
                                                    <h2 class="name"><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
                                                    <span class="designation"><?php echo isset($rttk_setting['team']['position']) ? esc_attr($rttk_setting['team']['position']): ''; ?></span>
                                                </div>
                                                <div class="team-info">
                                                    <span><a href="mailto:<?php echo isset($rttk_setting['team']['email']) ? esc_attr($rttk_setting['team']['email']): ''; ?>"><?php echo isset($rttk_setting['team']['email']) ? esc_attr($rttk_setting['team']['email']): ''; ?></a></span>
                                                    <span><?php $rttk_tel_text = __(', Tel: ','rara-theme-toolkit-pro' );  echo apply_filters('rttk_tel_text', $rttk_tel_text); ?><a href="tel:<?php echo isset($rttk_setting['team']['telephone']) ? esc_attr($rttk_setting['team']['telephone']): '#'; ?>"><?php echo isset($rttk_setting['team']['telephone']) ? esc_attr($rttk_setting['team']['telephone']): '-'; ?></a></span>
                                                    <ul class="rttk-team-sortable-icons">
                                                        <?php
                                                        $obj = new Rara_Theme_Toolkit_Pro_Functions;
                                                        if(isset($rttk_setting['team']['social']))
                                                        {
                                                            $icons  = $rttk_setting['team']['social'];
                                                            $arr_keys  = array_keys( $icons );
                                                            foreach ($arr_keys as $key => $value)
                                                            { 
                                                                if ( array_key_exists( $value,$rttk_setting['team']['social'] ) )
                                                                { 
                                                                    $icon = $obj->rttk_get_team_social_icon_name( $rttk_setting['team']['social'][$value] );
                                                                    ?>
                                                                    <li class="rttk-social-icon-wrap">
                                                                            <a href="<?php echo esc_url($rttk_setting['team']['social'][$value]);?>" target="_blank"><span class="rttk-social-icons-field-handle fontawesome fa-<?php echo esc_attr( $icon )?>" style="font-family: 'FontAwesome'"></span></a>
                                                                    </li>
                                                                <?php
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </ul>
                                                </div>
                                                <?php do_action('rttk_team_member_specialization_description'); ?>
                                            </div>
                                        </div>
                                        <div class="text-holder">
                                            <?php 
                                            $content = $post->post_content;
                                            echo $content;
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            <?php
                        endwhile;
                        endif;
                    ?>
                </div>
            </article>
        </main>
    </div>
    <?php do_action('rttk_sidebar_action');?>
    <?php get_footer(); ?>