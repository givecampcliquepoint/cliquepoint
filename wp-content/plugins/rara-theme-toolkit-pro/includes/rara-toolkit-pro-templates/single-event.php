<?php
   /**
    * The template for displaying all single events
    *
    * @package Rara_Theme_Toolkit_Pro
    * @subpackage Rara_Theme_Toolkit_Pro/includes/rara-toolkit-pro-templates
    * @since 1.0.0
    */
    get_header(); ?>
    <div id="primary" class="content-area">
    	<main id="main" class="site-main" role="main">
        	<article class="page event-detail-page">
                <?php do_action('rttk_event_before_image'); ?>
                <div class="holder">
                	<?php
                		global $post; 
                		$event_img_size = apply_filters('event_img_size','thumbnail'); 
 						$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
                		$src = wp_get_attachment_image_src( $post_thumbnail_id, $event_img_size );
						$rttk_setting = get_post_meta( $post->ID, '_rttk_setting', true );
                	?>
                    <div class="post-thumbnail">
                    	<img src="<?php echo esc_url($src[0]);?>" alt="">
                    </div>
                    <?php do_action('rttk_event_before_image'); ?>
                    <div class="event-info">
                        <div class="col">
                            <div class="date"><?php echo isset($rttk_setting['event']['sdate']) ? esc_attr($rttk_setting['event']['sdate']): ''; ?> - <?php echo isset($rttk_setting['event']['edate']) ? esc_attr($rttk_setting['event']['edate']): ''; ?></div>
                        </div>
                        <div class="col">
                            <address><?php echo isset($rttk_setting['event']['address']) ? esc_attr($rttk_setting['event']['address']): ''; ?></address>
                        </div>
                        <div class="col">
                            <div class="time"><?php echo isset($rttk_setting['event']['time']) ? esc_attr($rttk_setting['event']['time']): ''; ?></div>
                        </div>
                    </div>
                </div>
                <div class="detail-info">
                    <!-- <h3 class="title">Course Information</h3> -->
                    <div class="col">
                        <h3 class="title"><?php _e('Detail','rara-theme-toolkit-pro');?></h3>
                        <ul class="information-list">
                            <li>
                                <strong><?php _e('Start Date','rara-theme-toolkit-pro');?></strong>
                                <span><?php echo isset($rttk_setting['event']['sdate']) ? esc_attr($rttk_setting['event']['sdate']): ''; ?> <?php echo isset($rttk_setting['event']['stime']) ? esc_attr($rttk_setting['event']['stime']): ''; ?></span>
                            </li>
                            <li>
                                <strong><?php _e('End:','rara-theme-toolkit-pro');?></strong>
                                <span><?php echo isset($rttk_setting['event']['edate']) ? esc_attr($rttk_setting['event']['edate']): ''; ?> <?php echo isset($rttk_setting['event']['etime']) ? esc_attr($rttk_setting['event']['etime']): ''; ?> </span>
                            </li>
                            <li>
                                <strong><?php _e('Cost:','rara-theme-toolkit-pro');?></strong>
                                <span><?php echo isset($rttk_setting['event']['cost']) ? esc_attr($rttk_setting['event']['cost']): ''; ?></span>
                            </li>
                            <li>
                                <strong><?php _e('Event Category:','rara-theme-toolkit-pro');?></strong>
                                <?php
                                $cats = get_the_terms($post->ID,'event_categories');
                                if ($cats!='' && array_filter($cats))
                                {
                                    foreach ($cats as $cat) { ?>
                                        <span><a href="<?php echo get_term_link($cat->term_id,'event_categories'); ?>"><?php echo esc_attr($cat->name);?></a></span>
                                <?php
                                    }
                                }
                                ?>
                            </li>
                        </ul>
                    </div>
                    <div class="col">
                        <h3 class="title"><?php _e('Organizer','rara-theme-toolkit-pro');?></h3>
                        <ul class="information-list">
                            <li>
                                <strong><?php echo isset($rttk_setting['event']['organizer']) ? esc_attr($rttk_setting['event']['organizer']): ''; ?></strong>
                            </li>
                            <li>
                                <strong><?php _e('Phone','rara-theme-toolkit-pro');?></strong>
                                <span><?php echo isset($rttk_setting['event']['phone']) ? esc_attr($rttk_setting['event']['phone']): ''; ?></span>
                            </li>
                            <li>
                                <strong><?php _e('Email','rara-theme-toolkit-pro');?></strong>
                                <span><?php echo isset($rttk_setting['event']['email']) ? esc_attr($rttk_setting['event']['email']): ''; ?></span>
                            </li>
                            <li>
                                <strong><?php _e('Website','rara-theme-toolkit-pro');?></strong>
                                <span><?php echo isset($rttk_setting['event']['website']) ? esc_attr($rttk_setting['event']['website']): ''; ?></span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="entry-content">
                    <?php 
                    $content = $post->post_content;
                    echo $content;
                    ?>
                </div>
                <div class="venue">
                    <?php
                    $obj = new Rara_Theme_Toolkit_Pro_Functions;
                    ?>
                    <div class="map-holder"><?php echo $obj->rrtk_sanitize_iframe($rttk_setting['event']['map']);?></div>
                    <div class="text-holder">
                        <h3 class="title"><?php _e('Venue','rara-theme-toolkit-pro');?></h3>
                        <address>
                            <?php echo isset($rttk_setting['event']['venue']) ? esc_attr($rttk_setting['event']['venue']): ''; ?>   
                        </address>
                    </div>
                </div>
                <div class="btn-holder">
                    <?php do_action('rrtk_third_party_button_holder'); ?>
                </div>
            </article>
    	</main>
    </div>
    <?php do_action('rttk_sidebar_action');?>
    <?php get_footer(); ?>