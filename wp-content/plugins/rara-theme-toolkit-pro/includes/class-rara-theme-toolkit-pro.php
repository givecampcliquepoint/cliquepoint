<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://raratheme.com
 * @since      1.0.0
 *
 * @package    Rara_Theme_Toolkit_Pro
 * @subpackage Rara_Theme_Toolkit_Pro/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Rara_Theme_Toolkit_Pro
 * @subpackage Rara_Theme_Toolkit_Pro/includes
 * @author     Rara Theme <test@testmail.com>
 */
class Rara_Theme_Toolkit_Pro {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Rara_Theme_Toolkit_Pro_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'rara-theme-toolkit-pro';
		$this->version = '1.0.0';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Rara_Theme_Toolkit_Pro_Loader. Orchestrates the hooks of the plugin.
	 * - Rara_Theme_Toolkit_Pro_i18n. Defines internationalization functionality.
	 * - Rara_Theme_Toolkit_Pro_Admin. Defines all hooks for the admin area.
	 * - Rara_Theme_Toolkit_Pro_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-rara-theme-toolkit-pro-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-rara-theme-toolkit-pro-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-rara-theme-toolkit-pro-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-rara-theme-toolkit-pro-public.php';

		/**
		 * The class responsible for defining all basic functions.
		 */
		require_once RTTKPRO_BASE_PATH . '/includes/class-rara-theme-toolkit-pro-functions.php';

		/**
		 * The class responsible for defining all templates.
		 */
		require_once RTTKPRO_BASE_PATH . '/includes/class-rara-theme-toolkit-template-loader.php';

		/**
		 * Recent Post Widget
		 */
		require RTTKPRO_BASE_PATH . '/includes/widgets/widget-recent-post.php';

		/**
		 * Author Post Widget
		 */
		require RTTKPRO_BASE_PATH . '/includes/widgets/widget-author-post.php';

		/**
		 * Category Post Widget
		 */
		require RTTKPRO_BASE_PATH . '/includes/widgets/widget-cat-post.php';

		/**
		 * Contact Widget
		 */
		require RTTKPRO_BASE_PATH . '/includes/widgets/widget-contact.php';

		/**
		 * CTA Widget
		 */
		require RTTKPRO_BASE_PATH . '/includes/widgets/widget-cta.php';

		/**
		 * Facebook Page Widget
		 */
		require RTTKPRO_BASE_PATH . '/includes/widgets/widget-facebook-page.php';

		/**
		 * Featured Post Widget
		 */
		require RTTKPRO_BASE_PATH . '/includes/widgets/widget-featured-post.php';

		/**
		 * Flickr Widget
		 */
		require RTTKPRO_BASE_PATH . '/includes/widgets/widget-flickr.php';

		/**
		 * Icon Text Widget
		 */
		require RTTKPRO_BASE_PATH . '/includes/widgets/widget-icon-text.php';

		/**
		 * Instagram Widget
		 */
		require RTTKPRO_BASE_PATH . '/includes/widgets/widget-instagram.php';

		/**
		 * Popular Post Widget
		 */
		require RTTKPRO_BASE_PATH . '/includes/widgets/widget-popular-post.php';

		/**
		 * Social Link Widget
		 */
		require RTTKPRO_BASE_PATH . '/includes/widgets/widget-social-links.php';

		/**
		 * Stat Counter Widget
		 */
		require RTTKPRO_BASE_PATH . '/includes/widgets/widget-stat-counter.php';

		/**
		 * Twitter Widget
		 */
		require RTTKPRO_BASE_PATH . '/includes/widgets/widget-twitter-feeds.php';

		/**
		 * Ad Widget
		 */
		require RTTKPRO_BASE_PATH . '/includes/widgets/widget-ad.php';

		/**
		 * Pricing Table Widget
		 */
		require RTTKPRO_BASE_PATH . '/includes/widgets/widget-pricing-table.php';

		/**
		 * Author-bio Widget
		 */
		require RTTKPRO_BASE_PATH . '/includes/widgets/widget-author-bio.php';

		/**
		* Gallery for courses
		*/
		include RTTKPRO_BASE_PATH.'/includes/meta-parts/course-gallery.php';

		/**
		 * Shortcodes
		 */
		require RTTKPRO_BASE_PATH . '/includes/shortcodes.php';

		$this->loader = new Rara_Theme_Toolkit_Pro_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Rara_Theme_Toolkit_Pro_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Rara_Theme_Toolkit_Pro_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Rara_Theme_Toolkit_Pro_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
  		$this->loader->add_action( 'init',  $plugin_admin, 'rrtk_register_post_types' );
		$this->loader->add_action( 'init',  $plugin_admin,'rttk_create_post_type_taxonomies', 0 );
  		$this->loader->add_action( 'admin_footer',  $plugin_admin, 'rara_theme_tool_kit_team_social_template' );
		$this->loader->add_filter( 'mce_external_plugins', $plugin_admin, 'rara_theme_toolkit_pro_add_tinymce_plugin' );
  		$this->loader->add_action( 'admin_init',  $plugin_admin, 'rara_theme_tool_kit_test' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'rrtk_enable_pages' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'rrtk_register_settings' );
		$this->loader->add_filter( 'option_page_capability_rrtk_descriptions', $plugin_admin, 'rrtk_allow_edit_posts' );
		$this->loader->add_action( 'admin_bar_menu', $plugin_admin, 'rrtk_admin_bar_links',  100);
		$this->loader->add_filter( 'get_the_archive_description', $plugin_admin, 'rrtk_archive_description' );
		if ( ! defined( 'QTX_VERSION' ) ) {
			$this->loader->add_filter( 'rrtk_wp_editor_settings', $plugin_admin, 'rrtk_qtranslate_editor_args' );
			$this->loader->add_filter( 'qtranslate_load_admin_page_config', $plugin_admin, 'rrtk_qtranslate_support', 99); // 99 priority is important, loaded after registered post types
		}
		$this->loader->add_action( 'add_meta_boxes', $plugin_admin, 'rttk_create_boxes' );
		$this->loader->add_action( 'save_post', $plugin_admin, 'rttk_save_meta_box_data' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Rara_Theme_Toolkit_Pro_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Rara_Theme_Toolkit_Pro_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
