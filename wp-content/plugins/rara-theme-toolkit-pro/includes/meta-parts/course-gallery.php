<?php
/*
 * @author    Daan Vos de Wael
 * @copyright Copyright (c) 2013, Daan Vos de Wael, http://www.daanvosdewael.com
 * @license   http://en.wikipedia.org/wiki/MIT_License The MIT License
*/
  function rttk_add_course_metabox($post_type) {
    $types = array('course');

    if (in_array($post_type, $types)) {
      add_meta_box(
        'course-metabox',
        'Course Slider Images',
        'rttk_course_meta_callback',
        $post_type,
        'normal',
        'high'
      );
    }
  }
  add_action('add_meta_boxes', 'rttk_add_course_metabox');

  function rttk_course_meta_callback($post) {
    wp_nonce_field( basename(__FILE__), 'course_meta_nonce' );
    $ids = get_post_meta($post->ID, '_rttk_images_course_id', true);
    ?>
    <table class="form-table">
      <tr><td>
        <a class="course-add button" href="#" data-uploader-title="Add image(s) to course" data-uploader-button-text="Add image(s)">Add image(s)</a>

        <ul id="course-metabox-list">
        <?php if ($ids) : foreach ($ids as $key => $value) : $image = wp_get_attachment_image_src($value); ?>

          <li>
            <input type="hidden" name="_rttk_images_course_id[<?php echo $key; ?>]" value="<?php echo $value; ?>">
            <img class="image-preview" src="<?php echo $image[0]; ?>">
            <a class="change-image button button-small" href="#" data-uploader-title="Change image" data-uploader-button-text="Change image">Change image</a><br>
            <small><a class="remove-image" href="#">Remove image</a></small>
          </li>

        <?php endforeach; endif; ?>
        </ul>

      </td></tr>
    </table>
  <?php }

  function rttk_course_meta_save($post_id) {
    // print_r($_POST['rttk_images_course_id']);
    // die;
    if (!isset($_POST['course_meta_nonce']) || !wp_verify_nonce($_POST['course_meta_nonce'], basename(__FILE__))) return;

    if (!current_user_can('edit_post', $post_id)) return;

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

    if(isset($_POST['_rttk_images_course_id'])) {
      update_post_meta($post_id, '_rttk_images_course_id', $_POST['_rttk_images_course_id']);
    } else {
      delete_post_meta($post_id, '_rttk_images_course_id');
    }
  }
  add_action('save_post', 'rttk_course_meta_save');
?>