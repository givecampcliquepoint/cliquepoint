<?php
global $post;
$rttk_setting = get_post_meta( $post->ID, '_rttk_setting', true );
    /**
    * Get event post type fields
    *
    * @return array of default fields
    */
    function rttk_get_event_fields_array() {

        $fields = array(
            'sdate' => 
                    array( 
                        'name'          =>'Start Date',
                        'key'           =>'sdate',
                        'class'         =>'date-cpt',    
                        'id'            =>'sdate',
                        'type'          =>'text'
                        ),
            'edate' => 
                    array( 
                        'name'          =>'End Date',
                        'key'           =>'edate',
                        'class'         =>'date-cpt',    
                        'id'            =>'edate',
                        'type'          =>'text'
                        ),
            'cost' => 
                    array( 
                        'name'          =>'Cost',
                        'key'           =>'cost',
                        'class'         =>'',    
                        'id'            =>'staff',
                        'type'          =>'text'
                        ),
            'organizer' => 
                    array( 
                        'name'          =>'Organizer',
                        'key'           =>'organizer',
                        'class'         =>'',    
                        'id'            =>'organizer',
                        'type'          =>'text'
                        ),
            'phone' => 
                    array( 
                        'name'          =>'Phone',
                        'key'           =>'phone',
                        'class'         =>'',    
                        'id'            =>'phone',
                        'type'          =>'text'
                        ),
            'email' => 
                    array( 
                        'name'          =>'Email',
                        'key'           =>'email',
                        'class'         =>'',    
                        'id'            =>'email',
                        'type'          =>'email'
                        ),
            'website' => 
                    array( 
                        'name'          =>'Website',
                        'key'           =>'website',
                        'class'         =>'',    
                        'id'            =>'website',
                        'type'          =>'text'
                        ),
            'stime' => 
                    array( 
                        'name'          =>'Starting Time',
                        'key'           =>'stime',
                        'class'         =>'time-cpt',    
                        'id'            =>'stime',
                        'type'          =>'text'
                        ),
            'etime' => 
                    array( 
                        'name'          =>'Ending Time',
                        'key'           =>'etime',
                        'class'         =>'time-cpt',    
                        'id'            =>'etime',
                        'type'          =>'text'
                        ),
            'address' => 
                    array( 
                        'name'          =>'Address',
                        'key'           =>'address',
                        'class'         =>'',    
                        'id'            =>'address',
                        'type'          =>'text'
                        ),
            'time' => 
                    array( 
                        'name'          =>'Time',
                        'key'           =>'time',
                        'class'         =>'',    
                        'id'            =>'time',
                        'type'          =>'text'
                        ),
            'venue' => 
                    array( 
                        'name'          =>'Venue',
                        'key'           =>'venue',
                        'class'         =>'',    
                        'id'            =>'venue',
                        'type'          =>'text'
                        ),
            'map' => 
                    array( 
                        'name'          =>'Map Iframe',
                        'key'           =>'map',
                        'class'         =>'',    
                        'id'            =>'map',
                        'type'          =>'text'
                        ),
            );
        $fields = apply_filters( 'rttk_get_event_fields_array', $fields );
        return $fields;
    }
    $event_fields = rttk_get_event_fields_array();
    foreach ($event_fields as $key => $value) { ?>
        <div class="event-info">
            <label for="<?php echo $key;?>"><?php _e($value['name'].':','rara-theme-toolkit-pro');?></label>
            <input type="<?php echo $value['type'];?>" class="<?php echo $value['class'];?>" name="rttk_setting[event][<?php echo $key;?>]" id="<?php echo $value['id'];?>" value="<?php echo isset($rttk_setting['event'][$key]) ? esc_attr($rttk_setting['event'][$key]): ''; ?>">
        </div>
    <?php
    }
    ?>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('.time-cpt').timepicker({ 'timeFormat': 'H:i:s'});
    });
</script>