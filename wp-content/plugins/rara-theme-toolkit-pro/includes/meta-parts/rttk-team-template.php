<?php
	global $post;
	$rttk_setting = get_post_meta( $post->ID, '_rttk_setting', true );

  /**
    * Get team post type fields
    *
    * @return array of default fields
    */
    function rttk_get_team_fields_array() {

        $fields = array(
            'position' => 
                    array( 
                        'name'          =>'Position',
                        'key'           =>'position',
                        'class'         =>'',    
                        'id'            =>'position',
                        'type'          =>'text'
                        ),
            'email' => 
                    array( 
                        'name'          =>'Email',
                        'key'           =>'email',
                        'class'         =>'',    
                        'id'            =>'email',
                        'type'          =>'email'
                        ),
            'telephone' => 
                    array( 
                        'name'          =>'Telephone',
                        'key'           =>'telephone',
                        'class'         =>'',    
                        'id'            =>'telephone',
                        'type'          =>'tel'
                        ),
            );
        $fields = apply_filters( 'rttk_get_team_fields_array', $fields );
        return $fields;
    }
$team_fields = rttk_get_team_fields_array();
foreach ($team_fields as $key => $value) { ?>
    <div class="team-info">
        <label for="<?php echo $key;?>"><?php _e($value['name'].':','rara-theme-toolkit-pro');?></label>
        <input type="<?php echo $value['type'];?>" class="<?php echo $value['class'];?>" name="rttk_setting[team][<?php echo $key;?>]" id="<?php echo $value['id'];?>" value="<?php echo isset($rttk_setting['team'][$key]) ? esc_attr($rttk_setting['team'][$key]): ''; ?>">
    </div>
<?php
}
?>

<ul class="rttk-team-sortable-icons">
	<?php
    $obj = new Rara_Theme_Toolkit_Pro_Functions;
	if(isset($rttk_setting['team']['social']))
	{
	    $icons  = $rttk_setting['team']['social'];
	    $arr_keys  = array_keys( $icons );
	    foreach ($arr_keys as $key => $value)
	    { 
	        if ( array_key_exists( $value,$rttk_setting['team']['social'] ) )
	        { 
	            $icon = $obj->rttk_get_team_social_icon_name( $rttk_setting['team']['social'][$value] );
	            ?>
	            <li class="rttk-social-icon-wrap">
	                <p>
	                    <span class="rttk-social-icons-field-handle fontawesome fa-<?php echo esc_attr( $icon )?>" style="font-family: 'FontAwesome'"></span>
	                    <input class="team-social-length" name="rttk_setting[team][social][<?php echo esc_attr($value);?>]" type="text" value="<?php echo esc_url($rttk_setting['team']['social'][$value]);?>" />
	                    <span class="del-team-icon dashicons-no" style="font-family: 'dashicons'"></span>
	                </p>
	            </li>
	        <?php
	        }
	    }
	}
	?>
    <div class="rttk-social-icon-holder"></div>
</ul>
<input class="rttk-team-social-add button-secondary" type="button" value="<?php _e('Add Icon','rara-theme-toolkit-pro');?>">
<span class="widget-note"><?php _e('Click the above button to add social icons. You can sort them as well.','rara-theme-toolkit-pro');?></span>

