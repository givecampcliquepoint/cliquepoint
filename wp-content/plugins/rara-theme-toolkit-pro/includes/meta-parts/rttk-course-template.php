<?php
    global $post;
    $rttk_setting = get_post_meta( $post->ID, '_rttk_setting', true );
    /**
    * Get course post type fields
    *
    * @return array of default fields
    */
    function rttk_get_course_fields_array() {

        $fields = array(
            'sdate' => 
                    array( 
                        'name'          =>'Start Date',
                        'key'           =>'sdate',
                        'class'         =>'date-cpt',    
                        'id'            =>'sdate',
                        'type'          =>'text'
                        ),
            'staff' => 
                    array( 
                        'name'          =>'Class Staff',
                        'key'           =>'staff',
                        'class'         =>'',    
                        'id'            =>'staff',
                        'type'          =>'text'
                        ),
            'size' => 
                    array( 
                        'name'          =>'Class Size',
                        'key'           =>'size',
                        'class'         =>'',    
                        'id'            =>'size',
                        'type'          =>'text'
                        ),
            'transportation' => 
                    array( 
                        'name'          =>'Transportation',
                        'key'           =>'transportation',
                        'class'         =>'',    
                        'id'            =>'transportation',
                        'type'          =>'text'
                        ),
            'old' => 
                    array( 
                        'name'          =>'Years Old',
                        'key'           =>'old',
                        'class'         =>'',    
                        'id'            =>'old',
                        'type'          =>'text'
                        ),
            'duration' => 
                    array( 
                        'name'          =>'Class Duration',
                        'key'           =>'duration',
                        'class'         =>'',    
                        'id'            =>'duration',
                        'type'          =>'text'
                        ),
            'join' => 
                    array( 
                        'name'          =>'Join Now Link',
                        'key'           =>'join',
                        'class'         =>'',    
                        'id'            =>'join',
                        'type'          =>'text'
                        ),
            );
        $fields = apply_filters( 'rttk_get_course_fields_array', $fields );
        return $fields;
    }
$course_fields = rttk_get_course_fields_array();
foreach ($course_fields as $key => $value) { ?>
    <div class="course-info">
        <label for="<?php echo $key;?>"><?php _e($value['name'].':','rara-theme-toolkit-pro');?></label>
        <input type="<?php echo $value['type'];?>" class="<?php echo $value['class'];?>" name="rttk_setting[course][<?php echo $key;?>]" id="<?php echo $value['id'];?>" value="<?php echo isset($rttk_setting['course'][$key]) ? esc_attr($rttk_setting['course'][$key]): ''; ?>">
    </div>
<?php
}