<?php

/**
* 
*/
class Rewrite
{
// register_deactivation_hook( __FILE__, 'flush_rewrite_rules' );
// register_activation_hook( __FILE__, 'myplugin_flush_rewrites' );
function __construct() {
	// call your CPT registration function here (it should also be hooked into 'init')
	add_action( 'init', array( $this, 'rrtk_register_post_types' ) );
	add_action( 'init', array( $this, 'rttk_create_post_type_taxonomies', 0 ) );
	flush_rewrite_rules();
}


  /**
	* Get post types for templates
	*
	* @return array of default settings
	*/
	function rttk_get_posttype_array() {

		$posts = array(
			'course' => array( 
						'singular_name'			=> 'Course',
						'general_name'			=> 'Courses',
						'dashicon'				=> 'dashicons-list-view',
						'taxonomy'				=> 'course_categories',
						'taxonomy_slug'			=> 'courses',
						'has_archive'           => true,		
						'exclude_from_search'   => false,
						'show_in_nav_menus'		=> true,
						'supports' 			 	=> array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
						),
			'event' => array( 
						'singular_name'			=> 'Event',
						'general_name'			=> 'Events',
						'dashicon'				=> 'dashicons-calendar',
						'taxonomy'				=> 'event_categories',
						'taxonomy_slug'			=> 'events',
						'has_archive'           => true,		
						'exclude_from_search'   => false,
						'show_in_nav_menus'		=> true,
						'supports' 			 	=> array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
						),
			'portfolio' => array( 
						'singular_name'			=> 'Portfolio',
						'general_name'			=> 'Portfolios',
						'dashicon'				=> 'dashicons-portfolio',
						'taxonomy'				=> 'portfolio_categories',
						'taxonomy_slug'			=> 'portfolios',
						'has_archive'           => true,		
						'exclude_from_search'   => false,
						'show_in_nav_menus'		=> true,
						'supports' 			 	=> array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
						),
			'team' => array( 
						'singular_name'			=> 'Team Member',
						'general_name'			=> 'Team Members',
						'dashicon'	    		=> 'dashicons-groups',
						'has_archive'           => true,		
						'exclude_from_search'   => false,
						'show_in_nav_menus'		=> true,
						'supports' 			 	=> array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
						),
			'testimonial' => array( 
						'singular_name'			=>'Testimonial',
						'general_name'			=>'Testimonials',
						'dashicon'				=>'dashicons-testimonial',
						'has_archive'           => true,		
						'exclude_from_search'   => false,
						'show_in_nav_menus'		=> true,
						'supports' 			 	=> array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
						),
			);
		$posts = apply_filters( 'rttk_get_posttype_array', $posts );
		return $posts;
	}

	/**
	 * Register post types.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/register_post_type
	 */
	function rrtk_register_post_types() {
		$myarray = $this->rttk_get_posttype_array();
		foreach ($myarray as $key => $value) {
			$labels = array(
		'name'                  => _x( $value['general_name'], 'Post Type General Name', 'rara-theme-toolkit-pro' ),
		'singular_name'         => _x( $value['singular_name'], 'Post Type Singular Name', 'rara-theme-toolkit-pro' ),
		'menu_name'             => __( $value['general_name'], 'rara-theme-toolkit-pro' ),
		'name_admin_bar'        => __( $value['singular_name'], 'rara-theme-toolkit-pro' ),
		'archives'              => __( $value['singular_name'].' Archives', 'rara-theme-toolkit-pro' ),
		'attributes'            => __( $value['singular_name'].' Attributes', 'rara-theme-toolkit-pro' ),
		'parent_item_colon'     => __( 'Parent '. $value['singular_name'].':', 'rara-theme-toolkit-pro' ),
		'all_items'             => __( 'All '. $value['general_name'], 'rara-theme-toolkit-pro' ),
		'add_new_item'          => __( 'Add New '. $value['singular_name'], 'rara-theme-toolkit-pro' ),
		'add_new'               => __( 'Add New', 'rara-theme-toolkit-pro' ),
		'new_item'              => __( 'New '. $value['singular_name'], 'rara-theme-toolkit-pro' ),
		'edit_item'             => __( 'Edit '. $value['singular_name'], 'rara-theme-toolkit-pro' ),
		'update_item'           => __( 'Update '. $value['singular_name'], 'rara-theme-toolkit-pro' ),
		'view_item'             => __( 'View '. $value['singular_name'], 'rara-theme-toolkit-pro' ),
		'view_items'            => __( 'View '. $value['general_name'], 'rara-theme-toolkit-pro' ),
		'search_items'          => __( 'Search '. $value['singular_name'], 'rara-theme-toolkit-pro' ),
		'not_found'             => __( 'Not found', 'rara-theme-toolkit-pro' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'rara-theme-toolkit-pro' ),
		'featured_image'        => __( 'Featured Image', 'rara-theme-toolkit-pro' ),
		'set_featured_image'    => __( 'Set featured image', 'rara-theme-toolkit-pro' ),
		'remove_featured_image' => __( 'Remove featured image', 'rara-theme-toolkit-pro' ),
		'use_featured_image'    => __( 'Use as featured image', 'rara-theme-toolkit-pro' ),
		'insert_into_item'      => __( 'Insert into '.$value['singular_name'], 'rara-theme-toolkit-pro' ),
		'uploaded_to_this_item' => __( 'Uploaded to this '.$value['singular_name'], 'rara-theme-toolkit-pro' ),
		'items_list'            => __( $value['general_name'] .' list', 'rara-theme-toolkit-pro' ),
		'items_list_navigation' => __( $value['general_name'] .' list navigation', 'rara-theme-toolkit-pro' ),
		'filter_items_list'     => __( 'Filter '. $value['general_name'] .'list', 'rara-theme-toolkit-pro' ),
	);
	$args = array(
		'label'                 => __( $value['singular_name'].'', 'rara-theme-toolkit-pro' ),
		'description'           => __( $value['singular_name'].' Post Type', 'rara-theme-toolkit-pro' ),
		'labels'                => $labels,
		'supports'              =>  $value['supports'],
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_icon'             => $value['dashicon'],
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => $value['show_in_nav_menus'],
		'can_export'            => true,
		'has_archive'           => $value['has_archive'],		
		'exclude_from_search'   => $value['exclude_from_search'],
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
		register_post_type( $key, $args );
		}
	}

	/**
	 * Register a taxonomy, post_types_categories for the post types.
	 *
	 * @link https://codex.wordpress.org/Function_Reference/register_taxonomy
	 */
	function rttk_create_post_type_taxonomies() {
		// Add new taxonomy, make it hierarchical
		$myarray = $this->rttk_get_posttype_array();
		foreach ($myarray as $key => $value) {
			if(isset($value['taxonomy']))
			{
				$labels = array(
					'name'              => _x( $value['singular_name'].' Categories', 'taxonomy general name', 'rara-theme-toolkit-pro' ),
					'singular_name'     => _x( $value['singular_name'].' Categories', 'taxonomy singular name', 'rara-theme-toolkit-pro' ),
					'search_items'      => __( 'Search Categories', 'rara-theme-toolkit-pro' ),
					'all_items'         => __( 'All Categories', 'rara-theme-toolkit-pro' ),
					'parent_item'       => __( 'Parent Categories', 'rara-theme-toolkit-pro' ),
					'parent_item_colon' => __( 'Parent Categories:', 'rara-theme-toolkit-pro' ),
					'edit_item'         => __( 'Edit Categories', 'rara-theme-toolkit-pro' ),
					'update_item'       => __( 'Update Categories', 'rara-theme-toolkit-pro' ),
					'add_new_item'      => __( 'Add New Categories', 'rara-theme-toolkit-pro' ),
					'new_item_name'     => __( 'New Categories Name', 'rara-theme-toolkit-pro' ),
					'menu_name'         => __( $value['singular_name'].' Categories', 'rara-theme-toolkit-pro' ),
				);

				$args = array(
					'hierarchical'      => true,
					'labels'            => $labels,
					'show_ui'           => true,
					'show_admin_column' => true,
					'show_in_nav_menus' => true,
					'rewrite'           => array( 'slug' => $value['taxonomy_slug'], 'hierarchical' => true ),
				);
				register_taxonomy( $value['taxonomy'], array( $key ), $args );
			}
		}
	}
}
