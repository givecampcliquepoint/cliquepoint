<?php

/**
 * Fired during plugin activation
 *
 * @link       https://raratheme.com
 * @since      1.0.0
 *
 * @package    Rara_Theme_Toolkit_Pro
 * @subpackage Rara_Theme_Toolkit_Pro/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Rara_Theme_Toolkit_Pro
 * @subpackage Rara_Theme_Toolkit_Pro/includes
 * @author     Rara Theme <test@testmail.com>
 */
class Rara_Theme_Toolkit_Pro_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
